#!/bin/bash

SESSIONNAME="nakama-sandbox"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOT_DIR=$( dirname $SCRIPT_DIR)
WATCHIFY_BIN=$ROOT_DIR/node_modules/watchify/bin/cmd.js

mkdir -p $ROOT_DIR/server/dst/client
mkdir -p $ROOT_DIR/server/log

tmux has-session -t $SESSIONNAME &> /dev/null

if [ $? != 0 ]; then
    tmux new-session -s $SESSIONNAME -n "devlog" -d
    tmux split-window -h -t $SESSIONNAME
    tmux last-pane -t $SESSIONNAME

    tmux new-window -n "vim"
    tmux send-keys -t $SESSIONNAME "vim server/server.js" C-m
    tmux send-keys -t $SESSIONNAME ":tabnew" C-m ":e sandbox/app.js" C-m
    tmux send-keys -t $SESSIONNAME ":tabnew" C-m ":e shared/socket_events.js" C-m
    tmux send-keys -t $SESSIONNAME ":tabnext" C-m


    tmux new-window -n "server"
    tmux send-keys -t $SESSIONNAME "pushd $ROOT_DIR/server" C-m
    tmux send-keys -t $SESSIONNAME "node server.js" C-m 
    tmux split-window -h -t $SESSIONNAME
    tmux send-keys -t $SESSIONNAME "tail -f server/log/nakama.log" C-m


    tmux new-window -n "sandbox-watchers"
    tmux send-keys -t $SESSIONNAME "pushd $ROOT_DIR" C-m
    tmux send-keys -t $SESSIONNAME "$WATCHIFY_BIN sandbox/app.js -o server/dst/client/bundle.js -t [ babelify --presets [ es2015 ] --plugins [ transform-runtime ] ] -v --debug" C-m 

    tmux next-window
    # Moving this to the end; the watching seems to mess up the switch to vim
    tmux send-keys -t $SESSIONNAME "vim devlog.md" C-m
fi

tmux attach -t $SESSIONNAME;
