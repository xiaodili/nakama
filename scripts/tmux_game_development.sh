#!/bin/bash

SESSIONNAME="simulation"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOT_DIR=$( dirname $SCRIPT_DIR)
WATCHIFY_BIN=$ROOT_DIR/node_modules/watchify/bin/cmd.js

open $ROOT_DIR/simulation/index.html;

mkdir -p $ROOT_DIR/simulation/dst;

tmux has-session -t $SESSIONNAME &> /dev/null

if [ $? != 0 ]; then
    tmux new-session -s $SESSIONNAME -n "devlog" -d
    tmux send-keys -t $SESSIONNAME "vim devlog.md" C-m
    tmux split-window -h -t $SESSIONNAME
    tmux last-pane -t $SESSIONNAME

    tmux new-window -n "vim"
    tmux send-keys -t $SESSIONNAME "vim" C-m

    tmux new-window -n "watchify"
    tmux send-keys -t $SESSIONNAME "pushd $ROOT_DIR/simulation" C-m
    tmux send-keys -t $SESSIONNAME "$WATCHIFY_BIN src/app.js -o dst/bundle.js -t [ babelify --presets [ es2015 ] --plugins [ transform-runtime ] ] -v --debug" C-m 

    tmux next-window
fi

tmux attach -t $SESSIONNAME;
