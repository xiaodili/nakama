#!/bin/bash

SESSIONNAME="nakama-platform"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOT_DIR=$( dirname $SCRIPT_DIR)
WATCHIFY_BIN=$ROOT_DIR/node_modules/watchify/bin/cmd.js

tmux has-session -t $SESSIONNAME &> /dev/null

if [ $? != 0 ]; then
    pushd $ROOT_DIR
    tmux new-session -s $SESSIONNAME -n "devlog" -d
    tmux send-keys -t $SESSIONNAME "vim devlog.md" C-m
    tmux split-window -h -t $SESSIONNAME
    tmux last-pane -t $SESSIONNAME

    tmux new-window -n "vim"
    tmux send-keys -t $SESSIONNAME "vim server/server.js" C-m
    tmux send-keys -t $SESSIONNAME ":tabnew" C-m ":e client/app.js" C-m
    tmux send-keys -t $SESSIONNAME ":tabnew" C-m ":e client/config/development.js" C-m
    tmux send-keys -t $SESSIONNAME ":tabnext" C-m

    tmux new-window -n "server"
    tmux send-keys -t $SESSIONNAME "pushd $ROOT_DIR/server" C-m
    tmux send-keys -t $SESSIONNAME "node server.js" C-m 
    tmux split-window -h -t $SESSIONNAME
    tmux send-keys -t $SESSIONNAME "tail -f server/log/nakama.log" C-m

    tmux new-window -n "client-watchers"
    tmux send-keys -t $SESSIONNAME "pushd $ROOT_DIR" C-m
    tmux send-keys -t $SESSIONNAME "$WATCHIFY_BIN client/app.js -o server/dst/client/bundle.js -t [ babelify --presets [ es2015 ] --plugins [ transform-runtime ] ] -v --debug" C-m 

    tmux new-window -n "admin-watchers"
    tmux send-keys -t $SESSIONNAME "pushd $ROOT_DIR" C-m
    tmux send-keys -t $SESSIONNAME "$WATCHIFY_BIN admin/app.js -o server/dst/admin/bundle.js -t [ babelify --presets [ es2015 ] --plugins [ transform-runtime ] ] -v --debug" C-m 

    tmux next-window
fi

tmux attach -t $SESSIONNAME
