const browserify = require('browserify')
const fs = require('fs')
const path = require('path')

const buildClient = (source_path, destination_path) => {
  console.log(`bundling client bundle at entry point ${source_path} to ${destination_path}...`)

  browserify(source_path, {
    paths: [path.join(__dirname, '../node_modules')] //required otherwise complains about not being able to find it
  })
  .transform("babelify", {
    presets: ["es2015"],
    plugins: ["transform-runtime"]
  })
  .transform("uglifyify", {
    global: true
  })
  .bundle()
  .pipe(fs.createWriteStream(destination_path))
}

if (require.main === module) {
  try {
    const source_path = process.argv[2]
    const destination_path = process.argv[3]
    buildClient(path.resolve(source_path), path.resolve(destination_path))
  } catch(e) {
    console.error(e)
    console.log('Usage: node build_client.js <entry_filepath> <destination_filepath>')
  }
}

module.exports = buildClient
