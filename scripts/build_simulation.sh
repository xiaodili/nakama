#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOT_DIR=$( dirname $SCRIPT_DIR)
BROWSERIFY_BIN=$ROOT_DIR/node_modules/browserify/bin/cmd.js

mkdir -p $ROOT_DIR/simulation/dst;

$BROWSERIFY_BIN $ROOT_DIR/simulation/src/app.js -o $ROOT_DIR/simulation/dst/bundle.js -t [ babelify --presets [ es2015 ] --plugins [ transform-runtime ] ] -t browserify-css
