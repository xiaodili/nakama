const browserify = require('browserify')
const watchify = require('watchify')
const fs = require('fs')
const path = require('path')

//https://github.com/browserify/watchify
const watchClient = (source_path, destination_path) => {
  console.log(`watching client bundle build at entry point ${source_path} to ${destination_path}...`)

  const b = browserify({
    cache: {},
    debug: true,
    entries: [source_path],
    packageCache: {},
    paths: [path.join(__dirname, '../node_modules')],
    plugin: [watchify]
  })
    .transform('babelify', {
      presets: ['es2015'],
      plugins: ['transform-runtime']
    })

  b.on('update', bundle)
  b.on('log', (msg) => {
    console.log(msg)
  })

  function error(err) {
    console.error(err.message)
  }

  function bundle() {
    b.bundle()
      .on('error', error) //https://github.com/browserify/watchify/issues/323
      .pipe(fs.createWriteStream(destination_path))
  }
  bundle()
}

if (require.main === module) {
  try {
    const source_path = process.argv[2]
    const destination_path = process.argv[3]
    watchClient(path.resolve(source_path), path.resolve(destination_path))
  } catch(e) {
    console.error(e)
    console.log('Usage: node watch_client.js <entry_filepath> <destination_filepath>')
  }
}

module.exports = watchClient
