const browserify = require('browserify')
const fs = require('fs')
const path = require('path')
const insertGlobals = require('insert-module-globals');

const buildServer = (source_path, destination_path, debug = false) => {
  console.log(`bundling node server at entry point ${source_path} to ${destination_path}...`)

  //https://github.com/browserify/browserify/issues/1277
  browserify(source_path, {
    debug,
    builtins: false,
    commondir: false,
    browserField: false,
    insertGlobalVars: {
      __filename: insertGlobals.vars.__filename,
      __dirname: insertGlobals.vars.__dirname,
      process: function() {
        return;
      }
    }
  })
  .bundle()
  .pipe(fs.createWriteStream(destination_path))
}

if (require.main === module) {
  try {
    const source_path = process.argv[2]
    const destination_path = process.argv[3]
    const debug = process.argv[4]
    buildServer(
      path.resolve(source_path),
      path.resolve(destination_path),
      Boolean(debug))
  } catch(e) {
    console.error(e)
    console.log('Usage: node build_server.js <entry_filepath> <destination_filepath>')
  }
}

module.exports = buildServer
