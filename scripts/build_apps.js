const browserify = require('browserify')
const fs = require('fs')
const path = require('path')

const Index = () => {
  const index = {}
  return {
    add: (id, app) => {
      index[id] = {
        id,
        title: app.title,
        description: app.description,
        url: app.url
      }
    },
    getAsModule: () => {
      return `module.exports = ${JSON.stringify(index)}`
    }
  }
}

const buildApps = (source_path, destination_path) => {
  const METADATA_FILENAME = 'metadata.js'
  const INDEX_FILENAME = 'index.js'

  if (!fs.existsSync(destination_path)){
    fs.mkdirSync(destination_path);
  }
  const server_dst_dir = path.join(destination_path, 'server')
  if (!fs.existsSync(server_dst_dir)){
    fs.mkdirSync(server_dst_dir);
  }

  const client_dst_dir = path.join(destination_path, 'client')
  if (!fs.existsSync(client_dst_dir)){
    fs.mkdirSync(client_dst_dir);
  }

  const index = Index()

  fs.readdirSync(source_path).forEach((filename) => {
    const current_file_path = path.join(source_path, filename);
    //get all the apps:
    if (fs.lstatSync(current_file_path).isDirectory()) {
      const metadata = require(path.join(current_file_path, METADATA_FILENAME))
      const to_publish = metadata.to_publish
      if (to_publish) {
        let rel_client_path = metadata.client_path
        let rel_server_path = metadata.server_path
        let title = metadata.title
        let url = metadata.url
        let description = metadata.description
        const id = index.add(filename, {
          title,
          description,
          url
        })
        console.log(`processing ${filename}...`)
        const client_path = path.join(current_file_path, rel_client_path)
        const client_dst = path.join(client_dst_dir, `${filename}.js`)
        browserify(client_path,
          {
            standalone: "Client",
            paths: [path.join(__dirname, '../node_modules')]
          })
          .transform("babelify", {
            presets: ["es2015"],
            plugins: [
              ["transform-runtime", {
                "regenerator": false
              }]]
          })
          .transform("uglifyify", {
            global: true
          })
          //.ignore('jss') //the client will already have jss; can't get this working!
          .bundle()
          .pipe(fs.createWriteStream(client_dst))

        const server_path = path.join(current_file_path, rel_server_path)
        const server_dst = path.join(server_dst_dir, `${filename}.js`)
        browserify(server_path,
          {
            "standalone": "Server",
            paths: [path.join(__dirname, '../node_modules')]
          })
          .transform("uglifyify", {
            global: true
          })
          .bundle()
          .pipe(fs.createWriteStream(server_dst))
      }
    }
  })

  const index_filepath = path.join(destination_path, INDEX_FILENAME)
  fs.writeFileSync(index_filepath, index.getAsModule(), 'utf-8')

}

if (require.main === module) {
  try {
    const source_path = process.argv[2]
    const destination_path = process.argv[3]
    buildApps(path.resolve(source_path), path.resolve(destination_path))
  } catch(e) {
    console.error(e)
    console.log('Usage: node build_apps.js <source_path> <destination_path>')
  }
}

module.exports = buildApps
