#!/bin/bash

#SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#ROOT_DIR=$( dirname $SCRIPT_DIR)
#BROWSERIFY_BIN=$ROOT_DIR/node_modules/browserify/bin/cmd.js

#mkdir -p $ROOT_DIR/dist;
#mkdir -p $ROOT_DIR/dist/games;
#mkdir -p $ROOT_DIR/dist/client;
#mkdir -p $ROOT_DIR/dist/admin;

## copying over index.htmls
#cp $ROOT_DIR/client/index.html $ROOT_DIR/dist/client;
#cp $ROOT_DIR/admin/index.html $ROOT_DIR/dist/admin;

##node $SCRIPT_DIR/bundle_games.js;

#$BROWSERIFY_BIN $ROOT_DIR/client/app.js -t [ babelify --presets [ es2015 ] --plugins [ transform-runtime ] ] -g uglifyify > $ROOT_DIR/dist/client/bundle.js

#$BROWSERIFY_BIN $ROOT_DIR/client/app.js -t [ babelify --presets [ es2015 ] --plugins [ transform-runtime ] ] -g uglifyify > $ROOT_DIR/dist/client/bundle.js

#$BROWSERIFY_BIN $ROOT_DIR/admin/app.js -t [ babelify --presets [ es2015 ] --plugins [ transform-runtime ] ] -g uglifyify > $ROOT_DIR/dist/admin/bundle.js

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOT_DIR=$( dirname $SCRIPT_DIR)
BROWSERIFY_BIN=$ROOT_DIR/node_modules/browserify/bin/cmd.js

mkdir -p $ROOT_DIR/server/dst;
mkdir -p $ROOT_DIR/server/dst/games;
mkdir -p $ROOT_DIR/server/dst/client;
mkdir -p $ROOT_DIR/server/dst/admin;

# copying over index.htmls
cp $ROOT_DIR/client/index.html $ROOT_DIR/server/dst/client;
cp $ROOT_DIR/admin/index.html $ROOT_DIR/server/dst/admin;

node $SCRIPT_DIR/bundle_games.js;

# Needs games_catalogue.js
$BROWSERIFY_BIN $ROOT_DIR/client/app.js -t [ babelify --presets [ es2015 ] --plugins [ transform-runtime ] ] -g uglifyify > $ROOT_DIR/server/dst/client/bundle.js

$BROWSERIFY_BIN $ROOT_DIR/admin/app.js -t [ babelify --presets [ es2015 ] --plugins [ transform-runtime ] ] -g uglifyify > $ROOT_DIR/server/dst/admin/bundle.js

