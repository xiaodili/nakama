const h = require('../../shared/client_helpers.js')
const handleValidate = require('../../shared/helpers.js').handleValidate
const passphraseValidate = (passphrase) => {
  return passphrase.trim().length > 0
}
const rc = require('../../shared/response_codes.js')
const ep = require('../../shared/endpoints.js')

const ONBOARD_MSG = 'No admin accounts detected. Set one up now by choosing a handle and passphrase'
const SETUP_MSG = 'Set up'
const INVALID_HANDLE_MSG = "Handle must be under 18 characters and only contain A-Z, a-z, 0-9, - and _ characters"
const INVALID_PASSPHRASE_MSG = "Enter a passphrase"
const HANDLE_TAKEN_MSG = "Handle already taken. Try another one"

const SetupApp = (mount, router, self) => {
  var handle_input_node,
    passphrase_input_node,
    action_btn_node,
    feedback_node
  const view = h.View(
    mount,
    'div',
    [],
    () => { return `
      <h2>Setup</h2>
      <p>${ONBOARD_MSG}</p>
      <div nakama='feedback'></div>
      Handle: <input type='text' nakama='handle-input'/><br/>
      Passphrase: <input type='password' nakama='passphrase-input'/>
      <button nakama='action'></button>
    `}
  )

  const handleActionBtnPressed = () => {
    const handle = handle_input_node.value
    const passphrase = passphrase_input_node.value
    const handle_valid = handleValidate(handle) 
    const passphrase_valid = passphraseValidate(passphrase)
    if (handle_valid && passphrase_valid) {
      h.post(ep.ADMIN_REGISTER, {
        handle,
        passphrase
      }, (payload, status_code) => {
        if (status_code === 200) {
          const {
            session_id,
            userid,
            config_for_client
          } = payload
          self.writer.login(userid, handle, session_id, config_for_client)
          router.goToRoute('/')
        } else if (status_code === 400) {
          if (payload === rc.HANDLE_TAKEN) {
            feedback_node.innerHTML = HANDLE_TAKEN_MSG
          }
        }
      })
    } else {
      if (!handle_valid) {
        feedback_node.innerHTML = INVALID_HANDLE_MSG
      } else if (!passphrase_valid) {
        feedback_node.innerHTML = INVALID_PASSPHRASE_MSG
      }
    }
  }
  view.renderApp((dom) => {
    h.bindAttributes('nakama', {
      'handle-input': (elem) => {
        handle_input_node = elem
      },
      'passphrase-input': (elem) => {
        passphrase_input_node = elem
        h.registerEnterPress(passphrase_input_node, handleActionBtnPressed)
      },
      'action': (elem) => {
        elem.innerHTML = SETUP_MSG
        action_btn_node = elem
        h.registerTapOrClick(action_btn_node, handleActionBtnPressed)
      },
      'feedback': (elem) => {
        feedback_node = elem
      }
    }, dom)
  })
  return {
    //testing
    populateFields: () => {
      const username = 'eddie123'
      const password ='qwerty'
      handle_input_node.value = username
      passphrase_input_node.value = password
    },
    submit: () => {
      handleActionBtnPressed()
    },
    //for realz
    destroy: () => {
      view.removeApp()
    }
  }
}

module.exports = SetupApp
