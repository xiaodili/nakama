const h = require('../../../shared/client_helpers.js')
const ep = require('../../../shared/endpoints.js')

const LogoutApp = (mount, router, self) => {
  var logout_btn_node
  const view = h.View(
    mount,
    'div',
    [],
    () => { return `
      <div><button nakama='logout-btn'>Logout</button></div>
    `}
  )
  const handleLogoutPressed = () => {
    const {
      userid,
      session_id
    } = self.reader.getHandShakeData()
    h.post(ep.LOGOUT, {
      userid,
      session_id
    }, (payload, status_code) => {
      if (status_code === 200) {
        self.forget()
        router.goToRoute('/login')
      }
    })
  }
  view.renderApp((dom) => {
    h.bindAttributes('nakama', {
      'logout-btn': (elem) => {
        logout_btn_node = elem
        h.registerTapOrClick(logout_btn_node, handleLogoutPressed)
      }
    }, dom)
  })
  return {
    destroy: () => {
      view.removeApp()
    }
  }
}

module.exports = LogoutApp
