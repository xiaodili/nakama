const h = require('../../../shared/client_helpers.js')
const intra = require('../../symbols/intra.js')

const PlayerItem = (mount, _id, _handle) => {
  var id = _id
  var handle = _handle

  //DOM
  var handle_node

  const view = h.View(
    mount,
    'li',
    [],
    () => { return `
      <span nakama='handle'>${handle}</span>
    `}
  )

  return {
    remove: () => {
      view.removeApp()
    },
    render: () => {
      view.renderApp((dom) => {
        h.bindAttributes('nakama', {
          'handle': (elem) => {
            handle_node = elem
          }
        }, dom)
      })
    }
  }
}

const PlayerList = (mount, dashboard_pubsub) => {
  const renderer = h.ItemRenderer()
  var player_items = {}

  const handleSyncedPlayers = (players) => {
    renderer.renderItems({})
    player_items = {}
    for (let id of Object.keys(players)) {
      const player = players[id]
      const handle = player.handle
      player_items[id] = PlayerItem(mount, id, handle)
    }
    renderer.renderItems(player_items)
  }

  const subs = [
    dashboard_pubsub.sub(intra.SYNCED_PLAYERS, handleSyncedPlayers)
  ]

  return {
    destroy: () => {
      for (let unsub of subs) {
        unsub()
      }
      renderer.renderItems({})
    }
  }
}

const PlayersApp = (mount, dashboard_pubsub) => {

  var player_list

  const view = h.View(
    mount,
    'div',
    [],
    () => { return `
      <h2>Players</h2>
      <ul nakama='player-list'></ul>
    `}
  )

  view.renderApp((dom) => {
    h.bindAttributes('nakama', {
      'player-list': (elem) => {
        player_list = PlayerList(elem, dashboard_pubsub)
      }
    }, dom)
  })
  return {
    destroy: () => {
      player_list.destroy()
      view.removeApp()
    }
  }
}

module.exports = PlayersApp
