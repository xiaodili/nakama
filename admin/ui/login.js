const h = require('../../shared/client_helpers.js')
const ep = require('../../shared/endpoints.js')

const BAD_INPUT_MSG = 'Details not recognised. Try again?'

const LoginApp = (mount, router, self) => {
  var handle_input_node,
    passphrase_input_node,
    submit_btn_node,
    feedback_node
  const view = h.View(
    mount,
    'div',
    [],
    () => { return `
      <h2>Login</h2>
      <div nakama='feedback'></div>
      <div>
        Handle: <input type='text' nakama='handle-input'/><br/>
        Passphrase: <input type='password' nakama='passphrase-input'/>
        <button nakama='submit'>Submit</button>
      </div>
    `}
  )

  const handleSubmitBtnPressed = () => {
    const handle = handle_input_node.value
    const passphrase = passphrase_input_node.value
    handle_input_node.value = ""
    passphrase_input_node = ""
    h.post(ep.ADMIN_LOGIN, {
      handle,
      passphrase
    }, (payload, status_code) => {
      if (status_code === 401) {
        feedback_node.innerHTML = BAD_INPUT_MSG
      } else if (status_code === 200) {
        const {
          session_id,
          userid,
          config_for_client
        } = payload
        self.writer.login(userid, handle, session_id, config_for_client)
        router.goToRoute('/')
      }
    })
  }
  view.renderApp((dom) => {
    h.bindAttributes('nakama', {
      'feedback': (elem) => {
        feedback_node = elem
      },
      'handle-input': (elem) => {
        handle_input_node = elem
      },
      'passphrase-input': (elem) => {
        passphrase_input_node = elem
        h.registerEnterPress(passphrase_input_node, handleSubmitBtnPressed)
      },
      'submit': (elem) => {
        submit_btn_node = elem
        h.registerTapOrClick(submit_btn_node, handleSubmitBtnPressed)
      }
    }, dom)
  })
  return {
    destroy: () => {
      view.removeApp()
    },
    populateFields: () => {
      const username = 'eddie'
      const password ='qwerty'
      handle_input_node.value = username
      passphrase_input_node.value = password
      handleSubmitBtnPressed()
    }
  }
}

module.exports = LoginApp
