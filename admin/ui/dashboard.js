const config = require('../config/index.js')
const URL = config.url
const h = require('../../shared/client_helpers.js')
const PubSub = require('../../shared/helpers.js').PubSub
const ep = require('../../shared/endpoints.js')
const PlayersApp = require('./dashboard/players.js')
const LogoutApp = require('./dashboard/logout.js')
const io = require('socket.io-client')
const inter = require('../../shared/socket_events.js')
const intra = require('../symbols/intra.js')

const DashboardApp = (mount, router, self) => {
  var players_app,
    logout_app

  const self_reader = self.reader
  const self_writer = self.writer

  const dashboard_pubsub = PubSub()

  const view = h.View(
    mount,
    'div',
    [],
    () => { return `
      <h2>Dashboard</h2>
      <div nakama='logout-container'></div>
      <div nakama='players-container'></div>
    `}
  )

  //init
  const socket = io.connect(URL)
  const handleStartedConnection = () => {
    socket.emit(inter.ESTABLISH_SESSION, self_reader.getHandShakeData())
  }
  const handleSessionEstablished = (msg) => {
    const {
      config_for_client,
      players,
      rooms,
      user
    } = msg
    dashboard_pubsub.pub(intra.SYNCED_PLAYERS, players)
  }

  const socket_subs = [
    h.socketSub(socket, inter.I_CONNECTED, handleStartedConnection),
    h.socketSub(socket, inter.ESTABLISHED_SESSION, handleSessionEstablished)
  ]

  view.renderApp((dom) => {
    h.bindAttributes('nakama', {
      'logout-container': (elem) => {
        logout_app = LogoutApp(elem, router, self)
      },
      'players-container': (elem) => {
        players_app = PlayersApp(elem, dashboard_pubsub)
      }
    }, dom)
  })
  return {
    destroy: () => {
      for (const unsub of socket_subs) {
        unsub()
      }
      socket.disconnect()
      players_app.destroy()
      logout_app.destroy()
      view.removeApp()
    }
  }
}

module.exports = DashboardApp
