const h = require('../shared/client_helpers.js')
const SetupApp = require('./ui/setup.js')
const LoginApp = require('./ui/login.js')
const DashboardApp = require('./ui/dashboard.js')
const self = require('./lib/self.js')()
const ep = require('../shared/endpoints.js')

const checkAdminSetup = (cb) => {
  h.get(ep.GET_ADMIN_STATUS, cb)
}

window.init = () => {
  const root_node = document.getElementById('app')
  const router = h.Router()

  const pages = {
    login: {
      app: null,
      init: () => {
        return LoginApp(root_node, router, self)
      }
    },
    setup: {
      app: null,
      init: () => {
        return SetupApp(root_node, router, self)
      }
    },
    dashboard: {
      app: null,
      init: () => {
        return DashboardApp(root_node, router, self)
      }
    }
  }

  const transitionTo = (page_name) => {
    const focused_page = pages[page_name]
    if (focused_page.app) {
      //nop
    } else {
      //remove the other apps
      for (let _page_name of Object.keys(pages)) {
        if (_page_name !== page_name) {
          let page_to_destroy = pages[_page_name]
          if (page_to_destroy.app) {
            page_to_destroy.app.destroy()
            page_to_destroy.app = null
          }
        } else {
          focused_page.app = focused_page.init()
        }
      }
    }
  }

  const goToLogin = () => {
    transitionTo('login')
  }
  const goToSetup = () => {
    transitionTo('setup')
  }
  const goToDashboard = () => {
    transitionTo('dashboard')
  }

  router.registerRoute('/', 'Nakama Zone Admin | Dashboard', goToDashboard)
  router.registerRoute('/login', 'Nakama Zone Admin | Login', goToLogin)
  router.registerRoute('/setup', 'Nakama Zone Admin | Setup', goToSetup)

  checkAdminSetup((admin_setup) => {
    if (admin_setup) {
      router.goToRoute('/login')
    } else {
      router.goToRoute('/setup')
    }
  })
}
