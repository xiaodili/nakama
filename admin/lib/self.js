const ct = require('../../shared/client_types.js')
const Self = () => {

  //init
  const cookie_preference = false

  var handle,
    session_id,
    userid,
    server_config

  return {
    reader: {
      getHandShakeData: () => {
        return {
          client_type: ct.ADMIN_APP,
          userid,
          handle,
          session_id,
          server_id: server_config.server_id
        }
      }
    },
    writer: {
      login: (_userid, _handle, _session_id, _server_config) => {
        userid = _userid
        handle = _handle
        session_id = _session_id
        server_config = _server_config
      }
    },
    forget: () => {
    }
  }
}

module.exports = Self
