const io = require('socket.io-client')
const PubSub = require('../shared/helpers.js').PubSub
const GameEventHandler = require('./lib/game/handler.js')
const inter = require('../shared/socket_events.js')
const intra = require('./lib/symbols/intra.js')
const handleSessionEstablished = require('./lib/establish_session.js').handleSessionEstablished
const handleIConnected = require('./lib/establish_session.js').handleIConnected

const config_default = require('./config/default.js')
const configMerge = (built_in, custom, ui) => {
  //Do a dumb assign for the moment
  return Object.assign({},
    built_in,
    custom,
    ui)
}
const debugConfig = (config) => {
  if (!config.pages.home.welcome) {
    throw Error('config.pages.home.welcome not configured')
  }
  if (!config.pages.about) {
    throw Error('config.pages.about not configured')
  }
  if (!config.theme) {
    console.warn("You haven't defined a theme; default colour scheme and font will be used")
  }
  if (!config.resources) {
    console.info("You haven't defined any resources; make sure your fonts resources are included")
  }
}

//Top Level
//DOM app
const MainApp = require('./ui/main.js')
//Non UI
const Router = require('../shared/client_helpers.js').Router

//don't need this anymore, since body has hidden: overflow
//require('./lib/hacks.js').StopChromePullUpRefresh()

module.exports = (custom_config, ui_config) => {
  const config = configMerge(config_default, custom_config, ui_config)
  debugConfig(config)
  const players_registry = require('./lib/registries/players.js')()
  const rooms_registry = require('./lib/registries/rooms.js')()
  const games_registry = require('./lib/registries/games.js')()
  //Convenience
  const rooms_reader = rooms_registry.reader
  const rooms_writer = rooms_registry.writer
  const players_reader = players_registry.reader
  const players_writer = players_registry.writer
  const games_reader = games_registry.reader
  const games_writer = games_registry.writer
  const on_mobile = players_reader.getSelfOnMobile()

  const clearRegistries = () => {
    players_writer.clearRegistry()
    rooms_writer.clearRegistry()
    games_writer.clearRegistry()
  }

  const URL = config.url

  //Aggregates on the client
  const dom_pubsub = PubSub()
  const router = Router()

  //connect to the socket:
  const dsocket = io(URL, {
    //Currently, GlobalFocus is doing the connect() now
    autoConnect: false
  })
  //default options:
  //'reconnection': true, 
  //'reconnectionDelay': 1000,
  //'reconnectionDelayMax' : 5000,
  //'reconnectionAttempts': Infinity
  
  //Game Event Handler:
  const game_event_handler = GameEventHandler(games_registry)

  window.init = () => {
    const main_app = MainApp(document.body, dsocket, dom_pubsub, players_registry, rooms_registry, games_registry, router, config, ui_config)

    //Debugging/Developing:
    window.players = players_registry
    window.rooms = rooms_registry
    window.games = games_registry
    window.pubsub = dom_pubsub
    //Useful in the window:
    //dsocket.connect()
    //dsocket.disconnect()
    window.dsocket = dsocket

    const finishInit = () => {
      main_app.getLoadingApp().destroy()
    }

    //Session Establishment
    dsocket.on(inter.ESTABLISHED_SESSION, (msg) => {
      handleSessionEstablished(dsocket, dom_pubsub, msg, main_app.getRoomsApp(), players_registry, rooms_registry, games_registry, finishInit)
    })

    dsocket.on(inter.PLAYER_ACTIVE, (userid) => {
      players_writer.setPlayerActive(userid)
      dom_pubsub.pub(intra.PLAYER_ACTIVE, userid)
    })
    dsocket.on(inter.PLAYER_INACTIVE, (userid) => {
      players_writer.setPlayerInactive(userid)
      dom_pubsub.pub(intra.PLAYER_INACTIVE, userid)
    })

    //International events
    dsocket.on(inter.PLAYER_WAS_KICKED, (msg) => {
      const playerid = msg.playerid
      const kickerid = msg.kickerid
      const roomid = msg.roomid
      rooms_writer.playerLeftRoom(playerid, roomid)
      if (playerid === players_reader.getSelfId() && players_reader.isSelfInRoom(roomid)) {
        players_writer.removeSelfRoom(roomid)
      }
      dom_pubsub.pub(intra.PLAYER_WAS_KICKED, {
        userid: playerid,
        kickerid,
        roomid
      })
    })

    dsocket.on(inter.PLAYER_ASSIGNED_AS_ADMIN, (msg) => {
      const userid = msg.userid
      const roomid = msg.roomid
      if (rooms_reader.getRoom(roomid)) { //required since admin_assigner fires before establish_session is finished
        rooms_writer.assignAdmin(roomid, userid)
      }
      dom_pubsub.pub(intra.PLAYER_ASSIGNED_AS_ADMIN, msg)
    })

    //Room Settings
    dsocket.on(inter.PLAYER_CHANGED_DEFAULT_TAB, (msg) => {
      rooms_writer.changeDefaultTab(msg.roomid, msg.tab_type)
      dom_pubsub.pub(intra.PLAYER_CHANGED_DEFAULT_TAB, msg)
    })
    dsocket.on(inter.PLAYER_CHANGED_SHORTLINK, (msg) => {
      rooms_writer.changeShortlink(msg.roomid, msg.shortlink)
      dom_pubsub.pub(intra.PLAYER_CHANGED_SHORTLINK, msg)
    })
    dsocket.on(inter.PLAYER_REMOVED_SHORTLINK, (roomid) => {
      rooms_writer.changeShortlink(roomid)
      dom_pubsub.pub(intra.PLAYER_REMOVED_SHORTLINK, roomid)
    })
    dsocket.on(inter.PLAYER_REMOVED_PASSPHRASE, (roomid) => {
      rooms_writer.removePassphraseRequirement(roomid)
      dom_pubsub.pub(intra.PLAYER_REMOVED_PASSPHRASE, roomid)
    })
    dsocket.on(inter.PLAYER_ADDED_PASSPHRASE, (roomid) => {
      rooms_writer.addPassphraseRequirement(roomid)
      dom_pubsub.pub(intra.PLAYER_ADDED_PASSPHRASE, roomid)
    })
    dsocket.on(inter.PLAYER_PROTECTED_ROOM, (msg) => {
      const roomid = msg.roomid
      rooms_writer.protectRoom(roomid)
      dom_pubsub.pub(intra.PLAYER_PROTECTED_ROOM, msg)
    })
    dsocket.on(inter.PLAYER_UNPROTECTED_ROOM, (msg) => {
      const roomid = msg.roomid
      rooms_writer.unprotectRoom(roomid)
      dom_pubsub.pub(intra.PLAYER_UNPROTECTED_ROOM, msg)
    })
    dsocket.on(inter.PLAYER_CHANGED_ROOM_NAME, (msg) => {
      const roomid = msg.roomid
      const name = msg.name
      rooms_writer.changeName(roomid, name)
      dom_pubsub.pub(intra.PLAYER_CHANGED_ROOM_NAME, msg)
    })
    dsocket.on(inter.PLAYER_CHANGED_ROOM_DESCRIPTION, (msg) => {
      const roomid = msg.roomid
      const description = msg.description
      rooms_writer.changeDescription(roomid, description)
      dom_pubsub.pub(intra.PLAYER_CHANGED_ROOM_DESCRIPTION, msg)
    })
    dsocket.on(inter.PLAYER_DESTROYED_ROOM, (msg) => {
      const roomid = msg.roomid
      if (players_reader.isSelfInRoom(roomid)) {
        players_writer.removeSelfRoom(roomid)
      }
      rooms_writer.removeRoom(roomid)
      dom_pubsub.pub(intra.PLAYER_DESTROYED_ROOM, msg)
    })
    dsocket.on(inter.PLAYER_ENABLED_GAMES, (msg) => {
      let roomid = msg.roomid
      rooms_writer.enableGames(roomid)
      dom_pubsub.pub(intra.PLAYER_ENABLED_GAMES, msg)
    })
    dsocket.on(inter.PLAYER_DISABLED_GAMES, (msg) => {
      let roomid = msg.roomid
      rooms_writer.disableGames(roomid)
      rooms_writer.removeGameOngoing(roomid)
      dom_pubsub.pub(intra.PLAYER_DISABLED_GAMES, msg)
    })
    dsocket.on(inter.PLAYER_ENABLED_NEVENT_LOG, (msg) => {
      let roomid = msg.roomid
      rooms_writer.enableNeventLog(roomid)
      dom_pubsub.pub(intra.PLAYER_ENABLED_NEVENT_LOG, msg)
    })
    dsocket.on(inter.PLAYER_DISABLED_NEVENT_LOG, (msg) => {
      let roomid = msg.roomid
      rooms_writer.disableNeventLog(roomid)
      dom_pubsub.pub(intra.PLAYER_DISABLED_NEVENT_LOG, msg)
    })
    dsocket.on(inter.PLAYER_ENABLED_PLAYER_LIST, (msg) => {
      let roomid = msg.roomid
      rooms_writer.enablePlayerList(roomid)
      dom_pubsub.pub(intra.PLAYER_ENABLED_PLAYER_LIST, msg)
    })
    dsocket.on(inter.PLAYER_DISABLED_PLAYER_LIST, (msg) => {
      let roomid = msg.roomid
      rooms_writer.disablePlayerList(roomid)
      dom_pubsub.pub(intra.PLAYER_DISABLED_PLAYER_LIST, msg)
    })

    //handshake
    dsocket.on(inter.ROOM_CREATION_REQUEST_FUFILLED, (roomid) => {
      const selfid = players_reader.getSelfId()

      //queued callback; join room after creating it
      players_writer.addSelfRoom(roomid)
      rooms_writer.playerJoinedRoom(selfid, roomid)
      dom_pubsub.pub(intra.I_JOINED_ROOM, roomid)
      dsocket.emit(inter.I_JOINED_ROOM, {
        roomid,
        userid: selfid
      })
    })

    dsocket.on(inter.ROOM_CREATED, (room) => {
      rooms_writer.createRoom(room)
      dom_pubsub.pub(intra.ROOM_CREATED, room)
    })
    dsocket.on(inter.GAME_ENDED, (msg) => {
      const roomid = msg.roomid
      rooms_writer.removeGameOngoing(roomid)
      dom_pubsub.pub(intra.GAME_ENDED, msg)
    })

    dsocket.on(inter.GAME_ERRORED, (msg) => {
      const gseshid = msg.gseshid
      const roomid = msg.roomid
      rooms_writer.removeGameOngoing(roomid, gseshid)
      dom_pubsub.pub(intra.GAME_ERRORED, msg)
    })

    dsocket.on(inter.GAME_STARTED, (msg) => {
      const gseshid = msg.gseshid
      const roomid = msg.roomid
      rooms_writer.setGameOngoing(roomid, gseshid)
      dom_pubsub.pub(intra.GAME_STARTED, msg)
    })

    dsocket.on(inter.GAME_STARTED_FROM_URL, (roomid) => {
      rooms_writer.playerJoinedRoom(players_reader.getSelfId(), roomid)
      players_writer.addSelfRoom(roomid)
      dom_pubsub.pub(intra.I_JOINED_ROOM, roomid)
      dsocket.emit(inter.I_JOINED_ROOM, {
        roomid,
        userid: players_reader.getSelfId()
      })
    })

    dsocket.on(inter.GAME_EVENT, (msg) => {
      game_event_handler.handleIncomingMessage(msg)
    })

    dsocket.on(inter.NEW_PLAYER_REGISTERED, (msg) => {
      const userid = msg.userid
      const handle = msg.handle
      players_writer.addPlayer(userid, handle)
    })

    dsocket.on(inter.PLAYER_JOINED_ROOM, (msg) => {
      const userid = msg.userid
      const roomid = msg.roomid
      rooms_writer.playerJoinedRoom(userid, roomid)
      if (userid === players_reader.getSelfId()) {
        players_writer.addSelfRoom(roomid)
      }
      dom_pubsub.pub(intra.PLAYER_JOINED_ROOM, msg)
    })

    dsocket.on(inter.PLAYER_LEFT_ROOM, (msg) => {
      const userid = msg.userid
      const roomid = msg.roomid
      rooms_writer.playerLeftRoom(userid, roomid)
      if (userid === players_reader.getSelfId()) {
        players_writer.removeSelfRoom(roomid)
      }
      dom_pubsub.pub(intra.PLAYER_LEFT_ROOM, msg)
    })

    dsocket.on(inter.PLAYER_CHANGED_HANDLE, (msg) => {
      const userid = msg.userid
      const handle = msg.handle
      const old_handle = msg.old_handle
      players_writer.updateHandle(userid, old_handle, handle)
      if (userid === players_reader.getSelfId()) {
        players_writer.setSelfHandle(handle)
      }
      dom_pubsub.pub(intra.PLAYER_CHANGED_HANDLE, msg)
    })

    dsocket.on(inter.PLAYER_LEFT, (msg) => {
      const userid = msg.userid
      const rooms = msg.rooms
      for (let roomid of rooms) {
        rooms_writer.playerLeftRoom(userid, roomid)
        dom_pubsub.pub(intra.PLAYER_LEFT_ROOM, {
          userid,
          roomid
        })
      }
      //Finally, remove from the registry
      players_writer.removePlayer(userid)
    })

    dsocket.on(inter.SESSION_INVALID, () => {
      clearRegistries()
      router.goToRoute('/login')
    })

    dsocket.on(inter.LOGIN_REQUIRED, () => {
      clearRegistries()
      router.goToRoute('/login')
    })

    dsocket.on(inter.I_CONNECTED, () => {
      handleIConnected(dsocket, players_reader)
      game_event_handler.handleClientConnected()
    })
    dsocket.on(inter.I_DISCONNECTED, () => {
      game_event_handler.handleClientDisconnected()
    })
  }
}
