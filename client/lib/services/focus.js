const inter = require('../../../shared/socket_events.js')
const intra = require('../symbols/intra.js')
const focus = require('../symbols/focus.js')
const ttypes = require('../../../shared/symbols/tab_types.js')

//Intraroom tab management
const RoomFocuser = (dsocket, default_tab, dom_pubsub, roomid, settings_app, roomtabs_app, getPlayersApp, getNeventLogApp, getGameApp, inroom_apps_node) => {
  let current_tab_focus
  const focused_elements = {
    settings_shown: false,
    current_tab_focus: null //nevents, games, players
  }

  const handleRoomSettingsPressed = (_roomid) => {
    if (roomid === _roomid) {
      if (focused_elements.settings_shown) {
        settings_app.hide()
        inroom_apps_node.style.display = 'block'
        roomtabs_app.show()
        focused_elements.settings_shown = false
      } else {
        settings_app.show()
        inroom_apps_node.style.display = 'none'
        roomtabs_app.hide()
        focused_elements.settings_shown = true
      }
    }
  }

  const handleNeventLogTabPressed = (_roomid) => {
    if (roomid === _roomid) {
      if (getGameApp()) {
        getGameApp().hide()
      }
      if (getPlayersApp()) {
        getPlayersApp().hide()
      }
      getNeventLogApp().focus()
      focused_elements.current_tab_focus = focus.room.NEVENTS
    }
  }
  const handlePlayersTabPressed = (_roomid) => {
    if (roomid === _roomid) {
      if (getNeventLogApp()) {
        getNeventLogApp().hide()
      }
      if (getGameApp()) {
        getGameApp().hide()
      }
      getPlayersApp().focus()
      focused_elements.current_tab_focus = focus.room.PLAYERS
    }
  }
  const handleGamesTabPressed = (_roomid) => {
    if (roomid === _roomid) {
      if (getPlayersApp()) {
        getPlayersApp().hide()
      }
      if (getNeventLogApp()) {
        getNeventLogApp().hide()
      }
      getGameApp().focus()
      focused_elements.current_tab_focus = focus.room.GAMES
    }
  }

  const handleAdminSettingsEvent = (msg) => {
    if (msg.roomid === roomid) {
      refocus()
    }
  }

  //The flow for this is a bit weird; we're publishing these artifical press events and then relistening in on themselves
  const refocus = () => {
    if (getGameApp() && default_tab === ttypes.APP) {
      dom_pubsub.pub(intra.I_PRESSED_ON_GAMES_TAB, roomid)
    } else if (getNeventLogApp() && default_tab === ttypes.MESSAGES) {
      dom_pubsub.pub(intra.I_PRESSED_ON_NEVENT_LOG_TAB, roomid)
    } else if (getPlayersApp() && default_tab === ttypes.OCCUPANTS) {
      dom_pubsub.pub(intra.I_PRESSED_ON_PLAYERS_TAB, roomid)
    } else if (getGameApp()) {
      dom_pubsub.pub(intra.I_PRESSED_ON_GAMES_TAB, roomid)
    } else if (getNeventLogApp()) {
      dom_pubsub.pub(intra.I_PRESSED_ON_NEVENT_LOG_TAB, roomid)
    } else if (getPlayersApp()) {
      dom_pubsub.pub(intra.I_PRESSED_ON_PLAYERS_TAB, roomid)
    }
  }

  const dom_subs = [
    dom_pubsub.sub(intra.ROOM_SETTINGS_PRESSED, handleRoomSettingsPressed),
    dom_pubsub.sub(intra.I_PRESSED_ON_PLAYERS_TAB, handlePlayersTabPressed),
    dom_pubsub.sub(intra.I_PRESSED_ON_GAMES_TAB, handleGamesTabPressed),
    dom_pubsub.sub(intra.I_PRESSED_ON_NEVENT_LOG_TAB, handleNeventLogTabPressed),
    dom_pubsub.sub(intra.PLAYER_ENABLED_PLAYER_LIST, handleAdminSettingsEvent),
    dom_pubsub.sub(intra.PLAYER_DISABLED_PLAYER_LIST, handleAdminSettingsEvent),
    dom_pubsub.sub(intra.PLAYER_ENABLED_NEVENT_LOG, handleAdminSettingsEvent),
    dom_pubsub.sub(intra.PLAYER_DISABLED_NEVENT_LOG, handleAdminSettingsEvent),
    dom_pubsub.sub(intra.PLAYER_ENABLED_GAMES, handleAdminSettingsEvent),
    dom_pubsub.sub(intra.PLAYER_DISABLED_GAMES, handleAdminSettingsEvent)
  ]
  //init
  refocus()
  return {
    focused_elements,
    destroy: () => {
      for (let unsub of dom_subs) {
        unsub()
      }
    }
  }
}

const LocalFocuser = (router, dsocket, dom_pubsub, players_registry, rooms_registry, games_reader, rooms_app, welcome_app, instance_name) => {
  //convenience
  const players_writer = players_registry.writer
  const players_reader = players_registry.reader
  const rooms_writer = rooms_registry.writer
  const rooms_reader = rooms_registry.reader
  const on_mobile = players_reader.getSelfOnMobile()
  const getSelfId = players_reader.getSelfId

  //init
  const focused_elements = {
    current_room_focus: null, //roomid || null
    welcome_focused: true // false || true
  }

  const focusOnRoom = (roomid) => {
    if (focused_elements.current_room_focus !== roomid) {
      const room_app = rooms_app.getRoomApp(focused_elements.current_room_focus)
      if (room_app) { //required in case the room's been destroyed
        room_app.hide()
      }
      focused_elements.current_room_focus = roomid
      rooms_writer.setCurrentRoomFocus(roomid)
      rooms_app.getRoomApp(roomid).show()
      dom_pubsub.pub(intra.I_FOCUSED_ON_ROOM, roomid)
    }
    if (focused_elements.welcome_focused) {
      focused_elements.welcome_focused = false
      welcome_app.hide()
    }
    //finally, change URL
    const shortlink = rooms_reader.getShortlink(roomid)
    if (shortlink) {
      router.goToRoute(`/r/${shortlink}`)
    } else {
      router.goToRoute(`/rooms/${roomid}`)
    }
  }

  const focusOnWelcome = () => {
    for (const room_app of rooms_app.getRoomApps()) {
      room_app.hide()
    }
    focused_elements.current_room_focus = null
    rooms_writer.setCurrentRoomFocus(null)
    focused_elements.welcome_focused = true
    welcome_app.show()
    dom_pubsub.pub(intra.I_FOCUSED_ON_WELCOME)
    //Pretty sure we don't need this; that's a top level concern
    //Nope we do; for when leaving rooms. Is there a better way though, I reckon...
    router.goToRoute('/')
  }

  const handleIJoinedRoom = (roomid) => {
    focusOnRoom(roomid)
  }
  const handlePlayerJoinedRoom = (msg) => {
    const roomid = msg.roomid
    const userid = msg.userid
    if (userid === getSelfId()) {
      if (focused_elements.current_room_focus !== roomid) {
        focusOnRoom(roomid)
      }
    }
  }

  const handleILeftRoom = (roomid) => {
    if (roomid === focused_elements.current_room_focus) {
      focusOnWelcome()
    }
  }
  const handlePlayerLeftRoom = (msg) => {
    const roomid = msg.roomid
    const userid = msg.userid
    if (userid === getSelfId()) {
      if (focused_elements.current_room_focus === roomid) {
        focusOnWelcome()
      }
    }
  }

  const handleLogoPressed = () => {
    focusOnWelcome()
  }
  const handleHomePressed = () => {
    focusOnWelcome()
  }
  const handleSyncedSelfData = (msg) => {
    const rooms_occupied = msg.rooms_occupied
    if (rooms_occupied.indexOf(focused_elements.current_room_focus) === -1) {
      focusOnWelcome()
    }
  }

  const handleRoombarMenuPressed = () => {
    focusOnWelcome()
  }

  const handlePlayerWasKicked = (msg) => {
    const userid = msg.userid
    const roomid = msg.roomid
    if (userid === getSelfId() && focused_elements.current_room_focus === roomid) {
      focusOnWelcome()
    }
  }
  const handlePlayerDestroyedRoom = (msg) => {
    const roomid = msg.roomid 
    if (focused_elements.current_room_focus === roomid) {
      focusOnWelcome()
    }
  }
  const handleISwitchedToRoom = (roomid) => {
    focusOnRoom(roomid)
  }

  dom_pubsub.sub(intra.I_JOINED_ROOM, handleIJoinedRoom)
  dom_pubsub.sub(intra.PLAYER_JOINED_ROOM, handlePlayerJoinedRoom)

  dom_pubsub.sub(intra.I_LEFT_ROOM, handleILeftRoom)
  dom_pubsub.sub(intra.PLAYER_LEFT_ROOM, handlePlayerLeftRoom)

  dom_pubsub.sub(intra.LOGO_PRESSED, handleLogoPressed)
  dom_pubsub.sub(intra.HOME_PRESSED, handleHomePressed)
  dom_pubsub.sub(intra.SYNCED_SELF_DATA, handleSyncedSelfData)

  dom_pubsub.sub(intra.PLAYER_WAS_KICKED, handlePlayerWasKicked)
  dom_pubsub.sub(intra.PLAYER_DESTROYED_ROOM, handlePlayerDestroyedRoom)

  //mob only
  //Not doing this; going back to the welcome screen instead
  dom_pubsub.sub(intra.ROOMBAR_MENU_PRESSED, handleRoombarMenuPressed)
  dom_pubsub.sub(intra.I_PRESSED_SWITCH_TO_ROOM, handleISwitchedToRoom)

  const handlePlayerChangedShortlink = (msg) => {
    const {
      roomid,
      shortlink
    } = msg
    if (focused_elements.current_room_focus === roomid) {
      router.goToRouteSilently(`/r/${shortlink}`)
    }
  }

  const handlePlayerRemovedShortlink = (roomid) => {
    if (focused_elements.current_room_focus === roomid) {
      router.goToRouteSilently(`/rooms/${roomid}`)
    }
  }

  dom_pubsub.sub(intra.PLAYER_CHANGED_SHORTLINK, handlePlayerChangedShortlink)
  dom_pubsub.sub(intra.PLAYER_REMOVED_SHORTLINK, handlePlayerRemovedShortlink)

  router.registerRoute('/r/{shortlink}', (shortlink) => {
    if (!players_reader.getInitialisedStatus()) {
      players_writer.onAppInitialised(() => {
        const roomid = rooms_reader.getRoomidFromShortlink(shortlink)
        if (rooms_reader.getRoom(roomid)) {
          document.title = `${instance_name} | ${rooms_reader.getRoomName(roomid)}`
        } else {
          return instance_name
        }
      })
      return instance_name
    } else {
      const roomid = rooms_reader.getRoomidFromShortlink(shortlink)
      return `${instance_name} | ${rooms_reader.getRoomName(roomid)}`
    }
  }, (shortlink) => {
    if (!players_reader.getInitialisedStatus()) {
      //needs to trigger this off, since it was the / handler's job originally
      dsocket.connect()
      players_writer.onAppInitialised(() => {
        const roomid = rooms_reader.getRoomidFromShortlink(shortlink)
        if (!players_reader.getSelfRooms().has(roomid) && rooms_reader.getRoom(roomid) //only if it exists!
        ) {
          dsocket.emit(inter.I_JOINED_ROOM, {
            roomid,
            userid: getSelfId()
          })
        } else if (players_reader.getSelfRooms().has(roomid) && rooms_reader.getRoom(roomid)) {
          focusOnRoom(roomid)
        } else {
          router.goToRoute('/')
        }
      })
    }
  })

  router.registerRoute('/rooms/{roomid}', (roomid) => {
    if (!players_reader.getInitialisedStatus()) {
      players_writer.onAppInitialised(() => {
        if (rooms_reader.getRoom(roomid)) {
          document.title = `${instance_name} | ${rooms_reader.getRoomName(roomid)}`
        } else {
          return instance_name
        }
      })
      return instance_name
    } else {
      return `${instance_name} | ${rooms_reader.getRoomName(roomid)}`
    }
  }, (roomid) => {
    if (!players_reader.getInitialisedStatus()) {
      //needs to trigger this off, since it was the / handler's job originally
      dsocket.connect()
      players_writer.onAppInitialised(() => {
        if (!players_reader.getSelfRooms().has(roomid) && rooms_reader.getRoom(roomid) //only if it exists!
        ) {
          dsocket.emit(inter.I_JOINED_ROOM, {
            roomid,
            userid: getSelfId()
          })
        } else if (players_reader.getSelfRooms().has(roomid) && rooms_reader.getRoom(roomid)) {
          focusOnRoom(roomid)
        } else {
          router.goToRoute('/')
        }
      })
    }
  })
  router.registerRoute('/play/{game_url}', `${instance_name} | Home`, (game_url) => {
    //needs to trigger this off, since it was the / handler's job originally
    dsocket.connect()
    players_writer.onAppInitialised(() => {
      const gameid = games_reader.getGameIdFromUrl(game_url)
      if (gameid) {
        dsocket.emit(inter.I_REQUESTED_GAME_START_FROM_URL, {
          gameid,
          userid: getSelfId()
        })
      }
    })
    router.goToRoute('/')
  })
  return {
    focused_elements
  }
}

const GlobalFocuser = (router, on_mobile, dsocket, dom_pubsub, home_app, top_sidebar_app, about_app, user_app, login_app, instance_name) => {
  const focused_elements = {
    top_sidebar_shown: false,
    current_page_focus: null //home, user, login, about
  }

  const handleTopNavIconPressed = () => {
    if (focused_elements.top_sidebar_shown) {
      top_sidebar_app.hide()
      focused_elements.top_sidebar_shown = false
    } else {
      top_sidebar_app.focus()
      focused_elements.top_sidebar_shown = true
    }
  }
  const handleIPressedLink = (link) => {
    if (on_mobile) {
      top_sidebar_app.hide()
      focused_elements.top_sidebar_shown = false
    }
    router.goToRoute(link)
  }

  const dom_subs = [
    dom_pubsub.sub(intra.TOP_NAV_ICON_PRESSED, handleTopNavIconPressed),
    dom_pubsub.sub(intra.I_PRESSED_LINK, handleIPressedLink)
  ]

  //registering routes
  router.registerRoute('/about', `${instance_name} | About`, () => {
    home_app.hide()
    user_app.hide()
    login_app.hide()
    about_app.focus()

    focused_elements.current_page_focus = focus.page.ABOUT
  })
  router.registerRoute('/user', `${instance_name} | User`, () => {
    //try for auth
    dsocket.connect()

    home_app.hide()
    about_app.hide()
    login_app.hide()
    user_app.focus()

    focused_elements.current_page_focus = focus.page.USER
  })
  router.registerRoute('/', `${instance_name} | Home`, () => {
    //try for auth
    dsocket.connect()

    user_app.hide()
    about_app.hide()
    login_app.hide()
    home_app.focus()

    focused_elements.current_page_focus = focus.page.HOME
  })
  router.registerRoute('/login', `${instance_name} | Login`, () => {
    home_app.hide()
    user_app.hide()
    about_app.hide()
    login_app.focus()
    
    focused_elements.current_page_focus = focus.page.LOGIN
  })

  //Init
  router.registerDefault('/')
  router.init()
  return {
    focused_elements,
    destroy: () => {
      for (let unsub of dom_subs) {
        unsub()
      }
    }
  }
}

module.exports = {
  RoomFocuser,
  LocalFocuser,
  GlobalFocuser
}
