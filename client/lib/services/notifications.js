const inter = require('../../../shared/socket_events.js')
const focus = require('../symbols/focus.js')
const intra = require('../symbols/intra.js')
const PubSub = require('../../../shared/helpers.js').PubSub

//These are only doing deltas; no init from the backend yet

//Should be able to get focused_elements from these Focus objects


//gn.onNewNotification(focus.page.ABOUT, () => {
//})

//gn.onNotificationsDismissed(focus.page.ABOUT, () => {
//})

//const GlobalNotifications = (dsocket, global_focus) => {
  ////const handleSomethingRelevantHappened
  //const subs = [
    ////dsocket.on
  //]
  //return {
    //onNewNotification: (source, callback) => {
    //},
    //onNotificationsDismissed: (course,callback) => {
    //}
  //}
//}

//const LocalNotifications = (global_focus, local_focus) => {
  //return {
    //onNewNotification: () => {
    //},
    //onNotificationsDismissed: () => {
    //}
  //}
//}


//const IntraRoomNotifications = (global_focus, local_focus, room_focus) => {
  //return {
    //onNewNotification: () => {
    //},
    //onNotificationsDismissed: () => {
    //},
    //destroy: () => {
    //}
  //}
//}


const NEW_NOTIFICATION = 'NEW_NOTIFICATION'
const NOTIFICATIONS_DISMISSED = 'NOTIFICATIONS_DISMISSED'


//This is only doing unread IMs atm
//const Notifications = (dsocket, global_focus, local_focus) => {

  //const global_pubsub = PubSub()
  //const local_pubsub = PubSub()
  //const room_focusers = {}
  //const room_pubsubs = {}

  //const onPlayerSentIm = (msg) => {
    //const {
      //roomid
    //} = msg
    //const room_focuser = room_focusers[roomid]
    //if (room_focuser.focused_elements.current_tab_focus !== focus.room.NEVENTS) {
      //room_pubsubs[roomid].pub(NEW_NOTIFICATION)
    //}
  //}
  //dsocket.on(inter.PLAYER_SENT_IM, onPlayerSentIm)
  //return {
    //registerTab: (roomid, tab_type, onNewNotification, onNotificationsDismissed) => {
      //const unsub_new = room_pubsubs[roomid].sub(NEW_NOTIFICATION, onNewNotification)
      //const unsub_dismissed = room_pubsubs[roomid].sub(NOTIFICATIONS_DISMISSED, onNotificationsDismissed)
      //return () => {
        //unsub_new()
        //unsub_dismissed()
      //}
    //},
    //deregisterRoomFocuser: (roomid) => {
      //delete room_focusers[roomid]
      //delete room_pubsubs[roomid]
    //},
    //registerRoomFocuser: (roomid, focuser) => {
      //room_focusers[roomid] = focuser
      //room_pubsubs[roomid] = PubSub()
    //}
  //}
//}

const room_focusers = {}
const room_pubsubs = {}

module.exports = {
  registerNeventTab: (roomid, onNewNotification, onNotificationsDismissed) => {
    if (!(roomid in room_pubsubs)) {
      room_pubsubs[roomid] = PubSub()
    }
    const unsub_new = room_pubsubs[roomid].sub(NEW_NOTIFICATION, onNewNotification)
    const unsub_dismissed = room_pubsubs[roomid].sub(NOTIFICATIONS_DISMISSED, onNotificationsDismissed)
    return () => {
      unsub_new()
      unsub_dismissed()
    }
  },
  registerSubscriptions: (dsocket, dom_pubsub) => {
    const onPlayerSentIm = (msg) => {
      const {
        roomid
      } = msg
      const room_focuser = room_focusers[roomid]
      if (room_focuser.focused_elements.current_tab_focus !== focus.room.NEVENTS) {
        room_pubsubs[roomid].pub(NEW_NOTIFICATION)
      }
    }
    const onPlayerPressedNeventLogTab = (roomid) => {
      if (room_pubsubs[roomid]) {
        room_pubsubs[roomid].pub(NOTIFICATIONS_DISMISSED)
      }
    }
    dsocket.on(inter.PLAYER_SENT_IM, onPlayerSentIm)
    dom_pubsub.sub(intra.I_PRESSED_ON_NEVENT_LOG_TAB, onPlayerPressedNeventLogTab)
  },
  deregisterRoomFocuser: (roomid) => {
    delete room_focusers[roomid]
    delete room_pubsubs[roomid]
  },
  registerRoomFocuser: (roomid, focuser) => {
    room_focusers[roomid] = focuser
  }
}
  //{
  //IntraRoomNotifications,
  //LocalNotifications,
  //GlobalNotifications
//}
