//shared between ui/topbar and ui/top_sidebar.js
const h = require('../../shared/client_helpers.js')
const intra = require('./symbols/intra.js')

const goToHome = (elem, dom_pubsub) => {
  h.registerTapOrClick(elem, () => {
    dom_pubsub.pub(intra.I_PRESSED_LINK, '/')
  })
}
const goToAbout = (elem, dom_pubsub) => {
  h.registerTapOrClick(elem, () => {
    dom_pubsub.pub(intra.I_PRESSED_LINK, '/about')
  })
}
const goToUser = (elem, dom_pubsub) => {
  h.registerTapOrClick(elem, () => {
    dom_pubsub.pub(intra.I_PRESSED_LINK, '/user')
  })
}

module.exports = {
  goToHome,
  goToAbout,
  goToUser
}
