const jss = require('jss').default
const preset = require('jss-preset-default').default
jss.setup(preset())

//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WeakMap
const cache = new WeakMap()

//https://github.com/cssinjs/jss#example
const createJssClasses = (styles) => {
  if (!cache.has(styles)) {
    cache.set(styles, jss.createStyleSheet(styles).attach().classes)
  }
  return cache.get(styles)
}

module.exports = createJssClasses
