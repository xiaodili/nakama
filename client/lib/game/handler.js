const GameEventHandler = (games_registry) => {
  const games_reader = games_registry.reader
  const games_writer = games_registry.writer
  return {
    handleClientConnected: () => {
      games_writer.triggerReconnectionEvents()
    },
    handleClientDisconnected: () => {
      games_writer.triggerDisconnectionEvents()
    },
    handleIncomingMessage: (msg) => {
      const gseshid = msg.gseshid
      const body = msg.body
      const event = msg.event
      const api = games_reader.getGameApi(gseshid)
      if (api) {
        let game_pubsub = api.getGamePubSub()
        game_pubsub.pub(event, body)
      }
    }
  }
}

module.exports = GameEventHandler
