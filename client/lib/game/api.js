const inter = require('../../../shared/socket_events.js')
const PubSub = require('../../../shared/helpers.js').PubSub

const Api = (selfid, gseshid, dsocket) => {
  const game_pubsub = PubSub()
  let client

  const wrap = (event, body) => {
    return {
      userid: selfid,
      gseshid,
      event,
      body

    }
  }
  return {
    forClient: {
      input: {
        on: (evt, fn) => {
          return game_pubsub.sub(evt, fn)
        }
      },
      output: {
        emit: (evt, msg) => {
          dsocket.emit(inter.GAME_EVENT, wrap(evt, msg))
        }
      }
    },
    forGameWrapper: {
      registerClient: (_client) => {
        client = _client
      }
    },
    forHandler: {
      triggerReconnection: () => {
        if (typeof client.onReconnection === 'function') {
          client.onReconnection()
        }
      },
      triggerDisconnection: () => {
        if (typeof client.onDisconnection === 'function') {
          client.onDisconnection()
        }
      },
      getGamePubSub: () => {
        return game_pubsub
      },
      destroy: () => {
        client.destroy()
      }
    }
  }
}

module.exports = Api
