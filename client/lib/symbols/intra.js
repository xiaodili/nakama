//Syncs

//Source: lib/establish_session.js
//Sinks: rooms_listing.js, components/players.js
//{<id: room>}
exports.SYNCED_ROOMS = "SYNCED_ROOMS"

//Source: lib/establish_session.js
//Sinks: ui/user.js, ui/components/name_select.js, ui/top_sidebar.js, lib/focus.js
//userid, handle, rooms_occupied: [roomid...], using_cookies
exports.SYNCED_SELF_DATA = "SYNCED_SELF_DATA"

//Source: lib/establish_session.js
//Sinks: rooms_listing.js, ui/user.js
//{serverid, create_room_enabled, cookies_enabled, account: {allow_conversion_to_account}, app_catalogue}
exports.SYNCED_SERVER_CONFIG = "SYNCED_SERVER_CONFIG"

//Source: lib/establish_session.js
//Sinks: components/nevent_log.js, components/game_wrapper.js (GameWrapperApp)
//
exports.CLIENT_RECONNECTED = "CLIENT_RECONNECTED"

//Handshakes

//Done over the room_item pubsub
//Source: components/rooms_listing/room_item/action.js
//Sinks: components/rooms_listing/room_item/passphrase.js
//{roomid}
exports.I_JOINED_PASSPHRASED_ROOM_INTENT = "I_JOINED_PASSPHRASED_ROOM_INTENT"

//Done over the room_item pubsub
//Source: components/rooms_listing/room_item/passphrase.js
//Sinks: components/rooms_listing/room_item/action.js
//{roomid, passphrase}
exports.PASSPHRASE_CHECK_PASSED = "PASSPHRASE_CHECK_PASSED"

//Updates

//Source: app.js
//Sinks: components/game_wrapper.js, components/nevent_log.js
//{gseshid, roomid}
exports.GAME_ENDED = "GAME_ENDED"

//Source: app.js
//Sinks: components/players.js (PlayerItem), components/nevent_log.js, components/game_wrapper.js, TODO components/room/settings.js
//{userid, roomid}
exports.PLAYER_ASSIGNED_AS_ADMIN = "PLAYER_ASSIGNED_AS_ADMIN"

//Source: app.js
//Sinks: rooms_listing (RoomItemActionApp, RoomItem)
//roomid
exports.PLAYER_ADDED_PASSPHRASE = "PLAYER_ADDED_PASSPHRASE"

//Source: app.js
//Sinks: rooms_listing (RoomItemActionApp, RoomItem)
//roomid
exports.PLAYER_REMOVED_PASSPHRASE = "PLAYER_REMOVED_PASSPHRASE"

//Source: components/players.js (PlayerItem)
//Sinks: components/players.js (PlayerListApp), rooms_listing.js
//{playerid, kickerid, roomid}
exports.I_KICKED_PLAYER = "I_KICKED_PLAYER"

//Source: app.js
//Sinks: components/players.js
//userid
exports.PLAYER_INACTIVE = "PLAYER_INACTIVE"

//Source: app.js
//Sinks: components/players.js
//userid
exports.PLAYER_ACTIVE = "PLAYER_ACTIVE"

//Source: app.js
//Sinks: room.js, rooms_listing.js
//{roomid, name}
exports.PLAYER_CHANGED_ROOM_NAME = "PLAYER_CHANGED_ROOM_NAME"

//Source: components/room.js (RoomSettingsApp)
//Sinks: rooms_listing.js, room.js (RoomTitleApp)
//{roomid, name}
exports.I_CHANGED_ROOM_NAME = "I_CHANGED_ROOM_NAME"

//Source: app.js
//Sinks: components/room/settings/settings.js
//{roomid, description}
exports.PLAYER_CHANGED_ROOM_DESCRIPTION = "PLAYER_CHANGED_ROOM_DESCRIPTION"

//Source: components/room.js (RoomSettingsApp)
//Sinks: components/room/settings/settings.js
//{roomid, name}
exports.I_CHANGED_ROOM_DESCRIPTION = "I_CHANGED_ROOM_DESCRIPTION"

//Source: app.js
//Sinks: components/room.js (RoomApp), lib/focus.js
//{roomid, userid}
exports.PLAYER_DESTROYED_ROOM = "PLAYER_DESTROYED_ROOM"

//Source: app.js
//Sinks: components/room.js (RoomApp), room/roombar.js, lib/services/focus.js (RoomFocuser), admin_settings.js
//{roomid, userid}
exports.PLAYER_ENABLED_GAMES = "PLAYER_ENABLED_GAMES"

//Source: app.js
//Sinks: components/room.js (RoomApp), room/roombar.js, lib/services/focus.js (RoomFocuser), admin_settings.js
//{roomid, userid}
exports.PLAYER_DISABLED_GAMES = "PLAYER_DISABLED_GAMES"

//Source: app.js
//Sinks: components/room.js (RoomApp), room/roombar.js, lib/services/focus.js (RoomFocuser), admin_settings.js
//{roomid, userid}
exports.PLAYER_ENABLED_NEVENT_LOG = "PLAYER_ENABLED_NEVENT_LOG"

//Source: app.js
//Sinks: components/room.js (RoomApp), room/roombar.js, lib/services/focus.js (RoomFocuser), admin_settings.js
//{roomid, userid}
exports.PLAYER_DISABLED_NEVENT_LOG = "PLAYER_DISABLED_NEVENT_LOG"

//Source: app.js
//Sinks: components/room.js (RoomApp), rooms_listing.js (RoomItemNumPlayersApp), room/roombar.js, lib/services/focus.js (RoomFocuser), admin_settings.js
//{roomid, userid}
exports.PLAYER_ENABLED_PLAYER_LIST = "PLAYER_ENABLED_PLAYER_LIST"

//Source: app.js
//Sinks: components/room.js (RoomApp), rooms_listing.js, room/roombar.js, lib/services/focus.js (RoomFocuser), admin_settings.js
//{roomid, userid}
exports.PLAYER_DISABLED_PLAYER_LIST = "PLAYER_DISABLED_PLAYER_LIST"

//Source: app.js
//Sinks: room/settings/admin_settings.js
//{userid, roomid}
exports.PLAYER_UNPROTECTED_ROOM = "PLAYER_UNPROTECTED_ROOM"

//Source: app.js
//Sinks: room/settings/admin_settings.js
//{userid, roomid}
exports.PLAYER_PROTECTED_ROOM = "PLAYER_PROTECTED_ROOM"

//Source: app.js
//Sinks: components/rooms_listing
//room
exports.ROOM_CREATED = "ROOM_CREATED"

//Source: app.js
//Sinks: components/game_wrapper (GameWrapperApp), components/nevent_log.js
//{roomid, userid, game_name, gseshid, program_commands}
exports.GAME_STARTED = "GAME_STARTED"

//Source: app.js
//Sinks: components/game_wrapper (GameWrapperApp → GameItem), components/nevent_log.js
//{roomid, gseshid}
exports.GAME_ERRORED = "GAME_ERRORED"

//Root: GameWrapper
//Source: components/game_wrapper.js (GameItem)
//Sinks: components/game_wrapper.js (GameItem, GameWrapperApp)
//gameid
exports.I_PRESSED_PLAY_GAME = "I_PRESSED_PLAY_GAME"

//Root: GameWrapper
//Source: components/game_wrapper.js (GameSelectApp)
//Sinks: components/game_wrapper.js (GameItem)
//gameid
exports.PLAYER_PRESSED_PLAY_GAME = "PLAYER_PRESSED_PLAY_GAME"

//Source: rooms_listing/action.js, app.js
//Sinks: components/rooms_listing.js (RoomItemNumPlayersApp, RoomItemActionApp), rooms.js, focus.js (LocalFocuser)
//roomid
exports.I_JOINED_ROOM = "I_JOINED_ROOM"

//Source: rooms_listing/action.js
//Sinks: components/rooms_listing.js (RoomItemNumPlayersApp, RoomItemActionApp), rooms.js, focus.js (LocalFocuser)
//roomid
exports.I_JOINED_ROOM_INTENT = "I_JOINED_ROOM_INTENT"

//Source: rooms_listing.js (RoomItemActionApp), components/room.js (RoomTitleApp)
//Sinks: components/rooms_listing.js (RoomItemNumPlayersApp, RoomItemActionApp), rooms.js, focus.js (LocalFocuser)
//roomid
exports.I_LEFT_ROOM = "I_LEFT_ROOM"


//Source: name_select.js
//Sinks: components/players.js (PlayerItem)
//{handle, userid, old_handle}
exports.I_CHANGED_HANDLE = "I_CHANGED_HANDLE"

//Source: app.js
//Sinks: components/players.js (PlayerListApp), components/rooms_listing.js (RoomItemNumPlayersApp, RoomItemActionApp), components/nevent_log.js, lib/focus.js (LocalFocuser), rooms.js
//{userid, kickerid, roomid}
exports.PLAYER_WAS_KICKED = "PLAYER_WAS_KICKED"

//Source: app.js
//Sinks: components/players.js (PlayerListApp), components/nevent_log.js (NeventLogApp), components/rooms_listing.js (RoomItemNumPlayersApp, RoomItemActionApp), rooms.js, lib/focus.js
//{roomid, userid}
exports.PLAYER_JOINED_ROOM = "PLAYER_JOINED_ROOM"

//Source: app.js
//Sinks: components/players.js (PlayerListApp), components/nevent_log.js (NeventLogApp), components/rooms_listing.js (RoomItemNumPlayersApp, RoomItemActionApp), rooms.js, services/focus.js
//{roomid, userid}
exports.PLAYER_LEFT_ROOM = "PLAYER_LEFT_ROOM"

//Source: app.js
//Sinks: components/players.js (PlayerItem), components/nevent_log.js (NeventLogApp), components/name_select.js
//{old_handle, userid, handle}
exports.PLAYER_CHANGED_HANDLE = "PLAYER_CHANGED_HANDLE"

//Source: app.js
//Sinks: components/settings/admin_settings.js, lib/services/focus.js (LocalFocuser)
//{roomid, shortlink}
exports.PLAYER_CHANGED_SHORTLINK = "PLAYER_CHANGED_SHORTLINK"

//Source: app.js
//Sinks: components/settings/admin_settings.js
//{roomid, userid, tab_type}
exports.PLAYER_CHANGED_DEFAULT_TAB = "PLAYER_CHANGED_DEFAULT_TAB"

//Source: app.js
//Sinks: components/settings/admin_settings.js, lib/services/focus.js (LocalFocuser)
//roomid
exports.PLAYER_REMOVED_SHORTLINK = "PLAYER_REMOVED_SHORTLINK"

//Ephermeral Events (UX-y stuff)
/****************/

//Source: ui/topbar.js
//Sinks: lib/services/focus.js
//
exports.LOGO_PRESSED = "LOGO_PRESSED"

//Source: ui/topbar.js
//Sinks: lib/services/focus.js
//
exports.HOME_PRESSED = "HOME_PRESSED"

//Source: services/focus.js
//Sinks: rooms_listing/room_item/action.js
//roomid
exports.I_FOCUSED_ON_ROOM = "I_FOCUSED_ON_ROOM"

//Source: services/focus.js
//Sinks: rooms_listing/room_item/action.js
//
exports.I_FOCUSED_ON_WELCOME = "I_FOCUSED_ON_WELCOME"

//Source: rooms_listing/room_item/action.js
//Sinks: services/focus.js
//roomid
exports.I_PRESSED_SWITCH_TO_ROOM = "I_PRESSED_SWITCH_TO_ROOM"

//Source: components/room/roombar.js
//Sinks: lib/services/focus.js (RoomFocuser)
//roomid
exports.ROOM_SETTINGS_PRESSED = "ROOM_SETTINGS_PRESSED"

//Source: components/topbar.js
//Sinks: lib/services/focus.js
//
exports.TOP_NAV_ICON_PRESSED = "TOP_NAV_ICON_PRESSED"

//The back icon on the room (should really rename this event)
//Source: components/room/roombar.js
//Sinks: lib/services/focus.js
//
exports.ROOMBAR_MENU_PRESSED = "ROOMBAR_MENU_PRESSED"

//Source: components/room/roombar.js
//Sinks: services/focus.js, components/room/roombar.js (TabApp), services/notification.js
//roomid
exports.I_PRESSED_ON_PLAYERS_TAB = "I_PRESSED_ON_PLAYERS_TAB"

//Source: components/room/roombar.js
//Sinks: services/focus.js, components/room/roombar.js (TabApp), services/notification.js
//roomid
exports.I_PRESSED_ON_NEVENT_LOG_TAB = "I_PRESSED_ON_NEVENT_LOG_TAB"

//Source: components/room/roombar.js
//Sinks: services/focus.js, components/room/roombar.js (TabApp), services/notification.js
//roomid
exports.I_PRESSED_ON_GAMES_TAB = "I_PRESSED_ON_GAMES_TAB"

//Source: lib/links
//Sinks: services/focus.js
//page (e.g. /, /about)
exports.I_PRESSED_LINK = "I_PRESSED_LINK"
