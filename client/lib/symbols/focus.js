const page = {
  ABOUT: 'ABOUT',
  USER: 'USER',
  HOME: 'HOME',
  LOGIN: 'LOGIN',
  TOP_SIDEBAR: 'TOP_SIDEBAR'
}

const home = {
  WELCOME: 'WELCOME',
  ROOM: 'ROOM',
  ROOMLIST: 'ROOMLIST'
}

const room = {
  PLAYERS: 'PLAYERS',
  NEVENTS: 'NEVENTS',
  GAMES: 'GAMES',
  SETTINGS: 'SETTINGS'
}

module.exports = {
  page,
  home,
  room
}
