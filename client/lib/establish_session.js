const inter = require('../../shared/socket_events.js')
const intra = require('./symbols/intra.js')
const difference = require('../../shared/helpers.js').difference
const intersection = require('../../shared/helpers.js').intersection
const ct = require('../../shared/client_types.js')

const handleSessionEstablished = (dsocket, dom_pubsub, msg, rooms_app, players_registry, rooms_registry, games_registry, finishInit) => {
  //convenience
  const players_reader = players_registry.reader
  const players_writer = players_registry.writer
  const rooms_reader = rooms_registry.reader
  const rooms_writer = rooms_registry.writer

  const players = msg.players
  const rooms = msg.rooms

  //self
  const user = msg.user
  const userid = user.userid
  const handle = user.handle
  const rooms_occupied = user.subscribed_rooms
  const using_cookies = user.using_cookies

  const server_config = msg.config_for_client //{server_id, create_room_enabled, cookies_enabled, account, app_catalogue}
  const server_id = server_config.server_id

  const old_rooms_occupied = new Set(players_reader.getSelfRooms())

  players_writer.syncSelfData(userid, handle, rooms_occupied, using_cookies)
  players_writer.syncPlayers(players)
  rooms_writer.syncRooms(rooms)


  if (!players_reader.getInitialisedStatus()) { //first init
    players_writer.setServerId(server_id)
    games_registry.writer.setCatalogue(server_config.app_catalogue)
    finishInit()
  } else { //syncing's fine
    if (server_id !== players_reader.getServerId()) { //everyone you love and hold dear is gone; blat everything
      players_writer.setServerId(server_id)
      games_registry.writer.setCatalogue(server_config.app_catalogue)
      rooms_app.destroyAllRooms()
    } else {
      //UI consistency: leave any rooms you are no longer part of, since socket events sent from these will no longer be valid
      for (let roomid of difference(old_rooms_occupied, new Set(rooms_occupied))) {
        rooms_app.destroyRoom(roomid)
      }
    }
  }

  dom_pubsub.pub(intra.SYNCED_ROOMS, rooms)
  //Resync existing individual nevent log and games
  dom_pubsub.pub(intra.CLIENT_RECONNECTED)

  //join any rooms you should be a part of (these will resync nevent_log and games by themselves)
  for (let roomid of difference(new Set(rooms_occupied), old_rooms_occupied)) {
    rooms_app.joinRoom(roomid)
  }

  if (!using_cookies) {
    players_writer.disableCookies()
  }
  players_writer.setServerId(server_id) //needs to be done after checking server
  dom_pubsub.pub(intra.SYNCED_SERVER_CONFIG, server_config)

  //needs to be done after rooms are recreated (for focus.js)
  dom_pubsub.pub(intra.SYNCED_SELF_DATA, {
    userid,
    handle,
    rooms_occupied,
    using_cookies
  })

  //autojoin some rooms if player is new
  if (!players_reader.getInitialisedStatus()) {
    for (let roomid of Object.keys(rooms)) {
      let room = rooms[roomid]
      if (room.autojoin) {
        rooms_writer.playerJoinedRoom(userid, roomid)
        players_writer.addSelfRoom(roomid)
        dom_pubsub.pub(intra.I_JOINED_ROOM, roomid)
        dsocket.emit(inter.I_JOINED_ROOM, {
          roomid,
          userid
        })
      }
    }
    // finally, we can toggle this flag
    players_writer.setAppInitialised()
  }
}

const handleIConnected = (dsocket, players_reader) => {
  if (players_reader.getInitialisedStatus()) { //already initialised
    dsocket.emit(inter.ESTABLISH_SESSION, {
      userid: players_reader.getSelfId(),
      handle: players_reader.getSelfHandle(),
      on_mobile: players_reader.getSelfOnMobile(),
      server_id: players_reader.getServerId(),
      rooms_occupied: [...players_reader.getSelfRooms()],
      client_type: ct.CLIENT_APP
    })
  } else { //first time connecting
    dsocket.emit(inter.ESTABLISH_SESSION, {
      cookie: players_reader.getCookiesDetected(),
      on_mobile: players_reader.getSelfOnMobile(),
      client_type: ct.CLIENT_APP
    })
  }
};

exports.handleSessionEstablished = handleSessionEstablished
exports.handleIConnected = handleIConnected
