const intra = require('./symbols/intra.js')
const RoomApp = require('../ui/components/room.js')

const RoomsApp = (mount, dsocket, dom_pubsub, players_registry, rooms_registry, games_registry, config, theme) => {

  //convenience
  const rooms_reader = rooms_registry.reader
  const rooms_writer = rooms_registry.writer
  const players_reader = players_registry.reader

  var rooms = {}
  const handleIJoinedRoom = (roomid) => {
    rooms[roomid] = RoomApp(mount, roomid, dsocket, dom_pubsub, players_registry, rooms_registry, games_registry, config, theme)
  }
  const handleILeftRoom = (roomid) => {
    rooms[roomid].destroy()
    delete rooms[roomid]
  }
  const handlePlayerLeftRoom = (msg) => {
    const roomid = msg.roomid
    const userid = msg.userid
    if (userid === players_reader.getSelfId()) {
      if (roomid in rooms) {
        rooms[roomid].destroy()
        delete rooms[roomid]
      }
    }
  }
  const handlePlayerJoinedRoom = (msg) => {
    const roomid = msg.roomid
    const userid = msg.userid
    if (userid === players_reader.getSelfId()) {
      if (!(roomid in rooms)) {
        rooms[roomid] = RoomApp(mount, roomid, dsocket, dom_pubsub, players_registry, rooms_registry, games_registry, config, theme)
      }
    }
  }
  const handlePlayerDestroyedRoom = (msg) => {
    const roomid = msg.roomid
    rooms[roomid].destroy()
    delete rooms[roomid]
  }

  const dom_subs = [
    dom_pubsub.sub(intra.I_JOINED_ROOM, handleIJoinedRoom),
    dom_pubsub.sub(intra.PLAYER_JOINED_ROOM, handlePlayerJoinedRoom),
    dom_pubsub.sub(intra.I_LEFT_ROOM, handleILeftRoom),
    dom_pubsub.sub(intra.PLAYER_LEFT_ROOM, handlePlayerLeftRoom),
    dom_pubsub.sub(intra.PLAYER_WAS_KICKED, handlePlayerLeftRoom),
    dom_pubsub.sub(intra.PLAYER_DESTROYED_ROOM, handlePlayerDestroyedRoom)
  ]

  return {
    joinRoom: handleIJoinedRoom,
    destroyRoom: (roomid) => {
      rooms[roomid].destroy()
      delete rooms[roomid]
    },
    getRoomApp: (id) => {
      return rooms[id]
    },
    getRoomApps: () => {
      return Object.keys(rooms).map((roomid) => {
        return rooms[roomid]
      })
    },
    destroyAllRooms: () => {
      for (let roomid of Object.keys(rooms)) {
        let room = rooms[roomid]
        room.destroy()
        delete rooms[roomid]
      }
    }
  }
}

module.exports = RoomsApp
