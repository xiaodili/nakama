const h = require('../helpers.js')
const cookies = require('../../../shared/cookies.js').cookies

//aggregates
const PlayersRegistry = () => {
  var players = {}
  var handles = new Set()

  //self init
  const self_on_mobile = h.onMobile(window.navigator.userAgent)

  var self_id,
    self_handle,
    using_cookies,
    session_id,
    server_id

  var self_rooms = new Set()
  var self_account = {
    registered: false
    //session stuff
  }

  var app_initialised = false
  const post_initialisation_hooks = []
  const cookies_detected = {}

  //try to retrieve from cookie:
  cookies_detected.userid = cookies.getItem('userid')
  cookies_detected.handle = cookies.getItem('handle')
  cookies_detected.rooms = cookies.getItem('rooms') ? cookies.getItem('rooms').split(',') : []
  cookies_detected.server_id = cookies.getItem('server_id')
  cookies_detected.session_id = cookies.getItem('session_id')

  const syncCookies = () => {
    if (self_id) {
      cookies.setItem('userid', self_id)
    }
    if (self_handle) {
      cookies.setItem('handle', self_handle)
    }
    cookies.setItem('rooms', [...self_rooms].join(','))
    if (server_id) {
      cookies.setItem('server_id', server_id)
    }
    if (session_id) {
      cookies.setItem('session_id', session_id)
    }
  }

  const removeCookies = () => {
    cookies.removeItem('userid')
    cookies.removeItem('handle')
    cookies.removeItem('rooms')
    cookies.removeItem('server_id')
    cookies.removeItem('session_id')
  }

  const removeSelf = () => {
    self_id = null
    self_handle = null
    using_cookies = null
    session_id = null
    server_id = null
    removeCookies()
  }

  return {
    writer: {
      setSelfHandle: (handle) => {
        self_handle = handle
        players[self_id].handle = handle
      },
      convertSelfIntoAccount: (session_id) => {
        self_account.registered = true
        self_account.session_id = session_id
        if (using_cookies) {
          cookies.setItem('session_id', session_id)
        }
      },
      updateHandle: (id, old_handle, new_handle) => {
        handles.delete(old_handle)
        handles.add(new_handle)
        players[id].handle = new_handle
      },
      setPlayerActive: (id) => {
        players[id].active = true 
      },
      setPlayerInactive: (id) => {
        players[id].active = false
      },
      addPlayer: (id, handle) => {
        players[id] = {
          id,
          handle,
          active: true
        }
      },
      removePlayer: (id) => {
        delete players[id]
      },
      addSelfRoom: (room_id) => {
        self_rooms.add(room_id)
        if (using_cookies) {
          cookies.setItem('rooms', [...self_rooms].join(','))
        }
      },
      removeSelfRoom: (room_id) => {
        self_rooms.delete(room_id)
        if (using_cookies) {
          cookies.setItem('rooms', [...self_rooms].join(','))
        }
      },
      //userid, string, [roomid...]
      syncSelfData: (id, handle, rooms, _using_cookies) => {
        self_id = id
        self_handle = handle
        self_rooms = new Set(rooms)
        using_cookies = _using_cookies
        if (using_cookies) {
          syncCookies()
        }
      },
      syncPlayers: (_players) => {
        players = _players
        handles = new Set(...[Object.keys(_players).map((userid) => {
          return _players[userid].handle
        })])
      },
      //session stuff
      setServerId: (id) => {
        server_id = id
        if (using_cookies) {
          cookies.setItem('server_id', server_id)
        }
      },
      setAppInitialised: () => {
        app_initialised = true
        for (const fn of post_initialisation_hooks) {
          fn()
        }
      },
      disableCookies: () => {
        using_cookies = false
        removeCookies()
      },
      enableCookies: () => {
        using_cookies = true
        syncCookies()
      },
      clearRegistry: () => {
        players = {}
        handles = new Set()
        removeSelf()
      },
      onAppInitialised: (fn) => {
        post_initialisation_hooks.push(fn)
      }
    },
    reader: {
      isHandleTaken: (handle) => {
        return handles.has(handle)
      },
      isSelfInRoom: (roomid) => {
        return self_rooms.has(roomid)
      },
      getPlayer: (id) => {
        return players[id]
      },
      getHandleFromId: (id) => {
        return players[id].handle
      },
      getPlayers: () => { return players },
      getSelfId: () => { return self_id },
      getSelfHandle: () => { return self_handle },
      isLoggedIn: () => {
        return Boolean(session_id)
      },
      //returns first self room
      getSelfRoom: () => {
        return self_rooms.keys().next().value
      },
      getSelfRooms: () => {
        return self_rooms
      },
      getSelfOnMobile: () => { return self_on_mobile },
      //session stuff
      getServerId: () => {
        return server_id
      },
      getInitialisedStatus: () => {
        return app_initialised
      },
      getUsingCookies: () => {
        return using_cookies
      },
      getCookiesDetected: () => {
        return cookies_detected
      }
    }
  }
}

module.exports = PlayersRegistry
