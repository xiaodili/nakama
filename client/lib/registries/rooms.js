var RoomsRegistry = () => {

  //cba passing LocalFocuser into everything
  var current_room_focus = null

  var rooms = {
    //room properties:
    //from server:
    
    //admin: string?
    //autojoin: bool
    //created_by: string
    //default_tab: string?
    //id: string
    //name: string
    //description: string
    //shortlink: string?
    //players: Set
    //nevent_log_enabled: bool
    //games_enabled: bool
    //player_list_enabled: bool
    //protected: bool
    //game_ongoing: string?
    //passphrase_required: bool
  }
  return {
    writer: {
      setCurrentRoomFocus: (value) => { 
        current_room_focus = value // roomid || null
      },
      assignAdmin: (roomid, userid) => {
        rooms[roomid].admin = userid
      },
      changeDefaultTab: (roomid, tab_type) => {
        rooms[roomid].default_tab = tab_type
      },
      syncRooms: (_rooms) => {
        rooms = _rooms
        for (let id of Object.keys(rooms)) {
          rooms[id].players = new Set(rooms[id].players)
        }
      },
      createRoom: (room) => {
        const roomid = room.id
        rooms[roomid] = room
        rooms[roomid].players = new Set(rooms[roomid].players)
      },
      addPassphraseRequirement: (roomid) => {
        rooms[roomid].passphrase_required = true
      },
      removePassphraseRequirement: (roomid) => {
        rooms[roomid].passphrase_required = false
      },
      removeRoom: (roomid) => {
        delete rooms[roomid]
      },
      changeName: (roomid, new_name) => {
        rooms[roomid].name = new_name
      },
      changeDescription: (roomid, new_description) => {
        rooms[roomid].description = new_description
      },
      playerJoinedRoom: (userid, roomid) => {
        rooms[roomid].players.add(userid)
      },
      playerLeftRoom: (userid, roomid) => {
        rooms[roomid].players.delete(userid)
      },
      setGameOngoing: (roomid, gseshid) => {
        rooms[roomid].game_ongoing = gseshid
      },
      enablePlayerList: (roomid) => {
        rooms[roomid].player_list_enabled = true
      },
      disablePlayerList: (roomid) => {
        rooms[roomid].player_list_enabled = false
      },
      enableGames: (roomid) => {
        rooms[roomid].games_enabled = true
      },
      disableGames: (roomid) => {
        rooms[roomid].games_enabled = false
      },
      enableNeventLog: (roomid) => {
        rooms[roomid].nevent_log_enabled = true
      },
      disableNeventLog: (roomid) => {
        rooms[roomid].nevent_log_enabled = false
      },
      removeGameOngoing: (roomid) => {
        delete rooms[roomid].game_ongoing
      },
      protectRoom: (roomid) => {
        rooms[roomid].protected = true
      },
      removeShortlink: (roomid) => {
        delete rooms[roomid].shortlink
      },
      changeShortlink: (roomid, shortlink) => {
        rooms[roomid].shortlink = shortlink
      },
      unprotectRoom: (roomid) => {
        rooms[roomid].protected = false
      },
      clearRegistry: () => {
        rooms = {}
      }
    },
    reader: {
      isPlayerOccupyingRoom: (userid, roomid) => {
        return rooms[roomid].players.has(userid)
      },
      getCurrentRoomFocus: () => {
        return current_room_focus
      },
      isPlayerAdminOf: (roomid, userid) => {
        return rooms[roomid].admin === userid
      },
      getRoomAdmin: (id) => {
        const room = rooms[id]
        const admin_policy = room.admin_policy
        if (admin_policy === 'creator') {
          return room.created_by
        } else if (admin_policy === 'oldest_occupant') {
          if (room.players.size > 0) {
            return room.players.keys().next().value
          }
        }
        return null;
      },
      getNumRooms: () => {
        return Object.keys(rooms).length
      },
      getShortlink: (roomid) => {
        return rooms[roomid].shortlink
      },
      getRoomIds: () => {
        return Object.keys(rooms)
      },
      getRoomidFromShortlink: (shortlink) => { //Linear time for now
        for (const roomid of Object.keys(rooms)) {
          const room = rooms[roomid]
          if (room.shortlink === shortlink) {
            return room.id
          }
        }
        return null
      },
      getRoomName: (id) => {
        return rooms[id].name
      },
      getRoom: (id) => {
        return rooms[id]
      }
    }
  }
}

module.exports = RoomsRegistry
