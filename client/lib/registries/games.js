const createUrltoIdsLookup = (catalogue) => {
  const urlToIds = {}
  for (const game_id of Object.keys(catalogue)) {
    urlToIds[catalogue[game_id].url] = game_id
  }
  return urlToIds
}

var GamesRegistry = () => {
  //gseshid → <Api.forHandler>
  let sessions = {},
    urlToIds,
    catalogue

  return {
    reader: {
      getCatalogue: () => {
        return catalogue
      },
      getGameApi: (gseshid) => {
        return sessions[gseshid]
      },
      getGameIdFromUrl: (url) => {
        return urlToIds[url]
      }
    },
    writer: {
      setCatalogue: (_catalogue) => {
        catalogue = _catalogue
        urlToIds = createUrltoIdsLookup(catalogue)
      },
      triggerReconnectionEvents: () => {
        for (const gseshid of Object.keys(sessions)) {
          const api = sessions[gseshid]
          api.triggerReconnection()
        }
      },
      triggerDisconnectionEvents: () => {
        for (const gseshid of Object.keys(sessions)) {
          const api = sessions[gseshid]
          api.triggerDisconnection()
        }
      },
      storeGameApi: (gseshid, api) => {
        const stale_api = sessions[gseshid]
        if (stale_api) {
          stale_api.destroy()
        }
        sessions[gseshid] = api
      },
      removeGameApi: (gseshid) => {
        let api = sessions[gseshid]
        api.destroy()
        delete sessions[gseshid]
      },
      clearRegistry: () => {
        for (const gseshid of Object.keys(sessions)) {
          const api = sessions[gseshid]
          api.destroy()
        }
        sessions = {}
      }
    }
  }
}

module.exports = GamesRegistry
