//http://stackoverflow.com/questions/3514784/what-is-the-best-way-to-detect-a-mobile-device-in-jquery?page=1&tab=active#tab-top
const onMobile = (user_agent) => {
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(user_agent) ) {
    return true
  }
  else {
    return false
  }
}

exports.onMobile = onMobile
