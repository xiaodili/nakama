//Adapted from this thread: https://bugs.chromium.org/p/chromium/issues/detail?id=456515
const StopChromePullUpRefresh = () => {
  var maybePreventPullToRefresh = false
  var lastTouchY = 0
  const touchstartHandler = (e) => {
    if (e.touches.length != 1) {
      return
    } 
    lastTouchY = e.touches[0].clientY;
    // Pull-to-refresh will only trigger if the scroll begins when the
    // document's Y offset is zero.
    maybePreventPullToRefresh = (window.pageYOffset === 0)
  }

  const touchmoveHandler = (e) => {
    const touchY = e.touches[0].clientY
    const touchYDelta = touchY - lastTouchY
    lastTouchY = touchY

    if (maybePreventPullToRefresh) {
      // To suppress pull-to-refresh it is sufficient to preventDefault the
      // first overscrolling touchmove.
      maybePreventPullToRefresh = false
      if (touchYDelta > 0) {
        e.preventDefault()
      }
    }
  }

  document.addEventListener('touchstart', touchstartHandler, false)
  document.addEventListener('touchmove', touchmoveHandler, false)
}

const WindowHeight = {
  getHeight: () => {
    return window.innerHeight
  },
  onWindowResize: (fn) => {
    const wrapper = () => {
      fn(window.innerHeight)
    }
    window.addEventListener('resize', wrapper)
    return () => {
      window.removeEventListener('resize', wrapper)
    }
  }
}

exports.StopChromePullUpRefresh = StopChromePullUpRefresh
exports.WindowHeight = WindowHeight
