module.exports = {
  instance_name: 'Nakama',
  nevent_log_limit: 50,
  typing_timer_length_milliseconds: 1000,
  notifications_enabled: true //if true, notifications are enabled on the client
}
