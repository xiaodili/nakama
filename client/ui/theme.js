const jss = require('../lib/css.js')

//Colours
//-------

//const brunswick_green = '#1b4d3e'
//const tango_pink = '#e4717a'
const seashell = '#f1f0f1'
const emperor = '#505050'

//Components
const baseButton = {
  fontWeight: 'bold',
  border: 'none',
  borderRadius: '5px',
  fontSize: '12px',
  padding: '5px 8px',
  color: seashell,
  textAlign: 'center',
  cursor: 'pointer',
  '&:disabled': {
    cursor: 'default',
    opacity: '0.3'
  }
}


const coloursMerge = (colours) => {
  return Object.assign(
    {}, {
      event_grey: emperor,
      primary: '#3aafb4',
      secondary: '#dc5066',
      topbarBackground: '#50504e',
      background: '#f1f0f1'
    }, colours)
}

const fontsMerge = (fonts) => {
  return Object.assign(
    {}, {
      standard: {
        fontFamily: `'Open Sans', sans-serif`
      },
      heading: {
        fontFamily: `'Open Sans', sans-serif`,
        fontWeight: 'bold'
      },
      subHeading: {
        fontFamily: `'Open Sans', sans-serif`,
        fontWeight: 'bold'
      }
    }, fonts)
}

module.exports = (theme) => {
  const colours = coloursMerge(theme ? theme.colours : null)
  const fonts = fontsMerge(theme? theme.fonts : null)

  //Prebuilt
  const prebuilt_styles = jss({
    button_secondary: Object.assign({}, baseButton, {
      backgroundColor: colours.secondary
    }),
    button_ghost: Object.assign({}, baseButton, {
      backgroundColor: 'white',
      color: emperor
    }),
    heading: {
      fontFamily: fonts.heading.fontFamily,
      fontWeight: fonts.heading.fontWeight,
      fontSize: '16px'
      
    },
    sub_heading: {
      fontFamily: fonts.subHeading.fontFamily,
      fontWeight: fonts.subHeading.fontWeight,
      fontSize: '16px'
    }
  })


  return {
    colours,
    fonts,
    styles: {
      raw: {
        buttonPrimary: Object.assign({}, baseButton, {
          backgroundColor: colours.primary
        })
      },
      prebuilt: prebuilt_styles
    }
  }
}
