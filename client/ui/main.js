const h = require('../../shared/client_helpers.js')
const jss = require('../lib/css')
const Theme = require('./theme.js')

//Lib
const GlobalFocuser = require('../lib/services/focus.js').GlobalFocuser
const registerSubscriptions = require('../lib/services/notifications.js').registerSubscriptions

//Main
const TopBarApp = require('./main/topbar.js')
const TopSidebarApp = require('./main/top_sidebar.js')

//Pages
const AboutApp = require('./pages/about.js')
const UserApp = require('./pages/user.js')
const LoginApp = require('./pages/login.js')
const HomeApp = require('./pages/home.js')


const renderTopSidebar = (on_mobile, styles) => {
  if (on_mobile) {
    return `<div class='${styles.topSidebarContainer}' nakama='top-sidebar-app'></div>`
  } else {
    return ''
  }
}
 
const applyGlobalMainStyling = (theme) => {

  const styles = jss({
    '@global': {
      html: {
        height: '100%',
        overflowY: 'hidden',
        overflowX: 'hidden'
      },
      button: theme.styles.raw.buttonPrimary,
      body: {
        overflowY: 'hidden',
        overflowX: 'hidden',
        height: '100%',
        margin: '0px',
        fontFamily: theme.fonts.standard.fontFamily
      },
      ul: {
        listStyleType: 'none',
        padding: '0px',
        margin: '0px'
      }
    },
    mainApp: {
      height: '100%'
    },
    pageContainer: {
      height: '100%'
    },
    pageApp: {
      height: '100%',
      backgroundColor: theme.colours.background
    },
    aboutContainer: { //Should just be pageApp, but needs to be centered
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      backgroundColor: theme.colours.background
    },
    topSidebarContainer: {
      display: 'none', //init
      height: '100%'
    }
  })
  return styles
}

const injectHeadResources = (theme, custom_resources) => {
  //Make the address bar different coloured...?
  const theme_node = document.createElement('meta')
  theme_node.setAttribute('name', 'theme-color')
  theme_node.setAttribute('content', theme.colours.topbarBackground)
  document.getElementsByTagName('head')[0].appendChild(theme_node)

  const open_sans = 'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,700'
  const resources = [open_sans].concat(custom_resources)
  for (const resource of resources) {
    const resource_node = document.createElement('link')
    resource_node.setAttribute('href', resource)
    resource_node.setAttribute('rel', 'stylesheet')
    document.getElementsByTagName('head')[0].appendChild(resource_node)
  }
}

const MainApp = (mount, dsocket, dom_pubsub, players_registry, rooms_registry, games_registry, router, config, ui_config) => {
  const theme = Theme(ui_config.theme)
  const styles = applyGlobalMainStyling(theme)
  injectHeadResources(theme, ui_config.resources || [])
  const notifications_enabled = config.notifications_enabled
  const players_reader = players_registry.reader
  const on_mobile = players_reader.getSelfOnMobile()
  const view = h.View(
    mount,
    'div',
    [['class', styles.mainApp]],
    (on_mobile) => { return `
      <div nakama='topbar-app'></div>
      ${renderTopSidebar(on_mobile, styles)}
      <div class='${styles.pageContainer}'>
        <div nakama='home-app' class='${styles.pageApp}'></div>
        <div nakama='about-app' class='${styles.aboutContainer}'></div>
        <div nakama='user-app' class='${styles.pageApp}'></div>
        <div nakama='login-app' class='${styles.pageApp}'></div>
      </div>
    `}, on_mobile)

  let home_app, top_sidebar_app, about_app, user_app, login_app
  view.renderApp((dom) => {
    h.bindAttributes('nakama', {
      'topbar-app': (elem) => { //The top part of app
        TopBarApp(elem, dom_pubsub, players_reader, theme)
      },
      'top-sidebar-app': (elem) => { //Mobile only hamburger menu
        top_sidebar_app = TopSidebarApp(elem, dom_pubsub, players_reader, theme)
      },
      'home-app': (elem) => { //Home Page
        home_app = HomeApp(elem, dsocket, dom_pubsub, players_registry, rooms_registry, games_registry, router, config, ui_config.pages.home, theme)
      },
      'about-app': (elem) => { //About Page
        about_app = AboutApp(elem, ui_config.pages.about, theme)
      },
      'user-app': (elem) => { //User Settings Page
        user_app = UserApp(elem, dsocket, dom_pubsub, players_registry, theme)
      },
      'login-app': (elem) => { //Login Page
        login_app = LoginApp(elem, dom_pubsub, players_registry, theme)
      }
    }, dom)
    GlobalFocuser(router, on_mobile, dsocket, dom_pubsub, home_app, top_sidebar_app, about_app, user_app, login_app, config.instance_name)
    if (notifications_enabled) {
      registerSubscriptions(dsocket, dom_pubsub)
    }
  })
  return {
    getLoadingApp: home_app.getLoadingApp,
    getRoomsApp: home_app.getRoomsApp
  }
}

module.exports = MainApp
