//Utility
const pixelify = (unit) => {
  return `${unit}px`
}

//Measurements
//------------
const topbar_height = 30 //Nakama ≡ OR Nakama     Home About
const roombar_height = 25 //cool_squirel's Pirate Treehouse
const roomtab_height = 25 //Occupants | Messages | App

const static_screen_max_width = 600

//Nevent constants
const indicator_height = 20 //Someone is typing...
const input_height = 40 //Enter slash command or message here
const bottom_container = indicator_height + input_height

const page_container_offset = topbar_height
const settings_container_offset = topbar_height + roombar_height
const tab_container_offset = topbar_height + roombar_height + roomtab_height
const messages_container_offset = topbar_height + roombar_height + roomtab_height + bottom_container //from bottom

//Colours
//-------
const brunswick_green = '#1b4d3e'
const tango_pink = '#e4717a'
const seashell = '#f1f0f1'
const emperor = '#505050'

module.exports = {
  measurements: {
    topbar_height_pixels: pixelify(topbar_height),
    bottom_container_pixels: pixelify(bottom_container),
    indicator_height_pixels: pixelify(indicator_height),
    input_height_pixels: pixelify(input_height),
    roombar_height_pixels: pixelify(roombar_height),
    roomtab_height_pixels: pixelify(roomtab_height),
    messages_container_offset,
    page_container_offset,
    settings_container_offset,
    tab_container_offset,
    static_screen_max_width_pixels: pixelify(static_screen_max_width)
  },
  colours: { //TODO these need to go out
    topbar: brunswick_green,
    logo: tango_pink,
    background: seashell,
    event_grey: emperor
  }
}
