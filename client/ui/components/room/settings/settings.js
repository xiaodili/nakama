const h = require('../../../../../shared/client_helpers.js')
const css = require('../../../../lib/css.js')
const cssc = require('../../../css_constants.js')
const inter = require('../../../../../shared/socket_events.js')
const intra = require('../../../../lib/symbols/intra.js')
const WindowHeight = require('../../../../lib/hacks.js').WindowHeight
const AdminSettingsApp = require('./admin_settings.js')

const visible_display = 'block'
const hidden_display = 'none'
const styles = css({
  settingsHeading: {
    fontWeight: 'lighter',
    textAlign: 'center'
  },
  optionHeader: {
    fontWeight: 'bold'
  },
  optionDescription: {
    fontWeight: 'lighter',
    fontStyle: 'italic'
  },
  optionActionContainer: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  settingsApp: {
    marginLeft: '5px',
    marginRight: '5px',
    display: hidden_display,
    overflowY: 'auto'
  }
})

const SettingsApp = (mount, dsocket, dom_pubsub, roomid, players_registry, rooms_registry, _description, theme) => {
  var self_node

  //convenience
  const rooms_reader = rooms_registry.reader
  const rooms_writer = rooms_registry.writer
  const players_reader = players_registry.reader
  const players_writer = players_registry.writer
  const selfid = players_reader.getSelfId()
  const on_mobile = players_reader.getSelfOnMobile()

  let is_admin = rooms_reader.isPlayerAdminOf(roomid, selfid)
  let admin_app,
    admin_settings_app_node,
    leave_btn_node,
    destroyWindowHeightListener

  const view = h.View(
    mount,
    'div',
    //[['class', `settings-app ${on_mobile? 'mobile' : ''}`]],
    [['class', styles.settingsApp]],
    () => { return `
      <div nakama='settings-app-container'>
        <div nakama="description"></div>
        <div class="${styles.settingsHeading}">SETTINGS</div>
        <div class='${styles.optionActionContainer}'>
          <span class='${styles.optionHeader}'>Leave Room</span>
          <button class='${theme.styles.prebuilt.button_secondary}' nakama="leave">Leave</button>
        </div>
        <div class='${styles.optionDescription}'>
          If an app is running, it will be notified of your departure
        </div>
        <br/>
        <div nakama='admin-settings-app'></div>
      </div>
    `})

  const handleLeaveRoomBtnPressed = () => {
    leave_btn_node.innerHTML = 'Leaving...'
    leave_btn_node.disabled = true
    dsocket.emit(inter.I_LEFT_ROOM, {
      userid: selfid,
      roomid
    })
  }
  const handlePlayerAssignedAsAdmin = (msg) => {
    if (roomid === msg.roomid && selfid === msg.userid) {
      is_admin = true
      admin_app = AdminSettingsApp(admin_settings_app_node, dsocket, dom_pubsub, roomid, players_registry, rooms_registry, theme)
    }
  }

  const dom_subs = [
    dom_pubsub.sub(intra.PLAYER_ASSIGNED_AS_ADMIN, handlePlayerAssignedAsAdmin)
  ]

  view.renderApp((dom) => {
    h.bindAttributes('nakama', {
      'admin-settings-app': (elem) => {
        admin_settings_app_node = elem
        //admin_app = AdminSettingsApp(elem, dsocket, dom_pubsub, roomid, players_registry, rooms_registry, is_admin)
      },
      'settings-app-container': () => {
        //Eh, it's not really this guy's job but might as well stick it here
        const fudge = 0 //yay!
        const calculateMessagesHeightCss = (window_height) => {
          return (window_height - cssc.measurements.settings_container_offset - fudge) + 'px'
        }
        destroyWindowHeightListener = WindowHeight.onWindowResize((new_height) => {
          dom.style.height = calculateMessagesHeightCss(new_height)
        })
        //init
        dom.style.height = calculateMessagesHeightCss(WindowHeight.getHeight())
      },
      'description': (elem) => {
        let description = _description
        const description_node = elem
        const handleRoomDescriptionChanged = (msg) => {
          if (roomid === msg.roomid) {
            description = msg.description
            updateDescription()
          }
        }
        const updateDescription = () => {
          if (description.trim()) {
            description_node.innerHTML = h.escapeHtml(description)
            description_node.display = "block"
          } else {
            description_node.display = "none"
          }
        }
        updateDescription()
        dom_subs.push(dom_pubsub.sub(intra.I_CHANGED_ROOM_DESCRIPTION, handleRoomDescriptionChanged))
        dom_subs.push(dom_pubsub.sub(intra.PLAYER_CHANGED_ROOM_DESCRIPTION, handleRoomDescriptionChanged))
      },
      'leave': (elem) => {
        leave_btn_node = elem
        h.registerTapOrClick(elem, handleLeaveRoomBtnPressed)
      }
    }, dom)
  })
  if (is_admin) {
    admin_app = AdminSettingsApp(admin_settings_app_node, dsocket, dom_pubsub, roomid, players_registry, rooms_registry, theme)
  }
  return {
    show: () => {
      view.getSelfNode().style.display = visible_display
    },
    hide: () => {
      view.getSelfNode().style.display = hidden_display
    },
    destroy: () => {
      for (let unsub of dom_subs) {
        unsub()
      }
      if (is_admin) {
        admin_app.destroy()
      }
      destroyWindowHeightListener()
      view.removeApp()
    }
  }
}

module.exports = SettingsApp
