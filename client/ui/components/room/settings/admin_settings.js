const h = require('../../../../../shared/client_helpers.js')
const jss = require('../../../../lib/css.js')
const roomNameValidate = require('../../../../../shared/helpers.js').roomNameValidate
const inter = require('../../../../../shared/socket_events.js')
const intra = require('../../../../lib/symbols/intra.js')
const ttypes = require('../../../../../shared/symbols/tab_types.js')

const admin_styles = jss({
  //Main app
  adminApp: {
    //boxSizing: 'border-box'
  },
  adminSettingsHeading: {
    fontWeight: 'lighter',
    textAlign: 'center'
  },
  roomNameInput: {
    width: '200px'
  },
  descriptionTextarea: {
    width: '75%'
  },
  defaultTabContainer: {
    //float: 'right'
  },
  optionActionContainer: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  optionHeader: {
    fontWeight: 'bold'
  },
  optionDescription: {
    fontWeight: 'lighter',
    fontStyle: 'italic'
  }
  //Passphrase app
})


const PassphraseApp = (mount, dsocket, roomid, _passphrase_required, theme) => {
  const ACTION_ADD_TEXT = "Add"
  const ACTION_CHANGE_TEXT = "Change"
  const CHANGE_PASS_TEXT = "A passphrase is currently set"
  const NO_PASSPHRASE_SET = "A passphrase has not been set"
  const view = h.View(
    mount,
    'div',
    [],
    () => { return `
      <div class='${admin_styles.optionHeader}'>Passphrase</div>
      <div nakama="input-desc"></div>
      <div class='${admin_styles.optionActionContainer}'>
        <input type="text" nakama="input" placeholder='Enter passphrase here'/>
        <span>
          <button nakama="confirm-btn">${_passphrase_required ? ACTION_CHANGE_TEXT : ACTION_ADD_TEXT}</button>
          <button class='${theme.styles.prebuilt.button_secondary}'nakama="remove-btn">Remove</button>
        </span>
      </div>
      <div class='${admin_styles.optionDescription}'>
        Locks the room so that before joining, users are required to enter the passphrase
      </div>
      <br/>
    `}
  )

  var passphrase_required = _passphrase_required
  var input_node, input_desc_node, confirm_btn_node, remove_btn_node

  const updatePassphraseRequiredChanged = () => {
    if (passphrase_required) {
      confirm_btn_node.innerHTML = ACTION_CHANGE_TEXT
      remove_btn_node.style.display = 'inline-block'
      input_desc_node.innerHTML = CHANGE_PASS_TEXT
    } else {
      confirm_btn_node.innerHTML = ACTION_ADD_TEXT
      remove_btn_node.style.display = 'none'
      input_desc_node.innerHTML = NO_PASSPHRASE_SET
    }
    confirm_btn_node.disabled = true
  }

  const handleConfirmBtnPressed = () => {
    const passphrase = input_node.value
    if (passphrase.length > 0) {
      passphrase_required = true
      input_node.value = ""
      updatePassphraseRequiredChanged()
      dsocket.emit(inter.I_CHANGED_PASSPHRASE, {
        roomid,
        passphrase
      })
    }
  }

  const handleRemoveBtnPressed = () => {
    passphrase_required = false
    updatePassphraseRequiredChanged()
    dsocket.emit(inter.I_REMOVED_PASSPHRASE, roomid)
  }

  const handlePassphraseValueChanged = (evt) => {
    const input_value = evt.target.value
    if (input_value.length > 0) {
      confirm_btn_node.disabled = false
    }
  }

  //init
  view.renderApp((dom) => {
    const bindTypes = {
      'input': (elem) => {
        input_node = elem
        h.registerEnterPress(input_node, handleConfirmBtnPressed)
        input_node.addEventListener('keyup', handlePassphraseValueChanged)
      },
      'input-desc': (elem) => {
        input_desc_node = elem
      },
      'confirm-btn': (elem) => {
        confirm_btn_node = elem
        confirm_btn_node.disabled = true
        h.registerTapOrClick(confirm_btn_node, handleConfirmBtnPressed)
      },
      'remove-btn': (elem) => {
        remove_btn_node = elem
        h.registerTapOrClick(remove_btn_node, handleRemoveBtnPressed)
      }
    }
    h.bindAttributes('nakama', bindTypes, dom)
    updatePassphraseRequiredChanged()
  })
  return {
    destroy: () => {
      view.removeApp()
    }
  }
}

const ShortlinkApp = (mount, dsocket, dom_pubsub, roomid, _shortlink, theme) => {
  const CHANGE_SHORTLINK_TEXT = `A shortlink is currently set`
  const ADD_SHORTLINK_TEXT = `A shortlink has not been set`
  const ADD_SHORTLINK_BTN_TEXT = "Add"
  const CHANGE_SHORTLINK_BTN_TEXT = "Change"

  let shortlink = _shortlink
  let input_node, input_desc_node, confirm_btn_node, remove_btn_node

  const updateShortlinkChanged = () => {
    if (shortlink) {
      confirm_btn_node.disabled = false
      confirm_btn_node.innerHTML = CHANGE_SHORTLINK_BTN_TEXT
      remove_btn_node.style.display = 'inline-block'
      input_desc_node.innerHTML = CHANGE_SHORTLINK_TEXT
    } else {
      confirm_btn_node.disabled = true
      confirm_btn_node.innerHTML = ADD_SHORTLINK_BTN_TEXT
      remove_btn_node.style.display = 'none'
      input_desc_node.innerHTML = ADD_SHORTLINK_TEXT
    }
  }

  const handleConfirmBtnPressed = () => {
    const sl = input_node.value
    if (sl.length > 0) {
      dsocket.emit(inter.PLAYER_CHANGED_SHORTLINK, {
        roomid,
        shortlink: sl
      })
    }
  }

  const handleRemoveBtnPressed = () => {
    dsocket.emit(inter.PLAYER_REMOVED_SHORTLINK, roomid)
  }

  const handleShortlinkValueChanged = (evt) => {
    const input_value = evt.target.value
    if (input_value.length > 0) {
      confirm_btn_node.disabled = false
    }
  }

  const view = h.View(
    mount,
    'div',
    [],
    () => { return `
      <div class='${admin_styles.optionHeader}'>Shortlink</div>
      <div nakama="input-desc"></div>
      <div class='${admin_styles.optionActionContainer}'>
        <label>
          <code>/r/</code>
          <input type="text" nakama="input" placeholder='Enter shortlink here'/>
        </label>
        <span>
          <button nakama="confirm-btn">${shortlink ? ADD_SHORTLINK_BTN_TEXT : CHANGE_SHORTLINK_BTN_TEXT}</button>
          <button class='${theme.styles.prebuilt.button_secondary}'nakama="remove-btn">Remove</button>
        </span>
      </div>
      <div class='${admin_styles.optionDescription}'>
        Set a memorable shortlink so that the room can be shared easily
      </div>
      <br/>
    `})
  view.renderApp((dom) => {
    h.bindAttributes('nakama', {
      'input': (elem) => {
        input_node = elem
        if (shortlink) {
          input_node.value = shortlink
        }
        input_node.addEventListener('keyup', handleShortlinkValueChanged)
        h.registerEnterPress(input_node, handleConfirmBtnPressed)
      },
      'input-desc': (elem) => {
        input_desc_node = elem
      },
      'confirm-btn': (elem) => {
        confirm_btn_node = elem
        h.registerTapOrClick(confirm_btn_node, handleConfirmBtnPressed)
      },
      'remove-btn': (elem) => {
        remove_btn_node = elem
        h.registerTapOrClick(remove_btn_node, handleRemoveBtnPressed)
      }
    }, dom)
    updateShortlinkChanged()
  })
  const handleShortlinkChanged = (msg) => {
    if (msg.roomid === roomid) {
      shortlink = msg.shortlink
      updateShortlinkChanged()
    }
  }
  const handleShortlinkRemoved = (_roomid) => {
    if (roomid === _roomid) {
      shortlink = null
      input_node.value = ""
      updateShortlinkChanged()
    }
  }
  const dom_subs = [
    dom_pubsub.sub(intra.PLAYER_CHANGED_SHORTLINK, handleShortlinkChanged),
    dom_pubsub.sub(intra.PLAYER_REMOVED_SHORTLINK, handleShortlinkRemoved)
  ]
  return {
    destroy: () => {
      view.removeApp()
      for (let unsub of dom_subs) {
        unsub()
      }
    }
  }
}

const AdminSettingsApp = (mount, dsocket, dom_pubsub, roomid, players_registry, rooms_registry, theme) => {
  const DESCRIPTION_PLACEHOLDER = "Put in a short description of your room is about here"
  const HIDE_SETTINGS_BTN_TEXT = "Hide Settings"
  const ENABLE_PLAYER_LIST_MSG = "Disabling this option will hide the occupants tab and number of occupants in the room"
  const ENABLE_NEVENT_LOG_MSG = "Disabling this option will disable the nevent log feature for this room, as well as blatting the message log"
  const ENABLE_GAMES_MSG = "Disabling this option will prevent games being played in this room, as well as ending any ongoing app"
  const PROTECT_ROOM_MSG = "Rooms are automatically destroyed after a configured interval of unoccupancy. Enable this option to prevent this from happening while you are active"
  const DESTROY_ROOM_MSG = "Permanently deletes room, including the message log and any ongoing app"

  const COPY_DEFAULT_TAB = "The default tab gains focus when a room is first joined"

  const COPY_PLAYER_LIST_DESC_ENABLED = "Occupants tab is enabled"
  const COPY_PLAYER_LIST_DESC_DISABLED = "Occupants tab is disabled"
  const COPY_NEVENT_LOG_DESC_ENABLED = "Messages tab is enabled"
  const COPY_NEVENT_LOG_DESC_DISABLED = "Messages tab is disabled"
  const COPY_GAMES_DESC_ENABLED = "App tab is enabled"
  const COPY_GAMES_DESC_DISABLED = "App tab is disabled"

  const COPY_PROTECT_ROOM_DESC_ENABLED = "Room is protected"
  const COPY_PROTECT_ROOM_DESC_DISABLED = "Room is unprotected"

  //convenience
  const rooms_writer = rooms_registry.writer
  const players_writer = players_registry.writer
  const rooms_reader = rooms_registry.reader
  const players_reader = players_registry.reader

  //init values
  const room = rooms_reader.getRoom(roomid)
  const passphrase_required = room.passphrase_required
  const shortlink = room.shortlink

  let default_tab = room.default_tab
  let nevent_log_enabled = room.nevent_log_enabled
  let player_list_enabled = room.player_list_enabled
  let games_enabled = room.games_enabled
  let room_protected = room.protected
  let description = room.description
  let name = room.name

  const view = h.View(mount,
    'div',
    [['class', admin_styles.adminApp]],
    () => { return `
      <div class='${admin_styles.adminSettingsHeading}'>ADMIN SETTINGS</div>

      <div nakama='passphrase-app'></div>

      <div nakama='shortlink-app'></div>

      <div class='${admin_styles.optionHeader}'>Room Name</div>
      <div class='${admin_styles.optionActionContainer}'>
        <input type="text" class='${admin_styles.roomNameInput}' value="${name}" nakama="name-input">
        <button nakama="name-confirm-btn">Confirm</button>
      </div>
      <br/>

      <div class='${admin_styles.optionHeader}'>Room Description</div>
      <div class='${admin_styles.optionActionContainer}'>
        <textarea class ='${admin_styles.descriptionTextarea}' nakama="description-input" placeholder="${DESCRIPTION_PLACEHOLDER}">${h.escapeHtml(description)}</textarea>
        <button nakama="description-confirm-btn">Confirm</button>
      </div>
      <div class='${admin_styles.optionDescription}'>This is currently only shown to users in settings</div>
      <br/>

      <div class='${admin_styles.optionHeader}'>Default Tab</div>
      <div nakama='default-tab-desc'></div>
      <ul>
        <li>
          <label class='${admin_styles.defaultTabContainer}'>
            <div class='${admin_styles.optionActionContainer}'>
              <span>Occupants</span>
              <input type="radio" nakama='player-list-default-tab'>
            </div>
          </label>
        </li>
          <label class='${admin_styles.defaultTabContainer}'>
            <div class='${admin_styles.optionActionContainer}'>
              <span>Messages</span>
              <input type="radio" nakama='nevent-log-default-tab'>
            </div>
          </label>
        <li>
        </li>
        <li>
          <label class='${admin_styles.defaultTabContainer}'>
            <div class='${admin_styles.optionActionContainer}'>
              <span>App</span>
              <input type="radio" nakama='games-default-tab'>
            </div>
          </label>
        </li>
      </ul>
      <div class='${admin_styles.optionDescription}'>${COPY_DEFAULT_TAB}</div>
      <br/>

      <div class='${admin_styles.optionHeader}'>Occupants</div>
      <div class='${admin_styles.optionActionContainer}'>
        <span nakama='player-list-desc'></span>
        <button nakama='player-list-action-btn'></button>
      </div>
      <div class='${admin_styles.optionDescription}'>${ENABLE_PLAYER_LIST_MSG}</div>
      <br/>

      <div class='${admin_styles.optionHeader}'>Messages</div>
      <div class='${admin_styles.optionActionContainer}'>
        <span nakama='nevent-log-desc'></span>
        <button nakama='nevent-log-action-btn'></button>
      </div>
      <div class='${admin_styles.optionDescription}'>${ENABLE_NEVENT_LOG_MSG}</div>
      <br/>

      <div class='${admin_styles.optionHeader}'>App</div>
      <div class='${admin_styles.optionActionContainer}'>
        <span nakama='games-desc'></span>
        <button nakama='games-action-btn'></button>
      </div>
      <div class='${admin_styles.optionDescription}'>${ENABLE_GAMES_MSG}</div>
      <br/>

      <div class='${admin_styles.optionHeader}'>Protect Room</div>
      <div class='${admin_styles.optionActionContainer}'>
        <span nakama='protect-room-desc'></span>
        <button nakama='protect-room-action-btn'></button>
      </div>
      <div class='${admin_styles.optionDescription}'>${PROTECT_ROOM_MSG}</div>
      <br/>

      <div class='${admin_styles.optionActionContainer}'>
        <span class='${admin_styles.optionHeader}'>Destroy Room</span>
        <button nakama='destroy-room-btn' class='${theme.styles.prebuilt.button_secondary}'>Destroy Room</button>
      </div>
      <div class='${admin_styles.optionDescription}'>${DESTROY_ROOM_MSG}</div>
      <br/>
    `}
  )

  let passphrase_app, shortlink_app

  const nameConfirmBtnPressed = () => {
    const _name = name_input_node.value
    const valid = roomNameValidate(_name)
    if (valid) {
      name = _name
      rooms_writer.changeName(roomid, name)
      dom_pubsub.pub(intra.I_CHANGED_ROOM_NAME, {
        roomid,
        name
      })
      dsocket.emit(inter.I_CHANGED_ROOM_NAME, {
        roomid,
        name
      })
    } else {
    }
  }

  const descriptionConfirmBtnPressed = () => {
    description = description_input_node.value
    rooms_writer.changeDescription(roomid, description)
    dom_pubsub.pub(intra.I_CHANGED_ROOM_DESCRIPTION, {
      roomid,
      description
    })
    dsocket.emit(inter.I_CHANGED_ROOM_DESCRIPTION, {
      roomid,
      description
    })
  }

  const playerListDefaultTabChanged = () => {
    dsocket.emit(inter.I_CHANGED_DEFAULT_TAB, {
      roomid,
      tab_type: ttypes.OCCUPANTS
    })
  }

  const neventLogDefaultTabChanged = () => {
    dsocket.emit(inter.I_CHANGED_DEFAULT_TAB, {
      roomid,
      tab_type: ttypes.MESSAGES
    })
  }

  const gamesDefaultTabChanged = () => {
    dsocket.emit(inter.I_CHANGED_DEFAULT_TAB, {
      roomid,
      tab_type: ttypes.APP
    })
  }
  
  const destroyRoomPressed = (elem) => {
    elem.innerHTML = "Destroying..."
    elem.disabled = true
    dsocket.emit(inter.I_DESTROYED_ROOM, roomid)
  }

  //init
  let name_input_node,
    description_input_node,

    player_list_action_btn_node,
    nevent_log_action_btn_node,
    games_action_btn_node,

    player_list_desc_node,
    nevent_log_desc_node,
    games_desc_node,

    player_list_default_tab_node,
    nevent_log_default_tab_node,
    games_default_tab_node,

    default_tab_desc_node,

    protect_room_action_btn_node,
    protect_room_desc_node

  const playerListActionBtnPressed = () => {
    player_list_action_btn_node.disabled = true
    if (player_list_enabled) {
      player_list_action_btn_node.innerHTML = 'Disabling...'
      dsocket.emit(inter.I_DISABLED_PLAYER_LIST, roomid)
    } else {
      player_list_action_btn_node.innerHTML = 'Enabling...'
      dsocket.emit(inter.I_ENABLED_PLAYER_LIST, roomid)
    }
  }

  const updatePlayerListView = () => {
    player_list_action_btn_node.disabled = false
    if (player_list_enabled) {
      player_list_action_btn_node.innerHTML = 'Disable'
      player_list_action_btn_node.className = theme.styles.prebuilt.button_secondary
      player_list_desc_node.innerHTML = COPY_PLAYER_LIST_DESC_ENABLED
    } else {
      player_list_action_btn_node.innerHTML = 'Enable'
      player_list_action_btn_node.className = ''
      player_list_desc_node.innerHTML = COPY_PLAYER_LIST_DESC_DISABLED
    }
  }

  const neventLogActionBtnPressed = () => {
    nevent_log_action_btn_node.disabled = true
    if (nevent_log_enabled) {
      nevent_log_action_btn_node.innerHTML = 'Disabling...'
      dsocket.emit(inter.I_DISABLED_NEVENT_LOG, roomid)
    } else {
      nevent_log_action_btn_node.innerHTML = 'Enabling...'
      dsocket.emit(inter.I_ENABLED_NEVENT_LOG, roomid)
    }
  }

  const updateNeventLogView = () => {
    nevent_log_action_btn_node.disabled = false
    if (nevent_log_enabled) {
      nevent_log_action_btn_node.innerHTML = 'Disable'
      nevent_log_action_btn_node.className = theme.styles.prebuilt.button_secondary
      nevent_log_desc_node.innerHTML = COPY_NEVENT_LOG_DESC_ENABLED
    } else {
      nevent_log_action_btn_node.innerHTML = 'Enable'
      nevent_log_action_btn_node.className = ''
      nevent_log_desc_node.innerHTML = COPY_NEVENT_LOG_DESC_DISABLED
    }
  }

  const gamesActionBtnPressed = () => {
    games_action_btn_node.disabled = true
    if (games_enabled) {
      games_action_btn_node.innerHTML = 'Disabling...'
      dsocket.emit(inter.I_DISABLED_GAMES, roomid)
    } else {
      games_action_btn_node.innerHTML = 'Enabling...'
      dsocket.emit(inter.I_ENABLED_GAMES, roomid)
    }
  }

  const updateGamesView = () => {
    games_action_btn_node.disabled = false
    if (games_enabled) {
      games_action_btn_node.innerHTML = 'Disable'
      games_action_btn_node.className = theme.styles.prebuilt.button_secondary
      games_desc_node.innerHTML = COPY_GAMES_DESC_ENABLED
    } else {
      games_action_btn_node.innerHTML = 'Enable'
      games_action_btn_node.className = ''
      games_desc_node.innerHTML = COPY_GAMES_DESC_DISABLED
    }
  }

  const updateProtectRoomView = () => {
    protect_room_action_btn_node.disabled = false
    if (room_protected) {
      protect_room_action_btn_node.innerHTML = 'Unprotect'
      protect_room_action_btn_node.className = theme.styles.prebuilt.button_secondary
      protect_room_desc_node.innerHTML = COPY_PROTECT_ROOM_DESC_ENABLED
    } else {
      protect_room_action_btn_node.innerHTML = 'Protect'
      protect_room_action_btn_node.className = ''
      protect_room_desc_node.innerHTML = COPY_PROTECT_ROOM_DESC_DISABLED
    }
  }

  const updateDefaultTabView = () => {
    //depressing
    if (default_tab === ttypes.OCCUPANTS && player_list_enabled) {
      player_list_default_tab_node.checked = true
      nevent_log_default_tab_node.checked = false
      games_default_tab_node.checked = false
    } else if (default_tab === ttypes.MESSAGES && nevent_log_enabled) {
      player_list_default_tab_node.checked = false
      nevent_log_default_tab_node.checked = true
      games_default_tab_node.checked = false
    } else if (default_tab === ttypes.APP && games_enabled) {
      player_list_default_tab_node.checked = false
      nevent_log_default_tab_node.checked = false
      games_default_tab_node.checked = true
    } else {
      player_list_default_tab_node.checked = false
      nevent_log_default_tab_node.checked = false
      games_default_tab_node.checked = false
    }

    //disabling
    if (!player_list_enabled) {
      player_list_default_tab_node.disabled = true
    }
    if (!nevent_log_enabled) {
      nevent_log_default_tab_node.disabled = true
    }
    if (!games_enabled) {
      games_default_tab_node.disabled = true
    }
  }

  const protectRoomActionBtnPressed = () => {
    protect_room_action_btn_node.disabled = true
    if (room_protected) {
      protect_room_action_btn_node.innerHTML = 'Unprotecting...'
      dsocket.emit(inter.I_UNPROTECTED_ROOM, roomid)
    } else {
      protect_room_action_btn_node.innerHTML = 'Protecting...'
      dsocket.emit(inter.I_PROTECTED_ROOM, roomid)
    }
  }

  view.renderApp((dom) => {
    h.bindAttributes('nakama', {
      'passphrase-app': (elem) => {
        passphrase_app = PassphraseApp(elem, dsocket, roomid, passphrase_required, theme)
      },
      'shortlink-app': (elem) => {
        shortlink_app = ShortlinkApp(elem, dsocket, dom_pubsub, roomid, shortlink, theme)
      },
      'leave': (elem) => {
        h.registerTapOrClick(leave_room_btn_node, handleLeaveRoomBtnPressed)
      },
      'name-input': (elem) => {
        name_input_node = elem
      },
      'name-confirm-btn': (elem) => {
        h.registerTapOrClick(elem, nameConfirmBtnPressed)
      },
      'description-input': (elem) => {
        description_input_node = elem
      },
      'description-confirm-btn': (elem) => {
        h.registerTapOrClick(elem, descriptionConfirmBtnPressed)
      },
      'default-tab-desc': (elem) => {
        default_tab_desc_node = elem
        //TODO: do something about this
      },
      'player-list-default-tab': (elem) => {
        player_list_default_tab_node = elem
        player_list_default_tab_node.addEventListener('change', playerListDefaultTabChanged)
        //player_list_default_tab_node.checked = ttypes.OCCUPANTS === default_tab
        //player_list_default_tab_node.disabled = !player_list_enabled
      },
      'player-list-action-btn': (elem) => {
        player_list_action_btn_node = elem
        h.registerTapOrClick(elem, playerListActionBtnPressed)
      },
      'player-list-desc': (elem) => {
        player_list_desc_node = elem
      },
      'nevent-log-default-tab': (elem) => {
        nevent_log_default_tab_node = elem
        nevent_log_default_tab_node.addEventListener('change', neventLogDefaultTabChanged)
        //nevent_log_default_tab_node.checked = ttypes.MESSAGES === default_tab
        //nevent_log_default_tab_node.disabled = !nevent_log_enabled
      },
      'nevent-log-action-btn': (elem) => {
        nevent_log_action_btn_node = elem
        h.registerTapOrClick(elem, neventLogActionBtnPressed)
      },
      'nevent-log-desc': (elem) => {
        nevent_log_desc_node = elem
      },
      'games-default-tab': (elem) => {
        games_default_tab_node = elem
        games_default_tab_node.addEventListener('change', gamesDefaultTabChanged)
        //games_default_tab_node.checked = ttypes.APP === default_tab
        //games_default_tab_node.disabled = !games_enabled
      },
      'games-action-btn': (elem) => {
        games_action_btn_node = elem
        h.registerTapOrClick(elem, gamesActionBtnPressed)
      },
      'games-desc': (elem) => {
        games_desc_node = elem
      },
      'protect-room-action-btn': (elem) => {
        protect_room_action_btn_node = elem
        h.registerTapOrClick(elem, protectRoomActionBtnPressed)
      },
      'protect-room-desc': (elem) => {
        protect_room_desc_node = elem
      },
      'destroy-room-btn': (elem) => {
        h.registerTapOrClick(elem, () => {
          destroyRoomPressed(elem)
        })
      }
    }, dom)
    updatePlayerListView()
    updateNeventLogView()
    updateGamesView()
    updateProtectRoomView()
    updateDefaultTabView()
  })

  const handlePlayerEnabledPlayerList = (msg) => {
    if (msg.roomid === roomid) {
      player_list_enabled = true
      player_list_default_tab_node.disabled = false
      updatePlayerListView()
      updateDefaultTabView()
    }
  }

  const handlePlayerDisabledPlayerList = (msg) => {
    if (msg.roomid === roomid) {
      player_list_enabled = false
      player_list_default_tab_node.disabled = true
      updatePlayerListView()
      updateDefaultTabView()
    }
  }

  const handlePlayerEnabledNeventLog = (msg) => {
    if (msg.roomid === roomid) {
      nevent_log_enabled = true
      nevent_log_default_tab_node.disabled = false
      updateNeventLogView()
      updateDefaultTabView()
    }
  }

  const handlePlayerDisabledNeventLog = (msg) => {
    if (msg.roomid === roomid) {
      nevent_log_enabled = false
      nevent_log_default_tab_node.disabled = true
      updateNeventLogView()
      updateDefaultTabView()
    }
  }

  const handlePlayerEnabledGames = (msg) => {
    if (msg.roomid === roomid) {
      games_enabled = true
      updateGamesView()
      updateDefaultTabView()
    }
  }

  const handlePlayerDisabledGames = (msg) => {
    if (msg.roomid === roomid) {
      games_enabled = false
      updateGamesView()
      updateDefaultTabView()
    }
  }

  const handlePlayerProtectedRoom = (msg) => {
    if (msg.roomid === roomid) {
      room_protected = true
      updateProtectRoomView()
    }
  }

  const handlePlayerUnprotectedRoom = (msg) => {
    if (msg.roomid === roomid) {
      room_protected = false
      updateProtectRoomView()
    }
  }

  const handlePlayerChangedDefaultTab = (msg) => {
    if (msg.roomid === roomid) {
      default_tab = msg.tab_type
      updateDefaultTabView()
    }
  }

  const dom_subs = [
    dom_pubsub.sub(intra.PLAYER_CHANGED_DEFAULT_TAB, handlePlayerChangedDefaultTab),
    dom_pubsub.sub(intra.PLAYER_ENABLED_PLAYER_LIST, handlePlayerEnabledPlayerList),
    dom_pubsub.sub(intra.PLAYER_DISABLED_PLAYER_LIST, handlePlayerDisabledPlayerList),
    dom_pubsub.sub(intra.PLAYER_ENABLED_NEVENT_LOG, handlePlayerEnabledNeventLog),
    dom_pubsub.sub(intra.PLAYER_DISABLED_NEVENT_LOG, handlePlayerDisabledNeventLog),
    dom_pubsub.sub(intra.PLAYER_ENABLED_GAMES, handlePlayerEnabledGames),
    dom_pubsub.sub(intra.PLAYER_DISABLED_GAMES, handlePlayerDisabledGames),
    dom_pubsub.sub(intra.PLAYER_PROTECTED_ROOM, handlePlayerProtectedRoom),
    dom_pubsub.sub(intra.PLAYER_UNPROTECTED_ROOM, handlePlayerUnprotectedRoom)
  ]

  return {
    destroy: () => {
      for (const unsub of dom_subs) {
        unsub()
      }
      passphrase_app.destroy()
      shortlink_app.destroy()
      view.removeApp()
    }
  }
}

module.exports = AdminSettingsApp
