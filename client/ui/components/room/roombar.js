const h = require('../../../../shared/client_helpers.js')
const jss = require('../../../lib/css.js')
const cssc = require('../../css_constants.js')
const intra = require('../../../lib/symbols/intra.js')
const SettingsApp = require('./settings/settings.js')
const Notification = require('../../../lib/services/notifications.js')

const TabTypes = {
  PLAYERS: 'player',
  NEVENTS: 'nevents',
  GAMES: 'games'
}

const generateStyles = (theme) => {
  return jss({
    tab: {
      cursor: 'pointer',
      fontWeight: 'lighter'
    },
    tabSelected: {
      textDecoration: 'underline'
    },
    circle: {
      display: 'inline-block',
      borderRadius: '50%',
      width: '10px',
      height: '10px',
      position: 'absolute',
      marginTop: '-20px',
      marginLeft: '80px',
      background: theme.colours.primary
    },
    noNotification: {
      display: 'none'
    },
    roomtab: {
      height: cssc.measurements.roomtab_height_pixels,
      display: 'flex',
      justifyContent: 'space-around'
    },
    //roombar
    roombar: {
      display: 'flex',
      justifyContent: 'space-between',
      height: cssc.measurements.roombar_height_pixels
    },
    settingsMenuIcon: {
      cursor: 'pointer',
      width: '17px'
    },
    roomName: {
      fontWeight: 'bold'
    }
  })
}

const TabApp = (mount, dom_pubsub, roomid, text, tab_type, styles) => {
  const view = h.View(
    mount,
    'span',
    [],
    () => { return `
      <span nakama='tab' class='${styles.tab}'>${text}</span>
      <div nakama='notification' class='${styles.circle} ${styles.noNotification}'></div>
    `})

  let tab_node, notification_node

  const handleFocusSwitch = (_tab_type) => {
    if (tab_type === _tab_type) {
      tab_node.classList.add(styles.tabSelected)
    } else {
      tab_node.classList.remove(styles.tabSelected)
    }
  }

  //init
  view.renderApp((dom) => {
    h.bindAttributes('nakama', {
      'notification': (elem) => {
        notification_node = elem
      },
      'tab': (elem) => {
        tab_node = elem
        h.registerTapOrClick(tab_node, () => {
          switch (tab_type) {
            case TabTypes.PLAYERS:
              dom_pubsub.pub(intra.I_PRESSED_ON_PLAYERS_TAB, roomid)
              break
            case TabTypes.NEVENTS:
              dom_pubsub.pub(intra.I_PRESSED_ON_NEVENT_LOG_TAB, roomid)
              break
            case TabTypes.GAMES:
              dom_pubsub.pub(intra.I_PRESSED_ON_GAMES_TAB, roomid)
              break
            default:
              break
          }
        })
      }
    }, dom)
  })
  const subs = [
    dom_pubsub.sub(intra.I_PRESSED_ON_NEVENT_LOG_TAB, (_roomid) => {
      if (roomid === _roomid) {
        handleFocusSwitch(TabTypes.NEVENTS)
      }
    }),
    dom_pubsub.sub(intra.I_PRESSED_ON_PLAYERS_TAB, (_roomid) => {
      if (roomid === _roomid) {
        handleFocusSwitch(TabTypes.PLAYERS)
      }
    }),
    dom_pubsub.sub(intra.I_PRESSED_ON_GAMES_TAB, (_roomid) => {
      if (roomid === _roomid) {
        handleFocusSwitch(TabTypes.GAMES)
      }
    })
  ]

  if (tab_type === TabTypes.NEVENTS) {
    subs.push(Notification.registerNeventTab(roomid,
      () => {
        notification_node.classList.remove(styles.noNotification)
      },
      () => {
        notification_node.classList.add(styles.noNotification)
      }))
  }

  return {
    destroy: () => {
      for (let unsub of subs) {
        unsub()
      }
      view.removeApp()
    }
  }
}

const RoomTabBarApp = (mount, dsocket, dom_pubsub, roomid, players_registry, rooms_registry, styles) => {
  const room = rooms_registry.reader.getRoom(roomid)

  //initial values
  var player_list_enabled = room.player_list_enabled
  var nevent_log_enabled = room.nevent_log_enabled
  var games_enabled = room.games_enabled

  var players_tab_app,
    nevent_log_tab_app,
    games_tab_app

  //init
  if (player_list_enabled) {
    players_tab_app = TabApp(mount, dom_pubsub, roomid, "OCCUPANTS", TabTypes.PLAYERS, styles)
  }
  if (nevent_log_enabled) {
    nevent_log_tab_app = TabApp(mount, dom_pubsub, roomid, "MESSAGES", TabTypes.NEVENTS, styles)
  }
  if (games_enabled) {
    games_tab_app = TabApp(mount, dom_pubsub, roomid, "APP", TabTypes.GAMES, styles)
  }

  const handlePlayerEnabledNeventLog = (msg) => {
    if (msg.roomid === roomid) {
      if (!nevent_log_enabled) {
        nevent_log_enabled = true
        nevent_log_tab_app = TabApp(mount, dom_pubsub, roomid, "Nevent Log", TabTypes.NEVENTS, styles)
      }
    }
  }
  const handlePlayerEnabledGames = (msg) => {
    if (msg.roomid === roomid) {
      if (!games_enabled) {
        games_enabled = true
        games_tab_app = TabApp(mount, dom_pubsub, roomid, "Games", TabTypes.GAMES, styles)
      }
    }
  }
  const handlePlayerEnabledPlayerList = (msg) => {
    if (msg.roomid === roomid) {
      if (!player_list_enabled) {
        player_list_enabled = true
        players_tab_app = TabApp(mount, dom_pubsub, roomid, "Players", TabTypes.PLAYERS, styles)

      }
    }
  }
  const handlePlayerDisabledNeventLog = (msg) => {
    if (msg.roomid === roomid) {
      if (nevent_log_enabled) {
        nevent_log_enabled = false
        nevent_log_tab_app.destroy()
        nevent_log_tab_app = null
      }
    }
  }
  const handlePlayerDisabledGames = (msg) => {
    if (msg.roomid === roomid) {
      if (games_enabled) {
        games_enabled = false
        games_tab_app.destroy()
        games_tab_app = null
      }
    }
  }
  const handlePlayerDisabledPlayerList = (msg) => {
    if (msg.roomid === roomid) {
      if (player_list_enabled) {
        player_list_enabled = false
        players_tab_app.destroy()
        players_tab_app = null
      }
    }
  }

  const dom_subs = [
    dom_pubsub.sub(intra.PLAYER_ENABLED_PLAYER_LIST, handlePlayerEnabledPlayerList),
    dom_pubsub.sub(intra.PLAYER_DISABLED_PLAYER_LIST, handlePlayerDisabledPlayerList),
    dom_pubsub.sub(intra.PLAYER_ENABLED_NEVENT_LOG, handlePlayerEnabledNeventLog),
    dom_pubsub.sub(intra.PLAYER_DISABLED_NEVENT_LOG, handlePlayerDisabledNeventLog),
    dom_pubsub.sub(intra.PLAYER_ENABLED_GAMES, handlePlayerEnabledGames),
    dom_pubsub.sub(intra.PLAYER_DISABLED_GAMES, handlePlayerDisabledGames)
  ]
  return {
    show: () => {
      mount.style.display = 'flex';
    },
    hide: () => {
      mount.style.display = 'none';
    },
    destroy: () => {
      for (let unsub of dom_subs) {
        unsub()
      }
      if (player_list_enabled) {
        players_tab_app.destroy()
      }
      if (nevent_log_enabled) {
        nevent_log_tab_app.destroy()
      }
      if (games_enabled) {
        games_tab_app.destroy()
      }
    }
  }
}

const sidepaneTemplate = (styles, on_mobile) => {
  if (on_mobile) {
    //burger icon
    //return `<span nakama='rooms-sidepane'>&#9776;</span>`

    //back icon
    return `<span nakama='rooms-sidepane'>
        &larr;
      </span>`
  } else {
    //For the space-between to work
    return '<span></span>'
  }
}

const RoomBarApp = (mount, dsocket, dom_pubsub, roomid, _name, _description, players_registry, rooms_registry, theme) => {
  const players_reader = players_registry.reader
  const on_mobile = players_reader.getSelfOnMobile()
  var name = _name
  const styles = generateStyles(theme)

  var settings_shown = false
  const ROOM_SETTINGS_MENU_ICON = "&#8942;"
  const ROOM_SETTINGS_CLOSE_ICON = "&times;"

  var name_node,
    rooms_sidepane_node,
    settings_icon_node

  let settings_app,
    roomtabs_app
  const child_apps = h.ChildApps()

  const view = h.View(
    mount,
    'header',
    [],
    (on_mobile) => { return `
      <div class='${styles.roombar}'>
        ${sidepaneTemplate(styles, on_mobile)}
        <span nakama="name" class='${styles.roomName}'>
        </span>
        <span
          class="${styles.settingsMenuIcon}"
          nakama="settings-menu-icon">
            ${ROOM_SETTINGS_MENU_ICON}
        </span>
      </div>
      <div nakama="room-settings-app-container">
      </div>
      <div class='${styles.roomtab}' nakama="room-tab"></div>
  `}, on_mobile)

  const handleRoomNameChanged = (msg) => {
    if (roomid === msg.roomid) {
      name = msg.name
      updateName()
    }
  }
  const dom_subs = [
    dom_pubsub.sub(intra.I_CHANGED_ROOM_NAME, handleRoomNameChanged),
    dom_pubsub.sub(intra.PLAYER_CHANGED_ROOM_NAME, handleRoomNameChanged)
  ]
  const updateName = () => {
    name_node.innerHTML = h.escapeHtml(name)
  }
  const handleRoomsMenuPressed = () => {
    dom_pubsub.pub(intra.ROOMBAR_MENU_PRESSED)
  }
  const handleRoomSettingsPressed = () => {
    if (settings_shown) {
      settings_icon_node.innerHTML = ROOM_SETTINGS_MENU_ICON
      settings_shown = false
    } else {
      settings_icon_node.innerHTML = ROOM_SETTINGS_CLOSE_ICON
      settings_shown = true
    }
    dom_pubsub.pub(intra.ROOM_SETTINGS_PRESSED, roomid)
  }

  //init
  view.renderApp((dom) => {
    const bindTypes = {
      'name': (elem) => {
        name_node = elem
      },
      'settings-menu-icon': (elem) => {
        settings_icon_node = elem
        h.registerTapOrClick(settings_icon_node, handleRoomSettingsPressed)
      },
      'room-settings-app-container': (elem) => {
        settings_app = SettingsApp(elem, dsocket, dom_pubsub, roomid, players_registry, rooms_registry, _description, theme)
      },
      'rooms-sidepane': (elem) => {
        rooms_sidepane_node = elem
        h.registerTapOrClick(rooms_sidepane_node, handleRoomsMenuPressed)
      },
      'room-tab': (elem) => {
        roomtabs_app = RoomTabBarApp(elem, dsocket, dom_pubsub, roomid, players_registry, rooms_registry, styles)
        child_apps.add(roomtabs_app)
      }
    }
    h.bindAttributes('nakama', bindTypes, dom)
  })
  updateName()
  return {
    getSettingsApp: () => {
      return settings_app
    },
    getRoomsTabApp: () => {
      return roomtabs_app
    },
    destroy: () => {
      for (let unsub of dom_subs) {
        unsub()
      }
      settings_app.destroy()
      child_apps.destroyAll()
      view.removeApp()
    }
  }
  }

  module.exports = RoomBarApp
