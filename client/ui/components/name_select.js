const h = require('../../../shared/client_helpers.js')
const hs = require('../../../shared/helpers.js')
const intra = require('../../lib/symbols/intra.js')
const inter = require('../../../shared/socket_events.js')
const jss = require('../../lib/css.js')

const checkHandleTaken = (handle, players_reader) => {
  return players_reader.isHandleTaken(handle)
}

const styles = jss({
  name: {
    fontStyle: 'italic'
  }
})

const NameSelectApp = (mount, dsocket, dom_pubsub, players_registry, theme, prelude = 'You are currently known as') => {
  //private
  const view = h.View(
    mount,
    'div',
    [],
    () => { return `
      <span nakama="welcome-msg">
        ${prelude} <span class='${styles.name}' nakama="handle"></span>.
      </span>
      <span nakama="handle-edit">
        <input nakama="handle-input"></input>
        <button nakama="handle-confirm">Confirm</button>
      </span>
      <br/>
      <div nakama="feedback-msg"></div>
      <button nakama="edit-cancel-btn">Edit</button>
      <span>or</span>
      <button nakama="handle-random"></button>
    `}
  )

  //convenience
  const players_reader = players_registry.reader
  const players_writer = players_registry.writer

  //init
  const handle = players_reader.getSelfHandle()

  var handle_node,
    handle_random_node
  const RANDOM_HANDLE_TEXT = "Random Handle"
  const RANDOM_HANDLE_TEXT_GENERATING = "Generating..."

  //init
  view.renderApp((dom) => {
    var handle_input_node,
      handle_confirm_node,
      handle_edit_node,
      edit_cancel_btn_node,
      welcome_msg_node,
      feedback_msg_node
    var editing = false
    const VALIDATION_MSG = "Handle must be under 18 characters and only contain A-Z, a-z, 0-9, - and _ characters"
    const HANDLE_TAKEN_MSG = "This handle is already taken by another user. Sorry!"
    const CURRENT_HANDLE_MSG = "You already have that handle!"

    const transformToCancelBtn = () => {
      handle_edit_node.style.display = "inline-block"
      welcome_msg_node.style.display = "none"
      edit_cancel_btn_node.innerHTML = "Cancel"
      edit_cancel_btn_node.className = theme.styles.prebuilt.button_ghost
    }
    const transformToEditBtn = () => {
      handle_edit_node.style.display = "none"
      welcome_msg_node.style.display = "inline-block"
      edit_cancel_btn_node.innerHTML = "Edit"
      edit_cancel_btn_node.className = ''
    }

    const toggleEditDom = () => {
      if (editing) {
        transformToEditBtn()
        editing = false
      } else {
        transformToCancelBtn()
        handle_input_node.value = ""
        handle_input_node.focus()
        editing = true
      }
    }

    const editOrCancelBtnPressed = () => {
      feedback_msg_node.style.display = "none"
      toggleEditDom()
    }

    const randomBtnPressed = () => {
      handle_random_node.innerHTML = RANDOM_HANDLE_TEXT_GENERATING
      handle_random_node.disabled = true
      feedback_msg_node.style.display = "none"
      let userid = players_reader.getSelfId()
      let handle = players_reader.getSelfHandle()
      dsocket.emit(inter.I_REQUESTED_NEW_HANDLE, {
        userid,
        handle
      })
      if (editing) {
        toggleEditDom()
      }
    }

    const confirmBtnPressed = () => {
      const handle = handle_input_node.value
      const valid = hs.handleValidate(handle)
      if (valid) {
        const taken = checkHandleTaken(handle, players_reader)
        if (!taken) {
          handle_node.innerHTML = handle
          let old_handle = players_reader.getSelfHandle()
          let userid = players_reader.getSelfId()
          let msg = {
            userid,
            handle,
            old_handle
          }
          players_writer.updateHandle(userid, old_handle, handle)
          players_writer.setSelfHandle(handle)
          dom_pubsub.pub(intra.I_CHANGED_HANDLE, msg)
          handle_input_node.value = ""
          transformToEditBtn()
          editing = false
          dsocket.emit(inter.I_CHANGED_HANDLE, msg)
        } else {
          if (handle === players_reader.getSelfHandle()) {
            feedback_msg_node.innerHTML = CURRENT_HANDLE_MSG
          } else {
            feedback_msg_node.innerHTML = HANDLE_TAKEN_MSG
          }
          feedback_msg_node.style.display = "block"
        }
      } else {
        feedback_msg_node.innerHTML = VALIDATION_MSG
        feedback_msg_node.style.display = "block"
      }
    }

    const bindTypes = {
      "handle-input": (elem) => {
        handle_input_node = elem
        h.registerEnterPress(handle_input_node, confirmBtnPressed)
      },
      "handle-edit": (elem) => {
        handle_edit_node = elem
        handle_edit_node.style.display = "none"
      },
      "handle": (elem) => {
        handle_node = elem
        handle_node.innerHTML = h.escapeHtml(handle)
      },
      "handle-confirm": (elem) => {
        handle_confirm_node = elem
        h.registerTapOrClick(handle_confirm_node, confirmBtnPressed)
      },
      "edit-cancel-btn": (elem) => {
        edit_cancel_btn_node = elem
        h.registerTapOrClick(edit_cancel_btn_node, editOrCancelBtnPressed)
      },
      "handle-random": (elem) => {
        handle_random_node = elem
        handle_random_node.innerHTML = RANDOM_HANDLE_TEXT
        h.registerTapOrClick(handle_random_node, randomBtnPressed)
      },
      "feedback-msg": (elem) => {
        feedback_msg_node = elem
        feedback_msg_node.style.display = "none"
      },
      "welcome-msg": (elem) => {
        welcome_msg_node = elem
      }
    }
    h.bindAttributes('nakama', bindTypes, dom)
  })

  const handleSyncedSelfData = (msg) => {
    handle_node.innerHTML = h.escapeHtml(msg.handle)
  }
  const handlePlayerChangedHandle = (msg) => {
    if (msg.userid === players_reader.getSelfId()) {
      handle_random_node.disabled = false
      handle_random_node.innerHTML = RANDOM_HANDLE_TEXT
      handle_node.innerHTML = h.escapeHtml(msg.handle)
    }
  }

  //subs
  const dom_subs = [
    dom_pubsub.sub(intra.SYNCED_SELF_DATA, handleSyncedSelfData),
    dom_pubsub.sub(intra.PLAYER_CHANGED_HANDLE, handlePlayerChangedHandle)
  ]

  return {
    destroy: () => {
      for (let unsub of dom_subs) {
        unsub()
      }
      view.removeApp()
    }
  }
}

module.exports = NameSelectApp
