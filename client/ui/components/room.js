const h = require('../../../shared/client_helpers.js')
const jss = require('../../lib/css.js')
const intra = require('../../lib/symbols/intra.js')
const PlayersApp = require('./players.js')
const NeventLogApp = require('./nevent_log.js')
const GameWrapperApp = require('./game_wrapper.js')
const RoomBarApp = require('./room/roombar.js')
const RoomFocuser = require('../../lib/services/focus.js').RoomFocuser
const Notification = require('../../lib/services/notifications.js')

const styles = jss({
  roomApp: {
    height: '100%',
    width: '100%'
  },
  roomContainer: {
    height: '100%',
    width: '100%'
  }
})

const RoomApp = (mount, roomid, dsocket, dom_pubsub, players_registry, rooms_registry, games_registry, config, theme) => {
  const view = h.View(
    mount,
    'div',
    [['class', styles.roomApp]],
    () => { return `
      <div nakama="room-bar-app"></div>
      <div class='${styles.roomContainer}' nakama="inroom-apps">
        <div nakama='no-tabs-node'>
          Yikes, there doesn't seem to be anything to do here.
        </div>
      </div>
    `}
  )

  const room = rooms_registry.reader.getRoom(roomid)

  const description = room.description
  const name = room.name
  const default_tab = room.default_tab

  //convenience
  const players_reader = players_registry.reader
  const rooms_reader = rooms_registry.reader

  var nevent_log_enabled = room.nevent_log_enabled
  var player_list_enabled = room.player_list_enabled
  var games_enabled = room.games_enabled

  var room_bar_app,
    players_app,
    nevent_log_app,
    game_wrapper_app

  var inroom_apps_node,
    no_tabs_node

  //convenience creation methods
  const createGamesWrapperApp = () => {
    return GameWrapperApp(inroom_apps_node, dsocket, dom_pubsub, roomid, players_registry, rooms_registry, games_registry)
  }
  const createNeventLogApp = () => {
    return NeventLogApp(inroom_apps_node, dsocket, dom_pubsub, roomid, players_reader, rooms_reader, config)
  }
  const createPlayersApp = () => {
    return PlayersApp(inroom_apps_node, dsocket, dom_pubsub, roomid, players_registry, rooms_registry, theme)
  }
  const updateNoTabsVisibility = () => {
    if (!nevent_log_enabled && !player_list_enabled && !games_enabled) {
      no_tabs_node.style.display = 'block'
    } else {
      no_tabs_node.style.display = 'none'
    }
  }

  const handlePlayerEnabledPlayerList = (msg) => {
    if (roomid === msg.roomid) {
      if (!player_list_enabled) {
        players_app = createPlayersApp()
        player_list_enabled = true
        updateNoTabsVisibility()
      }
    }
  }
  const handlePlayerDisabledPlayerList = (msg) => {
    if (roomid === msg.roomid) {
      if (player_list_enabled) {
        players_app.destroy()
        players_app = null
        player_list_enabled = false
        updateNoTabsVisibility()
      }
    }
  }
  const handlePlayerEnabledNeventLog = (msg) => {
    if (roomid === msg.roomid) {
      if (!nevent_log_enabled) {
        nevent_log_app = createNeventLogApp()
        nevent_log_enabled = true
        updateNoTabsVisibility()
      }
    }
  }
  const handlePlayerDisabledNeventLog = (msg) => {
    if (roomid === msg.roomid) {
      if (nevent_log_enabled) {
        nevent_log_app.destroy()
        nevent_log_app = null
        nevent_log_enabled = false
        updateNoTabsVisibility()
      }
    }
  }
  const handlePlayerEnabledGames = (msg) => {
    if (roomid === msg.roomid) {
      if (!games_enabled) {
        game_wrapper_app = createGamesWrapperApp()
        games_enabled = true
        updateNoTabsVisibility()
      }
    }
  }
  const handlePlayerDisabledGames = (msg) => {
    if (roomid === msg.roomid) {
      if (games_enabled) {
        game_wrapper_app.destroy()
        game_wrapper_app = null
        games_enabled = false
        updateNoTabsVisibility()
      }
    }
  }

  const dom_subs = [
    dom_pubsub.sub(intra.PLAYER_ENABLED_PLAYER_LIST, handlePlayerEnabledPlayerList),
    dom_pubsub.sub(intra.PLAYER_DISABLED_PLAYER_LIST, handlePlayerDisabledPlayerList),
    dom_pubsub.sub(intra.PLAYER_ENABLED_NEVENT_LOG, handlePlayerEnabledNeventLog),
    dom_pubsub.sub(intra.PLAYER_DISABLED_NEVENT_LOG, handlePlayerDisabledNeventLog),
    dom_pubsub.sub(intra.PLAYER_ENABLED_GAMES, handlePlayerEnabledGames),
    dom_pubsub.sub(intra.PLAYER_DISABLED_GAMES, handlePlayerDisabledGames)
  ]

  //init
  view.renderApp((dom) => {
    const bindTypes =  {
      "room-bar-app": (elem) => {
        room_bar_app = RoomBarApp(elem, dsocket, dom_pubsub, roomid, name, description, players_registry, rooms_registry, theme)
      },
      'no-tabs-node': (elem) => {
        no_tabs_node = elem
      },
      "inroom-apps": (elem) => {
        inroom_apps_node = elem
        if (player_list_enabled) {
          players_app = createPlayersApp()
        }
        if (nevent_log_enabled) {
          nevent_log_app = createNeventLogApp()
        }
        if (games_enabled) {
          game_wrapper_app = createGamesWrapperApp()
        }
      }
    }
    h.bindAttributes('nakama', bindTypes, dom)
    updateNoTabsVisibility()
  })

  //focuser init (after tabs have been created)
  const room_focuser = RoomFocuser(
    dsocket,
    default_tab,
    dom_pubsub,
    roomid,
    room_bar_app.getSettingsApp(),
    room_bar_app.getRoomsTabApp(),
    () => { return players_app },
    () => { return nevent_log_app },
    () => { return game_wrapper_app },
    inroom_apps_node
  )
  Notification.registerRoomFocuser(roomid, room_focuser)
  return {
    hide: () => {
      view.hide()
    },
    show: () => {
      view.show()
    },
    destroy: () => {
      room_focuser.destroy()
      if (nevent_log_enabled) {
        nevent_log_app.destroy()
      }
      if (player_list_enabled) {
        players_app.destroy()
      }
      if (games_enabled) {
        game_wrapper_app.destroy()
      }
      for (let unsub of dom_subs) {
        unsub()
      }
      Notification.deregisterRoomFocuser(room_focuser)
      view.removeApp()
    }
  }
}

module.exports = RoomApp
