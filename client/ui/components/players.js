const h = require('../../../shared/client_helpers.js')
const jss = require('../../lib/css.js')
const intra = require('../../lib/symbols/intra.js')
const inter = require('../../../shared/socket_events.js')

const styles = jss({
  playersApp: {
    marginLeft: '5px',
    marginRight: '5px'
  },
  inactive: {
    color: 'gray'
  }
})

const PlayerItem = (parent, id, roomid, dom_pubsub, dsocket, players_reader, rooms_registry, theme) => {
  //convenience
  const rooms_reader = rooms_registry.reader
  const rooms_writer = rooms_registry.writer

  var self_node
  const is_self = id === players_reader.getSelfId()
  var handle = players_reader.getHandleFromId(id)
  var is_admin = rooms_reader.isPlayerAdminOf(roomid, id)
  var i_am_admin = rooms_reader.isPlayerAdminOf(roomid, players_reader.getSelfId())

  const self_indicator_node = h.createNode('span', [], ' (you)')
  const handle_node = h.createNode('span', [], '')
  const kick_node = h.createNode('button', [['class', theme.styles.prebuilt.button_secondary]], 'Kick')
  const admin_indicator_node = h.createNode('span', [['title', 'admin'], ['class', 'admin']], '&#9733;')

  const template = () => {
    return ['li',
      [],
      ''
    ]
  }

  const updateAdminUI = () => {
    if (i_am_admin && !is_self) {
      self_node.appendChild(kick_node)
    }
    if (is_admin) {
      self_node.appendChild(admin_indicator_node)
    }
  }

  const handlePlayerHandleChanged = (msg) => {
    if (id === msg.userid) {
      handle = msg.handle
      handle_node.innerHTML = h.escapeHtml(handle)
    }
  }
  const handlePlayerActive = (userid) => {
    if (id === userid) {
      handle_node.classList.remove(styles.inactive)
    }
  }
  const handlePlayerInactive = (userid) => {
    if (id === userid) {
      handle_node.classList.add(styles.inactive)
    }
  }
  const handleKickBtnPressed = () => {
    const msg = {
      playerid: id,
      kickerid: players_reader.getSelfId(),
      roomid
    }
    rooms_writer.playerLeftRoom(id, roomid)
    dom_pubsub.pub(intra.I_KICKED_PLAYER, msg)
    dsocket.emit(inter.I_KICKED_PLAYER, msg)
  }
  const handlePlayerAssignedAsAdmin = (msg) => {
    if (roomid === msg.roomid && id === msg.userid) {
      is_admin = true
      i_am_admin = id === players_reader.getSelfId()
      updateAdminUI()
    }
  }

  //dom registers
  h.registerTapOrClick(kick_node, handleKickBtnPressed)

  //subs
  const dom_subs = [
    dom_pubsub.sub(intra.I_CHANGED_HANDLE, handlePlayerHandleChanged),
    dom_pubsub.sub(intra.PLAYER_CHANGED_HANDLE, handlePlayerHandleChanged),
    dom_pubsub.sub(intra.PLAYER_ACTIVE, handlePlayerActive),
    dom_pubsub.sub(intra.PLAYER_INACTIVE, handlePlayerInactive),
    dom_pubsub.sub(intra.PLAYER_ASSIGNED_AS_ADMIN, handlePlayerAssignedAsAdmin)
  ]

  return {
    remove: () => {
      for (let unsub of dom_subs) {
        unsub()
      }
      parent.removeChild(self_node)
    },
    render: () => {
      //init
      self_node = h.createNode(...template())
      //always append handle:
      handle_node.innerHTML = h.escapeHtml(handle)
      self_node.appendChild(handle_node)
      //check activity on handle:
      const active = players_reader.getPlayer(id).active
      if (!active) {
        handle_node.classList.add(styles.inactive)
      }
      //check if it's you:
      if (is_self) {
        self_node.appendChild(self_indicator_node)
      }

      updateAdminUI()
      parent.appendChild(self_node)
    },
    //accessors
    getId: () => {
      return id
    }
  }
}

const PlayersListApp = (mount, dsocket, dom_pubsub, roomid, players_registry, rooms_registry, theme) => {
  const renderer = h.ItemRenderer()

  //convenience
  const rooms_reader = rooms_registry.reader
  const players_reader = players_registry.reader

  //dictionary
  var player_items //= {}

  const syncPlayerItems = (rooms) => {
    if (rooms[roomid]) {
      let player_ids = rooms[roomid].players
      player_items = {}
      renderer.renderItems({})
      for (let userid of player_ids) {
        player_items[userid] = PlayerItem(mount, userid, roomid, dom_pubsub, dsocket, players_reader, rooms_registry, theme)
      }
      renderer.renderItems(player_items)
    }
  }

  const initPlayerItems = () => {
    const room = rooms_reader.getRoom(roomid)
    if (room) {
      let player_ids = room.players
      player_items = {}
      for (let userid of player_ids) {
        player_items[userid] = PlayerItem(mount, userid, roomid, dom_pubsub, dsocket, players_reader, rooms_registry, theme)
      }
      renderer.renderItems(player_items)
    }
  }

  const handlePlayerJoinedRoom = (msg) => {
    if (msg.roomid === roomid) {
      let userid = msg.userid
      if (!(userid in player_items)) {
        player_items[userid] = PlayerItem(mount, userid, roomid, dom_pubsub, dsocket, players_reader, rooms_registry, theme)
        renderer.renderItems(player_items)
      }
    }
  }
  const handlePlayerLeftRoom = (msg) => {
    if (msg.roomid === roomid) {
      let userid = msg.userid
      if (userid in player_items) {
        delete player_items[userid]
        renderer.renderItems(player_items)
      }
    }
  }
  const handleIKickedPlayer = (msg) => {
    if (msg.roomid === roomid) {
      let userid = msg.playerid
      if (userid in player_items) {
        delete player_items[userid]
        renderer.renderItems(player_items)
      }
    }
  }

  //subs
  const dom_subs = [
    dom_pubsub.sub(intra.PLAYER_JOINED_ROOM, handlePlayerJoinedRoom),
    dom_pubsub.sub(intra.I_KICKED_PLAYER, handleIKickedPlayer),
    dom_pubsub.sub(intra.PLAYER_LEFT_ROOM, handlePlayerLeftRoom),
    dom_pubsub.sub(intra.PLAYER_WAS_KICKED, handlePlayerLeftRoom),
    dom_pubsub.sub(intra.SYNCED_ROOMS, syncPlayerItems)
  ]

  //init
  initPlayerItems()
  return {
    destroy: () => {
      Object.keys(player_items).forEach((id) => {
        let player_item = player_items[id]
        player_item.remove()
      })
      for (let unsub of dom_subs) {
        unsub()
      }
    }
  }
}


const PlayersApp = (mount, dsocket, dom_pubsub, roomid, players_registry, rooms_registry, theme) => {
  const view = h.View(
    mount,
    'div',
    [['class', styles.playersApp]],
    () => { return `
      <ul nakama="players-list">
      </ul>
    `}
  )
  //child component apps
  var players_list_app

  //init
  view.renderApp((dom) => {
    const bindTypes = {
      'players-list': (elem) => {
        players_list_app = PlayersListApp(elem, dsocket, dom_pubsub, roomid, players_registry, rooms_registry, theme)
      }
    }
    h.bindAttributes('nakama', bindTypes, dom)
  })
  return {
    focus: () => {
      view.getSelfNode().style.display = 'block'
    },
    hide: () => {
      view.getSelfNode().style.display = 'none'
    },
    destroy: () => {
      players_list_app.destroy()
      view.removeApp()
    }
  }
}

module.exports = PlayersApp
