const jss = require('../../lib/css.js')
const cssc = require('../css_constants.js')
const objectMapKeys = require('../../../shared/helpers.js').objectMapKeys
const nev = require('../../../shared/nevent.js')
const renderNevent = require('./nevents/renderer.js')
const hc = require('../../../shared/client_helpers.js')
const inter = require('../../../shared/socket_events.js')
const intra = require('../../lib/symbols/intra.js')
const WindowHeight = require('../../lib/hacks.js').WindowHeight

const AutoCompleteEntry = (parent, command, help) => {
  var self_node = null
  const template = () => {
    return ['li',
      [],
      `<b>${hc.escapeHtml(command)}</b>: ${hc.escapeHtml(help.description)}`
    ]
  }
  return {
    render: () => {
      if (!self_node) {
        self_node = hc.createNode(...template())
      }
      parent.appendChild(self_node)
    },
    remove: () => {
      parent.removeChild(self_node)
    }
  }
}

const AutoCompleteApp = (mount) => {
  //init
  //slash_command → AutoCompleteEntry
  const entries = {}
  const renderer = hc.ItemRenderer()
  mount.style.display = 'none' //start hidden
  return {
    dismissPrompt: () => {
      renderer.renderItems({})
      mount.style.display = 'none'
    },
    updatePrompt: (valid_completions) => {
      if (valid_completions && Object.keys(valid_completions).length !== 0) {
        for (const completion of Object.keys(valid_completions)) {
          if (!(completion in entries)) {
            entries[completion] = AutoCompleteEntry(mount, completion, valid_completions[completion])
          }
        }
        const entries_to_render = objectMapKeys(valid_completions, (entry) => {
          return entries[entry]
        })
        renderer.renderItems(entries_to_render)
        mount.style.display = 'block'
      } else {
        mount.style.display = 'none'
      }
    },
    destroy: () => {
    }
  }
}

const isSlashCommand = (slash_commands, message) => {
  const matchesExactCommand = (cmd) => {
    return (
      (cmd in slash_commands.default_commands)
      ||
      (slash_commands.program_commands && (cmd in slash_commands.program_commands))
    )
  }
  if (message[0] === '/') {
    const cmd = message.slice(1).split(' ')[0]
    if (cmd.length > 0) {
      if (matchesExactCommand(cmd)) {
        return true
      } else {
        return false
      }
    } else {
      return false
    }
  } else {
    return false
  }
}

const getValidCompletions = (slash_commands, cmd) => {
  const valid_completions = {}
  const populateCompletions = (commands) => {
    for (const slash_cmd of Object.keys(commands)) {
      if (slash_cmd.indexOf(cmd) === 0) {
        valid_completions[slash_cmd] = commands[slash_cmd]
      }
    }
  }
  populateCompletions(slash_commands.default_commands)
  if (slash_commands.program_commands) {
    populateCompletions(slash_commands.program_commands)
  }
  return valid_completions
}

const IndicatorView = (dom, useridToHandle) => {
  const users_typing = new Set()

  const template = () => {
    const typists = [...users_typing].map((userid) => {
      return hc.escapeHtml(useridToHandle(userid))
    })
    const num_typing = typists.length
    if (num_typing === 0) {
      return ""
    } else if (num_typing === 1) {
      return `${typists[0]} is typing...`
    } else if (num_typing > 2) {
      return `${typists.join(', ')} are typing...`
    } else {
      return 'Many people are typing...'
    }
  }
  return {
    reset: () => {
      users_typing.clear()
    },
    startedTyping: (userid) => {
      users_typing.add(userid)
      dom.innerHTML = template()
    },
    stoppedTyping: (userid) => {
      users_typing.delete(userid)
      dom.innerHTML = template()
    }
  }
}

var MessagesView = (dom, NEVENT_LOG_LIMIT) => {
  let container_node
  const scrollDown = () => {
    container_node.scrollTop = container_node.scrollHeight
  }
  const renderNevents = (nevents) => {
    let nevents_trimmed
    if (nevents.length <= NEVENT_LOG_LIMIT ) {
      nevents_trimmed = nevents
    } else {
      const difference = nevents.length - NEVENT_LOG_LIMIT
      nevents_trimmed = nevents.slice(difference)
    }
    const nevents_html = nevents_trimmed.map((nevent) => {
      return renderNevent(nevent)
    })
    dom.innerHTML = nevents_html.join('')
    scrollDown()
  }

  const showLoadingIndication = () => {
    dom.appendChild(loading_indicator_node)
  }

  const appendNevent = (nevent) => {
    const nevent_html = renderNevent(nevent)
    dom.appendChild(hc.htmlToNode(nevent_html))
    if (dom.childElementCount > NEVENT_LOG_LIMIT) {
      dom.removeChild(dom.firstChild)
    }
    //TODO: if user has scrolled up, don't cause scrolldown
    if (true) {
      scrollDown()
    }
  }
  //init: show loading indication...
  const loading_indicator_node = hc.createNode(
    'div',
    [['style', 'justify-content: center; align-items: center; display: flex; height: 100%;']],
    'Hang on second, syncing messages...')

  showLoadingIndication()
  return {
    renderNevents,
    appendNevent,
    scrollDown,
    assignContainerNode: (_container_node) => {
      container_node = _container_node
    }
  }
}

const visible_display = 'flex'
const hidden_display = 'none'
const styles = jss({
  neventLogApp: {
    //visible_display
    flexDirection: 'column',
    position: 'relative', //for autocomplete
    width: '100%'
  },
  messagesContainer: {
    overflowY: 'auto',
    marginLeft: '5px',
    marginRight: '5px'
  },
  indicator: {
    display: 'flex',
    paddingLeft: '4px',
    alignItems: 'center',
    height: cssc.measurements.indicator_height_pixels,
    //backgroundColor: 'yellow',
    fontSize: '12px'
  },
  neventInputContainer: {
    boxSizing: 'border-box',
    height: cssc.measurements.input_height_pixels,
    padding: '2px',
    display: 'flex'
  },
  bottomContainer: {
    height: cssc.measurements.bottom_container_pixels,
    width: '100%',
    //backgroundColor: 'seagreen'
    backgroundColor: cssc.colours.background
  },
  neventInput: {
    width: '100%',
    padding: '0px',
    paddingLeft: '10px',
    paddingRight: '10px',
    border: '0px',
    fontSize: '12px'
  },
  messages: {
    margin: '0px',
    height: '100%' //this is for the loading indicator to appear at the center...
  },
  autoCompleteContainer: {
    borderTop: '1px grey solid',
    boxSizing: 'border-box',
    position: 'absolute',
    bottom: cssc.measurements.input_height_pixels,
    width: '100%',
    //backgroundColor: 'plum',
    backgroundColor: cssc.colours.background
  }
})

var NeventLogApp = (mount, dsocket, dom_pubsub, roomid, players_reader, rooms_reader, config) => {
  const NEVENT_LOG_LIMIT = config.nevent_log_limit
  const TYPING_TIMER_LENGTH = config.typing_timer_length_milliseconds
  const on_mobile = players_reader.getSelfOnMobile()
  const selfid = players_reader.getSelfId()
  const view = hc.View(
    mount,
    'div',
    [['class', styles.neventLogApp]],
    (on_mobile) => { return `
      <div class='${styles.messagesContainer}' nakama='messages-container'>
        <ul class='${styles.messages}' nakama='messages'></ul>
      </div>
      <div class='${styles.autoCompleteContainer}'>
        <ul nakama='slash-autocomplete'></ul>
      </div>
      <div class='${styles.bottomContainer}'>
        <div nakama='indicator' class='${styles.indicator}'>
        </div>
        <div class='${styles.neventInputContainer}'>
          <input class='${styles.neventInput}' nakama='message' autocomplete="off" placeholder="Enter message or command here" />
          <!-- <button type="button" nakama='send-message'>Send</button> -->
      </div>
      </div>
    `},
    on_mobile
  )
  //DOM
  let autocomplete_app,
    indicator_view,
    messages_view,
    message_node,
    message_container_node
  //send_message_btn_node
  
  //nonDOM
  var typing = false
  var last_typing_time
  var slash_commands
  var destroyWindowHeightListener

  //internal
  const handleMessageSend = () => {
    const message = message_node.value
    if (isSlashCommand(slash_commands, message)) {
      dsocket.emit(inter.I_SENT_SLASH_COMMAND, {
        roomid,
        message
      })
    } else {
      const im = nev.serialiseImNevent(selfid, players_reader.getSelfHandle(), message)
      messages_view.appendNevent(im)
      dsocket.emit(inter.I_SENT_IM, {
        roomid,
        message
      })
    }
    //clear message
    message_node.value = ''
    typing = false
    dsocket.emit(inter.I_STOPPED_TYPING, {
      userid: selfid,
      roomid
    })
    autocomplete_app.dismissPrompt()
  }

  const triggerAutoCompleteHelp = (cmd) => {
    const valid_completions = getValidCompletions(slash_commands, cmd)
    autocomplete_app.updatePrompt(valid_completions)
  }

  const updateAutoComplete = () => {
    const typed = message_node.value
    if (typed[0] === '/') {
      const cmd = typed.slice(1).split(' ')[0]
      triggerAutoCompleteHelp(cmd)
    } else {
      autocomplete_app.dismissPrompt()
    }
  }

  const handleCheckAutoComplete = (e) => {
    //http://stackoverflow.com/a/1314479/1010076
    const key_code = e.keyCode || e.which; 

    if (key_code === 9) {
      e.preventDefault()
      const message = message_node.value
      if (message.length > 0 && message[0] === '/') {
        const valid_completions = getValidCompletions(slash_commands, message.slice(1))
        const commands = Object.keys(valid_completions)
        if (commands.length === 1) {
          if (!valid_completions[commands[0]].params || valid_completions[commands[0]].params.length === 0) {
            message_node.value = `/${commands[0]}`
          } else {
            message_node.value = `/${commands[0]} `
          }
        }
      }
    } 
  }

  const updateTyping = () => {
    //slash command autocompletion
    updateAutoComplete()
    //typing indicator
    if (!typing) {
      typing = true
      dsocket.emit(inter.I_STARTED_TYPING, {
        userid: selfid,
        roomid
      })
    }
    last_typing_time = (new Date()).getTime()
    setTimeout(() => {
      const typingTimer = (new Date()).getTime()
      const timeDiff = typingTimer - last_typing_time
      if (timeDiff >= TYPING_TIMER_LENGTH && typing) {
        dsocket.emit(inter.I_STOPPED_TYPING, {
          userid: selfid,
          roomid
        })
        typing = false
      }
    }, TYPING_TIMER_LENGTH)
  }


  //handling events
  const handleGameStarted = (msg) => {
    if (msg.roomid === roomid) {
      const {
        userid,
        game_name,
        program_commands
      } = msg
      slash_commands.program_commands = program_commands
      const handle = players_reader.getHandleFromId(userid)
      const nevent = nev.serialiseNotificationNevent(
        nev.notifications.APP_STARTED, {
          userid,
          app_name: game_name,
          handle
        })
      messages_view.appendNevent(nevent)
      updateAutoComplete()
    }
  }
  const handleGameEnded = (msg) => {
    if (msg.roomid === roomid) {
      const game_name = msg.game_name
      slash_commands.program_commands = msg.program_commands
      updateAutoComplete()
      const nevent = nev.serialiseNotificationNevent(
        nev.notifications.APP_ENDED, game_name)
      messages_view.appendNevent(nevent)
    }
  }

  const handleGameErrored = (msg) => {
    if (msg.roomid === roomid) {
      const game_name = msg.game_name
      delete slash_commands.program_commands
      updateAutoComplete()
      const nevent = nev.serialiseNotificationNevent(
        nev.notifications.GAME_ERRORED, game_name)
      messages_view.appendNevent(nevent)
    }
  }
  const handleSyncedNeventLog = (msg) => {
    if (msg.roomid === roomid) {
      slash_commands = msg.slash_commands
      indicator_view.reset()
      autocomplete_app.dismissPrompt()
      messages_view.renderNevents(msg.messages)
    }
  }
  const handlePlayerJoinedRoom = (msg) => {
    if (msg.roomid === roomid) {
      const userid = msg.userid
      let handle = players_reader.getHandleFromId(userid)
      const nevent = nev.serialiseNotificationNevent(nev.notifications.USER_JOINED, {
        userid,
        handle
      })
      messages_view.appendNevent(nevent)
    }
  }
  const handlePlayerLeftRoom = (msg) => {
    if (msg.roomid === roomid) {
      const userid = msg.userid
      let handle = players_reader.getHandleFromId(userid)
      const nevent = nev.serialiseNotificationNevent(nev.notifications.USER_LEFT, {
        userid,
        handle
      })
      messages_view.appendNevent(nevent)
    }
  }
  const handlePlayerStartedTyping = (msg) => {
    if (msg.roomid === roomid) {
      indicator_view.startedTyping(msg.userid)
    }
  }
  const handlePlayerStoppedTyping = (msg) => {
    if (msg.roomid === roomid) {
      indicator_view.stoppedTyping(msg.userid)
    }
  }
  const handlePlayerSentIm = (msg) => {
    if (msg.roomid === roomid) {
      const {
        userid,
        message
      } = msg
      const handle = players_reader.getHandleFromId(userid)
      let nevent = nev.serialiseImNevent(userid, handle, message)
      messages_view.appendNevent(nevent)
    }
  }
  const handlePlayerCausedProgramEvent = (msg) => {
    if (msg.roomid === roomid) {
      let program_nevent = msg.program_nevent
      messages_view.appendNevent(program_nevent)
    }
  }
  const handlePlayerChangedHandle = (msg) => {
    const userid = msg.userid
    if (rooms_reader.isPlayerOccupyingRoom(userid, roomid)) {
      const old_handle = msg.old_handle
      const handle = msg.handle
      const nevent = nev.serialiseNotificationNevent(nev.notifications.PLAYER_CHANGED_HANDLE, {
        userid,
        old_handle,
        handle
      })
      messages_view.appendNevent(nevent)
    }
  }

  const handlePlayerWasKicked = (msg) => {
    if (msg.roomid === roomid) {
      const playerid = msg.userid
      const kickerid = msg.kickerid
      const kicker = players_reader.getHandleFromId(kickerid)
      const kickee = players_reader.getHandleFromId(playerid)
      const nevent = nev.serialiseNotificationNevent(nev.notifications.PLAYER_WAS_KICKED, {
        kickee,
        kickeeid: playerid,
        kicker,
        kickerid
      })
      messages_view.appendNevent(nevent)
    }
  }
  const handleClientReconnected = () => {
    dsocket.emit(inter.SYNC_NEVENT_LOG_REQUESTED, roomid)
  }

  const handlePlayerAssignedAsAdmin = (msg) => {
    if (msg.roomid === roomid) {
      const {
        userid
      } = msg
      const handle = players_reader.getHandleFromId(userid)
      const nevent = nev.serialiseNotificationNevent(nev.notifications.USER_NOMINATED_ADMIN, {
        userid,
        handle
      })
      messages_view.appendNevent(nevent)
    }
  }

  const handleRenderCommandResult = (msg) => {
    const nson_result = msg.nson_result
    const nevent = nev.serialiseNsonNevent(nson_result)
    messages_view.appendNevent(nevent)
  }

  const handleAppEmittedNson = (msg) => {
    const nson = msg.nson
    const nevent = nev.serialiseNsonNevent(nson)
    messages_view.appendNevent(nevent)
  }

  //subs
  const dom_subs = [
    dom_pubsub.sub(intra.PLAYER_CHANGED_HANDLE, handlePlayerChangedHandle),
    dom_pubsub.sub(intra.CLIENT_RECONNECTED, handleClientReconnected),
    dom_pubsub.sub(intra.PLAYER_ASSIGNED_AS_ADMIN, handlePlayerAssignedAsAdmin),
    dom_pubsub.sub(intra.PLAYER_WAS_KICKED, handlePlayerWasKicked),
    dom_pubsub.sub(intra.PLAYER_JOINED_ROOM, handlePlayerJoinedRoom),
    dom_pubsub.sub(intra.PLAYER_LEFT_ROOM, handlePlayerLeftRoom),
    dom_pubsub.sub(intra.GAME_ERRORED, handleGameErrored),
    dom_pubsub.sub(intra.GAME_STARTED, handleGameStarted),
    dom_pubsub.sub(intra.GAME_ENDED, handleGameEnded)
  ]
  const socket_subs = [
    hc.socketSub(dsocket, inter.SLASH_COMMAND_RESULT_READY, handleRenderCommandResult),
    hc.socketSub(dsocket, inter.SYNCED_NEVENT_LOG, handleSyncedNeventLog),
    hc.socketSub(dsocket, inter.PLAYER_STARTED_TYPING, handlePlayerStartedTyping),
    hc.socketSub(dsocket, inter.PLAYER_STOPPED_TYPING, handlePlayerStoppedTyping),
    hc.socketSub(dsocket, inter.PLAYER_SENT_IM, handlePlayerSentIm),
    hc.socketSub(dsocket, inter.APP_EMITTED_NSON, handleAppEmittedNson)
  ]
  //init 
  view.renderApp((dom) => {
    const bindTypes = {
      'messages': (elem) => {
        messages_view = MessagesView(elem, NEVENT_LOG_LIMIT)
      },
      'slash-autocomplete': (elem) => {
        autocomplete_app = AutoCompleteApp(elem)
      },
      'indicator': (elem) => {
        indicator_view = IndicatorView(elem, players_reader.getHandleFromId)
      },
      'message': (elem) => {
        message_node = elem
        message_node.addEventListener('input', updateTyping)
        message_node.addEventListener('keydown', handleCheckAutoComplete)
        hc.registerEnterPress(message_node, handleMessageSend)
      },
      'messages-container': (elem) => {
        message_container_node = elem
        const fudge = 0 //yay!
        const calculateMessagesHeightCss = (window_height) => {
          return (window_height - cssc.measurements.messages_container_offset - fudge) + 'px'
        }
        destroyWindowHeightListener = WindowHeight.onWindowResize((new_height) => {
          elem.style.height = calculateMessagesHeightCss(new_height)
        })
        //init
        elem.style.height = calculateMessagesHeightCss(WindowHeight.getHeight())
      }
      //'send-message': (elem) => {
      //send_message_btn_node = elem
      //hc.registerTapOrClick(elem, handleMessageSend)
      //}
    }
    hc.bindAttributes('nakama', bindTypes, dom)
    messages_view.assignContainerNode(message_container_node)
  })
  dsocket.emit(inter.SYNC_NEVENT_LOG_REQUESTED, roomid)
  let focus_virgin = true
  return {
    focus: () => {
      view.getSelfNode().style.display = visible_display
      //scroll height isn't nonzero until display's been visible
      if (focus_virgin) {
        messages_view.scrollDown()
        focus_virgin = false
      }
    },
    hide: () => {
      view.getSelfNode().style.display = hidden_display
    },
    destroy: () => {
      destroyWindowHeightListener()
      for (let unsub of dom_subs) {
        unsub()
      }
      for (let unsub of socket_subs) {
        unsub()
      }
      view.removeApp()
      autocomplete_app.destroy()
    },
  }
}

module.exports = NeventLogApp
