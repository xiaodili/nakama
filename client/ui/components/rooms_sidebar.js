//Wrapper app for rooms_listing
const h = require('../../../shared/client_helpers.js')
const jss = require('../../lib/css.js')
const intra = require('../../lib/symbols/intra.js')
const RoomsListingApp = require('./rooms_listing/rooms_listing.js')

const styles = jss({
  sidebarContainer: {
    width: '100%',
    boxSizing: 'border-box',
    borderRight: '1px #8d8d8d solid'
  },
  roomListingContainer: {
    marginLeft: '5px',
    marginRight: '5px'
  }
})

//Wrapper around room-listing app. Is this actually needed...?
const RoomsSidebarApp = (mount, dsocket, dom_pubsub, players_registry, rooms_registry, theme) => {

  //convenience
  const players_reader = players_registry.reader
  var rooms_listing_app

  const view = h.View(
    mount,
    'div',
    [['class', styles.sidebarContainer]],
    () => { return `
      <div nakama="rooms-listing-app" class='${styles.roomListingContainer}'></div>
    `})

  const handleCloseSidepanePressed = () => {
    dom_pubsub.pub(intra.HIDE_ROOMS_SIDEPANE)
  }

  //init
  view.renderApp((dom) => {
    var bindTypes = {
      "rooms-listing-app": (elem) => {
        rooms_listing_app = RoomsListingApp(elem, dsocket, dom_pubsub, players_registry, rooms_registry, theme)
      }
    }
    h.bindAttributes('nakama', bindTypes, dom)
  })
  return {
  }
}

module.exports = RoomsSidebarApp
