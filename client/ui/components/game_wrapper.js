const h = require('../../../shared/client_helpers.js')
const PubSub = require('../../../shared/helpers.js').PubSub
const intra = require('../../lib/symbols/intra.js')
const inter = require('../../../shared/socket_events.js')
const Api = require('../../lib/game/api.js')
const jss = require('../../lib/css.js')
const cssc = require('../css_constants.js')
const WindowHeight = require('../../lib/hacks.js').WindowHeight

const styles = jss({
  appTitle: {
    fontWeight: 'bold',
  },
  gamesAppContainer: {
    height: '100%',
    marginLeft: '5px',
    marginRight: '5px'
  },
  gameMount: {
    overflowY: 'auto'
  }
})

// DOM node, roomid, gameid, selfid, {title, description}, pubsub, pubsub, bool
const GameItem = (parent, roomid, gameid, selfid, game, dom_pubsub, games_pubsub, _allow_select) => {
  var self_node
  var play_game_btn_node
  const title = game.title
  const description = game.description
  var allow_select = _allow_select
  const template = () => {
    return [
      'li',
      [],
      `
        <div>
          <span class='${styles.appTitle}'>${title}</span>
          <button nakama='play-game-btn'>Start</button>
        </div>
        <div>${description}</div>
    `]
  }
  const updatePlayBtn = () => {
    if (allow_select) {
      play_game_btn_node.style.display = "inline-block"
    } else {
      play_game_btn_node.style.display = "none"
    }
  }
  const handleIPressedPlayGame = (_gameid) => {
    if (gameid !== _gameid) {
      play_game_btn_node.disabled = true
    }
  }
  const handlePlayerPressedPlayGame = (_gameid) => {
    if (gameid === _gameid) {
      play_game_btn_node.innerHTML = "Loading..."
    } 
    play_game_btn_node.disabled = true
  }

  //occurred before game is loaded
  const handleGameErrored = () => {
    play_game_btn_node.innerHTML = "Play"
    play_game_btn_node.disabled = false
  }

  const handlePlayerAssignedAsAdmin = (msg) => {
    if (roomid === msg.roomid && selfid === msg.userid) {
      allow_select = true
      updatePlayBtn()
    }
  }

  const games_subs = [
    games_pubsub.sub(intra.I_PRESSED_PLAY_GAME, handleIPressedPlayGame),
    games_pubsub.sub(intra.GAME_ERRORED, handleGameErrored),
    games_pubsub.sub(intra.PLAYER_PRESSED_PLAY_GAME, handlePlayerPressedPlayGame)
  ]

  const dom_subs = [
    dom_pubsub.sub(intra.PLAYER_ASSIGNED_AS_ADMIN, handlePlayerAssignedAsAdmin)
  ]

  const handlePlayGameBtnPressed = () => {
    play_game_btn_node.disabled = true
    play_game_btn_node.innerHTML = "Loading..."
    games_pubsub.pub(intra.I_PRESSED_PLAY_GAME, gameid)
  }
  return {
    remove: () => {
      for (let unsub of games_subs) {
        unsub()
      }
      for (let unsub of dom_subs) {
        unsub()
      }
      parent.removeChild(self_node)
    },
    render: () => {
      self_node = h.createNode(...template())
      parent.appendChild(self_node)
      play_game_btn_node = h.qs("[nakama=play-game-btn]", self_node)
      h.registerTapOrClick(play_game_btn_node, handlePlayGameBtnPressed)
      updatePlayBtn()
    },
    //accessors
    getId: () => {
      return gameid
    }
  }
}

const GameCatalogueApp = (mount, dsocket, dom_pubsub, games_pubsub, roomid, games_registry, rooms_reader, players_reader) => {
  const games_renderer = h.ItemRenderer()
  const games_catalogue = games_registry.reader.getCatalogue()
  const game_items = {}
  var allow_select = rooms_reader.isPlayerAdminOf(roomid, players_reader.getSelfId())
  const view = h.View(
    mount,
    'div',
    [],
    () => { return `
      <div nakama='catalogue-description'></div>
      <ul nakama='game-list'>
      </ul>
      `},
    players_reader.getSelfOnMobile()
  )
  var catalogue_description_node

  const CAN_SELECT_MSG = "Choose from one of the games below to play"
  const CANT_SELECT_MSG = "Your game master can choose from one of the games below to play"
  const updateCatalogueDescription = () => {
    if (allow_select) {
      catalogue_description_node.innerHTML = CAN_SELECT_MSG
    } else {
      catalogue_description_node.innerHTML = CANT_SELECT_MSG
    }
  }

  const handlePlayerRequestedGameStart = (msg) => {
    if (roomid === msg.roomid) {
      let gameid = msg.gameid
      games_pubsub.pub(intra.PLAYER_PRESSED_PLAY_GAME, gameid)
    }
  }
  const handlePlayerAssignedAsAdmin = (msg) => {
    if (roomid === msg.roomid) {
      if (players_reader.getSelfId() === msg.userid) {
        allow_select = true
        updateCatalogueDescription()
      }
    }
  }

  const socket_subs = [
    h.socketSub(dsocket, inter.PLAYER_REQUESTED_GAME_START, handlePlayerRequestedGameStart)
  ]
  const dom_subs = [
    dom_pubsub.sub(intra.PLAYER_ASSIGNED_AS_ADMIN, handlePlayerAssignedAsAdmin)
  ]

  view.renderApp((dom) => {

    const bindTypes = {
      'game-list': (elem) => {
        for (let gameid of Object.keys(games_catalogue)) {
          game_items[gameid] = GameItem(elem, roomid, gameid, players_reader.getSelfId(), games_catalogue[gameid], dom_pubsub, games_pubsub, allow_select)
        }
        games_renderer.renderItems(game_items)
      },
      'catalogue-description': (elem) => {
        catalogue_description_node = elem
      }
    }
    h.bindAttributes('nakama', bindTypes, dom)
    updateCatalogueDescription()
  })
  return {
    destroy: () => {
      for (let unsub of socket_subs) {
        unsub()
      }
      for (let unsub of dom_subs) {
        unsub()
      }
      view.removeApp()
    }
  }
}

// loads up the game
const GameApp = (mount, dsocket, selfid, roomid, players_registry, games_registry) => {
  const view = h.View(
    mount,
    'div',
    [],
    () => { return `
      <div nakama='client'></div>
    `}
  )
  var client_node
  var gseshid

  //convenience
  const games_writer = games_registry.writer

  //init
  view.renderApp((dom) => {
    const bindTypes = {
      'client': (elem) => {
        client_node = elem
      }
    }
    h.bindAttributes('nakama', bindTypes, dom)
  })
  const handleClientReady = (msg) => {
    if (roomid === msg.roomid) {
      gseshid = msg.gseshid
      let Client = terribleBrowserBabelifyHackaround(msg.bundle)
      let api = Api(selfid, gseshid, dsocket)
      let fc = api.forClient
      let fh = api.forHandler
      let self = players_registry.reader.getPlayer(selfid)
      games_writer.storeGameApi(gseshid, fh)
      const client = Client(client_node, self, fc.input, fc.output)
      api.forGameWrapper.registerClient(client)
    }
  }

  const socket_subs = [
    h.socketSub(dsocket, inter.CLIENT_BUNDLE_READY, handleClientReady)
  ]

  //init
  dsocket.emit(inter.I_REQUESTED_CLIENT_BUNDLE, roomid)
  return {
    getGseshId: () => {
      return gseshid
    },
    destroy: () => {
      if (gseshid) {
        games_writer.removeGameApi(gseshid)
      }
      for (let unsub of socket_subs) {
        unsub()
      }
      for (let unsub of socket_subs) {
        unsub()
      }
      view.removeApp()
    }
  }
}

//bundle (string) → Client
const terribleBrowserBabelifyHackaround = (client_bundle) => {
  const module = {}
  eval(client_bundle)
  return module.exports
}

const GameWrapperApp = (mount, dsocket, dom_pubsub, roomid, players_registry, rooms_registry, games_registry) => {
  //convenience
  const rooms_reader = rooms_registry.reader
  const players_reader = players_registry.reader
  const view = h.View(
    mount,
    'div',
    [['class', styles.gamesAppContainer]],
    () => { return `
      <div class='${styles.gameMount}' nakama='game-mount'></div>
    `}
  )

  var game_ongoing = rooms_reader.getRoom(roomid).game_ongoing
  const selfid = players_registry.reader.getSelfId()
  var game_catalogue_app, game_app
  var game_mount
  const games_pubsub = PubSub()

  const handleIPressedPlayGame = (gameid) => {
    dsocket.emit(inter.I_REQUESTED_GAME_START, {
      userid: selfid,
      gameid,
      roomid
    })
  }

  const handleGameStarted = (msg) => {
    if (msg.roomid === roomid) {
      game_ongoing = true
      game_catalogue_app.destroy()
      game_app = GameApp(game_mount, dsocket, selfid, roomid, players_registry, games_registry)
    }
  }

  const handleGameEnded = (msg) => {
    if (msg.roomid === roomid) {
      game_ongoing = false
      game_app.destroy()
      game_catalogue_app = GameCatalogueApp(game_mount, dsocket, dom_pubsub, games_pubsub, roomid, games_registry, rooms_reader, players_reader)
    }
  }

  const handleGameErrored = (msg) => {
    if (msg.roomid === roomid) {
      if (game_ongoing) {
        game_ongoing = false
        game_app.destroy()
        game_catalogue_app = GameCatalogueApp(game_mount, dsocket, dom_pubsub, games_pubsub, roomid, games_registry, rooms_reader, players_reader)
      } else {
        games_pubsub.pub(intra.GAME_ERRORED, msg)
      }
    }
  }

  const handleClientReconnected = () => {
    const fresh_gseshid = rooms_reader.getRoom(roomid).game_ongoing
    const fresh_game_ongoing = fresh_gseshid? true : false
    if (fresh_game_ongoing && game_ongoing) { //compare gseshids
      if (game_app.getGseshId() !== fresh_gseshid) { //blat and recreate
        game_app.destroy()
        game_app = GameApp(game_mount, dsocket, selfid, roomid, players_registry, games_registry)
      }
    } else if (fresh_game_ongoing && !game_ongoing) {
      game_catalogue_app.destroy()
      game_app = GameApp(game_mount, dsocket, selfid, roomid, players_registry, games_registry)
      game_ongoing = true
    } else if (!fresh_game_ongoing && game_ongoing) {
      game_app.destroy()
      game_catalogue_app = GameCatalogueApp(game_mount, dsocket, dom_pubsub, games_pubsub, roomid, games_registry, rooms_reader, players_reader)
      game_ongoing = false
    }
  }

  //subs
  const dom_subs = [
    dom_pubsub.sub(intra.GAME_STARTED, handleGameStarted),
    dom_pubsub.sub(intra.GAME_ENDED, handleGameEnded),
    dom_pubsub.sub(intra.GAME_ERRORED, handleGameErrored),
    dom_pubsub.sub(intra.CLIENT_RECONNECTED, handleClientReconnected)
  ]
  const games_subs = [
    games_pubsub.sub(intra.I_PRESSED_PLAY_GAME, handleIPressedPlayGame)
  ]
  const socket_subs = [
  ]
  //init
  view.renderApp((dom) => {
    h.bindAttributes('nakama', {
      'game-mount': (elem) => {
        game_mount = elem
        if (game_ongoing) {
          game_app = GameApp(game_mount, dsocket, selfid, roomid, players_registry, games_registry)
        } else {
          game_catalogue_app = GameCatalogueApp(game_mount, dsocket, dom_pubsub, games_pubsub, roomid, games_registry, rooms_reader, players_reader)
        }
      }
    }, dom)
  })

  //WindowHeight hackery
  const calculateGameContainerHeightCss = (window_height) => {
    return (window_height - cssc.measurements.tab_container_offset) + 'px'
  }
  const destroyWindowHeightListener = WindowHeight.onWindowResize((new_height) => {
    game_mount.style.height = calculateGameContainerHeightCss(new_height)
  })
  game_mount.style.height = calculateGameContainerHeightCss(WindowHeight.getHeight())

  return {
    focus: () => {
      view.getSelfNode().style.display = 'block'
    },
    hide: () => {
      view.getSelfNode().style.display = 'none'
    },
    destroy: () => {
      for (let unsub of dom_subs) {
        unsub()
      }
      for (let unsub of games_subs) {
        unsub()
      }
      for (let unsub of socket_subs) {
        unsub()
      }
      if (game_ongoing) {
        game_app.destroy()
      } else {
        game_catalogue_app.destroy()
      }
      destroyWindowHeightListener()
      view.removeApp()
    }
  }
}

module.exports = GameWrapperApp
