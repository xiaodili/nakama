const h = require('../../../../../shared/client_helpers.js')
const intra = require('../../../../lib/symbols/intra.js')
const ep = require('../../../../../shared/endpoints.js')

const RoomPassphraseAppView = (parent) => {
  var self_node
  const template = () => {
    return [
      'div',
      [],
      `
        <label>Passphrase: <input type="password" nakama="input"></label><button nakama="submit-btn">Confirm</button>
        <div nakama='validation-msg'>Yikes - incorrect passphrase</div>
      `
    ]
  }
  return {
    renderApp: (bind) => {
      self_node = h.createNode(...template())
      parent.appendChild(self_node)
      bind(self_node)
    },
    removeApp: () => {
      parent.removeChild(self_node)
    },
    hide: () => {
      self_node.style.display = "none"
    },
    show: () => {
      self_node.style.display = "block"
    }
  }
}

const RoomPassphraseApp = (mount, room_item_pubsub, roomid) => {
  const view = RoomPassphraseAppView(mount)
  var input_node, submit_btn_node, validation_msg_node

  const handleSubmitPressed = () => {
    const passphrase = input_node.value
    input_node.value = ""
    h.post(ep.constructRoomChallenge(roomid), {
      passphrase
    }, (payload, status_code) => {
      if (status_code === 200) {
        if (payload) {
          view.hide()
          room_item_pubsub.pub(intra.PASSPHRASE_CHECK_PASSED, {
            roomid,
            passphrase
          })
        } else {
          showValidationMessage()
        }
      }
    })
  }
  const showValidationMessage = () => {
    validation_msg_node.style.display = "block"
  }
  const hideValidationMessage = () => {
    validation_msg_node.style.display = "none"
  }

  const renderApp = () => {
    view.renderApp((dom) => {
      const bindTypes = {
        'input': (elem) => {
          input_node = elem
          h.registerEnterPress(input_node, handleSubmitPressed)
          input_node.addEventListener('input', hideValidationMessage)
        },
        'submit-btn': (elem) => {
          submit_btn_node = elem
          h.registerTapOrClick(submit_btn_node, handleSubmitPressed)
        },
        'validation-msg': (elem) => {
          validation_msg_node = elem
          hideValidationMessage()
        }
      }
      h.bindAttributes('nakama', bindTypes, dom)
    })
    view.hide()
  }

  const handleIJoinedPassphrasedRoomIntent = (_roomid) => {
    if (roomid === _roomid) {
      view.show()
      hideValidationMessage()
      input_node.focus()
    }
  }
  const handleICanceledPassphrasedRoomIntent = (_roomid) => {
    if (roomid === _roomid) {
      input_node.value = ""
      view.hide()
    }
  }
  const room_item_subs = [
    room_item_pubsub.sub(intra.I_JOINED_PASSPHRASED_ROOM_INTENT, handleIJoinedPassphrasedRoomIntent),
    room_item_pubsub.sub(intra.I_CANCELED_PASSPHRASED_ROOM_INTENT, handleICanceledPassphrasedRoomIntent)
  ]

  //init
  renderApp()
  return {
    remove: () => {
      for (let unsub of room_item_subs) {
        unsub()
      }
      view.removeApp()
    }
  }
}

module.exports = RoomPassphraseApp
