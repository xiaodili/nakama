const h = require('../../../../../shared/client_helpers.js')
const inter = require('../../../../../shared/socket_events.js')
const intra = require('../../../../lib/symbols/intra.js')

const RoomItemActionApp = (parent, roomid, _in_room, dsocket, dom_pubsub, room_item_pubsub, players_registry, rooms_registry, _passphrase_required, theme) => {
  const s = {
    LEAVE: "Leave",
    SWITCH_TO: "Switch",
    JOIN: "Join",
    JOINING: "Joining...",
    CANCEL: "Cancel"
  }

  //saves typing
  const getSelfId = players_registry.reader.getSelfId
  const players_writer = players_registry.writer
  const rooms_writer = rooms_registry.writer
  const rooms_reader = rooms_registry.reader

  //initial values
  var in_room = _in_room
  var passphrase_required = _passphrase_required
  var in_focus = rooms_reader.getCurrentRoomFocus(roomid)

  var action_btn_node
  var action_state
  const template = `<button nakama="action"></button>`
  const updateActionState = (state) => {
    action_state = state
    action_btn_node.innerHTML = state
    //ignore the transitioning states
    switch (state) {
      case s.LEAVE:
        action_btn_node.className = theme.styles.prebuilt.button_secondary
        break
      case s.SWITCH_TO:
        action_btn_node.className = theme.styles.prebuilt.button_ghost
        break
      case s.JOIN:
        action_btn_node.className = '' //use default
        break
      default:
        break
    }
  }

  //internal
  const updateActionApp = () => {
    if (in_room) {
      if (in_focus) {
        updateActionState(s.LEAVE)
      } else {
        updateActionState(s.SWITCH_TO)
      }
    } 
    else {
      updateActionState(s.JOIN)
    }
  }

  const handleActionBtnPressed = () => {
    const userid = getSelfId()
    if (!in_room) { //joining room
      if (passphrase_required) { //passphrase flow
        if (action_state === s.CANCEL) {
          room_item_pubsub.pub(intra.I_CANCELED_PASSPHRASED_ROOM_INTENT, roomid)
          updateActionState(s.JOIN)
        } else if (action_state === s.JOIN) {
          room_item_pubsub.pub(intra.I_JOINED_PASSPHRASED_ROOM_INTENT, roomid)
          updateActionState(s.CANCEL)
        }
      } else { //regular flow
        players_writer.addSelfRoom(roomid)
        rooms_writer.playerJoinedRoom(userid, roomid)
        in_room = true
        dom_pubsub.pub(intra.I_JOINED_ROOM, roomid)
        dsocket.emit(inter.I_JOINED_ROOM, {
          roomid,
          userid
        })
      }
    } else {
      if (in_focus) { //leaving room
        players_writer.removeSelfRoom(roomid)
        rooms_writer.playerLeftRoom(userid, roomid)
        in_room = false
        dom_pubsub.pub(intra.I_LEFT_ROOM, roomid)
        dsocket.emit(inter.I_LEFT_ROOM, {
          roomid,
          userid
        })
      } else { //switch to
        dom_pubsub.pub(intra.I_PRESSED_SWITCH_TO_ROOM, roomid)
      }
    } 
  }
  const handleIJoinedRoom = (_roomid) => {
    if (roomid === _roomid) {
      in_room = true
      action_btn_node.disabled = true
      updateActionState(s.JOINING)
    }
  }
  const handlePlayerJoinedRoom = (msg) => {
    if (roomid === msg.roomid) {
      if (getSelfId() === msg.userid) {
        action_btn_node.disabled = false
        in_room = true
        updateActionApp()
      }
    }
  }
  const handleILeftRoom = (_roomid) => {
    if (roomid === _roomid) {
      in_room = false
      updateActionApp()
    }
  }
  const handlePlayerLeftRoom = (msg) => {
    if (roomid === msg.roomid) {
      if (getSelfId() === msg.userid) {
        action_btn_node.disabled = false
        in_room = false
        updateActionApp()
      }
    }
  }
  const handlePlayerWasKicked = (msg) => {
    if (roomid === msg.roomid) {
      let playerid = msg.userid
      if (getSelfId() == playerid) {
        in_room = false
        updateActionApp()
      }
    }
  }
  const handlePassphraseCheckPassed = (msg) => {
    if (roomid === msg.roomid) {
      let userid = getSelfId()
      let passphrase = msg.passphrase
      players_writer.addSelfRoom(roomid)
      rooms_writer.playerJoinedRoom(userid, roomid)
      dom_pubsub.pub(intra.I_JOINED_ROOM, roomid)
      dsocket.emit(inter.I_JOINED_ROOM, {
        roomid,
        userid,
        passphrase
      })
    }
  }
  const handlePlayerAddedPassphrase = (_roomid) => {
    if (roomid === _roomid) {
      passphrase_required = true
      updateActionApp()
    }
  }
  const handlePlayerRemovedPassphrase = (_roomid) => {
    if (roomid === _roomid) {
      passphrase_required = false
      updateActionApp()
    }
  }
  const handleFocusOnRoom = (_roomid) => {
    if (roomid === _roomid) {
      in_focus = true
    } else {
      in_focus = false
    }
    updateActionApp()
  }
  const handleFocusOnWelcome = () => {
    in_focus = false
    updateActionApp()
  }

  const dom_subs = [
    dom_pubsub.sub(intra.I_JOINED_ROOM, handleIJoinedRoom),
    dom_pubsub.sub(intra.PLAYER_JOINED_ROOM, handleIJoinedRoom),
    dom_pubsub.sub(intra.I_LEFT_ROOM, handleILeftRoom),
    dom_pubsub.sub(intra.PLAYER_LEFT_ROOM, handlePlayerLeftRoom),
    dom_pubsub.sub(intra.PLAYER_WAS_KICKED, handlePlayerWasKicked),
    dom_pubsub.sub(intra.PLAYER_JOINED_ROOM, handlePlayerJoinedRoom),
    dom_pubsub.sub(intra.PLAYER_ADDED_PASSPHRASE, handlePlayerAddedPassphrase),
    dom_pubsub.sub(intra.PLAYER_REMOVED_PASSPHRASE, handlePlayerRemovedPassphrase),
    dom_pubsub.sub(intra.I_FOCUSED_ON_ROOM, handleFocusOnRoom),
    dom_pubsub.sub(intra.I_FOCUSED_ON_WELCOME, handleFocusOnWelcome)
  ]
  const room_item_subs = [
    room_item_pubsub.sub(intra.PASSPHRASE_CHECK_PASSED, handlePassphraseCheckPassed)
  ]

  //init
  parent.innerHTML = template
  action_btn_node = h.qs("[nakama=action]", parent)
  h.registerTapOrClick(action_btn_node, handleActionBtnPressed)
  updateActionApp()
  return {
    remove: () => {
      for (let unsub of dom_subs) {
        unsub()
      }
      for (let unsub of room_item_subs) {
        unsub()
      }
    }
  }
}

module.exports = RoomItemActionApp
