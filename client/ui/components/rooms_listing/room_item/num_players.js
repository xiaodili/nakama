const intra = require('../../../../lib/symbols/intra.js')

const RoomItemNumPlayersApp = (parent, roomid, _occupants, dsocket, dom_pubsub, getSelfId, _player_list_enabled) => {
  const occupants = new Set(_occupants)
  var player_list_enabled = _player_list_enabled
  const template = () => {
    return `(${occupants.size})`
  }

  //internal
  const handleIJoinedRoom = (_roomid) => {
    if (roomid === _roomid) {
      let player_id = getSelfId()
      occupants.add(player_id)
      if (player_list_enabled) {
        parent.innerHTML = template()
      }
    }
  }
  const handleILeftRoom = (_roomid) => {
    if (roomid === _roomid) {
      let player_id = getSelfId()
      occupants.delete(player_id)
      if (player_list_enabled) {
        parent.innerHTML = template()
      }
    }
  }
  const handleIKickedPlayer = (msg) => {
    if (roomid === msg.roomid) {
      let playerid = msg.playerid
      occupants.delete(playerid)
      if (player_list_enabled) {
        parent.innerHTML = template()
      }
    }
  }
  const handlePlayerJoinedRoom = (msg) => {
    if (msg.roomid === roomid) {
      let player_id = msg.userid
      occupants.add(player_id)
      if (player_list_enabled) {
        parent.innerHTML = template()
      }
    }
  }
  const handlePlayerLeftRoom = (msg) => {
    if (msg.roomid === roomid) {
      let player_id = msg.userid
      occupants.delete(player_id)
      if (player_list_enabled) {
        parent.innerHTML = template()
      }
    }
  }
  const handleIEnabledPlayerList = (_roomid) => {
    if (roomid === _roomid) {
      player_list_enabled = true
      parent.innerHTML = template()
    }
  }
  const handleIDisabledPlayerList = (_roomid) => {
    if (roomid === _roomid) {
      player_list_enabled = false
      parent.innerHTML = ""
    }
  }
  const handlePlayerEnabledPlayerList = (msg) => {
    if (roomid === msg.roomid) {
      player_list_enabled = true
      parent.innerHTML = template()
    }
  }
  const handlePlayerDisabledPlayerList = (msg) => {
    if (roomid === msg.roomid) {
      player_list_enabled = false
      parent.innerHTML = ""
    }
  }

  //subs
  const dom_subs = [
    dom_pubsub.sub(intra.I_JOINED_ROOM, handleIJoinedRoom),
    dom_pubsub.sub(intra.I_LEFT_ROOM, handleILeftRoom),
    dom_pubsub.sub(intra.I_KICKED_PLAYER, handleIKickedPlayer),
    dom_pubsub.sub(intra.PLAYER_JOINED_ROOM, handlePlayerJoinedRoom),
    dom_pubsub.sub(intra.PLAYER_LEFT_ROOM, handlePlayerLeftRoom),
    dom_pubsub.sub(intra.PLAYER_WAS_KICKED, handlePlayerLeftRoom),
    dom_pubsub.sub(intra.I_ENABLED_PLAYER_LIST, handleIEnabledPlayerList),
    dom_pubsub.sub(intra.I_DISABLED_PLAYER_LIST, handleIDisabledPlayerList),
    dom_pubsub.sub(intra.PLAYER_DISABLED_PLAYER_LIST, handlePlayerDisabledPlayerList),
    dom_pubsub.sub(intra.PLAYER_ENABLED_PLAYER_LIST, handlePlayerEnabledPlayerList)
  ]
  //init
  if (player_list_enabled) {
    parent.innerHTML = template()
  }
  return {
    remove: () => {
      for (let unsub of dom_subs) {
        unsub()
      }
    }
  }
}

module.exports = RoomItemNumPlayersApp
