const h = require('../../../../shared/client_helpers.js')
const jss = require('../../../lib/css.js')
const PubSub = require('../../../../shared/helpers.js').PubSub
const intra = require('../../../lib/symbols/intra.js')
const RoomPassphraseApp = require('./room_item/passphrase.js')
const RoomItemNumPlayersApp = require('./room_item/num_players.js')
const RoomItemActionApp = require('./room_item/action.js')

const styles = jss({
  roomItem: {
    marginTop: '5px',
    marginBottom: '5px',
    display: 'flex',
    justifyContent: 'space-between'
  }
})

const RoomItem = (parent, id, room, dsocket, dom_pubsub, players_registry, rooms_registry, theme) => {
  //convenience
  const players_reader = players_registry.reader
  const room_item_pubsub = PubSub()
  var self_node,
    passphrase_node,
    name_node,
    lock_node,
    action_node,
    num_players_node
  var name = room.name
  var passphrase_required = room.passphrase_required
  var action_app, num_players_app, passphrase_app
  const template = () => {
    var inner_html = `
      <div class='${styles.roomItem}'>
        <div>
          <span nakama="name">${h.escapeHtml(name)}</span>
          <span nakama="lock" title="passphrase required">&#x1f512;</span>
          <span nakama="num-players"></span>
        </div>
        <div nakama="action"></div>
      </div>
      <div nakama="passphrase"></div>
    `
    //join/joined/leave?
    return ['li',
      [],
      inner_html]
  }
  const updateLockSymbol = () => {
    if (passphrase_required) {
      lock_node.style.display = 'inline-block'
    } else {
      lock_node.style.display = 'none'
    }
  }

  const handleRoomNameChanged = (msg) => {
    if (id === msg.roomid) {
      let name = msg.name
      name_node.innerHTML = h.escapeHtml(name)
    }
  }
  const handlePlayerAddedPassphrase = (_roomid) => {
    if (id === _roomid) {
      passphrase_required = true
      passphrase_app = createPassphraseApp()
      updateLockSymbol()
    }
  }
  const handlePlayerRemovedPassphrase = (_roomid) => {
    if (id === _roomid) {
      passphrase_required = false
      passphrase_app.remove()
      updateLockSymbol()
    }
  }
  const dom_subs = [
    dom_pubsub.sub(intra.I_CHANGED_ROOM_NAME, handleRoomNameChanged),
    dom_pubsub.sub(intra.PLAYER_CHANGED_ROOM_NAME, handleRoomNameChanged),
    dom_pubsub.sub(intra.PLAYER_ADDED_PASSPHRASE, handlePlayerAddedPassphrase),
    dom_pubsub.sub(intra.PLAYER_REMOVED_PASSPHRASE, handlePlayerRemovedPassphrase)
  ]

  const createPassphraseApp = () => {
    return RoomPassphraseApp(passphrase_node, room_item_pubsub, id)
  }

  return {
    remove: () => {
      for (let unsub of dom_subs) {
        unsub()
      }
      action_app.remove()
      num_players_app.remove()
      if (passphrase_required) {
        passphrase_app.remove()
      }
      parent.removeChild(self_node)
    },
    render: () => {
      self_node = h.createNode(...template())
      parent.appendChild(self_node)
      name_node = h.qs("[nakama=name]", self_node)
      lock_node = h.qs("[nakama=lock]", self_node)
      passphrase_node = h.qs("[nakama=passphrase]", self_node)
      num_players_node = h.qs("[nakama=num-players]", self_node)
      action_node = h.qs("[nakama=action]", self_node)

      updateLockSymbol()
      action_app = RoomItemActionApp(action_node, id, players_reader.isSelfInRoom(id), dsocket, dom_pubsub, room_item_pubsub, players_registry, rooms_registry, passphrase_required, theme)
      num_players_app = RoomItemNumPlayersApp(num_players_node, id, room.players, dsocket, dom_pubsub, players_registry.reader.getSelfId, rooms_registry.reader.getRoom(id).player_list_enabled)
      if (passphrase_required) {
        passphrase_app = createPassphraseApp()
      }
    },
    //accessors
    getId: () => {
      return id
    }
  }
}

module.exports = RoomItem
