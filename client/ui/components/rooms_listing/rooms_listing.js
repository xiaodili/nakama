const h = require('../../../../shared/client_helpers.js')
const intra = require('../../../lib/symbols/intra.js')
const inter = require('../../../../shared/socket_events.js')
const RoomItem = require('./room_item.js')
const jss = require('../../../lib/css.js')

const styles = jss({
  roomsHeader: {
    fontWeight: 'bold',
    textAlign: 'center',
  },
  createRoomBtnContainer: {
    display: 'flex',
    justifyContent: 'flex-end'
  }
})

const RoomsListApp = (mount, dsocket, dom_pubsub, players_registry, rooms_registry, theme) => {
  const renderer = h.ItemRenderer()
  var room_items // = {}

  const syncRoomItems = (rooms) => {
    room_items = {}
    renderer.renderItems({})
    Object.keys(rooms).forEach((roomid) => {
      let room = rooms[roomid]
      room_items[roomid] = RoomItem(mount, roomid, room, dsocket, dom_pubsub, players_registry, rooms_registry, theme)
    })
    renderer.renderItems(room_items)
  }

  const addRoomItem = (room) => {
    const roomid = room.id
    room_items[roomid] = RoomItem(mount, roomid, room, dsocket, dom_pubsub, players_registry, rooms_registry, theme)
    renderer.renderItems(room_items)
  }
  const removeRoomItem = (roomid) => {
    delete room_items[roomid]
    renderer.renderItems(room_items)
  }

  dom_pubsub.sub(intra.SYNCED_ROOMS, syncRoomItems)
  return {
    addRoomItem,
    removeRoomItem
  }
}

const RoomsListingApp = (mount, dsocket, dom_pubsub, players_registry, rooms_registry, theme, heading_text = 'ROOMS') => {

  //convenience
  const players_reader = players_registry.reader
  const rooms_reader = rooms_registry.reader
  const on_mobile = players_reader.getSelfOnMobile()

  const view = h.View(
    mount,
    'div',
    [],
    (on_mobile) => { return `
      <div class='${styles.roomsHeader}'>
        <div>${heading_text} (<span nakama='num-rooms'></span>)</div>
        <div class='${styles.createRoomBtnContainer}'>
          <button nakama="create-room-btn"></button>
        </div>
      </div>
      <ul nakama="rooms-list">
      </ul>
    `
    }, on_mobile)
  let rooms_list_app

  //DOM elements
  let create_room_enabled,
    create_room_btn_node,
    num_rooms_node

  const CREATING_MSG = "Creating..."
  const CREATE_ROOM_MSG = "Create Room"
  let num_rooms = rooms_reader.getNumRooms()

  const handleCreateRoomBtnPressed = () => {
    create_room_btn_node.innerHTML = CREATING_MSG
    create_room_btn_node.disabled = true
    const userid = players_reader.getSelfId()
    dsocket.emit(inter.I_REQUESTED_ROOM_CREATION, {
      userid
    })
  }

  const handleRoomCreated = (room) => {
    create_room_btn_node.innerHTML = CREATE_ROOM_MSG
    create_room_btn_node.disabled = false
    rooms_list_app.addRoomItem(room)
    num_rooms += 1
    num_rooms_node.innerHTML = num_rooms
  }
  const handleUnoccupiedRoomDestroyed = (roomid) => {
    rooms_registry.writer.removeRoom(roomid)
    rooms_list_app.removeRoomItem(roomid)
    num_rooms -= 1
    num_rooms_node.innerHTML = num_rooms
  }
  const handlePlayerDestroyedRoom = (msg) => {
    const roomid = msg.roomid
    rooms_list_app.removeRoomItem(roomid)
    num_rooms -= 1
    num_rooms_node.innerHTML = num_rooms
  }
  const handleServerConfigSynced = (msg) => {
    if (!create_room_enabled && msg.create_room_enabled) {
      create_room_btn_node.style.display = "inline-block"
      create_room_enabled = true
    } else if (create_room_enabled && !msg.create_room_enabled) {
      create_room_btn_node.style.display = "none"
      create_room_enabled = false
    }
    num_rooms = rooms_reader.getNumRooms()
    num_rooms_node.innerHTML = num_rooms
  }

  const dom_subs = [
    dom_pubsub.sub(intra.ROOM_CREATED, handleRoomCreated),
    dom_pubsub.sub(intra.PLAYER_DESTROYED_ROOM, handlePlayerDestroyedRoom),
    dom_pubsub.sub(intra.UNOCCUPIED_ROOM_DESTROYED, handleUnoccupiedRoomDestroyed),
    dom_pubsub.sub(intra.SYNCED_SERVER_CONFIG, handleServerConfigSynced)
  ]

  const socket_subs = [
    h.socketSub(dsocket, inter.UNOCCUPIED_ROOM_DESTROYED, handleUnoccupiedRoomDestroyed)
  ]

  //init
  view.renderApp((dom) => {
    const bindTypes = {
      'create-room-btn': (elem) => {
        create_room_btn_node = elem
        create_room_btn_node.style.display = "none"
        create_room_btn_node.innerHTML = CREATE_ROOM_MSG
        h.registerTapOrClick(create_room_btn_node, handleCreateRoomBtnPressed)
      },
      'num-rooms': (elem) => {
        num_rooms_node = elem
        num_rooms_node.innerHTML = num_rooms
      },
      'rooms-list': (elem) => {
        rooms_list_app = RoomsListApp(elem, dsocket, dom_pubsub, players_registry, rooms_registry, theme)
      },
    }
    h.bindAttributes('nakama', bindTypes, dom)
  })
  return {
    destroy: () => {
      for (let unsub of dom_subs) {
        unsub()
      }
      for (let unsub of socket_subs) {
        unsub()
      }
      view.removeApp()
    }
  }
}

module.exports = RoomsListingApp
