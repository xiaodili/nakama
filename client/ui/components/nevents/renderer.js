const cssc = require('../../css_constants.js')
const jss = require('../../../lib/css.js')
const hc = require('../../../../shared/client_helpers.js')
const nev = require('../../../../shared/nevent.js')

const styles = jss({
  timeStamp: {
    float: 'right',
  },
  imContainer: {
    backgroundColor: 'white',
    borderRadius: '5px',
    padding: '5px',
    margin: '5px 0px',
    clear: 'both'
  },
  imAuthor: {
    fontWeight: 'bold'
  },
  imMetadata: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  imTime: {
    fontWeight: 'lighter'
  },
  genericEventContainer: {
    color: cssc.colours.event_grey,
    clear: 'both'
  }
})

const nsonRenderLookup = {
  [nev.nsons.MESSAGE]: (params) => {
    const {
      content
    } = params
    return `<li class='${styles.genericEventContainer}'>
              <span>${hc.escapeHtml(content)}</span>
            </li>`
  },
  [nev.nsons.CHAT_MESSAGE]: (params) => {
    const {
      handle,
      time,
      message
    } = params
    const time_formatted = hc.unixToHourMinTime(time)
    return `<li class='${styles.imContainer}'>
              <div class='${styles.imMetadata}'>
                <span class='${styles.imAuthor}'>${hc.escapeHtml(handle)}</span>
                <span class='${styles.imTime}'>${time_formatted}</span>
              </div>
              <span>${hc.escapeHtml(message)}</span>
            </li>`
  }
}

const neventRenderLookup = {
  [nev.nevents.IM]: (params) => {
    const {
      handle,
      message,
      time
    } = params
    const time_formatted = hc.unixToHourMinTime(time)
    //NOTE: don't try to move li to the next line; h.htmlToNode doesn't like it when ` is on a line on its own
    return `<li class='${styles.imContainer}'>
              <div class='${styles.imMetadata}'>
                <span class='${styles.imAuthor}'>${hc.escapeHtml(handle)}</span>
                <span class='${styles.imTime}'>${time_formatted}</span>
              </div>
              <span>${hc.escapeHtml(message)}</span>
            </li>`
  },
  [nev.nevents.NOTIFICATION]: (params) => {
    const {
      time,
      content
    } = params
    const time_formatted = hc.unixToHourMinTime(time)
    return `<li class='${styles.genericEventContainer}'>
              <span>${hc.escapeHtml(content)}</span>
              <span class='${styles.timeStamp}'>${time_formatted}</span>
            </li>`
  },
  [nev.nevents.NSON]: (params) => {
    return nsonRenderLookup[params.type](params.params)
  },
}

const renderNevent = (nevent) => {
  const {
    type,
    params
  } = nevent
  return neventRenderLookup[type](params)
}

module.exports = renderNevent
