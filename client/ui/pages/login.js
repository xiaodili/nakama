const h = require('../../../shared/client_helpers.js')
const jss = require('../../lib/css.js')
const ep = require('../../../shared/endpoints.js')

const BAD_INPUT_MSG = 'Details not recognised. Try again?'

const visible_display = 'block';
const hidden_display = 'none';
const styles = jss({
  loginApp: {
    display: hidden_display
  }
})

const LoginApp = (mount, dom_pubsub, players_registry) => {

  const players_reader = players_registry.reader
  const players_writer = players_registry.writer

  var handle_input_node,
    passphrase_input_node,
    submit_btn_node,
    feedback_node
  const view = h.View(
    mount,
    'div',
    [['class', styles.loginApp]],
    () => { return `
      <h2>Login</h2>
      <div nakama='feedback'></div>
      <div>
        Handle: <input type='text' nakama='handle-input'/><br/>
        Passphrase: <input type='password' nakama='passphrase-input'/>
        <button nakama='submit'>Submit</button>
      </div>
    `}
  )

  const handleSubmitBtnPressed = () => {
    const handle = handle_input_node.value
    const passphrase = passphrase_input_node.value
    handle_input_node.value = ""
    passphrase_input_node = ""
    h.post(ep.LOGIN, {
      handle,
      passphrase
    }, (payload, status_code) => {
      if (status_code === 401) {
        feedback_node.innerHTML = BAD_INPUT_MSG
      } else if (status_code === 200) {
        console.log("looks good!!!");
        //const session_id = payload.session_id
        //self.writer.login(handle, session_id)
        //router.goToRoute('/')
      }
    })
  }
  view.renderApp((dom) => {
    h.bindAttributes('nakama', {
      'feedback': (elem) => {
        feedback_node = elem
      },
      'handle-input': (elem) => {
        handle_input_node = elem
      },
      'passphrase-input': (elem) => {
        passphrase_input_node = elem
        h.registerEnterPress(passphrase_input_node, handleSubmitBtnPressed)
      },
      'submit': (elem) => {
        submit_btn_node = elem
        h.registerTapOrClick(submit_btn_node, handleSubmitBtnPressed)
      }
    }, dom)
  })
  return {
    destroy: () => {
      view.removeApp()
    },
    focus: () => {
      mount.style.display = 'block'
      view.getSelfNode().style.display = visible_display
    },
    hide: () => {
      mount.style.display = hidden_display
      view.getSelfNode().style.display = hidden_display
    }
  }
}

module.exports = LoginApp
