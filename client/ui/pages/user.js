const h = require('../../../shared/client_helpers.js')
const jss = require('../../lib/css.js')
const cssc = require('../css_constants.js')
const inter = require('../../../shared/socket_events.js')
const intra = require('../../lib/symbols/intra.js')
const NameSelectApp = require('../components/name_select.js')
const WindowHeight = require('../../lib/hacks.js').WindowHeight

const CookieApp = (mount, dsocket, dom_pubsub, players_registry) => {
  //convenience
  const players_reader = players_registry.reader
  const players_writer = players_registry.writer

  let using_cookies_node,
    toggle_cookies_btn_node

  const ENABLE_COOKIES_TEXT = "Enable Cookies"
  const DISABLE_COOKIES_TEXT = "Disable Cookies"

  const view = h.View(
    mount,
    'div',
    [['class', 'cookie-app']],
    () => { return `
      <h3>Cookies</h3>
      Using Cookies <span nakama ='using-cookies'>
        ${using_cookies ? '✓' : '✗'}
      </span>
      <button nakama='toggle-cookies-btn'>
        ${using_cookies ? DISABLE_COOKIES_TEXT : ENABLE_COOKIES_TEXT}
      </button>
      <div>
        Enabling cookies will allow inactive players to reconnect to the server with the same handle and room memberships.
      </div>
    `}
  )

  //init
  var using_cookies = players_reader.getUsingCookies()

  const handleIDisabledCookies = () => {
    using_cookies = false
    updateCookieUI()
    players_writer.disableCookies()

  }
  const handleIEnabledCookies = () => {
    using_cookies = true
    updateCookieUI()
    players_writer.enableCookies()
  }

  const updateCookieUI = () => {
    if (using_cookies) {
      using_cookies_node.innerHTML = '✓'
      toggle_cookies_btn_node.innerHTML = DISABLE_COOKIES_TEXT
    } else {
      using_cookies_node.innerHTML = '✗'
      toggle_cookies_btn_node.innerHTML = ENABLE_COOKIES_TEXT
    }
  }
  const handleSyncedSelfData = (msg) => {
    using_cookies = msg.using_cookies
    updateCookieUI(using_cookies)
  }

  dsocket.on(inter.COOKIES_DISABLED_BY_MYSELF, handleIDisabledCookies)
  dsocket.on(inter.COOKIES_ENABLED_BY_MYSELF, handleIEnabledCookies)
  dom_pubsub.sub(intra.SYNCED_SELF_DATA, handleSyncedSelfData)

  view.renderApp((dom) => {
    h.bindAttributes('nakama', {
      'using-cookies': (elem) => {
        using_cookies_node = elem
      },
      'toggle-cookies-btn': (elem) => {
        toggle_cookies_btn_node = elem
        const handleToggleCookiesBtnPressed = () => {
          if (using_cookies) {
            using_cookies = false
            updateCookieUI()
            players_writer.disableCookies()
            dsocket.emit(inter.I_DISABLED_COOKIES)
          } else {
            using_cookies = true
            updateCookieUI()
            players_writer.enableCookies()
            dsocket.emit(inter.I_ENABLED_COOKIES)
          }
        }
        h.registerTapOrClick(toggle_cookies_btn_node, handleToggleCookiesBtnPressed)
      }
    }, dom)
  })
  return {
    destroy: () => {
      view.removeApp()
    }
  }
}

const LoginApp = (mount, dsocket, dom_pubsub, players_registry) => {
  const LOGIN_DESCRIPTION_MSG = "Enter your handle and passphrase to log in"
  var description_node,
    handle_input_node,
    passphrase_input_node,
    submit_btn_node
  const view = h.View(
    mount,
    'div',
    [['class', 'login-app']],
    () => { return `
      <h3>Login</h3>
      <p nakama='description'>${LOGIN_DESCRIPTION_MSG}</p>
      <div>
        Handle: <input type='text' nakama='handle-input'/><br/>
        Passphrase: <input type='password' nakama='passphrase-input'/>
        <button nakama='submit'>Submit</button>
      </div>
    `}
  )
  const handleSubmitBtnPressed = () => {
    const handle = handle_input_node.value
    const passphrase = passphrase_input_node.value
    dsocket.emit(inter.I_SUBMITTED_ACCOUNT_PASSPHRASE_ATTEMPT, {
      handle,
      passphrase
    })
  }

  const handleAccountPassphraseAttemptResultReady = (result) => {
    console.log("result: ", result);
  }

  view.renderApp((dom) => {
    h.bindAttributes('nakama', {
      'handle-input': (elem) => {
        handle_input_node = elem
      },
      'description': (elem) => {
        description_node = elem
      },
      'passphrase-input': (elem) => {
        passphrase_input_node = elem
        h.registerEnterPress(passphrase_input_node, handleSubmitBtnPressed)
      },
      'submit': (elem) => {
        submit_btn_node = elem
        h.registerTapOrClick(submit_btn_node, handleSubmitBtnPressed)
      }
    }, dom)
  })

  h.socketSub(dsocket, inter.ACCOUNT_PASSPHRASE_ATTEMPT_RESULT_READY, handleAccountPassphraseAttemptResultReady)
  return {
    destroy: () => {
      view.removeApp()
    }
  }
}

const validatePassphrase = (passphrase) => {
  return true
}

const ConversionApp = (mount, dsocket, dom_pubsub, players_registry) => {
  //init
  const players_reader = players_registry.reader

  var passphrase_input_node,
    description_node,
    convert_btn_node
  var first_passphrase_entry = null

  const CONVERT_DESCRIPTION_MSG = "Convert your user into account by supplying a passphrase"
  const CONVERT_CONFIRM = "One more time to confirm"
  const CONVERT_NO_MATCH = "Uh oh, passphrases don't match. Try again!"
  const CONVERT_BTN_MSG = "Convert"
  const CONFIRM_BTN_MSG = "Confirm"
  const view = h.View(
    mount,
    'div',
    [['class', 'conversion-app']],
    (on_mobile) => { return `
      <h3>Convert</h3>
      <p nakama='description'>${CONVERT_DESCRIPTION_MSG}</p>
      Passphrase: <input type='password' nakama='passphrase-input'/>
      <button nakama='convert-btn'>${CONVERT_BTN_MSG}</button>
    `}
  )

  const handleConvertBtnPressed = () => {
    const passphrase = passphrase_input_node.value
    passphrase_input_node.value = ""
    if (validatePassphrase(passphrase)) {
      if (first_passphrase_entry) {
        if (first_passphrase_entry === passphrase) {
          dsocket.emit(inter.I_SUBMITTED_CONVERT_TO_ACCOUNT_REQUEST, passphrase)
        } else {
          first_passphrase_entry = null
          description_node.innerHTML = CONVERT_NO_MATCH
          convert_btn_node.innerHTML = CONVERT_BTN_MSG
        }
      } else {
        first_passphrase_entry = passphrase
        description_node.innerHTML = CONVERT_CONFIRM
        convert_btn_node.innerHTML = CONFIRM_BTN_MSG
      }
    } else {
      //maybe show complexity requirements?
    }
  }

  view.renderApp((dom) => {
    h.bindAttributes('nakama', {
      'passphrase-input': (elem) => {
        passphrase_input_node = elem
        h.registerEnterPress(passphrase_input_node, handleConvertBtnPressed)
      },
      'description': (elem) => {
        description_node = elem
      },
      'convert-btn': (elem) => {
        convert_btn_node = elem
        h.registerTapOrClick(elem, handleConvertBtnPressed)
      }
    }, dom)
  })
  return {
    destroy: () => {
      view.removeApp()
    }
  }
}

const LogoutApp = (mount, dsocket, dom_pubsub, players_registry, anonymous_user_allowed) => {
  const view = h.View(
    mount,
    'div',
    [],
    () => { return `
      <h3>Logout</h3>
      <button nakama='logout-btn'>Logout</button>
      <div>${anonymous_user_allowed ? '(Note that after logging out, you\'ll be assigned a new user)' : ''}</div>
    `}
  )
  view.renderApp((dom) => {
    h.bindAttributes('nakama', {
      'logout-btn': (elem) => {
        //TODO: Send an event for this
      }
    }, dom)
  })
}

const UserStatusApp = (mount, dsocket, dom_pubsub, players_registry) => {
  // init values
  const players_reader = players_registry.reader
  var logged_in = players_reader.isLoggedIn()

  var name_select_app

  var registered_status_node

  const updateRegistrationStatus = () => {
    if (logged_in) {
      registered_status_node.innerHTML = 'Logged In'
    } else {
      registered_status_node.innerHTML = 'No Account'
    }
  }

  const view = h.View(
    mount,
    'div',
    [],
    () => { return `
      <h3>User Settings</h3>
      <div>User Status: 
        <span nakama='registered-status'>
        </span>
      </div>
      <div nakama='name-select-container'></div>
    `}
  )

  const handleAccountConversionRequestSuccessful = () => {
    logged_in = true
    updateRegistrationStatus()
  }

  view.renderApp((dom) => {
    h.bindAttributes('nakama', {
      'name-select-container': (elem) => {
        name_select_app = NameSelectApp(elem, dsocket, dom_pubsub, players_registry)
      },
      'registered-status': (elem) => {
        registered_status_node = elem
        updateRegistrationStatus()
      }
    }, dom)
  })
  dsocket.on(inter.ACCOUNT_CONVERSION_REQUEST_SUCCESSFUL, handleAccountConversionRequestSuccessful)
  return {
    destroy: () => {
    }
  }
}

const visible_display = 'block';
const hidden_display = 'none';
const styles = jss({
  userApp: {
    display: hidden_display,
    overflowY: 'auto'
  }
})

const UserApp = (mount, dsocket, dom_pubsub, players_registry) => {
  //convenience
  const players_reader = players_registry.reader
  const players_writer = players_registry.writer

  var cookie_app,
    conversion_app,
    login_app,
    logout_app,
    user_status_app

  //autoassign false by default
  var cookies_enabled = false
  var allow_conversion_to_account = false
  var anonymous_user_allowed = false

  var user_apps_node

  var destroyWindowHeightListener

  const createUserStatusApp = () => {
    return UserStatusApp(user_apps_node, dsocket, dom_pubsub, players_registry)
  }

  const createLoginApp = () => {
    return LoginApp(user_apps_node, dsocket, dom_pubsub, players_registry)
  }

  const createLogoutApp = () => {
    return LogoutApp(user_apps_node, dsocket, dom_pubsub, players_registry, anonymous_user_allowed)
  }

  const createCookieApp = () => {
    return CookieApp(user_apps_node, dsocket, dom_pubsub, players_registry)
  }

  const createConversionApp = () => {
    return ConversionApp(user_apps_node, dsocket, dom_pubsub, players_registry)
  }

  const view = h.View(
    mount,
    'div',
    [['class', styles.userApp]],
    () => { return `
      <h2>User Settings</h2>
      <div nakama="user-apps"></div>
    `}
  )

  const handleSyncedServerConfig = (server_config) => {

    if (cookies_enabled !== server_config.cookies_enabled) {
      cookies_enabled = server_config.cookies_enabled
      if (cookies_enabled) {
        cookie_app = createCookieApp()
      } else {
        cookie_app.destroy()
        cookie_app = null
      }
    }

    const account_config = server_config.account
    anonymous_user_allowed = account_config.anonymous_user_allowed
    if (anonymous_user_allowed && account_config.allow_conversion_to_account) {
      if (allow_conversion_to_account !== account_config.allow_conversion_to_account) {
        allow_conversion_to_account = account_config.allow_conversion_to_account
        if (allow_conversion_to_account && !players_reader.isLoggedIn()) {
          if (!conversion_app) {
            conversion_app = createConversionApp()
          }
        } else {
          if (conversion_app) {
            conversion_app.destroy()
            conversion_app = null
          }
        }

        //Login/Logout
        if (allow_conversion_to_account && !players_reader.isLoggedIn()) { //login
          if (!login_app) {
            login_app = createLoginApp()
          }
          if (logout_app) {
            logout_app.destroy()
            logout_app = null
          }
        } else { //logout
          if (!logout_app) {
            logout_app = createLogoutApp()
          }
          if (login_app) {
            login_app.destroy()
            login_app = null
          }
        }
      }
    }
  }

  const handleAccountConversionRequestSuccessful = (msg) => {
    const sessionid = msg.sessionid
    //registry stuff
    players_writer.convertSelfIntoAccount(sessionid)

    if (conversion_app) {
      conversion_app.destroy()
      conversion_app = null
    }
    if (login_app) {
      login_app.destroy()
      login_app = null
    }
    if (!logout_app) {
      logout_app = createLogoutApp()
    }
  }

  dom_pubsub.sub(intra.SYNCED_SERVER_CONFIG, handleSyncedServerConfig)
  dsocket.on(inter.ACCOUNT_CONVERSION_REQUEST_SUCCESSFUL, handleAccountConversionRequestSuccessful)

  view.renderApp((dom) => {
    h.bindAttributes('nakama', {
      'user-apps': (elem) => {
        user_apps_node = elem
        user_status_app = createUserStatusApp()
      }
    }, dom)
    const calculateUserContainerHeightCss = (window_height) => {
      return (window_height - cssc.measurements.tab_container_offset) + 'px'
    }
    destroyWindowHeightListener = WindowHeight.onWindowResize((new_height) => {
      dom.style.height = calculateUserContainerHeightCss(new_height)
    })
    dom.style.height = calculateUserContainerHeightCss(WindowHeight.getHeight())
  })


  return {
    focus: () => {
      mount.style.display = 'block'
      view.getSelfNode().style.display = visible_display
    },
    hide: () => {
      mount.style.display = hidden_display
      view.getSelfNode().style.display = hidden_display
    }
  }
}

module.exports = UserApp
