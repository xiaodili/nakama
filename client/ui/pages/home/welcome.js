const h = require('../../../../shared/client_helpers.js')
const RoomsListingApp = require('../../components/rooms_listing/rooms_listing.js')
const NameSelectApp = require('../../components/name_select.js')
const jss = require('../../../lib/css.js')
const cssc = require('../../css_constants.js')

const generateStyles = (theme) => {
  return jss({
    widgetContainer: {
      marginBottom: '15px'
    },
    headingContainer: {
      textAlign: 'center'
    },
    welcomeContainer: {
      //width: '100%', //better for Mobile display
      height: '100%',
      marginLeft: '5px',
      marginRight: '5px',
      maxWidth: cssc.measurements.static_screen_max_width_pixels
    }
  })
}


//Can't tell if this smart or the dumbest thing ever
const generateTemplate = (dsocket, dom_pubsub, players_registry, rooms_registry, styles, theme, ui_config_welcome, on_mobile) => {
  let rooms_listing_binding_id = 0
  let name_select_binding_id = 0
  const Widget = {
    'Heading': (params) => {
      return {
        snippet: `<div class='${styles.headingContainer}'>
          <span class='${theme.styles.prebuilt.heading}'>${params.text}</span>
        </div>`,
        bindings: null
      }
    },
    'Name': (params) => {
      let nakama_attribute = `name-select-app-${name_select_binding_id++}`
      return {
        snippet: `<div class='${styles.widgetContainer}' nakama='${nakama_attribute}'></div>`,
        bindings: [{
          type: nakama_attribute,
          fn: (elem) => {
            NameSelectApp(elem, dsocket, dom_pubsub, players_registry, theme, params.text)
          }
        }]
      }
    },
    'RoomListing': (params) => {
      let nakama_attribute = `rooms-listing-app-${rooms_listing_binding_id++}`
      return {
        snippet: `<div class='${styles.widgetContainer}' nakama='${nakama_attribute}'></div>`,
        bindings: [{
          type: nakama_attribute,
          fn: (elem) => {
            RoomsListingApp(elem, dsocket, dom_pubsub, players_registry, rooms_registry, theme, params.text)
          }
        }]
      }
    },
    'P': (params) => {
      return {
        snippet: `<div class='${styles.widgetContainer}'>${params.text}</div>`,
        bindings: null
      }
    },
    'Noop': () => {
      return {
        snippet: '',
        bindings: null
      }
    }
  }
  const Predicate = {
    'onMobile': () => on_mobile
  }
  const Fn = {
    'If': (params) => {
      const {
        condition,
        ifTrue,
        ifFalse
      } = params
      if (condition()) {
        return ifTrue.type(ifTrue.params)
      } else {
        return ifFalse.type(ifFalse.params)
      }
    }
  }
  let template = ''
  const bindTypes = {}
  for (const fragment of ui_config_welcome(Widget, Fn, Predicate)) {
    const {
      bindings,
      snippet
    } = fragment.type(fragment.params)
    template += snippet
    if (bindings) {
      for (const binding of bindings) {
        bindTypes[binding.type] = binding.fn
      }
    }
  }
  return {
    templateFn: () => template,
    bindTypes
  }
}

const WelcomeApp = (mount, dsocket, dom_pubsub, players_registry, rooms_registry, games_registry, ui_config_welcome, theme) => {

  const on_mobile = players_registry.reader.getSelfOnMobile()
  const styles = generateStyles(theme)

  const {
    templateFn,
    bindTypes
  } = generateTemplate(dsocket, dom_pubsub, players_registry, rooms_registry, styles, theme, ui_config_welcome, on_mobile)

  const view = h.View(
    mount,
    'div',
    [['class',styles.welcomeContainer]],
    templateFn)

  //init
  view.renderApp((dom) => {
    h.bindAttributes('nakama', bindTypes, dom)
  })
  return {
    hide: () => {
      mount.style.display = 'none'
      view.hide()
    },
    show: () => {
      mount.style.display = 'flex'
      view.show()
    }
  }
}

module.exports = WelcomeApp
