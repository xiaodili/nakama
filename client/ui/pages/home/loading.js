const h = require('../../../../shared/client_helpers.js')
const jss = require('../../../lib/css.js')

const generateStyles = (theme) => {
  return jss({
    loadingApp: {
      position: 'absolute',
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center',
      display: 'flex',
      height: '100%',
      backgroundColor: theme.colours.background
    }
  })
}

const LoadingApp = (mount, players_reader, theme) => {
  const on_mobile = players_reader.getSelfOnMobile()
  const styles = generateStyles(theme)
  const view = h.View(
    mount,
    'div',
    [['class', styles.loadingApp]],
    (on_mobile) => { return `
      Loading the good stuff...
    `},
    on_mobile
  )

  //init
  view.renderApp()
  return {
    destroy: () => {
      view.removeApp()
    }
  }
}

module.exports = LoadingApp
