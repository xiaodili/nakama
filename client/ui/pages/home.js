const h = require('../../../shared/client_helpers.js')
const css = require('../../lib/css.js')
const cssc = require('../css_constants.js')
//TODO: Rename this to HomeFocuser
const LocalFocuser = require('../../lib/services/focus.js').LocalFocuser

const LoadingApp = require('./home/loading.js')
const WelcomeApp = require('./home/welcome.js')
//TODO: I think this should probably be moved to the ui/ directory
const RoomsApp = require('../../lib/rooms.js')
//TODO: I think this should probably be moved to the home/ directory
const RoomsSidebarApp = require('../components/rooms_sidebar.js')

const visible_display = 'flex';
const hidden_display = 'none';
const styles = css({
  homeApp: {
    display: visible_display,
    height: '100%',
    backgroundColor: cssc.colours.background
  },
  welcomeContainer: {
    height: '100%',
    width: '100%',
    display: 'flex',
    justifyContent: 'center'
  },
  roomsSidebarContainer: {
    //Cribbed off FB Messenger
    //flex: '0 0 25%',
    
    //Cribbed off Slack
    flexShrink: '0',
    flexBasis: '220px',
    maxWidth: '300px', //Not sure this is actually doing anything...

    display: 'flex', //in order for the entire height to be taken up
    minWidth: '200px',
  },
  centralContainer: {
    flex: 3 //flex-grow: 3, flex-shrink: 1, flex-basis: 0%
  },
  centralContainerMobile: {
    width: '100%',
    height: '100%'
  },
  childContainer: {
    height: '100%',
    width: '100%'
  }
})

const getSidebarTemplate = (on_mobile) => {
  if (!on_mobile) {
    return `<div nakama='rooms-sidebar-app' class='${styles.roomsSidebarContainer}'></div>`;
  } else {
    return ''
  }
}

const HomeApp = (mount, dsocket, dom_pubsub, players_registry, rooms_registry, games_registry, router, config, ui_config_home, theme) => {
  const games_reader = games_registry.reader
  const players_reader = players_registry.reader
  const on_mobile = players_reader.getSelfOnMobile()
  const view = h.View(
    mount,
    'div',
    [['class', styles.homeApp]],
    () => { return `
      <div nakama='loading-app'></div>
      ${getSidebarTemplate(on_mobile)}
      <div class="${on_mobile ? styles.centralContainerMobile : styles.centralContainer}">
        <div nakama='welcome-app' class='${styles.welcomeContainer}'>
        </div>
        <div nakama='rooms-app' class='${styles.childContainer}'>
        </div>
      </div>
    `}, on_mobile)

  let welcome_app, rooms_app, loading_app

  view.renderApp((dom) => {
    h.bindAttributes('nakama', {
      'loading-app': (elem) => {
        //TODO: Get rid of players_reader
        loading_app = LoadingApp(elem, players_reader, theme)
      },
      'welcome-app': (elem) => {
        welcome_app = WelcomeApp(elem, dsocket, dom_pubsub, players_registry, rooms_registry, games_registry, ui_config_home.welcome, theme)
      },
      'rooms-sidebar-app': (elem) => {
        RoomsSidebarApp(elem, dsocket, dom_pubsub, players_registry, rooms_registry, theme)

      },
      'rooms-app': (elem) => {
        rooms_app = RoomsApp(elem, dsocket, dom_pubsub, players_registry, rooms_registry, games_registry, config, theme)
      }
    }, dom)
    LocalFocuser(router, dsocket, dom_pubsub, players_registry, rooms_registry, games_reader, rooms_app, welcome_app, config.instance_name)
  })
    
  return {
    focus: () => {
      mount.style.display = 'block'
      view.getSelfNode().style.display = visible_display
    },
    hide: () => {
      mount.style.display = hidden_display
      view.getSelfNode().style.display = hidden_display
    },
    getLoadingApp: () => {
      return loading_app
    },
    getRoomsApp: () => {
      return rooms_app
    }
  }
}

module.exports = HomeApp
