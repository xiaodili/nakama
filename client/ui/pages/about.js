const h = require('../../../shared/client_helpers.js')
const version = require('../../../package.json').version //TODO: Pass this through
const jss = require('../../lib/css.js')
const cssc = require('../css_constants.js')
const WindowHeight = require('../../lib/hacks.js').WindowHeight

const visible_display = 'block';
const hidden_display = 'none';

const default_css = {
  aboutContainer: {
    marginLeft: '5px',
    marginRight: '5px',
    marginBottom: '5px',
    overflowY: 'auto',
    maxWidth: cssc.measurements.static_screen_max_width_pixels
  }
}

const mergeCss = (custom_css) => {
  return Object.assign(
    {},
    default_css,
    custom_css)
}

const AboutApp = (mount, ui_config_about, theme) => {
  const {
    css,
    template
  } = ui_config_about(theme, {
    version
  })
  const styles = jss(mergeCss(css))
  const view = h.View(
    mount,
    'div',
    [['class', styles.aboutContainer]],
    () => {
      return template(styles)
    }
  )
  view.renderApp()

  const calculateMessagesHeightCss = (window_height) => {
    return (window_height - cssc.measurements.page_container_offset ) + 'px'
  }
  //init
  mount.style.height = calculateMessagesHeightCss(WindowHeight.getHeight())
  WindowHeight.onWindowResize((new_height) => {
    mount.style.height = calculateMessagesHeightCss(WindowHeight.getHeight())
  })

  return {
    focus: () => {
      mount.style.display = 'flex'
      view.getSelfNode().style.display = visible_display
    },
    hide: () => {
      mount.style.display = hidden_display
      view.getSelfNode().style.display = hidden_display
    }
  }
}

module.exports = AboutApp
