const h = require('../../../shared/client_helpers.js')
const jss = require('../../lib/css.js')
const intra = require('../../lib/symbols/intra.js')
const links = require('../../lib/links')

const visible_display = 'block'
const hidden_display = 'none'
const styles = jss({
  topSidebarApp: {
    //display: hidden_display, //init
    height: '100%',
    marginLeft: '5px',
    marginRight: '5px'
  },
  linkList: {
    textAlign: 'center'
  },
  handle: {
    fontWeight: 'bold'
  }
})

const TopSidebarApp = (mount, dom_pubsub, players_reader) => {
  //Hiding user app since Log Out isn't implemented yet
  //<li>
    //<a nakama-href='/user'>USER</a>
  //</li>

  const view = h.View(
    mount,
    'div',
    [['class', styles.topSidebarApp]],
    () => { return `
      <div class='${styles.handle}' nakama='handle'></div>
      <div>
        <ul class='${styles.linkList}'>
          <li>
            <a nakama-href='/'>HOME</a>
          </li>
          <li>
            <a nakama-href='/about'>ABOUT</a>
          </li>
        </ul>
      </div>
    `}
  )

  let handle_node
  const setHandle = (handle) => {
    if (handle) {
      handle_node.innerHTML = h.escapeHtml(handle)
      handle_node.display = 'div'
    } else {
      handle_node.display = 'none'
    }
  }

  view.renderApp((dom) => {
    h.bindAttributes('nakama', {
      'handle': (elem) => {
        handle_node = elem
        setHandle(players_reader.getSelfHandle())
      }
    }, dom)
    h.bindAttributes('nakama-href', {
      '/': (elem) => {
        links.goToHome(elem, dom_pubsub)
      },
      '/user': (elem) => {
        links.goToUser(elem, dom_pubsub)
      },
      '/about': (elem) => {
        links.goToAbout(elem, dom_pubsub)
      }
    }, dom)
  })
  const handleSyncedSelfData = (msg) => {
    const handle = msg.handle
    setHandle(handle)
  }
  const handlePlayerChangedHandle = (msg) => {
    if (msg.userid === players_reader.getSelfId()) {
      handle_node.innerHTML = msg.handle
    }
  }

  //subs
  const dom_subs = [
    dom_pubsub.sub(intra.SYNCED_SELF_DATA, handleSyncedSelfData),
    dom_pubsub.sub(intra.PLAYER_CHANGED_HANDLE, handlePlayerChangedHandle)
  ]
  return {
    hide: () => {
      mount.style.display = 'none'
    },
    focus: () => {
      mount.style.display = 'block'
    },
    destroy: () => {
      for (let unsub of dom_subs) {
        unsub()
      }
    }
  }
}

module.exports = TopSidebarApp
