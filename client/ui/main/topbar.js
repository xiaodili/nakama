const h = require('../../../shared/client_helpers.js')
const intra = require('../../lib/symbols/intra.js')
const links = require('../../lib/links')
const jss = require('../../lib/css.js')
const cssc = require('../css_constants.js')

const generateStyles = (theme) => {
  return jss({
    topbar: {
      display: 'flex',
      height: cssc.measurements.topbar_height_pixels,
      justifyContent: 'space-between',
      alignItems: 'center',
      backgroundColor: theme.colours.topbarBackground
    },
    link: {
      margin: '0px 3px'
    },
    links: {
      cursor: 'pointer',
      color: theme.colours.background,
      marginRight: '4px'
    },
    navIcon: {
      cursor: 'pointer'
    },
    lockup: {
      cursor: 'pointer'
    }
  })
}

const menuTemplate = (on_mobile, styles) => {
  if (on_mobile) {
    return `
      <div nakama='nav-icon-container' class='${styles.navIcon}'>
        <img nakama='nav-icon-menu' src='/icon_hamburger.svg'/>
        <img nakama='nav-icon-close' src='/icon_close.svg'/>
      </div>
    `
  } else {
        //Hiding user tab
        //<a class='${styles.link}' nakama-href='/user'>USER</a>
    return `
      <div class='${styles.links}'>
        <a class='${styles.link}' nakama-href='/'>HOME</a>
        <a class='${styles.link}' nakama-href='/about'>ABOUT</a>
      </div>
    `}
}

const TopBarApp = (mount, dom_pubsub, players_reader, theme) => {
  const on_mobile = players_reader.getSelfOnMobile()
  const styles = generateStyles(theme)

  //mob only
  var top_sidebar_shown = false

  const view = h.View(
    mount,
    'div',
    [['class', styles.topbar]],
    (on_mobile) => { return `
      <div nakama='logo' class='${styles.lockup}'>
        <img src='/logo.svg'/>
      </div>
      ${menuTemplate(on_mobile, styles)}
    `}, on_mobile
  )

  var nav_icon_container_node,
    nav_icon_menu_node,
    nav_icon_close_node

  const handleLogoPressed = () => {
    dom_pubsub.pub(intra.LOGO_PRESSED)
  }

  const handleHomePressed = () => {
    dom_pubsub.pub(intra.HOME_PRESSED)
  }

  //mob only
  const handleNavIconPressed = () => {
    if (top_sidebar_shown) {
      nav_icon_menu_node.style.display = 'inline'
      nav_icon_close_node.style.display = 'none'
      top_sidebar_shown = false
    } else {
      nav_icon_menu_node.style.display = 'none'
      nav_icon_close_node.style.display = 'inline'
      top_sidebar_shown = true
    }
    dom_pubsub.pub(intra.TOP_NAV_ICON_PRESSED)
  }

  //mob only
  //always hide sidebar when 
  const handleIPressedLink = () => {
    nav_icon_menu_node.style.display = 'inline'
    nav_icon_close_node.style.display = 'none'
    top_sidebar_shown = false
  }

  const dom_subs = [
    //dom_pubsub.sub(intra.I_PRESSED_LINK, handleIPressedLink)
  ]
  //init
  view.renderApp((dom) => {
    h.bindAttributes('nakama', {
      'logo': (elem) => {
        links.goToHome(elem, dom_pubsub)
        h.registerTapOrClick(elem, handleLogoPressed)
      },
      //mob only (menu stuff)
      'nav-icon-container': (elem) => {
        nav_icon_container_node = elem
        h.registerTapOrClick(nav_icon_container_node, handleNavIconPressed)
        //When you choose a link, you need to hide the sidebar again
        dom_subs.push(dom_pubsub.sub(intra.I_PRESSED_LINK, handleIPressedLink))
      },
      'nav-icon-menu': (elem) => {
        nav_icon_menu_node = elem
      },
      'nav-icon-close': (elem) => {
        nav_icon_close_node = elem
        //Should be dismissed by default
        nav_icon_close_node.style.display = 'none'
      }
    }, dom)
    h.bindAttributes('nakama-href', {
      '/': (elem) => {
        links.goToHome(elem, dom_pubsub)
        h.registerTapOrClick(elem, handleHomePressed)
      },
      '/user': (elem) => {
        links.goToUser(elem, dom_pubsub)
      },
      '/about': (elem) => {
        links.goToAbout(elem, dom_pubsub)
      }
    }, dom)
  })
  return {
    destroy: () => {
      for (let unsub of dom_subs) {
        unsub()
      }
    }
  }
}

module.exports = TopBarApp
