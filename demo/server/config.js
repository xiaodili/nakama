const path = require('path')
const dist_dir = path.join(__dirname, '../dist')
module.exports = {
  abs_assets_path: path.join(dist_dir, 'assets'),
  abs_apps_path: path.join(dist_dir, 'apps'),
  client: {
    abs_root_path: path.join(dist_dir, 'client'),
    protocol: "http",
    port: 8080
  }
}
