// [{userid, handle}...], creatorid, Input, Output, External
module.exports = (creatorid, _players, input, output, external) => {
  var players = {}
  for (const player of _players) {
    const id = player.id
    players[id] = player
  }
  //inputs:
  input.on("END_GAME", () => {
    external.endGame()
  })
  return {
    destroy: () => {},
    onPlayerSentMessage: (userid, msg) => {
      const prefix = '(chatbot)'
      const maybe_greeting = msg.toLowerCase()
      if (maybe_greeting === "hi" || maybe_greeting === "hello") {
        external.sendMessage(`${prefix} hi ${players[userid].handle}!`)
      } else {
        external.sendMessage(`${prefix} I don't understand '${msg}'`)
      }
    },
    onPlayerLeft: (userid) => {
      delete players[userid]
    },
    onPlayerJoined: (player) => {
      players[player.id] = player
    },
    onPlayerChangedHandle: (userid, handle) => {
      players[userid].handle = handle
    }
  }
}
