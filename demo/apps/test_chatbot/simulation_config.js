const server = require('./server.js')
const client = require('./client.js')
const title = require('./metadata.js').title
const description = require('./metadata.js').description

module.exports = {
  server, client, title, description
}
