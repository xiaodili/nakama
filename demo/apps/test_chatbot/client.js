const registerTapOrClick = (elem, fn) => {
  elem.addEventListener("click", (event) => {
    event.preventDefault()
    fn()
  }, false)
  elem.addEventListener("touchstart", (event) => {
    event.preventDefault()
    fn()
  }, false)
}

const qs = (selector, scope) => {
  return (scope || document).querySelector(selector)
}

module.exports = (mount, self, input, output) => {
  mount.innerHTML = `<div>
      <div>Running highly intelligent chatbot (try saying 'hi' or 'hello' in the messages tab!)</div>
      <div><button chatbot='stop'>Stop</button></div>
    </div>`
    registerTapOrClick(qs("[chatbot=stop]", mount), () => {
      output.emit("END_GAME")
    })
  return {
    destroy: () => {}
  }
}
