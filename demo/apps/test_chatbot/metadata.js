module.exports = {
  title: "Test Chatbot",
  url: "test_chatbot",
  description: "Highly intelligent chatbot",
  to_publish: true,
  client_path: "./client.js",
  server_path: "./server.js"
}
