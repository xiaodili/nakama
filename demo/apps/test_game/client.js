const registerTapOrClick = (elem, fn) => {
  elem.addEventListener("click", (event) => {
    event.preventDefault()
    fn()
  }, false)
  elem.addEventListener("touchstart", (event) => {
    event.preventDefault()
    fn()
  }, false)
}

const qs = (selector, scope) => {
  return (scope || document).querySelector(selector)
}

module.exports = (mount, self, input, output) => {
  const template = () => {
    return `
    <div>
      <span>Hello World!</span>
      <button test="end-game-btn">End Game</button>
    </div>
    `
  }
  mount.innerHTML = template()
  const end_game_btn_node = qs("[test=end-game-btn]", mount)
  registerTapOrClick(end_game_btn_node, () => {
    output.emit("END_GAME")
  })
  return {
    destroy: () => {}
  }
}
