module.exports = {
  title: "Demo Test Game",
  url: "test_game",
  description: "Get ready to be amazed",
  to_publish: true,
  client_path: "./client.js",
  server_path: "./server.js"
}
