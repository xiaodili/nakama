#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOT_DIR=$( dirname $SCRIPT_DIR)
NAKAMA_DIR=$( dirname $ROOT_DIR)
# Required otherwise complains about unknown plugin 'transform-runtime'
export NODE_PATH=$NODE_PATH:$NAKAMA_DIR/node_modules

mkdir -p $ROOT_DIR/dist;
mkdir -p $ROOT_DIR/dist/client;
cp $NAKAMA_DIR/client/index.html $ROOT_DIR/dist/client;

node $NAKAMA_DIR/scripts/watch_client.js $ROOT_DIR/client/index.js $ROOT_DIR/dist/client/bundle.js
