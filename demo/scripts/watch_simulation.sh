#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" ROOT_DIR=$( dirname $SCRIPT_DIR)
NAKAMA_DIR=$( dirname $ROOT_DIR)
# Required otherwise complains about unknown plugin 'transform-runtime'
export NODE_PATH=$NODE_PATH:$NAKAMA_DIR/node_modules

mkdir -p $ROOT_DIR/dist;
mkdir -p $ROOT_DIR/dist/simulation;
cp $NAKAMA_DIR/simulation/index.html $ROOT_DIR/dist/simulation;

echo "starting watcher..."
node $NAKAMA_DIR/scripts/watch_simulation.js $ROOT_DIR/simulation/index.js $ROOT_DIR/dist/simulation/bundle.js
