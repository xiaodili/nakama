#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOT_DIR=$( dirname $SCRIPT_DIR)
NAKAMA_DIR=$( dirname $ROOT_DIR)
BROWSERIFY_BIN=$NAKAMA_DIR/node_modules/browserify/bin/cmd.js
# Required otherwise complains about unknown plugin 'transform-runtime'
export NODE_PATH=$NODE_PATH:$NAKAMA_DIR/node_modules

echo "creating $ROOT_DIR/dist directories if they don't already exist..."
mkdir -p $ROOT_DIR/dist;
mkdir -p $ROOT_DIR/dist/games;
mkdir -p $ROOT_DIR/dist/client;

echo "copying over assets and .html files from $NAKAMA_DIR..."
cp $NAKAMA_DIR/client/index.html $ROOT_DIR/dist/client;

echo "building apps..."
$SCRIPT_DIR/build_apps.sh

#https://github.com/browserify/browserify/issues/1540
echo "building server..."
$SCRIPT_DIR/build_server.sh

echo "building client..."
node $NAKAMA_DIR/scripts/build_client.js $ROOT_DIR/client/index.js $ROOT_DIR/dist/client/bundle.js

echo "build finished!"
