#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOT_DIR=$( dirname $SCRIPT_DIR)
NAKAMA_DIR=$( dirname $ROOT_DIR)

APPS_SRC_DIR=$ROOT_DIR/apps
APPS_DST_DIR=$ROOT_DIR/dist/apps

# Using Nakama's node_modules as well:
export NODE_PATH=$NODE_PATH:$NAKAMA_DIR/node_modules
node $NAKAMA_DIR/scripts/build_apps.js $APPS_SRC_DIR $APPS_DST_DIR
