#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOT_DIR=$( dirname $SCRIPT_DIR)
NAKAMA_DIR=$( dirname $ROOT_DIR)
BROWSERIFY_BIN=$NAKAMA_DIR/node_modules/browserify/bin/cmd.js
# Required otherwise complains about unknown plugin 'transform-runtime'
export NODE_PATH=$NODE_PATH:$NAKAMA_DIR/node_modules

mkdir -p $ROOT_DIR/dist;
cp -rf $NAKAMA_DIR/assets $ROOT_DIR/dist;

node $NAKAMA_DIR/scripts/build_server.js $ROOT_DIR/server/index.js $ROOT_DIR/dist/server.js
