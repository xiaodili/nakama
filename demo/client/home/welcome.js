//Cribbing off the cloudformation format for now
//https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/intrinsic-function-reference-conditions.html
module.exports = (Widget, Fn, Predicate) => {
  return [
    {type: Widget.Heading,
      params: {
        text: 'WELCOME'
      }},
    {type: Widget.P,
      params: {
        text: 'This is the landing page. You can change what gets shown here, e.g. room listings or interface for changing names'
      }},
    {type: Widget.Heading,
      params: {
        text: 'YOURSELF'
      }},
    {type: Widget.Name,
      params: {
        text: 'You are currently known as'
      }},
    {type: Fn.If,
      params: {
        condition: Predicate.onMobile,
        ifTrue: {
          type: Widget.RoomListing,
          params: {
            text: 'AVAILABLE ROOMS'
          }},
        ifFalse: {
          type: Widget.Noop
        }
      }}
  ]
}
