const AboutPage = (theme, core) => {
  return {
    css: {
      mainContainer: {
        display: 'flex',
        height: '100%',
        flexDirection: 'column'
      },
      mainContents: {
        flex: 1
      },
      footer: {
        fontSize: 'x-small',
        color: '#aaaaaa',
        textAlign: 'center'
      }
    },
    template: (styles) => {
      return `<div class'${styles.mainContainer}'>
          <div>This is a customisable 'About' page. You can put whatever you want in here.</div>
          <div class='${styles.footer}'>Nakama v${core.version}</div>
        </div>`
    }
  }
}
module.exports = AboutPage
