const NakamaClient = require('../../client/index.js')

//UI tree
const AboutPage = require('./about.js')
const Welcome = require('./home/welcome.js')

//nonUI config
const config = require('./config.js')

const theme = {
  colours: {
    primary: '#3aafb4',
    secondary: '#dc5066',
    topbarBackground: '#50504e',
    background: '#f1f0f1'
  },
  fonts: {
    heading: {
      fontFamily: `'Ubuntu', sans-serif`,
      fontWeight: 'bold'
    },
    subHeading: {
      fontFamily: `'Ubuntu', sans-serif`

    },
    link: {
      fontFamily: `'Ubuntu', sans-serif`
    }
  }
}

NakamaClient(config, {
  resources: [
    'https://fonts.googleapis.com/css?family=Ubuntu:400,700'
  ],
  theme: {
    fonts: {
      heading: theme.fonts.heading,
      subHeading: theme.fonts.subHeading,
      link: theme.fonts.link
    },
    colours: {
      primary: theme.colours.primary,
      secondary: theme.colours.secondary,
      topbarBackground: theme.colours.topbarBackground,
      background: theme.colours.background
    }
  },
  pages: {
    about: AboutPage,
    home: {
      welcome: Welcome
    }
  }
})
