//Nevent Types

const nevents = {
  IM: "IM",
  NOTIFICATION: "NOTIFICATION",
  NSON: "NSON"
}

//Notification Types
const notifications = {
  APP_CRASHED: 'APP_CRASHED',
  APP_ENDED: 'APP_ENDED',
  APP_STARTED: 'APP_STARTED',
  PLAYER_WAS_KICKED: 'PLAYER_WAS_KICKED',
  PLAYER_CHANGED_HANDLE: 'PLAYER_CHANGED_HANDLE',
  USER_LEFT: 'USER_LEFT',
  USER_JOINED: 'USER_JOINED',
  USER_NOMINATED_ADMIN: 'USER_NOMINATED_ADMIN',
  USER_WAS_KICKED: 'USER_WAS_KICKED'
}

const nsons = {
  MESSAGE: "MESSAGE",
  CHAT_MESSAGE: "CHAT_MESSAGE"
}

//TODO: shouldn't userid be here as well?
const serialiseImNevent = (userid, handle, message) => {
  return {
    type: nevents.IM,
    params: {
      userid,
      handle,
      message,
      time: (new Date()).getTime()
    }
  }
}

//This can come from slash commands and apps
const serialiseNsonNevent = (nson) => {
  return {
    type: nevents.NSON,
    params: nson
  }
}

const serialiseNotificationNevent = (notification_type, payload) => {
  switch (notification_type) {
    case notifications.APP_CRASHED: {
      const app_name = payload
      return {
        type: nevents.NOTIFICATION,
        params: {
          type: notifications.APP_CRASHED,
          app_name: payload,
          time: (new Date()).getTime(),
          content: `${app_name} has crashed`
        }
      }
    }
    case notifications.APP_ENDED: {
      const app_name = payload
      return {
        type: nevents.NOTIFICATION,
        params: {
          type: notifications.APP_ENDED,
          app_name: payload,
          time: (new Date()).getTime(),
          content: `${app_name} has ended`
        }
      }
    }
    case notifications.APP_STARTED: {
      const {
        app_name,
        handle,
        userid
      } = payload
      return {
        type: nevents.NOTIFICATION,
        params: {
          type: notifications.APP_STARTED,
          app_name,
          handle,
          userid,
          time: (new Date()).getTime(),
          content: `${handle} has started ${app_name}`
        }
      }
    }
    case notifications.USER_JOINED: {
      const {
        userid,
        handle
      } = payload
      return {
        type: nevents.NOTIFICATION,
        params: {
          type: notifications.USER_JOINED_ROOM,
          handle,
          userid,
          time: (new Date()).getTime(),
          content: `${handle} joined`
        }
      }
    }
    case notifications.USER_LEFT: {
      const {
        userid,
        handle
      } = payload
      return {
        type: nevents.NOTIFICATION,
        params: {
          type: notifications.USER_LEFT,
          handle,
          userid,
          time: (new Date()).getTime(),
          content: `${handle} left`
        }
      }
    }
    case notifications.PLAYER_WAS_KICKED: {
      const {
        kickerid,
        kicker,
        kickee,
        kickeeid
      } = payload
      return {
        type: nevents.NOTIFICATION,
        params: {
          type: notifications.PLAYER_WAS_KICKED,
          kickerid,
          kicker,
          kickee,
          kickeeid,
          time: (new Date()).getTime(),
          content: `${kickee} was kicked by ${kicker}`
        }
      }
    }
    case notifications.PLAYER_CHANGED_HANDLE: {
      const {
        old_handle,
        handle,
        userid
      } = payload
      return {
        type: nevents.NOTIFICATION,
        params: {
          old_handle,
          handle,
          userid,
          time: (new Date()).getTime(),
          content: `${old_handle} is now known as ${handle}`
        }
      }
    }
    case notifications.USER_NOMINATED_ADMIN: {
      const {
        handle,
        userid
      } = payload
      return {
        type: nevents.NOTIFICATION,
        params: {
          handle,
          userid,
          time: (new Date()).getTime(),
          content: `${handle} was appointed as the new admin`
        }
      }
    }
    default: {
      console.log("notification_type: ", notification_type);
      console.log("payload: ", payload);
    }
  }
}

//Types
exports.notifications = notifications
exports.nevents = nevents
exports.nsons = nsons

//New style 'serialisation' functions
exports.serialiseNotificationNevent = serialiseNotificationNevent
exports.serialiseImNevent = serialiseImNevent
exports.serialiseNsonNevent = serialiseNsonNevent
