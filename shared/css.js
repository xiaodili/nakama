const jss = require('jss').default
const preset = require('jss-preset-default').default
jss.setup(preset())

//https://github.com/cssinjs/jss#example
const createJssClasses = (styles) => {
  return jss.createStyleSheet(styles).attach().classes
}

module.exports = createJssClasses
