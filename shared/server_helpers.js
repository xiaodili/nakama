//By simulation and server
//Session generation, from http://stackoverflow.com/questions/6599470/node-js-socket-io-with-ssl
var makeId = (size) => {
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
  return (new Array(size).fill()).map((elem) => {return possible.charAt(Math.floor(Math.random() * possible.length))}).join("")
}

const generateRandomHandle = () => {
  const adjs = ["cool", "tasty", "serene", "rad", "nice", "sweet", "groovy", "sleepy", "angry", "cute", "bashful", "slow", "timid", "coy", "awkward", "snoop", "soft", "warm", "happy", "speedy", "foxy", "wet"]
  const names = ["cat", "dog", "sloth", "koala", "monkey", "ferret", "lemur", "wombat", "owl", "llama", "badger", "turkey", "goose", "ewe", "turtle", "kitty", "bird", "fox", "raccoon", "weasel", "cheetah", "squirrel", "squid", "stoat", "ostrich"]
  return adjs[Math.floor(Math.random() * adjs.length)] + "_" + names[Math.floor(Math.random() * names.length)]
}

exports.makeId = makeId
exports.generateRandomHandle = generateRandomHandle
