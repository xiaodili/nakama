//DOM
//https://github.com/tastejs/todomvc/blob/gh-pages/examples/vanillajs/js/helpers.js
const qs = (selector, scope) => {
  return (scope || document).querySelector(selector)
}

const qsa = (selector, scope) => {
  return (scope || document).querySelectorAll(selector)
}

// <string> → <string>
const getAttrValue = (attr) => {
  return attr.split(':')[0]
};

// 'data-lobby', {'name-val' → λ, ...}, dom_element  → void
const bindAttributes = (attribute, bindTypes, dom) => {
  const query = `[${attribute}]`
  const elems = qsa(query, dom)

  //not a real array; can't use for (let...of)
  for (let i = 0; i < elems.length; i++) {
    let elem = elems[i]
    let attr = elem.getAttribute(attribute)
    let bind_type = getAttrValue(attr)
    bindTypes[bind_type](elem)
    //try {
    //bindTypes[bind_type](elem);
    //} catch(e) {
    //console.error("couldn't bind to: ", bind_type);
    //console.log("error: ", e);
    //}
  }
}

// 'div', [[name, value]...], '<span>...</span>' → dom_element
const createNode = (tag_name, attributes, node_body) => {
  const node = document.createElement(tag_name)
  for (let attribute of attributes) {
    node.setAttribute(...attribute)
  }
  node.innerHTML = node_body
  return node
}

//Validation
//From mustache.js:
//https://github.com/janl/mustache.js/blob/eae8aa3ba9396bd994f2d5bbe3b9fc14d702a7c2/mustache.js#L60
const entityMap = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#39;',
  '/': '&#x2F;',
  '`': '&#x60;',
  '=': '&#x3D;'
}

const escapeHtml = (string) => {
  return String(string).replace(/[&<>"'`=\/]/g, (s) => {
    return entityMap[s]
  })
}

//Mobile/Desktop shimming
//http://tutorials.jenkov.com/responsive-mobile-friendly-web-design/touch-events-in-javascript.html
//Rendering
//Using hash table as a poor man's virtual DOM for nested items
const ItemRenderer = ( getId = (item) => {return item.getId()}, render = (item) => {item.render()}, remove = (item) => {item.remove()}) => {
  const items = {}
  return {
    //might need this for inactive players
    //getItem: (id) => {
    //return items[id]
    //},
    renderItems: (updated_items) => {
      //removing
      Object.keys(items).forEach((id) => {
        let item = items[id]
        if (!(id in updated_items)) {
          remove(item)
          delete items[id]
        }
      })
      //rendering
      Object.keys(updated_items).forEach((id) => {
        let item = updated_items[id]
        if (!(id in items)) {
          render(item)
          items[id] = item
        }
      })
    }
  }
}

const registerTapOrClick = (elem, fn) => {
  elem.addEventListener("click", (event) => {
    event.preventDefault()
    fn()
  }, false)
  elem.addEventListener("touchstart", (event) => {
    event.preventDefault()
    fn()
  }, false)
}

const registerEnterPress = (elem, fn) => {
  const checkForEnter = (event) => {
    if (event.keyCode === 13) {
      fn()
    }
  }
  elem.addEventListener("keypress", checkForEnter)
}

const View = (parent, container_elem, attributes, templateFn, on_mobile = false) => {
  var self_node
  var visible_display
  const dom = templateFn(on_mobile)
  return {
    show: () => {
      self_node.style.display = visible_display
    },
    hide: () => {
      self_node.style.display = "none"
    },
    getSelfNode: () => {
      return self_node
    },
    renderApp: (bind) => {
      self_node = createNode(container_elem, attributes, dom)
      parent.appendChild(self_node)
      if (bind) {
        bind(self_node)
      }
      visible_display = self_node.style.display
    },
    removeApp: () => {
      parent.removeChild(self_node)
    }
  }
}

const ChildApps = () => {
  const apps = []
  return {
    add: (app) => {
      apps.push(app)
      const index = apps.length - 1
      return () => {
        app.destroy()
        delete apps[index]
      }
    },
    destroyAll: () => {
      apps.forEach((app) => {
        app.destroy()
      })
    }
  }
}

//Adapted from http://kosbie.net/cmu/fall-12/15-237/handouts/notes-ajax-json-rest.html
const get = (url, callback) => {
  const xhr = new XMLHttpRequest()
  xhr.open("GET", url)
  xhr.onreadystatechange = () => {
    if (xhr.readyState === 4) {
      callback(JSON.parse(xhr.responseText), xhr.status)
    }
  }
  xhr.send()
}

const post = (url, jsondata, callback) => {
  const xhr = new XMLHttpRequest()
  xhr.open("POST", url)
  xhr.setRequestHeader('Content-Type', 'application/json')
  xhr.onreadystatechange = () => {
    if (xhr.readyState === 4) {
      callback(JSON.parse(xhr.responseText), xhr.status)
    }
  }
  xhr.send(JSON.stringify(jsondata))
}

//Tree based server/lib/rest_api.js style routing to deal with arbitrary paths
const Router = () => {
  const PathNode = () => {
    const children = {}
    let createTitle,
      onChange
    return {
      hasHandlers: () => {
        return (createTitle && onChange)
      },
      getHandlers: () => {
        return [createTitle, onChange]
      },
      storeHandlers: (_createTitle, _onChange) => {
        createTitle = _createTitle
        onChange = _onChange
      },
      storeChild: (key, pnode) => {
        children[key] = pnode
      },
      hasChild: (key) => {
        return (key in children)
      },
      getChildKeys: () => {
        return Object.keys(children)
      },
      //<string> → [<string>, <PathNode>]
      getMatchingChildren: (key) => {
        return Object.keys(children).filter((child_key) => {
          if (isParameter(child_key)) {
            return true
          } else if (child_key === key) {
            return true
          } else {
            return false
          }
        }).map((child_key) => {
          return [child_key, children[child_key]]
        })
      },
      getChild: (key) => {
        return children[key]
      }
    }
  }
  const root = PathNode()
  let default_route

  const _store = (nodes_to_traverse, current_node, createTitle, onChange) => {
    if (nodes_to_traverse.length === 0) { //terminating condition
      current_node.storeHandlers(createTitle, onChange)
    } else {
      const next_node = nodes_to_traverse[0]
      const remaining_nodes = nodes_to_traverse.slice(1)
      if (current_node.hasChild(next_node)) {
        _store(remaining_nodes, current_node.getChild(next_node), createTitle, onChange)
      } else {
        const child = PathNode()
        current_node.storeChild(next_node, child)
        _store(remaining_nodes, child, createTitle, onChange)
      }
    }
  }

  const _traverse = (nodes_to_traverse, current_node) => {
    if (nodes_to_traverse.length === 0) { //terminating condition
      return current_node.getHandlers()
    } else {
      const next_node = nodes_to_traverse[0]
      const remaining_nodes = nodes_to_traverse.slice(1)
      const parameter = current_node
      if (parameter)
      if (next_node) {
        return _traverse(remaining_nodes, next_node)
      } else {
      }
    }
  }

  //<string> → <bool>
  const isParameter = (path_node) => {
    return (path_node[0] === '{' && path_node[path_node.length - 1] === '}')
  }

  //path <string> → [node <string>...]
  const pathToNodes = (path) => {
    return path.split('/').map((node) => {
      return unescape(node)
    })
  }

  //DFS
  //[node <string>...] → [createTitle, onChange, param...]
  const matchPath = (nodes_to_traverse) => {
    const paths_to_try = [{
      current_node: root,
      params: [],
      nodes_to_traverse
    }]
    while (paths_to_try.length > 0) {
      const {
        current_node,
        params,
        nodes_to_traverse
      } = paths_to_try.pop()
      if (nodes_to_traverse.length === 0) { //terminating condition
        if (current_node.hasHandlers()) {
          return [...current_node.getHandlers(), ...params]
        }
      } else {
        const next_node = nodes_to_traverse[0]
        const remaining_nodes = nodes_to_traverse.slice(1)
        for (const [key, child_node] of current_node.getMatchingChildren(next_node)) {
          paths_to_try.push({
            current_node: child_node,
            params: isParameter(key) ? [...params, next_node] : params,
            nodes_to_traverse: remaining_nodes
          })
        }
      }
    }
    //still not got anything :(
    return []
  }


  //<string>, <bool> → <bool>
  const _route = (path, silent) => {
    const [createTitle, onChange, ...params] = matchPath(pathToNodes(path))
    if (createTitle && onChange) {
      let title
      if (typeof createTitle === 'function') {
        title = createTitle(...params)
      } else {
        title = createTitle
      }
      document.title = title
      if (!silent) {
        window.history.pushState({
            location: path
          },
          title,
          path)
      }
      onChange(...params)
      return true
    } else {
      return false
    }
  }

  //Back behaviour
  window.onpopstate = (event) => {
    const last_state = event.state
    if (last_state) {
      if ('location' in last_state) {
        _route(last_state.location, true)
      }
    } else { //go home
      _route('/', true)
    }
  }

  return {
    //<string>, <string> | <λ(* → <string>)>, <λ> → ()
    registerRoute: (path, createTitle, onChange) => {
      _store(pathToNodes(path), root, createTitle, onChange)
    },
    registerDefault: (path) => {
      default_route = path
    },
    goToRoute: (path) => {
      _route(path, false)
    },
    goToRouteSilently: (path) => { //e.g. for when creating shortlinks. Don't trigger anything
      window.history.pushState({
        location: path},
        window.document.title,
        path)
    },
    init: () => {
      //TODO: doesn't do hash urls
      const initial_route = window.location.pathname
      if (!_route(initial_route, true)) {
        console.warn("couldn't find route, using default");
        _route(default_route, true)
      }
    }
  }
}

//keeping old router for posterity (only matches one level deep, no param matching)
const RouterDeprecated = () => {
  const pageChangeHandlers = {
  }

  //Back behaviour
  window.onpopstate = (event) => {
    const last_state = event.state
    if (last_state) {
      if ('location' in last_state) {
        pageChangeHandlers[last_state['location']](true)
      }
    } else { //go home
      pageChangeHandlers['/'](true)
    }
  }

  return {
    registerRoute: (route, title, fn) => {
      pageChangeHandlers[route] = (silent) => {
        //http://stackoverflow.com/questions/26324990/title-of-history-pushstate-is-unsupported-whats-a-good-alternative
        document.title = title
        if (!silent) {
          window.history.pushState({
              location: route
            },
            title,
            route)
        }
        fn()
      }
    },
    goToRoute: (route) => {
      pageChangeHandlers[route]()
    },
    //finish registering routes, then call this
    init: () => {
      //http://stackoverflow.com/questions/4758103/last-segment-of-url
      const page = '/' + window.location.pathname.split("/").pop()
      if (page in pageChangeHandlers) {
        pageChangeHandlers[page](true)
      }
    }
  }
}

//wrapper for removing socket.io subscriptions off dynamic UI components
const socketSub = (socket, event, listener) => {
  socket.on(event, listener)
  return () => {
    socket.off(event, listener)
  }
}

//Hack time formatters in lieu of Moment.js
//unix_time <int> → <HH:MM>
const unixToHourMinTime = (unix_time) => {
  //pads a digit to 2 characters
  //<int> → <string>
  const padLeftWithZero = (quantity) => {
    if (quantity < 10) {
      return "0" + quantity
    } else {
      return quantity.toString()
    }
  }

  const time = (new Date(unix_time))
  const hours = padLeftWithZero(time.getHours())
  const minutes = padLeftWithZero(time.getMinutes())
  return `${hours}:${minutes}`
}

//http://blog.blakesimpson.co.uk/read/42-append-an-html-string-to-the-dom-without-a-javascript-library
const htmlToNode = (html) => {
  const div = document.createElement('div')
  div.innerHTML = html
  return div.firstChild
}

exports.qs = qs
exports.qsa = qsa
exports.bindAttributes = bindAttributes
exports.createNode = createNode
exports.ItemRenderer = ItemRenderer
exports.escapeHtml = escapeHtml
exports.registerTapOrClick = registerTapOrClick
exports.registerEnterPress = registerEnterPress
exports.View = View
exports.ChildApps = ChildApps
exports.post = post
exports.get = get
exports.Router = Router
exports.socketSub = socketSub
exports.unixToHourMinTime = unixToHourMinTime
exports.htmlToNode = htmlToNode
