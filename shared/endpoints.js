//Source: admin/app.js
//GET
//Sink: players_registry.api.adminStatus
//Response: <bool>
exports.GET_ADMIN_STATUS = '/api/admin/status'

//Source: admin/app.js
//POST {session_id}
//Sink: players_registry.api.sessionValidity
//<bool> 200
exports.CHECK_SESSION_VALIDITY = '/api/admin/session/status'

//Source: admin/ui/login.js
//POST {handle, passphrase}
//Sink: api/auth.js
//{session_id <string>, userid, config_for_client} 200
//null 401
exports.LOGIN = '/api/users/login'

//Source: admin/ui/login.js
//POST {userid, session_id}
//Sink: api/auth.js
//"sucessfully logged out" 200
//"invalid session or user id" 400
exports.LOGOUT = '/api/users/logout'

//Source: client/room_item/passphrase.js
//POST {passphrase}
//Sink: api/auth.js
//<bool> 200
//null 400
exports.ROOM_CHALLENGE = '/api/rooms/{roomid}/challenge'
exports.constructRoomChallenge = (roomid) => {
  return `/api/rooms/${roomid}/challenge`
}

//I'm pretty sure we can use the same login for both admin and regular, but let's keep it lying around for now.
//Source: admin/ui/login.js
//POST {handle, passphrase}
//Sink: api/auth.js
//{session_id <string>, userid, config_for_client} 200
//null 401
exports.ADMIN_LOGIN = '/api/admin/login'

//Source: admin/ui/setup.js
//POST {handle, passphrase}
//Sink: api/auth.js
//{session_id <string>, userid, config_for_client} 200
//HANDLE_TAKEN 400
exports.ADMIN_REGISTER = '/api/admin/register'

//TODO: make this admin only?
//Source: curl
//PUT
//Sink: api/snapshot.js
exports.SNAPSHOT = '/api/snapshots/{snapshot_name}'

//TODO: make this admin only?
//Source: curl
//GET
//Sink: api/snapshot.js
exports.SNAPSHOT = '/api/snapshots/{snapshot_name}'

//TODO: make this admin only?
//Source: curl
//GET
//Sink: api/snapshot.js
exports.SNAPSHOTS = '/api/snapshots'

//TODO: make this admin only?
//Source: curl
//POST
//Sink: api/snapshot.js
exports.SNAPSHOT_RESTORE = '/api/snapshots/{snapshot_name}/restore'
