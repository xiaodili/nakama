//Patterns
//https://davidwalsh.name/pubsub-javascript
//Used by client, simulation/server.js
const PubSub = () => {
  const channels = {}
  return {
    sub: (channel, fn) => {
      if (!(channel in channels)) {
        channels[channel] = []
      }
      channels[channel].push(fn)
      const index = channels[channel].length - 1
      return () => {
        delete channels[channel][index]
      }
    },
    pub: (channel, ...args) => {
      if (channel in channels) {
        let triggers = channels[channel]
        triggers.forEach((trigger) => {
          trigger(...args)
        })
      }
    }
  }
}

// longer than 0
// shorter than 18
// can only contain A-Z, a-z, 0-9, -, _
const handleValidate = (handle) => {
  if (handle.length > 0 && handle.length < 18 &&  /^[A-Za-z0-9-_]+$/.test(handle)) {
    return true
  } else {
    return false
  }
}

const roomNameValidate = (room_name) => {
  return true
}

// Set(elem), Set(elem) → Set(elem)
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set
const difference = (s1, s2) => {
  const diff = new Set(s1)
  for (let x_i of s2) {
    diff.delete(x_i)
  }
  return diff
}

// Set(elem), Set(elem) → Set(elem)
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set
const intersection = (s1, s2) => {
  return new Set([...s1].filter((x_i) => {
    return s2.has(x_i)
  }))
}

//<Object>, λ(key <*> → <*>) → <Object>
const objectMapValues = (obj, fn) => {
  //http://stackoverflow.com/a/38829074/1010076
  return Object.assign({}, ...Object.keys(obj).map((key) => ({
    [key]: fn(obj[key])
  })))
}

//<Object>, λ(value <*> → <*>) → <Object>
const objectMapKeys = (obj, fn) => {
  //http://stackoverflow.com/a/38829074/1010076
  return Object.assign({}, ...Object.keys(obj).map((key) => ({
    [key]: fn(key)
  })))
}

exports.PubSub = PubSub
exports.handleValidate = handleValidate
exports.roomNameValidate = roomNameValidate
exports.difference = difference
exports.intersection = intersection
exports.objectMapValues = objectMapValues
exports.objectMapKeys = objectMapKeys
