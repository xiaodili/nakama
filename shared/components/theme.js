const h = require('../client_helpers.js')
const shuffle = require('../../shared/helpers.js').shuffle

const e = {
  THEME_SET: "THEME_SET",
  GAME_STARTED: "GAME_STARTED"
}

var randomLetter = () => {
  const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
  return alphabet[Math.floor(Math.random() * alphabet.length)]
}

const ThemeAppView = (parent) => {
  var self_node
  var theme_examples = ["ex-Huddlers", "Mythical creatures", "Robots", "Superpeople", "Pokemon", "Harry Potter", `Beginning with ${randomLetter()}`]
  shuffle(theme_examples)
  const theme_example = theme_examples[0]
  const template = () => {
    return [
      'div',
      [],
      `
        <div>
          <span theme='copywrite'></span>
          <span theme='theme'></span>
          <input type="text" theme="set-theme-input" placeholder="e.g. ${theme_example}"></input>
          <button theme='set-theme-btn'>Set Theme</button>
          <button theme='action-btn'></button>
        </div>
      `
    ]
  }
  return {
    renderApp: (bind) => {
      self_node = h.createNode(...template())
      parent.appendChild(self_node)
      bind(self_node)
    },
    removeApp: () => {
      parent.removeChild(self_node)
    }
  }
}

const ThemeApp = (mount, _theme, _started, input, output) => {
  const view = ThemeAppView(mount)
  const UNSET_MSG = "Suggest a theme: "
  const SET_MSG = "Theme: "
  var theme = ""
  var started = _started
  var copywrite_node, theme_node, set_theme_input_node, set_theme_btn_node, action_btn_node
  var editing = false

  const handleThemeSet = (_theme) => {
    if (editing === false) {
      if (theme.length > 0 && _theme.length === 0) {
        copywrite_node.innerHTML = UNSET_MSG
        set_theme_btn_node.style.display = "inline-block"
      } else if (theme.length === 0 && _theme.length > 0) {
        copywrite_node.innerHTML = SET_MSG
        action_btn_node.innerHTML = "Edit"
        action_btn_node.style.display = "inline-block"
        set_theme_btn_node.style.display = "none"
        set_theme_input_node.style.display = "none"
      }
    } else {
      if (_theme.length > 0) {
        set_theme_btn_node.style.display = "none"
        action_btn_node.innerHTML = "Edit"
        theme_node.style.display = "inline-block"
        set_theme_input_node.style.display = "none"
      } else {
        copywrite_node.innerHTML = UNSET_MSG
        action_btn_node.style.display = "none"
        set_theme_btn_node.style.display = "inline-block"
      }
      editing = false
    }
    theme = _theme
    theme_node.innerHTML = h.escapeHtml(theme)
  }

  const handleGameStarted = () => {
    set_theme_input_node.style.display = "none"
    set_theme_btn_node.style.display = "none"
    action_btn_node.style.display = "none"
    if (theme.length === 0) {
      copywrite_node.style.display = "none"
      theme_node.display = "none"
    }
  }

  const input_subs = [
    input.on(e.THEME_SET, handleThemeSet),
    input.on(e.GAME_STARTED, handleGameStarted)
  ]

  const handleSetThemePressed = () => {
    const _theme = set_theme_input_node.value.trim()
    set_theme_input_node.value = ""
    output.emit(e.THEME_SET, _theme)
  }

  const handleActionBtnPressed = () => {
    if (editing) {
      action_btn_node.innerHTML = "Edit"
      if (theme.length > 0) {
        set_theme_input_node.style.display = "none"
        set_theme_btn_node.style.display = "none"
      }
      theme_node.style.display = "inline-block"
      editing = false
    } else {
      action_btn_node.innerHTML = "Cancel"
      theme_node.style.display = "none"
      set_theme_btn_node.style.display = "inline-block"
      set_theme_input_node.style.display = "inline-block"
      set_theme_input_node.focus()
      editing = true
    }
  }

  //init
  view.renderApp((dom) => {
    const bindTypes = {
      'copywrite': (elem) => {
        copywrite_node = elem
        copywrite_node.innerHTML = UNSET_MSG
      },
      'theme': (elem) => {
        theme_node = elem
        theme_node.innerHTML = theme
      },
      'set-theme-input': (elem) => {
        set_theme_input_node = elem
        set_theme_input_node.addEventListener('keypress', (event) => {
          if (event.keyCode === 13) {
            handleSetThemePressed()
          }
        })
      },
      'set-theme-btn': (elem) => {
        set_theme_btn_node = elem
        h.registerTapOrClick(set_theme_btn_node, handleSetThemePressed)
      },
      'action-btn': (elem) => {
        action_btn_node = elem
        action_btn_node.style.display = "none"
        h.registerTapOrClick(action_btn_node, handleActionBtnPressed)
      }
    }
    h.bindAttributes('theme', bindTypes, dom)
  })
  //init
  handleThemeSet(_theme)
  if (started) {
    handleGameStarted()
  }
  return {
    destroy: () => {
      for (let unsub of input_subs) {
        unsub()
      }
      view.removeApp()
    }
  }
}

module.exports = ThemeApp
