//Socket.io events
//Sinks: Nakama Handler
exports.CONNECTION = "connection"
//Sinks: client/index.js
exports.I_CONNECTED = "connect"
//Sinks: client/index.js, Nakama Handler
exports.I_DISCONNECTED = "disconnect"

//Sync Events
/***********/

//From Client

//Source: client/index.js
//Sinks: Nakama Handler
//{userid?, handle?, on_mobile, server_id?, rooms_occupied?, session_id?, cookie?, client_type}
exports.ESTABLISH_SESSION = "ESTABLISH_SESSION"

//Source: components/nevent_log.js (NeventLogApp)
//Sinks: Nakama Handler
//{roomid}
exports.SYNC_NEVENT_LOG_REQUESTED = "SYNC_NEVENT_LOG_REQUESTED"

//Source: components/game_wrapper.js (GameApp)
//Sinks: Nakama Handler
//roomid
exports.I_REQUESTED_CLIENT_BUNDLE = "I_REQUESTED_CLIENT_BUNDLE"

//From Server

//Source: Nakama Handler
//Client Sinks: client/index.js, components/players.js
//Admin Sinks: ui/dashboard.js
//{players: [player...],
// rooms_occupied: [roomid...],
// rooms: [room...],
// config_for_client,
// using_cookies: bool,
// userid?,
// handle?,
exports.ESTABLISHED_SESSION = "ESTABLISHED_SESSION"

//Source: Nakama Handler
//Sinks: components/nevent_log.js
//{roomid, messages, slash_commands <{default_commands, program_commands}>}
exports.SYNCED_NEVENT_LOG = "SYNCED_NEVENT_LOG"

//Handshakes

//Source: server/establish_session
//Sinks: client/index.js
//
exports.LOGIN_REQUIRED = "LOGIN_REQUIRED"

//Source: server/establish_session
//Sinks: client/index.js
//
exports.SESSION_INVALID = "SESSION_INVALID"

//Source: ui/user.js
//Sinks: Nakama Handler
//passphrase
exports.I_SUBMITTED_CONVERT_TO_ACCOUNT_REQUEST = "I_SUBMITTED_CONVERT_TO_ACCOUNT_REQUEST"

//Source: ui/user.js
//Sinks: Nakama Handler
//{handle, passphrase}
exports.I_SUBMITTED_ACCOUNT_PASSPHRASE_ATTEMPT = "I_SUBMITTED_ACCOUNT_PASSPHRASE_ATTEMPT"

//Source: Nakama Handler
//Sinks: ui/user.js
//bool
exports.ACCOUNT_PASSPHRASE_ATTEMPT_RESULT_READY = "ACCOUNT_PASSPHRASE_ATTEMPT_RESULT_READY"

//Source: Nakama Handler
//Sinks: ui/user.js (UserApp)
//{sessionid}
exports.ACCOUNT_CONVERSION_REQUEST_SUCCESSFUL = "ACCOUNT_CONVERSION_REQUEST_SUCCESSFUL"

//Update Events
/*************/

//From Client

//Source: ui/user.js
//Sinks: 
//
exports.I_DISABLED_COOKIES = "I_DISABLED_COOKIES"

//Source: ui/user.js
//Sinks: Nakama Handler
//
exports.I_ENABLED_COOKIES = "I_ENABLED_COOKIES"

//Source: Nakama Handler
//Sinks: ui/user.js
//
exports.COOKIES_ENABLED_BY_MYSELF = "COOKIES_ENABLED_BY_MYSELF"

//Source: Nakama Handler
//Sinks: ui/user.js
//
exports.COOKIES_DISABLED_BY_MYSELF = "COOKIES_DISABLED_BY_MYSELF"

//Source: components/room/settings/settings.js (RoomSettingsApp)
//Sinks: Nakama Handler
//roomid
exports.I_REMOVED_PASSPHRASE = "I_REMOVED_PASSPHRASE"

//Source: components/room/settings/settings.js (RoomSettingsApp)
//Sinks: Nakama Handler
//{roomid, passphrase}
exports.I_CHANGED_PASSPHRASE = "I_CHANGED_PASSPHRASE"

//Source: components/room/settings/admin_settings.js
//Sinks: Nakama Handler
//{roomid, description}
exports.I_CHANGED_ROOM_DESCRIPTION = "I_CHANGED_ROOM_DESCRIPTION"

//Source: components/room.js (RoomSettingsApp)
//Sinks: Nakama Handler
//roomid
exports.I_PROTECTED_ROOM = "I_PROTECTED_ROOM"

//Source: components/room.js (RoomSettingsApp)
//Sinks: Nakama Handler
//roomid
exports.I_UNPROTECTED_ROOM = "I_UNPROTECTED_ROOM"

//Source: components/room.js (RoomSettingsApp)
//Sinks: Nakama Handler
//{roomid, name}
exports.I_CHANGED_ROOM_NAME = "I_CHANGED_ROOM_NAME"

//Source: components/room.js (RoomSettingsApp)
//Sinks: Nakama Handler
//roomid
exports.I_DESTROYED_ROOM = "I_DESTROYED_ROOM"

//Source: components/room/settings/admin_settings.js (RoomSettingsApp)
//Sinks: Nakama Handler
//{roomid, tab_type} tab_type one of shared/symbols/tab_types.js ('app', 'occupants' or 'messages'
exports.I_CHANGED_DEFAULT_TAB = "I_CHANGED_DEFAULT_TAB"

//Source: components/room/settings/admin_settings.js (RoomSettingsApp)
//Sinks: Nakama Handler
//roomid
exports.I_ENABLED_GAMES = "I_ENABLED_GAMES"

//Source: components/room/settings/admin_settings.js (RoomSettingsApp)
//Sinks: Nakama Handler
//roomid
exports.I_DISABLED_GAMES = "I_DISABLED_GAMES"

//Source: components/room/settings/admin_settings.js (RoomSettingsApp)
//Sinks: Nakama Handler
//roomid
exports.I_ENABLED_NEVENT_LOG = "I_ENABLED_NEVENT_LOG"

//Source: components/room/settings/admin_settings.js (RoomSettingsApp)
//Sinks: Nakama Handler
//roomid
exports.I_DISABLED_NEVENT_LOG = "I_DISABLED_NEVENT_LOG"

//Source: components/room/settings/admin_settings.js (RoomSettingsApp)
//Sinks: Nakama Handler
//roomid
exports.I_ENABLED_PLAYER_LIST = "I_ENABLED_PLAYER_LIST"

//Source: components/room/settings/admin_settings.js (RoomSettingsApp)
//Sinks: Nakama Handler
//roomid
exports.I_DISABLED_PLAYER_LIST = "I_DISABLED_PLAYER_LIST"

//Source: rooms_listing.js
//Sinks: Nakama Handler
//{userid}
exports.I_REQUESTED_ROOM_CREATION = "I_REQUESTED_ROOM_CREATION"

//Source: components/game_wrapper.js (GameWrapperclient/index.js)
//Sinks: Nakama Handler
//{userid, roomid, gameid}
exports.I_REQUESTED_GAME_START = "I_REQUESTED_GAME_START"

//Source: components/focus.js (LocalFocuser)
//Sinks: Nakama Handler
//{userid, gameid}
exports.I_REQUESTED_GAME_START_FROM_URL = "I_REQUESTED_GAME_START_FROM_URL"

//Source: components/game/api.js
//Sinks: Nakama Handler
//{gseshid, userid, event, body: {}}
exports.GAME_EVENT = "GAME_EVENT"

//Source: name_select.js
//Sinks: Nakama Handler
//{handle, userid, old_handle}
exports.I_CHANGED_HANDLE = "I_CHANGED_HANDLE"

//Source: rooms_listing.js (RoomItemActionApp), components/room.js (RoomTitleApp), rooms.js
//Sinks: Nakama Handler
//{roomid, userid}
exports.I_LEFT_ROOM = "I_LEFT_ROOM"

//Source: rooms_listing.js, client/index.js
//Sinks: Nakama Handler
//{roomid, userid, passphrase?}
exports.I_JOINED_ROOM = "I_JOINED_ROOM"

//Source: components/nevent_log.js
//Sinks: Nakama Handler
//{roomid, message <string>}
exports.I_SENT_IM = "I_SENT_IM"

//Source: Nakama Handler
//Sinks: components/nevent_log.js
//{nson_result <Nevent.NSON>}
exports.SLASH_COMMAND_RESULT_READY = "SLASH_COMMAND_RESULT_READY"

//Source: components/nevent_log.js
//Sinks: Nakama Handler
//{roomid, message}
exports.I_SENT_SLASH_COMMAND = "I_SENT_SLASH_COMMAND"

//Source: name_select.js
//Sinks: Nakama Handler
//
exports.I_REQUESTED_NEW_HANDLE = "I_REQUESTED_NEW_HANDLE"

//Source: components/players.js
//Sinks: Nakama Handler
//{playerid, roomid, kickerid}
exports.I_KICKED_PLAYER = "I_KICKED_PLAYER"

//Source: admin_settings/shortlink-app
//Sinks: Nakama Handler
//{shortlink, roomid}
exports.PLAYER_CHANGED_SHORTLINK = "PLAYER_CHANGED_SHORTLINK"

//Source: admin_settings/shortlink-app
//Sinks: Nakama Handler
//roomid
exports.PLAYER_REMOVED_SHORTLINK = "PLAYER_REMOVED_SHORTLINK"


//From Server

//Source: Nakama handler
//Sinks: client/index.js
//roomid
exports.ROOM_CREATION_REQUEST_FUFILLED = "ROOM_CREATION_REQUEST_FUFILLED"

//Source: system/admin_assigner.js
//Sinks: client/index.js
//{userid, roomid}
exports.PLAYER_ASSIGNED_AS_ADMIN = "PLAYER_ASSIGNED_AS_ADMIN"

//Source: Nakama Handler
//Sinks: client/index.js
//roomid
exports.PLAYER_REMOVED_PASSPHRASE = "PLAYER_REMOVED_PASSPHRASE"

//Source: Nakama Handler
//Sinks: client/index.js
//roomid
exports.PLAYER_ADDED_PASSPHRASE = "PLAYER_ADDED_PASSPHRASE"

//Source: Nakama Handler
//Sinks: client/index.js
//{roomid, description}
exports.PLAYER_CHANGED_ROOM_DESCRIPTION = "PLAYER_CHANGED_ROOM_DESCRIPTION"

//Source: Nakama Handler
//Sinks: client/index.js
//{userid, roomid}
exports.PLAYER_PROTECTED_ROOM = "PLAYER_PROTECTED_ROOM"

//Source: Nakama Handler
//Sinks: client/index.js
//{userid,roomid}
exports.PLAYER_UNPROTECTED_ROOM = "PLAYER_UNPROTECTED_ROOM"

//Source: Nakama Handler / system/unoccupied_rooms.js
//Sinks: rooms_listing.js
//roomid
exports.UNOCCUPIED_ROOM_DESTROYED = "UNOCCUPIED_ROOM_DESTROYED"

//Source: Nakama Handler
//Sinks: client/index.js
//{roomid, name}
exports.PLAYER_CHANGED_ROOM_NAME = "PLAYER_CHANGED_ROOM_NAME"

//Source: Nakama Handler
//Sinks: client/index.js
//{userid, roomid}
exports.PLAYER_DESTROYED_ROOM = "PLAYER_DESTROYED_ROOM"

//Source: Nakama Handler
//Sinks: client/index.js
//userid
exports.PLAYER_ACTIVE = "PLAYER_ACTIVE"

//Source: Nakama Handler
//Sinks: client/index.js
//userid
exports.PLAYER_INACTIVE = "PLAYER_INACTIVE"

//Source: Nakama Handler
//Sinks: client/index.js
//{userid, roomid, tab_type}
exports.PLAYER_CHANGED_DEFAULT_TAB = "PLAYER_CHANGED_DEFAULT_TAB"

//Source: Nakama Handler
//Sinks: client/index.js
//{userid, roomid}
exports.PLAYER_ENABLED_GAMES = "PLAYER_ENABLED_GAMES"

//Source: Nakama Handler
//Sinks: client/index.js
//{userid, roomid}
exports.PLAYER_DISABLED_GAMES = "PLAYER_DISABLED_GAMES"

//Source: Nakama Handler
//Sinks: client/index.js
//{userid, roomid}
exports.PLAYER_ENABLED_NEVENT_LOG = "PLAYER_ENABLED_NEVENT_LOG"

//Source: Nakama Handler
//Sinks: client/index.js
//{userid, roomid}
exports.PLAYER_DISABLED_NEVENT_LOG = "PLAYER_DISABLED_NEVENT_LOG"

//Source: Nakama Handler
//Sinks: client/index.js
//{userid, roomid}
exports.PLAYER_ENABLED_PLAYER_LIST = "PLAYER_ENABLED_PLAYER_LIST"

//Source: Nakama Handler
//Sinks: client/index.js
//{userid, roomid}
exports.PLAYER_DISABLED_PLAYER_LIST = "PLAYER_DISABLED_PLAYER_LIST"

//Source: Nakama Handler
//Sinks: client/index.js
//room
exports.ROOM_CREATED = "ROOM_CREATED"

//Source: game/handler
//Sinks: client/index.js
//{roomid, gseshid}
exports.GAME_ENDED = "GAME_ENDED"

//Source: Nakama Handler
//Sinks: components/game_wrapper (GameApp)
//{roomid, bundle, gseshid}
exports.CLIENT_BUNDLE_READY = "CLIENT_BUNDLE_READY"

//Source: game/handler
//Sinks: client/index.js
//{roomid, gseshid, program_commands}
exports.GAME_STARTED = "GAME_STARTED"

//Source: Nakama Handler
//Sinks: client/index.js
//roomid
exports.GAME_STARTED_FROM_URL = "GAME_STARTED_FROM_URL"

//Source: game/handler
//Sinks: client/index.js
//{roomid, gseshid}
exports.GAME_ERRORED = "GAME_ERRORED"

//Source: Nakama Handler
//Sinks: components/game_wrapper (GameSelectApp)
//{userid, roomid, gameid}
exports.PLAYER_REQUESTED_GAME_START = "PLAYER_REQUESTED_GAME_START"

//Source: Nakama Handler
//Sinks: client/index.js
//{userid, old_handle, handle}
exports.PLAYER_CHANGED_HANDLE = "PLAYER_CHANGED_HANDLE"

//Source: lib/game/api.js
//Sinks: components/nevent_log.js
//{nson}
exports.APP_EMITTED_NSON = "APP_EMITTED_NSON"

//Source: Nakama Handler
//Sinks: components/nevent_log.js, services/notifications.js
//{roomid, userid, message <string>}
exports.PLAYER_SENT_IM = "PLAYER_SENT_IM"

//Source: Nakama Handler, api/auth.js
//Sinks: client/index.js
//{handle,  userid}
exports.NEW_PLAYER_REGISTERED = "NEW_PLAYER_REGISTERED"

//Source: Nakama Handler
//Sinks: client/index.js
//{rooms: [roomid...], userid}
exports.PLAYER_LEFT = "PLAYER_LEFT"

//Source: Nakama Handler
//Sinks: client/index.js
//{roomid, userid, [passphrase]}
exports.PLAYER_JOINED_ROOM = "PLAYER_JOINED_ROOM"

//Source: Nakama Handler
//Sinks: client/index.js
//{roomid, userid}
exports.PLAYER_LEFT_ROOM = "PLAYER_LEFT_ROOM"

//Source: Nakama Handler
//Sinks: client/index.js
//{playerid, kickerid, roomid}
exports.PLAYER_WAS_KICKED = "PLAYER_WAS_KICKED"

//Ephemeral Events
/****************/

//From Server

//Source: Nakama Handler
//Sinks: components/nevent_log.js
//userid
exports.PLAYER_STARTED_TYPING = "PLAYER_STARTED_TYPING"

//Source: Nakama Handler
//Sinks: components/nevent_log.js
//userid
exports.PLAYER_STOPPED_TYPING = "PLAYER_STOPPED_TYPING"

//From Client

//Source: components/nevent_log.js
//Sinks: Nakama Handler
//{roomid, userid}
exports.I_STARTED_TYPING = "I_STARTED_TYPING"

//Source: components/nevent_log.js
//Sinks: Nakama Handler
//{roomid, userid}
exports.I_STOPPED_TYPING = "I_STOPPED_TYPING"
