//Adapted from:
//From https://developer.mozilla.org/en-US/docs/Web/API/Document/cookie/Simple_document.cookie_framework

const getItemFromCookie = (sKey, cookie) => {
  if (!sKey) { return null; }
  return decodeURIComponent(cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null
}

const cookieHasItem = (cookie, sKey) => {
  if (!sKey) { return false; }
  return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(cookie)
}

const cookies = {
  getItem: (sKey) => {
    return getItemFromCookie(sKey, document.cookie)
  },
  setItem: function (sKey, sValue, vEnd, _sPath, sDomain, bSecure) {
    //shadow it so that sPath always points to /
    const sPath = '/'
    //const sPath = _sPath
    if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return false; }
    var sExpires = "";
    if (vEnd) {
      switch (vEnd.constructor) {
        case Number:
          sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
          break;
        case String:
          sExpires = "; expires=" + vEnd;
          break;
        case Date:
          sExpires = "; expires=" + vEnd.toUTCString();
          break;
      }
    }
    document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
    return true;
  },
  removeItem: (sKey, _sPath, sDomain) => {
    //shadow it so that sPath always points to /
    const sPath = '/'
    if (!cookieHasItem(document.cookie, sKey)) { return false; }
    document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "");
    return true;
  },
  hasItem: (sKey) => {
    return cookieHasItem(document.cookie, sKey)
  },
  keys: () => {
    var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
    for (var nLen = aKeys.length, nIdx = 0; nIdx < nLen; nIdx++) { aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]); }
    return aKeys;
  }
}

exports.cookies = cookies
