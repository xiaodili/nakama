const PubSub = require('../../shared/helpers.js').PubSub
const difference = require('../../shared/helpers.js').difference
const e = require('../symbols/events.js')

const SelectivePubSub = () => {
  const channels = {}
  return {
    sub: (channel, id, fn) => {
      if (!(channel in channels)) {
        channels[channel] = {}
      }
      if (!(id in channels[channel])) {
        channels[channel][id] = []
      }
      channels[channel][id].push(fn)
      const index = channels[channel][id].length - 1
      return () => {
        delete channels[channel][id][index]
      }
    },
    pub: (channel, ...args) => {
      const applyFn = (fn) => {
        fn(...args)
      }
      if (channel in channels) {
        let chnl = channels[channel]
        for (let id of Object.keys(chnl)) {
          let fns = chnl[id]
          fns.forEach(applyFn)
        }
      }
    },
    to: (channel, toid, ...args) => {
      const applyFn = (fn) => {
        fn(...args)
      }
      if (channel in channels) {
        let fns = channels[channel][toid]
        fns.forEach(applyFn)
      }
    },
    toMany: (channel, ids, ...args) => {
      const applyFn = (fn) => {
        fn(...args)
      }
      if (channel in channels) {
        let chnl = channels[channel]
        for (let id of ids) {
          let fns = chnl[id]
          fns.forEach(applyFn)
        }
      }
    }
  }
}

//authentic de/reserialisation!
const overWire = (msg) => {
  //checks if undefined or null
  if (msg == null) {
    return undefined
  } else {
    return JSON.parse(JSON.stringify(msg))
  }
}

const EventStore = (event_obj) => {
  //optional constructor param
  let events,
    setup
  if (event_obj) {
    events = event_obj.events
    setup = event_obj.setup
  } else {
    events = []
    setup = {}
  }

  return {
    registerSetupPlayers: (players) => {
      setup.players = players
    },
    putEvent: (event) => {
      events.push(event)
    },
    getEvents: () => {
      return {
        setup,
        events
      }
    }
  }
}

const IO = () => {
  //for simulation
  let server,
    nevent_log_app,
    gameStateChanged,
    serverCleanup,
    clientsCleanup

  // id → <Client>
  const clients = {}

  //for server to pubsub to, set by forSimulation
  let player_ids
  const inactive_ids = new Set()

  //for events loading and exporting
  var event_store = EventStore()
  let loadEventsPressed

  var server_pubsub = PubSub()
  var client_pubsub = SelectivePubSub()

  const slash_commands = {}
  const isSlashCommand = (message) => {
    // "/generate dog" → "/generate"
    const maybe_cmd = message.split(' ')[0]
    return maybe_cmd[0] === '/' && maybe_cmd.slice(1) in slash_commands
  }

  //these need to be internal, since EventPlayer needs them to play back loaded events
  const externalTriggers = {
    [e.PLAYER_CHANGED_HANDLE]: (userid, handle) => {
      if (typeof server.onPlayerChangedHandle === 'function') {
        server.onPlayerChangedHandle(userid, handle)
      }
      event_store.putEvent({
        type: e.EXTERNAL_EVENT,
        event_name: e.PLAYER_CHANGED_HANDLE,
        event_params: [
          userid,
          handle
        ]
      })
    },
    [e.MESSAGE_SENT]: (userid, handle, msg) => {
      if (isSlashCommand(msg)) {
        if (typeof server.onSlashCommand === 'function') {
          server.onSlashCommand(userid, msg)
        }
      } else {
        nevent_log_app.onPlayerSentIm(userid, handle, msg)
        if (typeof server.onPlayerSentMessage === 'function') {
          server.onPlayerSentMessage(userid, msg)
        }
      }
      event_store.putEvent({
        type: e.EXTERNAL_EVENT,
        event_name: e.MESSAGE_SENT,
        event_params: [
          userid,
          handle,
          msg
        ]
      })
    },
    [e.PLAYER_JOINED]: (player) => {
      player_ids.add(player.id)
      if (typeof server.onPlayerJoined === 'function') {
        server.onPlayerJoined({
          id: player.id,
          handle: player.handle
        })
      }
      event_store.putEvent({
        type: e.EXTERNAL_EVENT,
        event_name: e.PLAYER_JOINED,
        event_params: [player]
      })
    },
    [e.PLAYER_LEFT]: (userid) => {
      player_ids.delete(userid)
      if (typeof server.onPlayerLeft === 'function') {
        server.onPlayerLeft(userid)
      }
      event_store.putEvent({
        type: e.EXTERNAL_EVENT,
        event_name: e.PLAYER_LEFT,
        event_params: [userid]
      })
    },
    [e.PLAYER_ACTIVE]: (userid) => {
      inactive_ids.delete(userid)
      if (typeof server.onPlayerActive === 'function') {
        server.onPlayerActive(userid)
      }
      if (typeof clients[userid].onReconnection === 'function') {
        clients[userid].onReconnection()
      }
      event_store.putEvent({
        type: e.EXTERNAL_EVENT,
        event_name: e.PLAYER_ACTIVE,
        event_params: [userid]
      })
    },
    [e.PLAYER_INACTIVE]: (userid) => {
      inactive_ids.add(userid)
      if (typeof server.onPlayerInactive === 'function') {
        server.onPlayerInactive(userid)
      }
      if (typeof clients[userid].onDisconnection === 'function') {
        clients[userid].onDisconnection()
      }
      event_store.putEvent({
        type: e.EXTERNAL_EVENT,
        event_name: e.PLAYER_INACTIVE,
        event_params: [userid]
      })
    }
  }


  return {
    forEventPlayer: {
      getEventStore: () => {
        return event_store
      },
      loadEventsTriggered: (events_obj) => {
        const players = events_obj.setup.players
        const events = events_obj.events
        //set up the new clients
        loadEventsPressed(players)
        //play back the events
        for (let event of events) {
          if (event.type === e.GAME_EVENT) {
            const name = event.event_name
            const userid = event.userid
            const body = event.event_body
            server_pubsub.pub(name, userid, body)
          } else if (event.type === e.EXTERNAL_EVENT) {
            const name = event.event_name
            const params = event.event_params
            externalTriggers[name](...params)
          }
        }
        gameStateChanged()
        //store events again
        event_store = EventStore(events_obj)
      }
    },
    forServer: {
      input: {
        on: (evt, fn) => {
          return server_pubsub.sub(evt, fn)
        }
      },
      output: {
        emit: (evt, msg) => {
          const recipients = difference(player_ids, inactive_ids)
          client_pubsub.toMany(evt, [...recipients], overWire(msg))
          gameStateChanged()
        },
        //to all but id
        broadcast: (id, evt, msg) => {
          const recipients = difference(player_ids, inactive_ids)
          recipients.delete(id)
          client_pubsub.toMany(evt, [...recipients], overWire(msg))
          gameStateChanged()
        },
        toMany: (ids) => {
          return {
            emit: (evt, msg) => {
              const recipients = difference(new Set(ids), inactive_ids)
              client_pubsub.toMany(evt, [...recipients], msg)
              gameStateChanged()
            }
          }
        },
        to: (userid) => {
          return {
            emit: (evt, msg) => {
              if (!inactive_ids.has(userid)) {
                client_pubsub.to(evt, userid, overWire(msg))
              }
              gameStateChanged()
            }
          }
        }
      },
      external: {
        sendMessage: (msg) => {
          nevent_log_app.onAppSentMsg(msg)
        },
        sendNson: (nson) => {
          nevent_log_app.onAppSentNson(nson)
        },
        endGame: () => {
          //clean up server
          serverCleanup()
          clientsCleanup()
        }
      }
    },
    ForClient: (userid) => {
      return {
        input: {
          on: (evt, fn) => {
            return client_pubsub.sub(evt, userid, fn)
          }
        },
        output: {
          emit: (evt, msg) => {
            const msg_serialised = overWire(msg)
            server_pubsub.pub(evt, userid, msg_serialised)
            //storing them into the event_store in case of exporting:
            event_store.putEvent({
              type: e.GAME_EVENT,
              event_name: evt,
              userid,
              event_body: msg_serialised
            })
            gameStateChanged()
          }
        }
      }
    },
    forSimulation: {
      addClient: (id, client) => {
        clients[id] = client
        //add slash commands
        if (typeof client.slash_commands === 'object') {
          for (const cmd of Object.keys(client.slash_commands)) {
            slash_commands[cmd] = client.slash_commands[cmd]
          }
        }
      },
      removeClient: (id) => {
        const client = clients[id]
        client.destroy()
        delete clients[id]
      },
      registerServer: (_server) => {
        server = _server
      },
      registerNeventLogApp: (_nevent_log_app) => {
        nevent_log_app = _nevent_log_app
      },
      registerPlayerId: (userid) => {
        player_ids.add(userid)
      },
      registerServerCleanup: (fn) => {
        serverCleanup = fn
      },
      registerClientsCleanup: (fn) => {
        clientsCleanup = fn
      },
      registerPlayers: (players) => {
        const userids = players.map((player) => player.id)
        player_ids = new Set(userids)
        event_store.registerSetupPlayers(players)
      },
      onLoadEventsPressed: (fn) => {
        loadEventsPressed = fn
      },
      onGameStateChange: (fn) => { //fn to call whenever client/server emits
        gameStateChanged = fn
      },
      getGameState: () => { //specifies to get the game state from server
        if (typeof server.onGameStateRequested === 'function') {
          return server.onGameStateRequested()
        } else {
          return {}
        }
      },
      triggerPlayerChangedHandle: (userid, handle) => {
        externalTriggers[e.PLAYER_CHANGED_HANDLE](userid, handle)
      },
      triggerMessageSent: (userid, handle, msg) => {
        externalTriggers[e.MESSAGE_SENT](userid, handle, msg)
      },
      triggerPlayerJoined: (player) => {
        externalTriggers[e.PLAYER_JOINED](player)
      },
      triggerPlayerLeft: (userid) => {
        externalTriggers[e.PLAYER_LEFT](userid)
      },
      triggerPlayerActive: (userid) => {
        externalTriggers[e.PLAYER_ACTIVE](userid)
      },
      triggerPlayerInactive: (userid) => {
        externalTriggers[e.PLAYER_INACTIVE](userid)
      }
    }
  }
}

module.exports = IO
