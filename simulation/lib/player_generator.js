const h = require('../../shared/server_helpers.js')

const PlayerGenerator = () => {
  var id = 0

  const generatePlayer = () => {
    return {
      id: (id++).toString(),
      handle: h.generateRandomHandle(),
      active: true,
      creator: false
    }
  }
  const seedPlayers = (num_players) => {
    //Neat array generation from int: http://stackoverflow.com/a/40772491/1010076
    const players = [...Array(num_players)].map(() => {
      return generatePlayer()
    })
    //needs one creator
    players[0].creator = true
    return players
  }
  const hydrateFromEventSetup = (players_from_setup) => {
    //let's just use the length as id
    id = players_from_setup.length
    const players = players_from_setup.map((p) => {
      return {
        id: p.id,
        handle: p.handle ? p.handle : h.generateRandomHandle(),
        active: true,
        creator: false
      }
    })
    players[0].creator = true
    return players
  }

  return {
    generatePlayer,
    seedPlayers,
    hydrateFromEventSetup
  }
}

module.exports = PlayerGenerator
