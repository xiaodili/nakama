const h = require('../shared/client_helpers.js')
const PlayerGenerator = require('./lib/player_generator.js')
const IO = require('./lib/io.js')
const ServerSimulationApp = require('./components/server.js')
const ClientsSimulationApp = require('./components/clients.js')
const NeventLogApp = require('./components/nevent_log.js')
const EventPlayerApp = require('./components/event_player.js')
const jss = require('../shared/css.js')
//probably shouldn't be doing this, but stealing the look and feel for the nakama theme and core
const theme = require('../client/ui/theme.js')()

const styles = jss({
  '@global': {
    body: {
      fontFamily: theme.fonts.standard.fontFamily,
      backgroundColor: theme.colours.background
    },
    ul: {
      listStyleType: 'none',
      padding: '0px',
      margin: '0px'
    },
    button: theme.styles.raw.buttonPrimary,
    '.heading': {
      fontFamily: theme.fonts.heading.fontFamily,
      fontSize: '16px',
      fontWeight: theme.fonts.heading.fontWeight
    },
    '.subHeading': {
      fontFamily: theme.fonts.subHeading.fontFamily,
      fontSize: '14px',
      fontWeight: theme.fonts.subHeading.fontWeight
    },
    '.subSubHeading': {
      fontFamily: theme.fonts.subHeading.fontFamily,
      fontSize: '14px',
      fontWeight: theme.fonts.subHeading.fontWeight
    },
    '.widgetContainer': { //going by iPhone 5 dimensions
      display: 'inline-block',
      width: '320px',
      minHeight: '200px',
      maxHeight: '700px',
      border: '1px grey solid',
      borderRadius: '5px',
      padding: '2px',
      margin: '2px',
      verticalAlign: 'top',
      overflowX: 'auto',
      overflowY: 'auto'
    }
  },
  serverAndNeventLog: {
    display: 'inline-block'
  }
})

const SetupApp = (mount, Server, Client, _num_players) => {
  const num_players = _num_players
  const view = h.View(
    mount,
    'div',
    [],
    () => { return `
      <div>
        <span>Number of players at game start: ${num_players}</span><br/>
          <span>Number of players joined after game start: <span simulator='joined-players'></span></span>
          <br/>
          <button simulator='add-player'>Add Player to Room</button>
      </div>
      <div>
        <div class='${styles.serverAndNeventLog}'>
          <div>
            <div class='subHeading'>Server</div>
            <div simulator='server-hud-app'></div>
          </div>
          <div>
            <div class='subHeading'>Nevent Log</div>
            <div class='widgetContainer'>
              <ul simulator="nevent-log-app"></ul>
            </div>
          </div>
        </div>
        <div style="display: inline-block; vertical-align: top">
          <div class='subHeading'>Clients</div>
          <div simulator='clients-app'></div>
        </div>
        <div>
          <div class='subHeading'>Event Player</div>
          <div simulator='event-player-app'></div>
        </div>
      </div>
    `}
  )
  const player_generator = PlayerGenerator()
  var players = player_generator.seedPlayers(num_players)
  const creatorid = players[0].id
  const io = IO()

  var clients_simulation_app,
    server_simulation_app,
    event_player_app

  var joined_players_node

  var num_joined_players = 0

  const handleLoadEventsPressed = (players_from_setup) => {
    players = player_generator.hydrateFromEventSetup(players_from_setup)
    server_simulation_app.reset(players)
    clients_simulation_app.reset(players)
    joined_players_node.innerHTML = num_joined_players
  }

  io.forSimulation.onLoadEventsPressed(handleLoadEventsPressed)

  const handleAddPlayerPressed = () => {
    num_joined_players += 1
    joined_players_node.innerHTML = num_joined_players
    const new_player = player_generator.generatePlayer()
    io.forSimulation.registerPlayerId(new_player.id)
    clients_simulation_app.addClient(new_player)
  }

  //initialising from simulation_config.js
  view.renderApp((dom) => {
    const bindTypes = {
      'server-hud-app': (elem) => {
        server_simulation_app = ServerSimulationApp(elem, io, Server, creatorid, players)
      },
      'clients-app': (elem) => {
        clients_simulation_app = ClientsSimulationApp(elem, io, Client, players)
      },
      'nevent-log-app': (elem) => {
        const nevent_log_app = NeventLogApp(elem)
        io.forSimulation.registerNeventLogApp(nevent_log_app)

      },
      'event-player-app': (elem) => {
        event_player_app = EventPlayerApp(elem, io.forEventPlayer)
      },
      'add-player': (elem) => {
        h.registerTapOrClick(elem, handleAddPlayerPressed)
      },
      'joined-players': (elem) => {
        joined_players_node = elem
        joined_players_node.innerHTML = num_joined_players
        h.registerTapOrClick(elem, handleAddPlayerPressed)
      }
    }
    h.bindAttributes('simulator', bindTypes, dom)
  })
}

const SimulatorApp = (mount, config, num_players) => {
  const title = config.title
  const description = config.description
  const view = h.View(
    mount,
    'div',
    [],
    () => { return `
      <div class='heading'>${title}</div>
      <div>${description}</div>
      <br/>
      <div simulator='setup-app'></div>
    `}
  )
  const Server = config.server
  const Client = config.client
  view.renderApp((dom) => {
    const bindTypes = {
      'setup-app': (elem) => {
        const setup_app = SetupApp(elem, Server, Client, num_players)
      }
    }
    h.bindAttributes('simulator', bindTypes, dom)
  })
}

module.exports = SimulatorApp
