const SimulationApp = require('./simulation.js')

module.exports = (config) => {
  const game_config = config.game_config
  const initial_num_players = config.initial_num_players
  SimulationApp(document.getElementById('simulation-app'), game_config, initial_num_players)
}
