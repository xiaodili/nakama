//For EventPlayer
exports.GAME_EVENT = "GAME"
exports.EXTERNAL_EVENT = "EXTERNAL"

//External events:
//event_body: {userid, handle}
exports.PLAYER_CHANGED_HANDLE = "PLAYER_CHANGED_HANDLE"
//event_body: {userid, msg}
exports.MESSAGE_SENT = "MESSAGE_SENT"
//event_body: <player>
exports.PLAYER_JOINED = "PLAYER_JOINED"
//event_body: userid
exports.PLAYER_LEFT = "PLAYER_LEFT"
//event_body: userid
exports.PLAYER_ACTIVE = "PLAYER_ACTIVE"
//event_body: userid
exports.PLAYER_INACTIVE = "PLAYER_INACTIVE"
