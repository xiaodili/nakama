const h = require('../../shared/client_helpers.js')

const ClientHudApp = (mount, self, forSimulation) => {
  const selfid = self.id
  const view = h.View(
    mount,
    'div',
    [],
    () => { return `
      <div class='subHeading'>Player State</div>
      <pre simulator="client-state"></pre>
      <input type="text" simulator='change-handle-input'></input><button simulator='change-handle-btn'>Change Handle</button>
      <br/>
      <button simulator='hud-connection-btn'></button>
      <button simulator='hud-leave-btn'></button>
      <br/>
      <input type="text" simulator='send-msg-input'></input><button simulator='send-msg-btn'>Send Message</button>
    `}
  )

  var connection_btn_node,
    room_btn_node,
    change_handle_input_node,
    client_state_node,
    send_msg_input_node
  const DEACTIVATE_PLAYER = "Make Player Inactive"
  const CONNECT_PLAYER = "Connect Player"
  const LEAVE = "Leave"

  var connected = true

  var clientAppLeft

  const updateClientState = () => {
    client_state_node.innerHTML = JSON.stringify(self, null, 2)
  }

  const handleConnectionBtnPressed = () => {
    if (connected) {
      connected = false
      connection_btn_node.innerHTML = CONNECT_PLAYER
      self.active = false
      updateClientState()
      forSimulation.triggerPlayerInactive(selfid)
    } else {
      connected = true
      connection_btn_node.innerHTML = DEACTIVATE_PLAYER
      self.active = true
      updateClientState()
      forSimulation.triggerPlayerActive(selfid)
    }
  }
  const handleLeaveBtnPressed = () => {
    forSimulation.triggerPlayerLeft(selfid)
    clientAppLeft()
  }
  const handleChangeHandlePressed = () => {
    const handle = change_handle_input_node.value
    self.handle = handle
    updateClientState()
    forSimulation.triggerPlayerChangedHandle(selfid, handle)
    change_handle_input_node.value = ""
  }
  const handleSendMsgPressed = () => {
    const msg = send_msg_input_node.value
    forSimulation.triggerMessageSent(selfid, self.handle, msg)
    send_msg_input_node.value = ""
  }

  view.renderApp((dom) => {
    const bindTypes = {
      'hud-connection-btn': (elem) => {
        connection_btn_node = elem
        connection_btn_node.innerHTML = DEACTIVATE_PLAYER
        h.registerTapOrClick(connection_btn_node, handleConnectionBtnPressed)
      },
      'hud-leave-btn': (elem) => {
        room_btn_node = elem
        room_btn_node.innerHTML = LEAVE
        h.registerTapOrClick(room_btn_node, handleLeaveBtnPressed)
      },
      'send-msg-input': (elem) => {
        send_msg_input_node = elem
        h.registerEnterPress(send_msg_input_node, handleSendMsgPressed)
      },
      'send-msg-btn': (elem) => {
        h.registerTapOrClick(elem, handleSendMsgPressed)
      },
      'change-handle-input': (elem) => {
        change_handle_input_node = elem
        h.registerEnterPress(change_handle_input_node, handleChangeHandlePressed)
      },
      'change-handle-btn': (elem) => {
        h.registerTapOrClick(elem, handleChangeHandlePressed)
      },
      'client-state': (elem) => {
        client_state_node = elem
      }
    }
    h.bindAttributes('simulator', bindTypes, dom)
  })
  updateClientState()
  return {
    setClientAppLeftHook: (fn) => {
      clientAppLeft = fn
    },
    disableHud: () => { //after end game is triggered
      connection_btn_node.disabled = true
      room_btn_node.disabled = true
    },
    destroy: () => {
      view.removeApp()
    }
  }
}

const ClientSimulationApp = (mount, io, Client, self)  => {
  const view = h.View(
    mount,
    'div',
    [["class", 'widgetContainer']],
    () => { return `
      <div simulator='client-hud-app'></div>
      <hr/>
      <div simulator='client-app'></div>
    `}
  )
  const id = self.id
  const client_io = io.ForClient(id)
  const input = client_io.input
  const output = client_io.output
  var client_app,
    client_hud_app

  view.renderApp((dom) => {
    const bindTypes = {
      'client-hud-app': (elem) => {
        client_hud_app = ClientHudApp(elem, self, io.forSimulation)
      },
      'client-app': (elem) => {
        client_app = Client(elem, self, input, output)
        io.forSimulation.addClient(id, client_app)
      }
    }
    h.bindAttributes('simulator', bindTypes, dom)
  })

  client_hud_app.setClientAppLeftHook(view.removeApp)
  return {
    destroy: () => {
      io.forSimulation.removeClient(id)
      view.removeApp()
    }
  }
}

const ClientsSimulationApp = (elem, io, Client, players) => {
  var client_simulation_apps = players.map((player) => {
    return ClientSimulationApp(elem, io, Client, player)
  })
  const handleClientsCleanup = () => {
    for (const client_simulation_app of client_simulation_apps) {
      client_simulation_app.destroy()
    }
  }
  io.forSimulation.registerClientsCleanup(handleClientsCleanup)

  return {
    reset: (players) => {
      for (const client_simulation_app of client_simulation_apps) {
        client_simulation_app.destroy()
      }
      client_simulation_apps = players.map((player) => {
        return ClientSimulationApp(elem, io, Client, player)
      })
    },
    addClient: (player) => {
      io.forSimulation.triggerPlayerJoined(player)
      client_simulation_apps.push(ClientSimulationApp(elem, io, Client, player))
    }
  }
}

module.exports = ClientsSimulationApp
