const h = require('../../shared/client_helpers.js')

const EventPlayerApp = (mount, forEventPlayer) => {

  var export_events_btn_node,
    load_events_btn_node,
    events_textarea_node
  const PLACEHOLDER_MSG = "Events JSON displayed/goes here"

  const view = h.View(
    mount,
    'div',
    [],
    () => { return `
      <div>
        <button simulation='export-events-btn'>Export Events</button>
        <span>Clicking this button will populate the text area below with the events that have occurred to result in the current game state</span>
      </div>
      <textarea simulation='events-textarea' placeholder='${PLACEHOLDER_MSG}'></textarea>
      <div>
        <button simulation='load-events-btn'>Load Events</button>
        <span>Clicking this button will re-recreate the game state from the events in the text area above</span>
      </div>
    `}
  )

  const handleExportEventsBtnPressed = () => {
    const event_store = forEventPlayer.getEventStore()
    events_textarea_node.value = JSON.stringify(event_store.getEvents(), null, 2)
    events_textarea_node.style.height = events_textarea_node.scrollHeight+'px'; 
    events_textarea_node.style.width = "100%";
  }

  const handleLoadEventsBtnPressed = () => {
    const events_obj = JSON.parse(events_textarea_node.value)
    forEventPlayer.loadEventsTriggered(events_obj)
  }

  //init
  view.renderApp((dom) => {
    h.bindAttributes('simulation', {
      'export-events-btn': (elem) => {
        export_events_btn_node = elem
        h.registerTapOrClick(export_events_btn_node, handleExportEventsBtnPressed)
      },
      'load-events-btn': (elem) => {
        load_events_btn_node = elem
        h.registerTapOrClick(load_events_btn_node, handleLoadEventsBtnPressed)
      },
      'events-textarea': (elem) => {
        events_textarea_node = elem
      },
    }, dom)
  })
}

module.exports = EventPlayerApp
