const h = require('../../shared/client_helpers.js')

//[{}...] → [{id, handle}...]
const playersForServer = (players) => {
  return players.map((p) => {
    return {
      id: p.id,
      handle: p.handle
    }
  })
}

const ServerSimulationApp = (mount, io, Server, creatorid, players) => {
  const view = h.View(
    mount,
    'div',
    [],
    () => { return `
      <div class='widgetContainer'>
        <div class='subHeading'>Game State</div>
        <pre simulator="game-state"></pre>
      </div>
    `}
  )
  const input = io.forServer.input
  const output = io.forServer.output
  const external = io.forServer.external
  var game_state_node
  io.forSimulation.registerPlayers(players)
  io.forSimulation.registerServerCleanup(() => {
    game_state_node.remove()
  })
  //init
  view.renderApp((dom) => {
    const bindTypes = {
      'game-state': (elem) => {
        game_state_node = elem
        const outputGameState = () => {
          //http://stackoverflow.com/questions/3515523/javascript-how-to-generate-formatted-easy-to-read-json-straight-from-an-object
          game_state_node.innerHTML = JSON.stringify(io.forSimulation.getGameState(), null, 2)
        }
        io.forSimulation.onGameStateChange(outputGameState)
      }
    }
    h.bindAttributes('simulator', bindTypes, dom)
  })
 
  var server = Server(creatorid, playersForServer(players), input, output, external)
  io.forSimulation.registerServer(server)

  return {
    reset: (players) => {
      server.destroy() //I think this is okay...?
      server = Server(creatorid, playersForServer(players), input, output, external)
      io.forSimulation.registerServer(server)
    }
  }
}

module.exports = ServerSimulationApp
