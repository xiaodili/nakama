const h = require('../../shared/client_helpers.js')
const nev = require('../../shared/nevent.js')
const renderNevent = require('../../client/ui/components/nevents/renderer.js')

const NeventLogApp = (mount) => {
  return {
    onAppSentNson: (nson) => {
      const nevent = nev.serialiseNsonNevent(nson)
      mount.insertBefore(h.htmlToNode(renderNevent(nevent)), mount.firstChild)
    },
    onAppSentMsg: (msg) => {
      const nevent = nev.serialiseNsonNevent({
        type: 'MESSAGE',
        params: {
          content: msg
        }
      })
      //mount.appendChild(h.htmlToNode(renderNevent(nevent)))
      mount.insertBefore(h.htmlToNode(renderNevent(nevent)), mount.firstChild)
    },
    onPlayerSentIm: (userid, handle, msg) => {
      const nevent = nev.serialiseImNevent(userid, handle, msg)
      mount.insertBefore(h.htmlToNode(renderNevent(nevent)), mount.firstChild)
    }
  }
}

module.exports = NeventLogApp
