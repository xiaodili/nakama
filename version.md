# Version history

Tracks releases in production

## Smoke Testing

### Server, Client behaviour

* Change version on The Fort
* Make new room and play through a game of Who Am I

### CSS

* Test all three tabs

## Upcoming

## 02/02/18

2.1.1

* Bug fix: turns out loading page does need a background, otherwise it turns transparent!
* Finally adding proper favicon

2.1.0

* Rendering chat messages properly for Eliza

## 01/02/18

2.0.1

### Changes

Got the simulation part of the refactor working.

* Bug fix: game/api.js for `handlePlayerSentMessage` wasn't destructuring properly
* Bug fix: nson events weren't being persisted through `nevent_audit.js`
* Bug fix: notification colour passed through properly. Also, got rid of some dangling obsolete classes
* Bug fix: messages arriving out of order for chatbot
* Bug fix: user changing name wasn't being handled properly (client) `USER_ → PLAYER_`
* Prepending gives better experience than appending

---

2.0.0

### Changes

* Breaking: NSON changes (!)
    * Remember to update API on Zone
    * API and slash results need to be in NSON
    * `sendToNeventLog` → `sendMessage` and `sendNson`
* Workflow: jshint → eslint
* Bugfix (regression): style for inactive player added in

## 29/01/18

1.1.2

### Changes

* Made nevent log input look a bit better
* Passing in version number into the About page

## 28/01/18

1.1.1

### Bugs

* Fix the logo (need to copy over the `nakama_logo_embed.svg` again since I didn't break apart one text node). Actually, let's just make the new Nakam Zone logo for this ✓
* Fix the about page scrolling ✓
* Look to make a spritesheet for the icons. Must be an easier way, I don't want to spend this much time fiddling around with this. ✓
	* Try preloading in a head resource: https://fvsch.com/code/svg-icons/how-to/ . Only issue is that I'll have to specify this in the resources part. Although actually, maybe they can just be part of the default resource configuration. Still need to define the spec for it then. On second thought, it doesn't seem like it's actually that supported. Actually screw this; let's just make sure one of them is display:none. Done.
* Colour fix (was using the wrong shade of red beforehand

### Internal

* Move `window.init` back into Nakama core ✓

---

This is mostly debt clearup from the refactoring work that's been going in (and to fix the config bug that was excluding them from the client)

1.1.0

### Bugs

* Fixed a catalogue issue
* Cleaned up merging configs
* Fixed watchify not emitting errors when invalid build
* Fixed a logger issue (not using filepath logger means process doesn't terminate cleanly)
* Server build actually outputting meaningful errors with source-map-support
* Fixed a logger method issue (`err` → `error`)

### Internal

* Starting to use index.js convention (instead of app.js/server.js) for client, apps, and server
* Better `release.sh` script (Zone)
* Client doesn't need to pass in app index (catalogue) anymore
* Tweaked room name generation
* Using WeakMaps to cache jss stylesheets

### Features

* Themeable instances

## 24/01/18

I think we can make this a major version then!

1.0.0

* Factored out core so this now has one dependency.

## 23/12/17

0.13.6

* Tweaked room listing ordering (number, then action button now)
* Better nav icon positioning
* Added kudos to about page, and associated stylings
* Got rid of logging on focus.js
* Bug fix: Desktop pressing home on the side puts you onto the welcome page okay
* Styling on the mobile nav sidebar page
* Word Bag
    * Harry Potter nouns
    * Music (Rolling Stones)
    * Bug fix: Got rid of empty places (I swear I had stuff in them before...?)

## 15/12/17

0.13.5

* Better Logo placement
* Better word bag generate word placement (and wording)

## 10/12/17

0.13.4

* Fill in default colours for rooms sidebar, messages well
* Hide users page (and fixing its scroll)
* Added more copy to About

0.13.3

* Bug fix: Leaving room focuses on welcome

0.13.2

* Bug fix: Adjusting production port number to match nginx (9080)

0.13.1

* Bug fix: Removing unneeded require for `uglify-js` in `bundle_games.js`

0.13.0

* Feature: Slash commands (half implemented)
* Feature: Shortlinks
* Feature: Admin can change default tab
* Feature: Removed user tab //TODO

Also:

* Removed HTTPS (using nginx to proxy)
* Moved to cssinjs. Nicer UI
* Bug fix: on whenever you try to go enter page through a url
* Bug fix: moved fixing sPath to `/` to prevent cookie issues
* Bug fix: removing/adding passphrases on rooms now emit to all sockets

## 01/05/17

0.12.1

* Word bag: added sayings
* Word bag: added hiding words
* Word bag: added select/deselect all categories

0.12.0

* Feature: External events can be played back on the simulation

Also:

* Refactored game Server/Client interface
* Programming topic on Word Bag

## 29/04/17

0.11.0

* Shareable room URLs

Also:

* Bug fix on userInRoomWithOngoingGame
* Bug fix on inactive_players for getAccountDetails
* More sophisticated client side router


## 21/04/17

0.10.10

* Fixing bug

0.10.9

* Adding Harry Potter and Pokemon

0.10.8

* Adding birthday message

0.10.7

* More nigel words

0.10.6

* Word bag

* Logout user deletes session
* Config change: `auto_create_user` to `anonymous_user_allowed`
* Handle in top sidebar invisible if not set instead of undefined

## 20/04/17

0.10.5

* Bug fix on multiple rooms created when joining a passphrased room

Also:

* Fleshing out admin app (login/logout, players, endpoints) 
* Simplifying configuration around `account`
* Hash/salted room passphrases

## 10/04/17

0.10.4

* Bug fix on name select

## 26/03/17

0.10.3

* Finished fixing the http server config option

## 25/03/17

Quick-deploy in case we play Empires at Matt's

0.10.1

* Fixing syntax config issue in server

0.10.0

* Converted to flex to stop new Chrome (`> v56`)  layout issue
* (Feature) Message load and reply feature on simulation
* Started fleshing out an admin app
* Logging in and logging out incompletely implemented

## 21/02/17

0.9.3

* fixing client side establish session bug 

## 19/02/17

Bug fixes following Sunday's playtest

0.9.2

* Increased room unoccupancy tolerancy (config change)
* Fixed multiple player joined messages written to chat log if you have multiple tabs open
* Destroy room causes focus not to go to welcome/next room (removed `I_DESTROYED_ROOM` intra event (focus wasn't listening to this) and moved to io.emit for `PLAYER_DESTROYED_ROOM`)
* Name changing was broadcasting into the chat, even when the user hadn't joined the room 
* Upon rejoining, name doesn't appear in players list apart from the user themselves ✓

## 18/02/17

0.9.1

* Fixed event handling on the server for Empires and Who Am I (apply all deltas onto `game_state` instead of `participants`)

0.9.0

Cookies and multisocket support

* Cleaner reconnection logic
* Minor UX improvement on Who Am I
* Removed send button on chat
* Simplified game api event handling API (removed rooms concept)
* Upping inactivity tolerance for production

## 11/02/17

0.8.1

* Reducing log size, and stop sending games catalogue

Big release, pushing out the UX changes:

0.8.0

* About/Account/Home pages carved out
* Room tabs
* Mobile and desktop UX
* Redirection feature on server
* Handles must now be unique

## 27/01/17

0.7.6

* Fixed settings not showing up
* Re-arched

## 26/01/17

0.7.5

* Deploy script was broken (zip should take relative paths)

0.7.4

* Architecture change on admin policy
* Can now play games (again) in the password locked rooms as a result

## 22/01/17

0.7.3

* Reintroducting playerJoinedRoom event
* Better UX on handle generation

0.7.2

* Bug fix: socketid needs to be passed into API

## 21/01/17

0.7.1

* Fixed a game error handling issue (getting roomid from API)
* Cleared up client bundling (removed unnecessecary callback)

0.7.0

* PoC chatbot for both simulation and nakama
* Removing PlayerLeft game event
* Error handling for game doesn't crash server
* Simulation: can simulate chatting
* Refactorings
    * Simulation
    * Chat logs

## 19/01/17

0.6.6

* Join room is idempotent as well
* Better linting config
* Bug fix on enabling chat

## 17/01/17

0.6.5

* UX improvement on room creation

## 16/01/17

0.6.4

* bug fix on Who Am I?

## 15/01/17

0.6.4

* bug fix on inactive users (players regression)

## 11/01/17

0.6.3

* (re)fixed bug on room name validation
* added admin policy to config
* (UI) game master status indication

0.6.2

* Removed shared config
* Bug fix on `JOIN ROOM` event in Who Am I

## 10/01/17

0.6.1

* (Bug fix) deploy.sh

0.6.0

* (Feature) More endpoints (player, game, games)
* Removed Ruby dependency (sass)
* (Bug fix) Room setting regression fix

## 08/01/17

0.5.0

* Moved games catalogue to client
* (Feature) Only admins can start games
* (Bug fix) Another regression from players refactoring (unoccupied rooms)

0.4.2

* (Bug fix) Fixed kick bug
* Changed room joining policy
* Added more documentation

0.4.1

* (Bug fix) Another regression from players refactoring

## 07/01/17

0.4.0

* (Feature) Passphrase can be set
* Bug fix on passphrase (forgot that no userid is supplied for passphrase check passed)

## 05/01/17

0.3.1

* Fixed a create room bug (regression/incomplete refactoring)

0.3.0

* (Feature) Passphrased rooms

## 04/01/17

0.2.1

* (Feature WIP) Passphrased rooms
* Bug fix on room serialisations

## 03/01/17

0.2.0

* (Feature) Room descriptions
* Bug fix on enabling chat

## 02/01/17

0.1.1

* Games now sync properly on reconnect
* Identities list is shuffled on Empire

## 01/01/17

0.1.0

* Fixed bug regarding verify check on a nonexistent room
* (Feature) Games now handle player changing handles
* More client-side escaping on the games
* Fixed bug regarding supplying fake identities on mobile


## 12/31/16

0.0.1

Decided to start versioning today. Minor patch consisted mainly of:

* Fixing kicking logic

Still seems to be a bug somewhere in there in the people becoming inactive logic, but haven't had a chance to dig into it yet.
