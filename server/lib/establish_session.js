const inter = require('../../shared/socket_events.js')
const intra = require('./symbols/intra.js')
const ssh = require('../../shared/server_helpers.js')
const ct = require('../../shared/client_types.js')
const h = require('./helpers.js')

//<RegistryReader>, <RegistryReader>, <userid> → [roomid...]
const getSubscribedRooms = (players_reader, rooms_reader, userid, client_type) => {
  if (client_type === ct.CLIENT_APP) {
    return [...players_reader.getRoomsOccupied(userid)]
  } else {
    return rooms_reader.getRoomIds()
  }
}

const getPlayersForSerialisation = (players_reader, userid, client_type) => {
  if (client_type === ct.CLIENT_APP) {
    return players_reader.getPlayersForClientSerialisation()
  } else if (client_type === ct.ADMIN_APP) {
    return players_reader.getPlayersForAdminSerialisation()
  }
}

// if `anonymous_user_allowed` is enabled, do a 'smart' reconstruction of client state
const reconstructClientState = (logger, socket, nakama_pubsub, players_registry, rooms_registry, userid, _handle, rooms_occupied) => {
  //convenience
  const players_reader = players_registry.reader
  const players_writer = players_registry.writer
  const rooms_reader = rooms_registry.reader
  const rooms_writer = rooms_registry.writer

  const filterToValidRooms = (rooms) => {
    return rooms.filter((roomid) => {
      //TODO: decide what to do with passphrased rooms (ignoring them for now)
      //actually, this works regardless, since it bypasses the passphrase check in nakama_handler.js and occupants have already been authorised
      return (rooms_reader.getRoomIds().indexOf(roomid) !== -1)
    })
  }

  logger.log(`attempting to reconstruct same setup as (${userid}, ${_handle}) left it`)
  const handle = h.generateUniqueHandle(_handle, players_reader.isHandleTaken)
  players_writer.registerPlayer(userid, handle)
  socket.broadcast.emit(inter.NEW_PLAYER_REGISTERED, {
    userid,
    handle
  })
  const valid_rooms = filterToValidRooms(rooms_occupied)
  for (let roomid of valid_rooms) {
    let msg = {
      userid,
      roomid
    }
    rooms_writer.playerJoinedRoom(userid, roomid)
    players_writer.addPlayerToRoom(userid, roomid)
    socket.broadcast.emit(inter.PLAYER_JOINED_ROOM, msg)
    logger.log(`emitted ${inter.PLAYER_JOINED_ROOM} with ${JSON.stringify(msg)}`)
    nakama_pubsub.pub(intra.PLAYER_JOINED_ROOM, msg)
  }
}

//
const syncSessionForUser = (userid, on_mobile, client_type, socket, players_reader, rooms_reader, nakama_pubsub, config_for_client) => {
  socket.self = {
    userid
  }
  const handle = players_reader.getHandleFromId(userid)
  const rooms = rooms_reader.getRoomsForClientAppSerialisation()
  const players = getPlayersForSerialisation(players_reader, userid, client_type)
  const subscribed_rooms = getSubscribedRooms(players_reader, rooms_reader, userid, client_type)
  const omsg = {
    players,
    rooms,
    user: {
      userid,
      handle,
      using_cookies: players_reader.getUsingCookies(userid),
      subscribed_rooms
    },
    config_for_client
  }
  for (let roomid of subscribed_rooms) {
    socket.join(roomid)
  }
  socket.emit(inter.ESTABLISHED_SESSION, omsg)
  //logger.log(`emitted ${inter.ESTABLISHED_SESSION} with ${JSON.stringify(Object.assign(
  //{},
  //omsg,
  //{rooms: rooms_reader.getRoomsForLoggingSerialisation()}
  //))} (room titles and descriptions omitted)`)
  nakama_pubsub.pub(intra.PLAYER_CONNECTED, {
    userid,
    socketid: socket.id,
    on_mobile,
    client_type
  })
}

const registerNewClientAppUser = (userid, handle, players_writer, socket, on_mobile) => {
  players_writer.registerPlayer(userid, handle)
  players_writer.addConnectionToPlayer(userid, socket.id, on_mobile, ct.CLIENT_APP)
  socket.broadcast.emit(inter.NEW_PLAYER_REGISTERED, {
    userid,
    handle
  })
  return userid
}

//after all this cookie business has been dealt with
const continueSessionSync = (logger, io, nakama_pubsub, socket, players_registry, rooms_registry, config_for_client, userid, handle, rooms, on_mobile, client_type, session_id) => {
  //convenience
  const players_reader = players_registry.reader
  const players_writer = players_registry.writer
  const rooms_reader = rooms_registry.reader
  const rooms_writer = rooms_registry.writer
  const {
    anonymous_user_allowed,
    allow_conversion_to_account
  } = config_for_client.account

  if (players_reader.getPlayer(userid)) { //temporary disconnection
    logger.log(`userid ${userid} found in registry`)
    const session_id_valid = players_reader.validateSessionId(userid, session_id)
    if (anonymous_user_allowed || session_id_valid) {
      syncSessionForUser(userid, on_mobile, client_type, socket, players_reader, rooms_reader, nakama_pubsub, config_for_client)
    } else {
      if (!session_id_valid) {
        socket.emit(inter.SESSION_INVALID)
        socket.disconnect()
      } else { //!anonymous_user_allowed
        socket.emit(inter.LOGIN_REQUIRED)
        socket.disconnect()
      }
    }
  } else { //stale connection (need to resurrect); attempt to build up client's last impression of setup
    logger.log(`userid ${userid} not found in registry but server_id consistent`)
    if (anonymous_user_allowed) {
      logger.log(`reconstructing setup to best ability`)
      reconstructClientState(logger, socket, nakama_pubsub, players_registry, rooms_registry, userid, handle, rooms)
      syncSessionForUser(userid, on_mobile, client_type, socket, players_reader, rooms_reader, nakama_pubsub, config_for_client)
    } else {
      logger.log(`emitting ${inter.LOGIN_REQUIRED}`)
      socket.emit(inter.LOGIN_REQUIRED)
      socket.disconnect()
    }
  }
}

const continueAsNewConnection = (logger, players_registry, rooms_reader, socket, on_mobile, client_type, nakama_pubsub, config_for_client) => {
  const players_reader = players_registry.reader
  const players_writer = players_registry.writer
  const {
    anonymous_user_allowed
  } = config_for_client.account

  logger.log(`server_id not present or consistent; treating as new user`)
  if (anonymous_user_allowed && client_type === ct.CLIENT_APP) {
    const userid = ssh.makeId(15)
    const handle = ssh.generateRandomHandle()
    logger.log(`creating a new anonymous user with userid ${userid} and handle ${handle}`)
    syncSessionForUser(
      registerNewClientAppUser(userid, handle, players_writer, socket, on_mobile),
      on_mobile, client_type, socket, players_reader, rooms_reader, nakama_pubsub, config_for_client)
  } else {
    logger.log(`emitting ${inter.LOGIN_REQUIRED}`)
    socket.emit(inter.LOGIN_REQUIRED)
    socket.disconnect()
  }
}

const handleEstablishSession = (logger, io, nakama_pubsub, socket, msg, players_registry, rooms_registry, config_for_client) => {
  //convenience
  const players_reader = players_registry.reader
  const players_writer = players_registry.writer
  const rooms_reader = rooms_registry.reader
  const rooms_writer = rooms_registry.writer
  const SERVERID = config_for_client.server_id
  const cookies_enabled = config_for_client.cookies_enabled

  const userid = msg.userid
  const handle = msg.handle
  const on_mobile = msg.on_mobile
  const server_id = msg.server_id
  const session_id = msg.session_id
  const client_type = msg.client_type
  const cookie = msg.cookie //{userid <string>, handle <string>, rooms: [roomid...], server_id <string>, session_id <string>}
  const rooms_occupied = msg.rooms_occupied //[roomid...]

  logger.log(`received ${inter.ESTABLISH_SESSION} with ${JSON.stringify(msg)}`)

  const failedSync = () => {
    continueAsNewConnection(
      logger,
      players_registry,
      rooms_reader,
      socket,
      on_mobile,
      client_type,
      nakama_pubsub,
      config_for_client)
  }

  if (!server_id) { //app not initialised
    if (cookies_enabled && cookie && cookie.server_id === SERVERID) { //cookies enabled on server and client cookie has valid server_id
      logger.log(`app not initialised but cookie valid. continuing session sync with cookie information`)
      continueSessionSync(
        logger,
        io,
        nakama_pubsub,
        socket,
        players_registry,
        rooms_registry,
        config_for_client,
        cookie.userid,
        cookie.handle,
        cookie.rooms,
        on_mobile,
        client_type,
        cookie.session_id)
    } else {
      failedSync()
    }
  } else { //app already initialised
    if (server_id === SERVERID) { //serverid matches up
      continueSessionSync(
        logger,
        io,
        nakama_pubsub,
        socket,
        players_registry,
        rooms_registry,
        config_for_client,
        userid,
        handle,
        rooms_occupied,
        on_mobile,
        client_type,
        session_id)
    } else {
      failedSync()
    }
  }
}

exports.handleEstablishSession = handleEstablishSession
