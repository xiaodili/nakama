const inter = require('../../shared/socket_events.js')
const e = require('./symbols/enums.js')
const intra = require('./symbols/intra.js')
const h = require('./helpers.js')
const sh = require('../../shared/helpers.js')
const ssh = require('../../shared/server_helpers.js')
const GameHandler = require('./game/handler.js')
const InactivePlayers = require('./system/inactive_players.js')
const UnoccupiedRooms = require('./system/unoccupied_rooms.js')
const NeventAudit = require('./system/nevent_audit.js')
const AdminAssigner = require('./system/admin_assigner.js')
const SlashCommand = require('./system/slash_command.js')
const handleEstablishSession = require('./establish_session.js').handleEstablishSession

const NakamaHandler = (io, players_registry, rooms_registry, nevents_registry, games_registry, config, config_for_client, logger) => {

  const players_writer = players_registry.writer
  const players_reader = players_registry.reader
  const rooms_writer = rooms_registry.writer
  const rooms_reader = rooms_registry.reader
  const nevents_writer = nevents_registry.writer
  const nevents_reader = nevents_registry.reader
  const games_reader = games_registry.reader
  const games_writer = games_registry.writer
  const default_room_creation_properties = config.default_room_creation_properties
  const nakama_pubsub = sh.PubSub()
  const game_handler = GameHandler(logger, io, players_registry, rooms_registry, games_registry, nakama_pubsub)
  const inactive_players = InactivePlayers(logger, io, nakama_pubsub, config.disconnection_grace_timeout_seconds.desktop, config.disconnection_grace_timeout_seconds.mobile, config.disconnection_grace_timeout_seconds.while_gaming, players_registry, rooms_registry)
  const nevent_audit = NeventAudit(logger, nakama_pubsub, rooms_reader, players_reader, nevents_writer)
  const unoccupied_rooms = UnoccupiedRooms(logger, config.unoccupied_room_cleanup_policy_seconds, rooms_registry, nakama_pubsub, (roomid) => {
    nakama_pubsub.pub(intra.ROOM_DESTROYED, roomid)
    rooms_writer.removeRoom(roomid)
    nevents_writer.removeNeventLog(roomid)
    io.emit(inter.UNOCCUPIED_ROOM_DESTROYED, roomid)
  })
  const admin_assigner = AdminAssigner(logger, io, nakama_pubsub, rooms_registry)
  const slash_command = SlashCommand(games_reader, players_reader, rooms_reader)

  //helper functions
  const createRoomForUser = (userid) => {
    const room_name = h.generateRandomRoomName()
    const admin_policy = default_room_creation_properties.admin_policy
    const roomid = ssh.makeId(15)
    rooms_writer.createRoom(
      roomid,
      room_name,
      "", //description
      default_room_creation_properties.autojoin,
      default_room_creation_properties.nevent_log_enabled,
      default_room_creation_properties.games_enabled,
      default_room_creation_properties.player_list_enabled,
      userid,
      null,
      admin_policy,
      default_room_creation_properties.default_tab
    )
    if (admin_policy === e.ADMIN_POLICIES.CREATOR) {
      rooms_writer.assignAdmin(roomid, userid)
    }
    nevents_writer.createNeventLog(roomid)
    return roomid
  }
  
  const handleSocketConnection = (socket) => {
    logger.log(`new socket connection with id ${socket.id}`)

    socket.on(inter.I_ENABLED_COOKIES, () => {
      const userid = socket.self.userid
      logger.log(`received ${inter.I_ENABLED_COOKIES} from ${userid}`)
      if (config.cookies.enabled) {
        players_writer.enableCookies(userid)
        for (let socketid of players_reader.getSocketIds(userid)) {
          //relay to any other synced users
          if (socketid !== socket.id) {
            io.sockets.connected[socketid].emit(inter.COOKIES_ENABLED_BY_MYSELF)
          }
        }
      }
    })

    //handshakes
    socket.on(inter.I_SUBMITTED_CONVERT_TO_ACCOUNT_REQUEST, (passphrase) => {
      const userid = socket.self.userid
      //TODO redact passphrase from logs
      logger.log(`received ${inter.I_SUBMITTED_CONVERT_TO_ACCOUNT_REQUEST} from ${userid} with <redacted>`)
      h.generatePasswordHashAsync(passphrase, (result) => {
        const hash = result[0]
        const salt = result[1]
        players_writer.convertUserToAccount(userid, salt, hash)
        h.generateSessionIdAsync((sessionid) => {
          for (let socketid of players_reader.getSocketIds(userid)) {
            io.sockets.connected[socketid].emit(inter.ACCOUNT_CONVERSION_REQUEST_SUCCESSFUL, {
              sessionid
            })
          }
          logger.log(`replied with ${inter.ACCOUNT_CONVERSION_REQUEST_SUCCESSFUL}`)
        })
      })
    })

    //TODO (probably going to scrap this and use the API instead...?)
    //remove socket ids for player
    //create a new user?
    socket.on(inter.I_SUBMITTED_LOGOUT_REQUEST, () => {
      const userid = socket.self.userid
    })

    socket.on(inter.I_SUBMITTED_ACCOUNT_PASSPHRASE_ATTEMPT, (msg) => {
      const userid = socket.self.userid
      const handle = msg.handle
      const passphrase = msg.passphrase
      logger.log(`received ${inter.I_SUBMITTED_ACCOUNT_PASSPHRASE_ATTEMPT} from ${userid} with ${handle}/<redacted>`)
      const account = players_reader.getAccountDetails(handle)
      if (account) {
        const hash = account.hash
        const salt = account.salt
        h.verifyPasswordHashAsync(passphrase, salt, hash, (is_match) => {
          //TODO this is like a conversion, of sorts. Need to remember to re-juggle the socket ids
          socket.emit(inter.ACCOUNT_PASSPHRASE_ATTEMPT_RESULT_READY, is_match)
        })
      } else {
        socket.emit(inter.ACCOUNT_PASSPHRASE_ATTEMPT_RESULT_READY, false)
      }
    })

    socket.on(inter.I_DISABLED_COOKIES, () => {
      const userid = socket.self.userid
      logger.log(`received ${inter.I_DISABLED_COOKIES} from ${userid}`)
      if (config.cookies.enabled) {
        players_writer.disableCookies(userid)
        for (let socketid of players_reader.getSocketIds(userid)) {
          //relay to any other synced users
          if (socketid !== socket.id) {
            io.sockets.connected[socketid].emit(inter.COOKIES_DISABLED_BY_MYSELF)
          }
        }
      }
    })

    socket.on(inter.PLAYER_REMOVED_SHORTLINK, (roomid) => {
      logger.log(`received ${inter.PLAYER_REMOVED_SHORTLINK}`)
      const userid = socket.self.userid
      if (rooms_reader.isPlayerAdminOf(roomid, userid)) {
        rooms_writer.removeShortlink(roomid)
        socket.emit(inter.PLAYER_REMOVED_SHORTLINK, roomid)
      }
    })

    socket.on(inter.PLAYER_CHANGED_SHORTLINK, (msg) => {
      logger.log(`received ${inter.PLAYER_CHANGED_SHORTLINK} with ${JSON.stringify(msg)}`)
      const userid = socket.self.userid
      const {
        roomid,
        shortlink
      } = msg
      if (rooms_reader.isPlayerAdminOf(roomid, userid)) {
        rooms_writer.changeShortlink(roomid, shortlink)
        socket.emit(inter.PLAYER_CHANGED_SHORTLINK, msg)
      }
    })

    socket.on(inter.I_REMOVED_PASSPHRASE, (roomid) => {
      logger.log(`received ${inter.I_REMOVED_PASSPHRASE} with ${roomid}`)
      const userid = socket.self.userid
      if (rooms_reader.isPlayerAdminOf(roomid, userid)) {
        rooms_writer.removePassphrase(roomid)
        io.emit(inter.PLAYER_REMOVED_PASSPHRASE, roomid)
      }
    })

    socket.on(inter.I_CHANGED_PASSPHRASE, (msg) => {
      logger.log(`received ${inter.I_CHANGED_PASSPHRASE} with ${JSON.stringify(msg)}`)
      const userid = socket.self.userid
      const roomid = msg.roomid
      const room = rooms_reader.getRoom(roomid)
      if (rooms_reader.isPlayerAdminOf(roomid, userid)) {
        let old_passphrase = room.passphrase
        let salt_hash = h.generatePasswordHashSync(msg.passphrase)
        rooms_writer.changePassphrase(roomid, salt_hash)
        if (!old_passphrase) {
          io.emit(inter.PLAYER_ADDED_PASSPHRASE, roomid)
        }
      }
    })

    socket.on(inter.I_KICKED_PLAYER, (msg) => {
      logger.log(`received ${inter.I_KICKED_PLAYER} with ${JSON.stringify(msg)}`)
      const userid = socket.self.userid
      const playerid = msg.playerid
      const roomid = msg.roomid
      if (rooms_reader.isPlayerAdminOf(roomid, userid)) {
        players_writer.removePlayerFromRoom(playerid, roomid)
        rooms_writer.playerLeftRoom(playerid, roomid)
        nakama_pubsub.pub(intra.PLAYER_WAS_KICKED, msg)
        io.emit(inter.PLAYER_WAS_KICKED, msg)
        for (let socketid of [...players_reader.getSocketIds(playerid)]) {
          io.sockets.connected[socketid].leave(roomid)
        }
      }
    })

    //Room Settings events
    socket.on(inter.I_UNPROTECTED_ROOM, (roomid) => {
      logger.log(`received ${inter.I_UNPROTECTED_ROOM} with ${roomid}`)
      const userid = socket.self.userid
      if (rooms_reader.isPlayerAdminOf(roomid, userid)) {
        rooms_writer.unprotectRoom(roomid)
        io.emit(inter.PLAYER_UNPROTECTED_ROOM, {
          userid,
          roomid
        })
      }
    })
    socket.on(inter.I_PROTECTED_ROOM, (roomid) => {
      logger.log(`received ${inter.I_PROTECTED_ROOM} with ${roomid}`)
      const userid = socket.self.userid
      if (rooms_reader.isPlayerAdminOf(roomid, userid)) {
        rooms_writer.protectRoom(roomid)
        io.emit(inter.PLAYER_PROTECTED_ROOM, {
          userid,
          roomid
        })
      }
    })

    //TODO: change these to IO.emit
    socket.on(inter.I_CHANGED_ROOM_DESCRIPTION, (msg) => {
      logger.log(`received ${inter.I_CHANGED_ROOM_DESCRIPTION} with ${JSON.stringify(msg)}`)
      const userid = socket.self.userid
      const roomid = msg.roomid
      if (rooms_reader.isPlayerAdminOf(roomid, userid)) {
        let description = msg.description
        rooms_writer.changeDescription(roomid, description)
        socket.broadcast.emit(inter.PLAYER_CHANGED_ROOM_DESCRIPTION, msg)
      }
    })
    socket.on(inter.I_CHANGED_ROOM_NAME, (msg) => {
      logger.log(`received ${inter.I_CHANGED_ROOM_NAME} with ${JSON.stringify(msg)}`)
      const userid = socket.self.userid
      const roomid = msg.roomid
      if (rooms_reader.isPlayerAdminOf(roomid, userid)) {
        let name = msg.name
        let valid = sh.roomNameValidate(name)
        if (valid) {
          rooms_writer.changeName(roomid, name)
          socket.broadcast.emit(inter.PLAYER_CHANGED_ROOM_NAME, msg)
        }
      }
    })
    socket.on(inter.I_DESTROYED_ROOM, (roomid) => {
      logger.log(`received ${inter.I_DESTROYED_ROOM} with ${roomid}`)
      const userid = socket.self.userid
      if (rooms_reader.isPlayerAdminOf(roomid, userid)) {
        nakama_pubsub.pub(intra.ROOM_DESTROYED, {
          roomid
        })
        rooms_writer.removeRoom(roomid)
        players_writer.removePlayerFromRoom(userid, roomid)
        nevents_writer.removeNeventLog(roomid)
        io.emit(inter.PLAYER_DESTROYED_ROOM, {
          roomid,
          userid
        })
      }
    })
    socket.on(inter.I_CHANGED_DEFAULT_TAB, (msg) => {
      logger.log(`received ${inter.I_CHANGED_DEFAULT_TAB} with ${JSON.stringify(msg)}`)
      const userid = socket.self.userid
      const {
        roomid,
        tab_type
      } = msg
      if (rooms_reader.isPlayerAdminOf(roomid, userid)) {
        //TODO validate on tab type
        rooms_writer.changeDefaultTab(roomid, tab_type)
        io.emit(inter.PLAYER_CHANGED_DEFAULT_TAB, {
          userid,
          roomid,
          tab_type
        })
      }
    })
    socket.on(inter.I_ENABLED_GAMES, (roomid) => {
      logger.log(`received ${inter.I_ENABLED_GAMES} with ${roomid}`)
      const userid = socket.self.userid
      if (rooms_reader.isPlayerAdminOf(roomid, userid)) {
        rooms_writer.enableGames(roomid)
        io.emit(inter.PLAYER_ENABLED_GAMES, {
          roomid,
          userid
        })
      }
    })
    socket.on(inter.I_DISABLED_GAMES, (roomid) => {
      logger.log(`received ${inter.I_ENABLED_GAMES} with ${roomid}`)
      const userid = socket.self.userid
      if (rooms_reader.isPlayerAdminOf(roomid, userid)) {
        rooms_writer.disableGames(roomid)
        let gseshid = rooms_reader.getGseshidMaybe(roomid)
        if (gseshid) {
          rooms_writer.unsetGameOngoing(roomid, gseshid)
          games_writer.removeGameApi(gseshid)
        }
        io.emit(inter.PLAYER_DISABLED_GAMES, {
          roomid,
          userid
        })
      }
    })
    socket.on(inter.I_ENABLED_NEVENT_LOG, (roomid) => {
      logger.log(`received ${inter.I_ENABLED_NEVENT_LOG} with ${roomid}`)
      const userid = socket.self.userid
      if (rooms_reader.isPlayerAdminOf(roomid, userid)) {
        rooms_writer.enableNeventLog(roomid)
        nevents_writer.createNeventLog(roomid)
        io.emit(inter.PLAYER_ENABLED_NEVENT_LOG, {
          roomid,
          userid
        })
      }
    })
    socket.on(inter.I_DISABLED_NEVENT_LOG, (roomid) => {
      logger.log(`received ${inter.I_DISABLED_NEVENT_LOG} with ${roomid}`)
      const userid = socket.self.userid
      if (rooms_reader.isPlayerAdminOf(roomid, userid)) {
        rooms_writer.disableNeventLog(roomid)
        nevents_writer.removeNeventLog(roomid)
        io.emit(inter.PLAYER_DISABLED_NEVENT_LOG, {
          roomid,
          userid
        })
      }
    })
    socket.on(inter.I_ENABLED_PLAYER_LIST, (roomid) => {
      logger.log(`received ${inter.I_ENABLED_PLAYER_LIST} with ${roomid}`)
      const userid = socket.self.userid
      if (rooms_reader.isPlayerAdminOf(roomid, userid)) {
        rooms_writer.enablePlayerList(roomid)
        io.emit(inter.PLAYER_ENABLED_PLAYER_LIST, {
          roomid,
          userid
        })
      }
    })
    socket.on(inter.I_DISABLED_PLAYER_LIST, (roomid) => {
      logger.log(`received ${inter.I_DISABLED_PLAYER_LIST} with ${roomid}`)
      const userid = socket.self.userid
      if (rooms_reader.isPlayerAdminOf(roomid, userid)) {
        rooms_writer.disablePlayerList(roomid)
        io.emit(inter.PLAYER_DISABLED_PLAYER_LIST, {
          roomid,
          userid
        })
      }
    })

    socket.on(inter.I_REQUESTED_ROOM_CREATION, (msg) => {
      logger.log(`received ${inter.I_REQUESTED_ROOM_CREATION} with ${JSON.stringify(msg)}`)
      const userid = msg.userid
      const roomid = createRoomForUser(userid)
      io.emit(inter.ROOM_CREATED, rooms_registry.reader.getRoomForClientAppSerialisation(roomid))
      socket.emit(inter.ROOM_CREATION_REQUEST_FUFILLED, roomid)
    })

    //TODO: move this into game/handler.js
    socket.on(inter.GAME_EVENT, (msg) => {
      logger.log(`received ${inter.GAME_EVENT} with ${JSON.stringify(msg)}`)
      game_handler.handleIncomingClientEvent(msg)
    })

    socket.on(inter.I_REQUESTED_CLIENT_BUNDLE, (roomid) => {
      logger.log(`received ${inter.I_REQUESTED_CLIENT_BUNDLE} with ${roomid}`)
      game_handler.fetchClientBundle(roomid, (gseshid, client_bundle) => {
        socket.emit(inter.CLIENT_BUNDLE_READY, {
          bundle: client_bundle,
          gseshid,
          roomid
        })
      })
    })

    socket.on(inter.I_REQUESTED_GAME_START_FROM_URL, (msg) => {
      logger.log(`received ${inter.I_REQUESTED_GAME_START_FROM_URL} with ${JSON.stringify(msg)}`)
      const userid = msg.userid
      const gameid = msg.gameid
      const handle = players_reader.getHandleFromId(userid)
      const roomid = createRoomForUser(userid, handle)
      //TODO no guarantee on the client receiving ROOM_CREATED, GAME_STARTED, and GAME_STARTED_FROM_URL events in order
      io.emit(inter.ROOM_CREATED, rooms_registry.reader.getRoomForClientAppSerialisation(roomid))
      game_handler.initialiseGame(userid, roomid, gameid)
      logger.log(`socket replying ${inter.GAME_STARTED_FROM_URL} with ${roomid}`)
      socket.emit(inter.GAME_STARTED_FROM_URL, roomid)
    })

    socket.on(inter.I_REQUESTED_GAME_START, (msg) => {
      logger.log(`received ${inter.I_REQUESTED_GAME_START} with ${JSON.stringify(msg)}`)
      const userid = msg.userid
      const roomid = msg.roomid
      const gameid = msg.gameid
      if (rooms_reader.isPlayerAdminOf(roomid, userid)) {
        game_handler.initialiseGame(userid, roomid, gameid)
      }
    })
    socket.on(inter.I_CHANGED_HANDLE, (msg) => {
      logger.log(`received ${inter.I_CHANGED_HANDLE} with ${JSON.stringify(msg)}`)
      const userid = msg.userid
      const handle = msg.handle
      const old_handle = msg.old_handle
      const valid = sh.handleValidate(handle)
      const taken = players_reader.isHandleTaken(handle)
      if (valid && !taken) {
        players_writer.updateHandle(userid, old_handle, handle)
        nakama_pubsub.pub(intra.PLAYER_CHANGED_HANDLE, {
          userid,
          handle,
          old_handle
        })
        io.emit(inter.PLAYER_CHANGED_HANDLE, {
          userid,
          handle,
          old_handle
        })
      } else {
        //Noop; will figure out what to do here
      }
    })

    socket.on(inter.I_REQUESTED_NEW_HANDLE, () => {
      const userid = socket.self.userid
      logger.log(`received ${inter.I_REQUESTED_NEW_HANDLE} with ${userid}`)
      const handle = h.generateUniqueHandle(ssh.generateRandomHandle(), players_reader.isHandleTaken)
      const old_handle = players_reader.getHandleFromId(userid)
      players_writer.updateHandle(userid, old_handle, handle)
      nakama_pubsub.pub(intra.PLAYER_CHANGED_HANDLE, {
        userid,
        handle,
        old_handle
      })
      const omsg = {
        userid,
        handle,
        old_handle
      }
      io.emit(inter.PLAYER_CHANGED_HANDLE, omsg)
      logger.log(`emitted ${inter.PLAYER_CHANGED_HANDLE} with ${JSON.stringify(omsg)}`)
    })

    socket.on(inter.I_JOINED_ROOM, (msg) => {
      logger.log(`received ${inter.I_JOINED_ROOM} with ${JSON.stringify(msg)}`)
      const userid = msg.userid
      const roomid = msg.roomid
      const hash_salt = rooms_reader.getRoom(roomid).passphrase
      if (hash_salt) {
        if (msg.passphrase) { //TODO: handle joining a passphrased room via shortlink
          const valid = h.verifyPasswordHashSync(msg.passphrase, hash_salt[1], hash_salt[0])
          if (!valid) {
            return //swallow request silently; client should have prompted user to supply a passphrase
          }
        } else {
          return //swallow request silently; client should have prompted user to supply a passphrase
        }
      }
      for (let socketid of players_reader.getSocketIds(userid)) {
        io.sockets.connected[socketid].join(roomid)
      }
      rooms_writer.playerJoinedRoom(userid, roomid)
      players_writer.addPlayerToRoom(userid, roomid)
      io.emit(inter.PLAYER_JOINED_ROOM, msg)
      logger.log(`emitted ${inter.PLAYER_JOINED_ROOM} with ${JSON.stringify(msg)}`)
      nakama_pubsub.pub(intra.PLAYER_JOINED_ROOM, {
        userid,
        roomid
      })
    })

    socket.on(inter.I_LEFT_ROOM, (msg) => {
      logger.log(`received ${inter.I_LEFT_ROOM} with ${JSON.stringify(msg)}`)
      const userid = msg.userid
      const roomid = msg.roomid
      rooms_writer.playerLeftRoom(userid, roomid)
      players_writer.removePlayerFromRoom(userid, roomid)
      for (let socketid of players_reader.getSocketIds(userid)) {
        io.sockets.connected[socketid].leave(roomid)
      }
      io.emit(inter.PLAYER_LEFT_ROOM, msg)
      nakama_pubsub.pub(intra.PLAYER_LEFT_ROOM, msg)
    })

    socket.on(inter.ESTABLISH_SESSION, (msg) => {
      handleEstablishSession(logger, io, nakama_pubsub, socket, msg, players_registry, rooms_registry, config_for_client)
    })

    socket.on(inter.SYNC_NEVENT_LOG_REQUESTED, (roomid) => {
      const userid = socket.self.userid
      logger.log(`received ${inter.SYNC_NEVENT_LOG_REQUESTED} with ${roomid}`)
      const messages = nevents_reader.getNeventsFor(userid, roomid)
      const slash_commands = slash_command.getSlashCommandsForRoom(roomid)
      socket.emit(inter.SYNCED_NEVENT_LOG, {
        roomid,
        messages,
        slash_commands
      })
    })

    //Client's decided that the bit of string it's sending the server is a slash command
    //Might as well, since it needs knowledge of how to autocomplete
    socket.on(inter.I_SENT_SLASH_COMMAND, (msg) => {
      const userid = socket.self.userid
      const roomid = msg.roomid
      const command = msg.message
      logger.log(`received ${inter.I_SENT_SLASH_COMMAND} with ${JSON.stringify(msg)}`)
      //const command_result =
      slash_command.handleCommandForRoom(userid, roomid, command, (nson_result) => {
        socket.emit(inter.SLASH_COMMAND_RESULT_READY, {
          nson_result
        })
      }) 
    })

    socket.on(inter.I_SENT_IM, (msg) => {
      const userid = socket.self.userid
      logger.log(`received ${inter.I_SENT_IM} with ${JSON.stringify(msg)}`)
      const {
        roomid,
        message
      } = msg
      const omsg = {
        userid,
        roomid,
        message
      }
      socket.to(roomid).broadcast.emit(inter.PLAYER_SENT_IM, omsg)
      //Do this after, to messages from app and sender arrive in the right order
      nakama_pubsub.pub(intra.PLAYER_SENT_IM, omsg)
    })

    socket.on(inter.I_STARTED_TYPING, (msg) => {
      const userid = msg.userid
      const roomid = msg.roomid
      socket.to(roomid).broadcast.emit(inter.PLAYER_STARTED_TYPING, msg)
    })
    socket.on(inter.I_STOPPED_TYPING, (msg) => {
      const userid = msg.userid
      const roomid = msg.roomid
      socket.to(roomid).broadcast.emit(inter.PLAYER_STOPPED_TYPING, msg)
    })

    socket.on(inter.I_DISCONNECTED, () => {
      logger.log(`received ${inter.I_DISCONNECTED} with socket id ${socket.id}`)
      nakama_pubsub.pub(intra.PLAYER_DISCONNECTED, {
        userid: socket.self? socket.self.userid : null,
        socketid: socket.id
      })
    })
  }

  //international events
  io.on(inter.CONNECTION, handleSocketConnection)

  return {
    init: (startup_options) => {
      const rooms = startup_options.rooms
      for (let room of rooms) {
        let name = ("name" in room)? room.name : h.generateRandomRoomName()
        let id = ssh.makeId(15)
        let autojoin = ("autojoin" in room)? room.autojoin : default_room_creation_properties.autojoin
        let games_enabled = ("games_enabled" in room)? room.games_enabled : default_room_creation_properties.games_enabled
        let nevent_log_enabled = ("nevent_log_enabled" in room)? room.nevent_log_enabled : default_room_creation_properties.nevent_log_enabled
        let player_list_enabled = ("player_list_enabled" in room)? room.player_list_enabled : default_room_creation_properties.player_list_enabled
        let description =  ("description" in room)? room.description : ""
        let passphrase = ("passphrase" in room)? h.generatePasswordHashSync(room.passphrase) : null
        let admin_policy = ("admin_policy" in room)? room.admin_policy : default_room_creation_properties.admin_policy
        let default_tab = ("default_tab" in room)? room.default_tab : default_room_creation_properties.default_tab
        rooms_writer.createRoom(id, name, description, autojoin, nevent_log_enabled, games_enabled, player_list_enabled, e.STARTUP, passphrase, admin_policy, default_tab)
        if (nevent_log_enabled) {
          nevents_writer.createNeventLog(id)
        }
      }
    },
    //needed for the snapshot API
    initialiseGameFromSnapshot: game_handler.initialiseGameFromSnapshot,
    getState: () => {
      return {
        players: players_reader.getPlayers(),
        rooms: rooms_reader.getRoomsForLoggingSerialisation(),
        nevents: nevents_reader.getAllNevents()
      }
    }
  }
}

module.exports = NakamaHandler
