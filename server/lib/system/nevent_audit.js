const intra = require('../symbols/intra.js')
const nev = require('../../../shared/nevent.js')

const NeventAudit = (logger, nakama_pubsub, rooms_reader, players_reader, nevents_writer) => {
  const handlePlayerLeftRoom = (msg) => {
    const userid = msg.userid
    const roomid = msg.roomid
    if (rooms_reader.isNeventLogEnabled(roomid)) {
      let handle = players_reader.getHandleFromId(userid)
      const notification = nev.serialiseNotificationNevent(
        nev.notifications.USER_LEFT, {
          userid,
          handle
        })
      nevents_writer.storeNevent(roomid, notification)
    }
  }
  const handlePlayerJoinedRoom = (msg) => {
    const userid = msg.userid
    const roomid = msg.roomid
    if (rooms_reader.isNeventLogEnabled(roomid)) {
      const handle = players_reader.getHandleFromId(userid)
      const notification = nev.serialiseNotificationNevent(
        nev.notifications.USER_JOINED, {
          userid,
          handle
        })
      nevents_writer.storeNevent(roomid, notification)
    }
  }

  const handlePlayerWasKicked = (msg) => {
    const kickerid = msg.kickerid
    const playerid = msg.playerid
    const roomid = msg.roomid
    if (rooms_reader.isNeventLogEnabled(roomid)) {
      const kicker = players_reader.getHandleFromId(kickerid)
      const kickee = players_reader.getHandleFromId(playerid)
      const notification = nev.serialiseNotificationNevent(
        nev.notifications.PLAYER_WAS_KICKED, {
          kickee,
          kickeeid: playerid,
          kicker,
          kickerid
        })
      nevents_writer.storeNevent(roomid, notification)
    }
  }

  const handlePlayerChangedHandle = (msg) => {
    const userid = msg.userid
    const old_handle = msg.old_handle
    const handle = msg.handle
      for (let roomid of players_reader.getRoomsOccupied(userid)) {
        if (rooms_reader.isNeventLogEnabled(roomid)) {
          const notification = nev.serialiseNotificationNevent(
            nev.notifications.USER_CHANGED_HANDLE, {
              userid,
              old_handle,
              handle
            })
          nevents_writer.storeNevent(roomid, notification)
        }
      }
    }

  const handleAppCrashed = (msg) => {
    const roomid = msg.roomid
    const game_name = msg.game_name
    if (rooms_reader.isNeventLogEnabled(roomid)) {
      const notification = nev.serialiseNotificationNevent(
        nev.notifications.APP_CRASHED, game_name)
      nevents_writer.storeNevent(roomid, notification)
    }
  }

  const handleAppEnded = (msg) => {
    const {
      game_name,
      roomid
    } = msg
    if (rooms_reader.isNeventLogEnabled(roomid)) {
      const notification = nev.serialiseNotificationNevent(
        nev.notifications.APP_ENDED, game_name)
      nevents_writer.storeNevent(roomid, notification)
    }
  }

  const handleAppStarted = (msg) => {
    const {
      roomid,
      app_name,
      userid
    } = msg
    const handle = players_reader.getHandleFromId(userid)
    if (rooms_reader.isNeventLogEnabled(roomid)) {
      const notification = nev.serialiseNotificationNevent(
        nev.notifications.APP_STARTED, {
          app_name,
          userid,
          handle
        })
      nevents_writer.storeNevent(roomid, notification)
    }
  }

  const handleUserNominatedAdmin = (msg) => {
    const {
      userid,
      roomid
    } = msg
    const handle = players_reader.getHandleFromId(userid)
    if (rooms_reader.isNeventLogEnabled(roomid)) {
      const notification = nev.serialiseNotificationNevent(
        nev.notifications.USER_NOMINATED_ADMIN, {
          userid,
          handle
        })
      nevents_writer.storeNevent(roomid, notification)
    }
  }

  const handlePlayerSentIm = (msg) => {
    const {
      userid,
      roomid,
      message
    } = msg
    const handle = players_reader.getHandleFromId(userid)
    if (rooms_reader.isNeventLogEnabled(roomid)) { //surely this has to be true
      const im = nev.serialiseImNevent(userid, handle, message)
      nevents_writer.storeNevent(roomid, im)
    }
  }
  const handleAppSentNson = (msg) => {
    const {
      nson,
      roomid
    } = msg
    if (rooms_reader.isNeventLogEnabled(roomid)) { //surely this has to be true
      const nevent = nev.serialiseNsonNevent(nson)
      nevents_writer.storeNevent(roomid, nevent)
    }
  }

  nakama_pubsub.sub(intra.APP_CRASHED, handleAppCrashed)
  nakama_pubsub.sub(intra.APP_ENDED, handleAppEnded)
  nakama_pubsub.sub(intra.APP_SENT_NSON, handleAppSentNson)
  nakama_pubsub.sub(intra.APP_STARTED, handleAppStarted)
  nakama_pubsub.sub(intra.PLAYER_LEFT_ROOM, handlePlayerLeftRoom)
  nakama_pubsub.sub(intra.PLAYER_JOINED_ROOM, handlePlayerJoinedRoom)
  nakama_pubsub.sub(intra.PLAYER_CHANGED_HANDLE, handlePlayerChangedHandle)
  nakama_pubsub.sub(intra.PLAYER_SENT_IM, handlePlayerSentIm)
  nakama_pubsub.sub(intra.PLAYER_WAS_KICKED, handlePlayerWasKicked)
  nakama_pubsub.sub(intra.USER_NOMINATED_ADMIN, handleUserNominatedAdmin)
}

module.exports = NeventAudit
