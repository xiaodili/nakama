const nev = require('../../../shared/nevent.js')

const badSlashCommandNson = (command_string) => {
  return {
    type: nev.nsons.MESSAGE,
    params: {
      content: `${command_string} was not recognised, sorry!`
    }
  }
}

const not_implemented_nson = {
  type: nev.nsons.MESSAGE,
  params: {
    content: "Yikes, this slash command hasn't been implemented yet!"
  }
}

const DefaultCommands = (players_reader) => {
  const _handle = {
    ls: (userid) => {
      return not_implemented_nson
    },
    whoami: (userid) => {
      const handle = players_reader.getHandleFromId(userid)
      return {
        type: nev.nsons.MESSAGE,
        params: {
          content: `Your handle is ${handle}`
        }
      }
    },
    describe: (userid) => {
      return not_implemented_nson
    },
    help: (userid) => {
      return not_implemented_nson
    }
  }

  return {
    onSlashCommand: (userid, command_string) => {
      const command = command_string.slice(1).split(' ')[0]
      return _handle[command](userid) //TODO: figure out what other params to pass it
    },
    getSlashCommands: () => {
      return {
        ls: {
          description: 'list current players'
        },
        whoami: {
          description: 'prints out handle name'
        },
        describe: {
          description: 'describes a player',
          params: [{
            name: 'player'
            //,description
          }]
        },
        help: {
          description: 'get help'
        }
      }
    }
  }
}


const SlashCommand = (games_reader, players_reader, rooms_reader) => {
  const default_commands = DefaultCommands(players_reader)
    return {
      handleCommandForRoom: (userid, roomid, command_string, callback) => {
        const command = command_string.slice(1).split(' ')[0]
        if (command in default_commands.getSlashCommands()) {
          callback(default_commands.onSlashCommand(userid, command_string))
        } else {
          const gseshid = rooms_reader.getGseshidMaybe(roomid)
          const api = games_reader.getGameApi(gseshid)
          if (api && command in api.getSlashCommands()) {
            api.processSlashCommand(userid, command_string, callback)
          } else {
            return callback(badSlashCommandNson(command_string))
          }
        }
      },
      getSlashCommandsForRoom: (roomid) => {
        const gseshid = rooms_reader.getGseshidMaybe(roomid)
        const api = games_reader.getGameApi(gseshid)
        if (api) {
          return {
            default_commands: default_commands.getSlashCommands(),
            program_commands: api.getSlashCommands()
          }
        } else {
          return {
            default_commands: default_commands.getSlashCommands()
          }
        }
      }
    }
}

module.exports = SlashCommand
