const intra = require('../symbols/intra.js')
const inter = require('../../../shared/socket_events.js')
const ct = require('../../../shared/client_types.js')

const InactivePlayers = (logger, io, nakama_pubsub, desktop_interval_seconds, mobile_interval_seconds, while_gaming_interval_seconds, players_registry, rooms_registry) => {
  //convenience
  const players_reader = players_registry.reader
  const players_writer = players_registry.writer
  const rooms_reader = rooms_registry.reader
  const rooms_writer = rooms_registry.writer

  const mobile_interval = mobile_interval_seconds * 1000
  const desktop_interval = desktop_interval_seconds * 1000
  const while_gaming_interval = while_gaming_interval_seconds * 1000
  const inactive_timeouts = {}

  const createInactiveTimeout = (userid) => {
    return () => {
      logger.log(`destroying user - applying timeout for ${userid}`)
      const handle = players_reader.getHandleFromId(userid)
      const rooms_occupied = players_reader.getRoomsOccupied(userid)
      for (let roomid of rooms_occupied) {
        let room = rooms_reader.getRoom(roomid)
        //in case room has been destroyed in the interim
        if (room) {
          rooms_writer.playerLeftRoom(userid, roomid)
          nakama_pubsub.pub(intra.PLAYER_LEFT_ROOM, {
            userid,
            roomid
          })
        }
      }
      players_writer.removePlayer(userid)
      const omsg = {
        userid,
        rooms: [...rooms_occupied]
      }
      io.emit(inter.PLAYER_LEFT, omsg)
      logger.log(`broadcast.emit ${inter.PLAYER_LEFT} with ${JSON.stringify(omsg)}`)
      delete inactive_timeouts[userid]
    }
  }

  const handlePlayerDisconnected = (msg) => {
    const userid = msg.userid
    const socketid = msg.socketid
    if (userid && userid in players_reader.getPlayers()) {
      const handle = players_reader.getHandleFromId(userid)
      const connection = players_writer.removeConnectionFromPlayer(userid, socketid)
      const is_lost_connection_from_mobile = connection.on_mobile
      if (players_reader.getNumConnections(userid) === 0) {
        const rooms_occupied = [...players_reader.getRoomsOccupied(userid)]
        players_writer.setPlayerInactive(userid)
        io.emit(inter.PLAYER_INACTIVE, userid)
        nakama_pubsub.pub(intra.PLAYER_INACTIVE, userid)
        logger.log(`io.emit ${inter.PLAYER_INACTIVE} with ${userid}`)
        if (!players_reader.getAccountDetails(handle)) { //users with accounts (this includes admins by definition) are exempt
          if (rooms_reader.userInRoomWithOngoingGame(userid)) {
            logger.log(`player ${userid} inactive but in an ongoing game; setting timeout of ${while_gaming_interval}`)
            inactive_timeouts[userid] = setTimeout(createInactiveTimeout(userid), while_gaming_interval)
          } else {
            if (is_lost_connection_from_mobile) {
              logger.log(`player ${userid} inactive while on mobile; setting timeout of ${mobile_interval}`)
              inactive_timeouts[userid] = setTimeout(createInactiveTimeout(userid), mobile_interval)
            } else {
              logger.log(`player ${userid} inactive while on desktop; setting timeout of ${desktop_interval}`)
              inactive_timeouts[userid] = setTimeout(createInactiveTimeout(userid), desktop_interval)
            }
          }
        }
      }
    } else {
      logger.log(`disconnection event occurred before socket has been assigned a user`)
    }
  }

  const handlePlayerConnected = (msg) => {
    const userid = msg.userid
    const socketid = msg.socketid
    const on_mobile = msg.on_mobile
    const client_type = msg.client_type
    const numPreviousSockets = players_reader.getNumConnections(userid)
    players_writer.addConnectionToPlayer(userid, socketid, on_mobile, client_type)
    if (numPreviousSockets === 0) {
      players_writer.setPlayerActive(userid)
      if (userid in inactive_timeouts) {
        clearTimeout(inactive_timeouts[userid])
        delete inactive_timeouts[userid]
        logger.log(`removed inactive timeout for ${userid}`)
      }
      io.emit(inter.PLAYER_ACTIVE, userid)
      logger.log(`emitted ${inter.PLAYER_ACTIVE} with ${userid}`)
    }
  }

  nakama_pubsub.sub(intra.PLAYER_DISCONNECTED, handlePlayerDisconnected)
  nakama_pubsub.sub(intra.PLAYER_CONNECTED, handlePlayerConnected)
}

module.exports = InactivePlayers
