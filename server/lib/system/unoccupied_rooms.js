const intra = require('../symbols/intra.js')
const e = require('../symbols/enums.js')
const UnoccupiedRooms = (logger, unoccupied_interval_seconds, rooms_registry, nakama_pubsub, destroyUnoccupiedRoom) => {
  const unoccupied_interval = unoccupied_interval_seconds * 1000
  const unoccupied_timeouts = {}
  const rooms_reader = rooms_registry.reader
  const rooms_writer = rooms_registry.writer

  const handlePlayerLeftRoom = (msg) => {
    const roomid = msg.roomid
    const room = rooms_reader.getRoom(roomid)
    //if 
    //1. not created by startup user
    //2. not protected
    //3. no players left
    //Set destruction timer
    if (room.created_by !== e.STARTUP && !room.protected && rooms_reader.getNumPlayers(roomid) === 0) {
      logger.log(`last person to leave an unpriviliged, unprotected room: ${JSON.stringify(msg)}. Setting room cleanup timeout for ${unoccupied_interval_seconds} seconds`)
      unoccupied_timeouts[roomid] = setTimeout(() => {
        logger.log(`destroying ${roomid} due to unoccupancy`)
        destroyUnoccupiedRoom(roomid)
        delete unoccupied_timeouts[roomid]
      }, unoccupied_interval)
    }
  }

  const handlePlayerJoinedRoom = (msg) => {
    const roomid = msg.roomid
    if (roomid in unoccupied_timeouts) {
      logger.log(`Removing room cleanup timeout for ${roomid}`)
      clearTimeout(unoccupied_timeouts[roomid])
      delete unoccupied_timeouts[roomid]
    }
  }

  nakama_pubsub.sub(intra.PLAYER_LEFT_ROOM, handlePlayerLeftRoom)
  nakama_pubsub.sub(intra.PLAYER_JOINED_ROOM, handlePlayerJoinedRoom)
  return {
  }
}

module.exports = UnoccupiedRooms
