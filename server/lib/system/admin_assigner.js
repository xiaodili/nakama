const intra = require('../symbols/intra.js')
const inter = require('../../../shared/socket_events.js')
const A = require('../symbols/enums.js').ADMIN_POLICIES
const serialiseUserWasNominatedAdminNevent = require('../../../shared/nevent.js').serialiseUserWasNominatedAdminNevent


const AdminAssigner = (logger, io, nakama_pubsub, rooms_registry) => {
  const rooms_reader = rooms_registry.reader
  const rooms_writer = rooms_registry.writer
  const handlePlayerLeftRoom = (msg) => {
    const userid = msg.userid
    const roomid = msg.roomid
    const room = rooms_reader.getRoom(roomid)
    //reassign admin
    if (room.admin_policy === A.OLDEST && rooms_reader.isPlayerAdminOf(roomid, userid) && rooms_reader.getNumPlayers(roomid) >= 1) {
      const oldest_player = rooms_reader.getOldestPlayer(roomid)
      rooms_writer.assignAdmin(roomid, oldest_player)
      nakama_pubsub.pub(intra.USER_NOMINATED_ADMIN, {
        userid: oldest_player,
        roomid
      })
      const omsg = {
        userid: oldest_player,
        roomid
      }
      io.emit(inter.PLAYER_ASSIGNED_AS_ADMIN, omsg)
      logger.log(`emitted ${inter.PLAYER_ASSIGNED_AS_ADMIN} with ${JSON.stringify(omsg)}`)
    }
  }
  const handlePlayerJoinedRoom = (msg) => {
    const userid = msg.userid
    const roomid = msg.roomid
    const room = rooms_reader.getRoom(roomid)
    if (room.admin_policy === A.OLDEST && rooms_reader.getNumPlayers(roomid) === 1) {
      rooms_writer.assignAdmin(roomid, userid)
      nakama_pubsub.pub(intra.USER_NOMINATED_ADMIN, {
        userid,
        roomid
      })
      const omsg = {
        userid,
        roomid
      }
      io.emit(inter.PLAYER_ASSIGNED_AS_ADMIN, omsg)
      logger.log(`emitted ${inter.PLAYER_ASSIGNED_AS_ADMIN} with ${JSON.stringify(omsg)}`)
    }
  }

  nakama_pubsub.sub(intra.PLAYER_LEFT_ROOM, handlePlayerLeftRoom)
  nakama_pubsub.sub(intra.PLAYER_JOINED_ROOM, handlePlayerJoinedRoom)
  return {
  }
}

module.exports = AdminAssigner
