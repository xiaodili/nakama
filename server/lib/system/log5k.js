const fs = require('fs')
const path = require('path')
const LOG = "LOG"
const WARN = "WARN"
const INFO = "INFO"
const ERROR = "ERROR"

const Logger = (filepath) => {
  const stream = fs.createWriteStream(filepath, {
    flags: 'a'
  })

  const log = (txt) => {
    stream.write(txt)
    stream.write('\n')
  }

  return {
    log,
    warn: log,
    info: log,
    err: log,
    destroy: (fn) => {
      stream.end('\n', 'utf8', fn)
    }
  }
} 

const Log5k = (filepath) => {
  var logger
  if (filepath) {
    let log_dir = path.dirname(filepath)
    if (!fs.existsSync(log_dir)){
      fs.mkdirSync(log_dir);
    }
    logger = Logger(filepath)
  } else {
    logger = console
  }
  return {
    log: (msg) => {
      logger.log(`${(new Date()).toISOString()} ${LOG}: ${msg}`)
    },
    warn: (msg) => {
      logger.warn(`${(new Date()).toISOString()} ${WARN}: ${msg}`)
    },
    info: (msg) => {
      logger.info(`${(new Date()).toISOString()} ${INFO}: ${msg}`)
    },
    error: (msg) => {
      logger.error(`${(new Date()).toISOString()} ${ERROR}: ${msg}`)
    },
    destroy: (fn) => {
      if (filepath) {
        logger.destroy(fn)
      } else {
        fn()
      }
    }
  }
}

module.exports = Log5k
