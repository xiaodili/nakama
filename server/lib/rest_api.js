const path = require('path')
const url = require('url')

const isParameter = (path_node) => {
  return (path_node[0] === '{' && path_node[path_node.length - 1] === '}')
}

const PathNode = () => {

  //key → node
  const children = {}
  const available_methods = new Set()
  let handler

  //internal methods
  //path → [PathNode, path || null], throws 'cannot route'
  const getNextNode = (head) => {
    for (let path of Object.keys(children)) {
      if (isParameter(path)) {
        return [children[path], head]
      } else if (head === path) {
        return [children[head], null]
      }
    }
    throw 'cannot route'
  }

  return {
    //construction
    storeEndpoint: (nodes, methods, fn) => {
      if (nodes.length === 0) { //terminating condition
        for (let method of methods) {
          available_methods.add(method)
        }
        handler = fn

      } else {
        let head = nodes[0]
        let rest = nodes.slice(1)
				let next_node
        if (head in children) {
          next_node = children[head]
        } else {
          next_node = PathNode()
          children[head] = next_node
        }
				next_node.storeEndpoint(rest, methods, fn)
      }
    },
    //routing
    //[path], method, [parameter] → [parameter], throws 'cannot route'
    route: (nodes, method, parameters) => {
      if (nodes.length === 0) { //terminating condition
        if (available_methods.has(method)) {
          return [handler].concat(parameters)
        } else {
          throw 'cannot route'
        }
      } else {
        let head = nodes[0]
        let rest = nodes.slice(1)
        let [next_node, parameter] = getNextNode(head)
        if (parameter == null) {
          return next_node.route(rest, method, parameters)
        } else {
          return next_node.route(rest, method, [parameter].concat(parameters))
        }
      }
    }
  }
}

const Router = (endpoints) => {
  const root = PathNode()
  for (const endpoint of endpoints) {
    const nodes = endpoint.path.split('/') 
    root.storeEndpoint(nodes, endpoint.methods, endpoint.handler)
  }
  return root
}

const urlToNodes = (req_url) => {
  return req_url.split('/').map((node) => {
    return unescape(node)
  })
}

module.exports = (endpoints) => {
  const router = Router(endpoints)
  const canRoute = (req_url, method) => {
    var nodes = urlToNodes(req_url)
    try {
      let _ = router.route(nodes, method, [])
      return true
    } catch (err) {
      return false
    }
  }
  //for processing request POST data, we need to pass in the entire Request as opposed to just `Request.on`, since the `on` method uses `this` in its implementation
  const handle = (req_url, method, res, req) => {
    const nodes = urlToNodes(req_url)
    const atoms = router.route(nodes, method, [])
    const [handler, ...url_params] = atoms

		//if POST, process request asyncrhonously, since POST data arrives in chunks
		if (method === 'POST') {
			//http://stackoverflow.com/questions/4295782/how-do-you-extract-post-data-in-node-js
			//Assume JSON
			let json_string = ''
			req.on('data', (chunk) => {
				json_string += chunk
			})
			req.on('end', () => {
        let request_data
        try {
          request_data = JSON.parse(json_string)
        } catch(e) {
          request_data = {}
        }
				const [payload, response_code] = handler(...url_params, method, request_data)
				res.writeHead(response_code, {"Content-Type": "application/json"});
				res.end(JSON.stringify(payload))
			})
		} else {
			const [payload, response_code] = handler(...url_params, method)
			res.writeHead(response_code, {"Content-Type": "application/json"});
			res.end(JSON.stringify(payload))
		}
  }

  return {
    canRoute,
    handle
  }
}
