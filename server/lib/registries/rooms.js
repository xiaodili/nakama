const h = require('../helpers.js')
const sh = require('../../../shared/helpers')
const RoomsRegistry = () => {
  var rooms = {}

  const serialiseRoomForLogging = (r) => {
    //serialises players <Set>
    //replaces passphrase with passphrase_required
    //strips off name
    //strips off description
    return {
      admin: r.admin,
      id: r.id,
      shortlink: r.shortlink,
      autojoin: r.autojoin,
      players: [...r.players],
      nevent_log_enabled: r.nevent_log_enabled,
      games_enabled: r.games_enabled,
      player_list_enabled: r.player_list_enabled,
      created_by: r.created_by,
      protected: r.protected,
      game_ongoing: r.game_ongoing,
      passphrase_required: r.passphrase? true : false,
      default_tab: r.default_tab
    }
  }
  const serialiseRoomForClientApp = (r) => {
    //serialises players <Set>
    //replaces passphrase with passphrase_required
    return {
      admin: r.admin,
      id: r.id,
      shortlink: r.shortlink,
      name: r.name,
      description: r.description,
      autojoin: r.autojoin,
      players: [...r.players],
      nevent_log_enabled: r.nevent_log_enabled,
      games_enabled: r.games_enabled,
      player_list_enabled: r.player_list_enabled,
      created_by: r.created_by,
      protected: r.protected,
      game_ongoing: r.game_ongoing,
      passphrase_required: r.passphrase? true : false,
      default_tab: r.default_tab
    }
  }
  const deserialiseRoomFromSnapshot = (r) => {
    return {
      admin: r.admin,
      id: r.id,
      shortlink: r.shortlink,
      name: r.name,
      description: r.description,
      autojoin: r.autojoin,
      players: new Set(r.players),
      nevent_log_enabled: r.nevent_log_enabled,
      games_enabled: r.games_enabled,
      player_list_enabled: r.player_list_enabled,
      created_by: r.created_by,
      protected: r.protected,
      game_ongoing: r.game_ongoing,
      passphrase: r.passphrase,
      default_tab: r.default_tab
    }
  }
  const serialiseRoomForSnapshot = (r) => {
    //serialises players <Set>
    return {
      admin: r.admin,
      id: r.id,
      shortlink: r.shortlink,
      name: r.name,
      description: r.description,
      autojoin: r.autojoin,
      players: [...r.players],
      nevent_log_enabled: r.nevent_log_enabled,
      games_enabled: r.games_enabled,
      player_list_enabled: r.player_list_enabled,
      created_by: r.created_by,
      protected: r.protected,
      game_ongoing: r.game_ongoing,
      passphrase: r.passphrase,
      default_tab: r.default_tab
    }
  }
  return {
    writer: {
      assignAdmin: (roomid, userid) => {
        rooms[roomid].admin = userid
      },
      createRoom: (
        id,
        name,
        description,
        autojoin,
        nevent_log_enabled,
        games_enabled,
        player_list_enabled,
        created_by,
        passphrase,
        admin_policy,
        default_tab
      ) => {
        rooms[id] = {
          id,
          name,
          description,
          autojoin,
          players: new Set(),
          nevent_log_enabled,
          games_enabled,
          player_list_enabled,
          created_by,
          passphrase,
          admin_policy,
          default_tab
          //,protected: <bool>?
          //,game_ongoing: <gseshid>?
          //,shortlink: <string>?
          //,admin: <userid>?
        }
      },
      changePassphrase: (roomid, hash_salt) => {
        rooms[roomid].passphrase = hash_salt
      },
      removePassphrase: (roomid) => {
        delete rooms[roomid].passphrase
      },
      protectRoom: (roomid) => {
        rooms[roomid].protected = true
      },
      unprotectRoom: (roomid) => {
        rooms[roomid].protected = false
      },
      removeRoom: (roomid) => {
        delete rooms[roomid]
      },
      changeName: (roomid, name) => {
        rooms[roomid].name = name
      },
      changeDescription: (roomid, description) => {
        rooms[roomid].description = description
      },
      //TODO validate on tab type
      changeDefaultTab: (roomid, tab_type) => {
        rooms[roomid].default_tab = tab_type
      },
      enableGames: (roomid) => {
        rooms[roomid].games_enabled = true
      },
      disableGames: (roomid) => {
        rooms[roomid].games_enabled = false
      },
      enableNeventLog: (roomid) => {
        rooms[roomid].nevent_log_enabled = true
      },
      disableNeventLog: (roomid) => {
        rooms[roomid].nevent_log_enabled = false
      },
      enablePlayerList: (roomid) => {
        rooms[roomid].player_list_enabled = true
      },
      disablePlayerList: (roomid) => {
        rooms[roomid].player_list_enabled = false
      },
      restoreSnapshot: (_rooms) => {
        rooms = sh.objectMapValues(_rooms, deserialiseRoomFromSnapshot)
      },
      removeShortlink: (roomid) => {
        delete rooms[roomid].shortlink
      },
      changeShortlink: (roomid, shortlink) => {
        rooms[roomid].shortlink = shortlink
      },
      playerJoinedRoom: (userid, roomid) => {
        rooms[roomid].players.add(userid)
      },
      playerLeftRoom: (userid, roomid) => {
        rooms[roomid].players.delete(userid)
      },
      setGameOngoing: (roomid, gseshid) => {
        rooms[roomid].game_ongoing = gseshid
      },
      unsetGameOngoing: (roomid, gseshid) => {
        if (rooms[roomid].game_ongoing === gseshid) {
          delete rooms[roomid].game_ongoing
        }
      }
    },
    reader: {
      isRoomPassphrased: (roomid) => {
        return rooms[roomid].passphrase ? true : false
      },
      isNeventLogEnabled: (roomid) => {
        return rooms[roomid].nevent_log_enabled
      },
      isPlayerAdminOf: (roomid, userid) => {
        return rooms[roomid].admin === userid
      },
      getOldestPlayer: (id) => {
        const room = rooms[id]
        return room.players.keys().next().value
      },
      getNumPlayers: (roomid) => {
        return rooms[roomid].players.size
      },
      getRoom: (id) => {
        return rooms[id]
      },
      getRoomIds: () => {
        return Object.keys(rooms)
      },
      playerInRoom: (roomid, userid) => {
        return rooms[roomid].players.has(userid)
      },
      userInRoomWithOngoingGame: (userid) => {
        for (let roomid of Object.keys(rooms)) {
          let room = rooms[roomid]
          if (room.players.has(userid) && room.game_ongoing) {
            return true
          }
        }
        return false
      },
      getGseshidMaybe: (roomid) => {
        return rooms[roomid].game_ongoing
      },
      getRoomForClientAppSerialisation: (roomid) => {
        return serialiseRoomForClientApp(rooms[roomid])
      },
      getRoomsForSnapshotSerialisation: () => {
        return sh.objectMapValues(rooms, serialiseRoomForSnapshot)
      },
      getRoomsForLoggingSerialisation: () => {
        return sh.objectMapValues(rooms, serialiseRoomForLogging)
      },
      getRoomsForClientAppSerialisation: () => {
        return sh.objectMapValues(rooms, serialiseRoomForClientApp)
      }
    },
    api: {
      rooms: (method) => {
        if (method === 'GET') {
          return [sh.objectMapValues(rooms, getRoomForClientAppSerialisation), 200]
        } else if (method === 'DELETE') { //TODO get rid of this one
          rooms = {}
          return ['ok', 200]
        } else {
          return [null, 405]
        }
      }
    }
  }
}

module.exports = RoomsRegistry
