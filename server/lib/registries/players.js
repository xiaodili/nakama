const sh = require('../../../shared/helpers')

const PlayersRegistry = (cookies_default_preference) => {

  let players = {}

  //handle → userid
  const handles = {}

  //session_id → userid
  const active_sessions = {}

  const getPlayerForSnapshotSerialisation = (userid) => {
    const p = players[userid]
    return {
      id: p.id,
      handle: p.handle,
      active: p.active,
      rooms_occupied: [...p.rooms_occupied],
      admin: p.admin,
      account: p.account
    }
  }

  const getPlayerForClientSerialisation = (userid) => {
    const p = players[userid]
    return {
      id: p.id,
      handle: p.handle,
      active: p.active,
      rooms_occupied: [...p.rooms_occupied]
    }
  }

  const getPlayerForAdminSerialisation = (userid) => {
    const p = players[userid]
    return {
      id: p.id,
      handle: p.handle,
      active: p.active,
      rooms_occupied: [...p.rooms_occupied],
      admin: p.admin
    }
  }

  const getAdminStatus = () => {
    for (const userid of Object.keys(players)) {
      const player = players[userid]
      if (player.admin) {
        return true
      }
    }
    return false
  }

  const createAnonymousPlayer = (id, handle) => {
    return {
      id,
      handle,
      connections: {},
      rooms_occupied: new Set(),
      using_cookies: cookies_default_preference
    }
  }
  
  return {
    reader: {
      getAccountDetails: (handle) => {
        const userid = handles[handle]
        if (userid in players) {
          return players[userid].account
        } else {
          return null
        }
      },
      getUsingCookies: (id) => {
        return players[id].using_cookies
      },
      getNumConnections: (id) => {
        return Object.keys(players[id].connections).length
      },
      getSocketIds: (id) => {
        return Object.keys(players[id].connections)
      },
      getRoomsOccupied: (id) => {
        return players[id].rooms_occupied
      },
      getUserIdFromHandle: (handle) => {
        return handles[handle]
      },
      getHandleFromId: (id) => { return players[id].handle},
      getPlayer: (id) => {
        return players[id]
      },
      //TODO: strip off last_active, on_mobile etc.
      getPlayers: () => {
        return players
      },
      getPlayersForSnapshotSerialisation: () => {
        const players_serialised = {}
        for (let userid of Object.keys(players)) {
          players_serialised[userid] = getPlayerForSnapshotSerialisation(userid)
        }
        return players_serialised
      },
      getPlayersForAdminSerialisation: () => {
        const players_serialised = {}
        for (let userid of Object.keys(players)) {
          players_serialised[userid] = getPlayerForAdminSerialisation(userid)
        }
        return players_serialised
      },
      getPlayersForClientSerialisation: () => {
        const players_serialised = {}
        for (let userid of Object.keys(players)) {
          players_serialised[userid] = getPlayerForClientSerialisation(userid)
        }
        return players_serialised
      },
      isPlayerAdmin: (id) => {
        return Boolean(players[id].admin)
      },
      isHandleTaken: (handle) => {
        return handle in handles
      },
      validateSessionId: (userid, session_id) => {
        return active_sessions[session_id] === userid
      }
    },
    writer: {
      convertUserToAccount: (id, salt, hash) => {
        players[id].account = {
          salt,
          hash
        }
      },
      addPlayerToRoom: (id, roomid) => {
        players[id].rooms_occupied.add(roomid)
      },
      removePlayerFromRoom: (id, roomid) => {
        players[id].rooms_occupied.delete(roomid)
      },
      removePlayer: (id) => {
        const handle = players[id].handle
        delete players[id]
        delete handles[handle]
      },
      setPlayerActive: (id) => {
        players[id].active = true
      },
      setPlayerInactive: (id) => {
        players[id].active = false
      },
      updateHandle: (id, old_handle, new_handle) => {
        delete handles[old_handle]
        handles[new_handle] = id
        players[id].handle = new_handle
      },
      addSocketId: (id, new_socketid) => {
        players[id].socketids.add(new_socketid)
      },
      removeConnectionFromPlayer: (id, socketid) => {
        const connection = players[id].connections[socketid]
        delete players[id].connections[socketid]
        return connection
      },
      addConnectionToPlayer: (id, socketid, on_mobile, client_type) => {
        players[id].connections[socketid] = {
          on_mobile,
          client_type
        }
      },
      registerAdmin: (id, handle, salt, hash) => {
        const player = createAnonymousPlayer(id, handle)
        handles[handle] = id

        player.account = {
          salt,
          hash
        }
        player.admin = true
        players[id] = player
      },
      registerPlayer: (id, handle) => {
        const player = createAnonymousPlayer(id, handle)
        handles[handle] = id

        player.active = true
        players[id] = player
      },
      logoutPlayer: (userid, session_id) => {
        if (active_sessions[session_id] === userid) {
          delete active_sessions[session_id]
          return true
        } else {
          return false
        }
      },
      loginPlayer: (userid, session_id) => {
        active_sessions[session_id] = userid
      },
      enableCookies: (id) => {
        players[id].using_cookies = true
      },
      disableCookies: (id) => {
        players[id].using_cookies = false
      },
      restoreSnapshot: (_players) => {
        players = sh.objectMapValues(_players, (p) => {
          return {
            id: p.id,
            handle: p.handle,
            active: p.active,
            rooms_occupied: new Set(p.rooms_occupied),
            admin: p.admin,
            account: p.account
          }
        })
      }
    },
    api: {
      players: (method) => {
        if (method !== 'GET') {
          return [null, 405]
        } else {
          return [Object.keys(players).map((userid) => {
            return getPlayerForSerialisation(userid)
          }), 200]
        }
      },
      player: (id, method) => {
        if (method !== 'GET') {
          return [null, 405]
        } else {
          return [getPlayerForSerialisation(id), 200]
        }
      },
      adminStatus: (method) => {
        if (method !== 'GET') {
          return [null, 405]
        } else {
          return [getAdminStatus(), 200]
        }
      },
      sessionValidity: (method, data) => {
        if (method !== 'POST') {
          return [null, 405]
        } else {
          const session_id = data.session_id
          const userid = active_sessions[session_id]
          if (userid) {
            if (players[userid].using_cookies) {
              return [true, 200]
            } else {
              return [false, 200]
            }
          } else {
            return [false, 200]
          }
        }
      }
    }
  }
}

module.exports = PlayersRegistry
