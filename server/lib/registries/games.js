const fs = require('fs')
const path = require('path')
const makeId = require('../../../shared/server_helpers.js').makeId
const sh = require('../../../shared/helpers.js')

const GamesRegistry = (logger, games_path) => {
  //console.log("games_path: ", games_path);
  let catalogue = {}
  if (games_path) {
    let catalogue_path = path.join(games_path, 'index.js')
    if (fs.existsSync(catalogue_path)) {
      catalogue = require(catalogue_path)
    }
  }

  //gseshid → api
  let sessions = {}

  return {
    reader: {
      //sync
      loadServerBundle: (gameid) => {
        const server_bundle_path = path.join(games_path, 'server', gameid + '.js')
        return require(server_bundle_path)
      },
      //async
      loadClientBundle: (gameid, cb) => {
        const client_bundle_path = path.join(games_path, 'client', gameid + '.js')
        fs.readFile(client_bundle_path, 'utf8', cb)
      },
      getGameApi: (gseshid) => {
        return sessions[gseshid]
      },
      getCatalogue: () => {
        return catalogue
      },
      getGsessions: () => { //TODO: For REST API
      },
      getGamesForSnapshotSerialisation: () => {
        return sh.objectMapValues(sessions, (api) => {
          return {
            game_state: api.getGameState(),
            gameid: api.getGameId(),
            creatorid: api.getCreatorId(),
            gseshid: api.getGseshId(),
            roomid: api.getRoomId()
          }
        })
      }
    },
    writer: {
      restoreSnapshot: (game_snapshots, initialiseGameFromSnapshot) => {
        //1. Stop any ongoing games
        for (const gseshid of Object.keys(sessions)) {
          const api = sessions[gseshid]
          api.destroy()
        }
        sessions = {}

        for (const gseshid of Object.keys(game_snapshots)) {
          const game_snapshot = game_snapshots[gseshid]
          const {
            game_state,
            gameid,
            creatorid,
            roomid
          } = game_snapshot
          const fh = initialiseGameFromSnapshot(creatorid, roomid, gameid)
          sessions[gseshid] = fh
          fh.restoreGameState(game_state)
        }
      },
      storeGameApi: (api) => {
        const gseshid = makeId(15)
        sessions[gseshid] = api
        return gseshid
      },
      removeGameApi: (gseshid) => {
        const api = sessions[gseshid]
        api.destroy()
        delete sessions[gseshid]
      }
    },
    api: {
      games: (method) => {
        if (method !== 'GET') {
          return [null, 405]
        } else {
          return [Object.keys(sessions), 200]
        }
      },
      game: (id, method) => {
        if (method !== 'GET') {
          return [null, 405]
        } else {
          return [sessions[id].getGameState(), 200]
        }
      }
    }
  }
}

module.exports = GamesRegistry
