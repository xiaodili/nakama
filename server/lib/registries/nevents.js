const sh = require('../../../shared/helpers.js')
//circular array
//
//When I restore this from a snapshot, do I need to update the message_limit?
const Nevents = (message_limit) => {
  let messages = []
  let msg_pointer = 0
  return {
    getMessages: () => {
      if (messages.length < message_limit) {
        return messages
      } else {
        return messages.slice(msg_pointer).concat(messages.slice(0, msg_pointer))
      }
    },
    storeMessage: (msg) => {
      if (messages.length < message_limit) {
        messages.push(msg)
      } else {
        if (msg_pointer < message_limit) {
          messages[msg_pointer] = msg
        } else {
          msg_pointer = 0
          messages[0] = msg
        }
      }
      msg_pointer++
    },
    restoreSnapshot: (_messages, _msg_pointer) => {
      messages = _messages
      msg_pointer = _msg_pointer
    },
    getNeventsForSnapshot: () => {
      return {
        messages,
        msg_pointer
      }
    }
  }
}

const NeventsRegistry = (message_limit) => {
  let nevents = {}
  return {
    reader: {
      //TODO: filter this on userid
      getNeventsFor: (userid, roomid) => {
        return nevents[roomid].getMessages()
      },
      getNevents: (id) => {
        return nevents[id].getMessages()
      },
      getAllNevents: () => {
        const messages = {}
        for (let id of Object.keys(nevents)) {
          messages[id] = nevents[id].getMessages()
        }
        return messages
      },
      getNeventsForSnapshotSerialisation: () => {
        return sh.objectMapValues(nevents, (nevent) => {
          return nevent.getNeventsForSnapshot()
        })
      }
    },
    writer: {
      createNeventLog: (id) => {
        nevents[id] = Nevents(message_limit)
      },
      removeNeventLog: (id) => {
        delete nevents[id]
      },
      restoreSnapshot: (_nevents) => {
        nevents = sh.objectMapValues(_nevents, (_ns) => {
          const ns = Nevents(message_limit)
          ns.restoreSnapshot(_ns.messages, _ns.msg_pointer)
          return ns
        })
      },
      storeNevent: (id, msg) => {
        if (id in nevents) {
          nevents[id].storeMessage(msg)
        }
      }
    },
    api: {
      nevents: (id, method) => {
        if (method === 'GET') {
          if (nevents[id]) {
            return [nevents[id].getMessages(), 200]
          } else {
            return [null, 405]
          }
        }
      }
    }
  }
}

exports.NeventsRegistry = NeventsRegistry
//exposed for tests
exports.Nevents = Nevents
