const intra = require('../symbols/intra.js')
const inter = require('../../../shared/socket_events.js')
const PubSub = require('../../../shared/helpers.js').PubSub

//io, <string> [player...]
const Api = (io, creatorid, players, nakama_pubsub, roomid, gameid, game_name, players_registry, logger) => {
  let gseshid,
    server

  const userIdToSocketIds = {}
  for (const player of players) {
    userIdToSocketIds[player.id] = new Set(player.connections)
  }

  const tryGameEvent = (fn) => {
    try {
      fn()
    } catch(err) {
      nakama_pubsub.pub(intra.APP_CRASHED, {
        gseshid,
        roomid,
        game_name
      })
      io.emit(inter.GAME_ERRORED, {
        roomid,
        gseshid
      })
      logger.error(`Caught app server for ${game_name} failing to react to external event. More details: `)
      logger.error(err.stack)
    }
  }

  const handlePlayerConnected = (msg) => {
    const userid = msg.userid
    if (userid in userIdToSocketIds) {
      let socketid = msg.socketid
      userIdToSocketIds[userid].add(socketid)
      if (typeof server.onPlayerActive === 'function') {
        tryGameEvent(() => {
          server.onPlayerActive(userid)
        })
      }
    }
  }

  const handlePlayerJoinedRoom = (msg) => {
    if (roomid === msg.roomid) {
      const userid = msg.userid
      const player = players_registry.reader.getPlayer(userid)
      userIdToSocketIds[userid] = new Set(Object.keys(player.connections))
      if (typeof server.onPlayerJoined === 'function') {
        tryGameEvent(() => {
          server.onPlayerJoined({
            id: userid,
            handle: player.handle
          })
        })
      }
    }
  }

  const handlePlayerWasKicked = (msg) => {
    if (roomid === msg.roomid) {
      let playerid = msg.playerid
      delete userIdToSocketIds[playerid]
      if (typeof server.onPlayerLeft === 'function') {
        tryGameEvent(() => {
          server.onPlayerLeft(playerid)
        })
      }
    }
  }
  const handlePlayerLeftRoom = (msg) => {
    if (roomid === msg.roomid) {
      let userid = msg.userid
      delete userIdToSocketIds[userid]
      if (typeof server.onPlayerLeft === 'function') {
        tryGameEvent(() => {
          server.onPlayerLeft(userid)
        })
      }
    }
  }

  const handlePlayerChangedHandle = (msg) => {
    const userid = msg.userid
    const handle = msg.handle
    if (userid in userIdToSocketIds) {
      if (typeof server.onPlayerChangedHandle === 'function') {
        tryGameEvent(() => {
          server.onPlayerChangedHandle(userid, handle)
        })
      }
    }
  }

  const handlePlayerInactive = (userid) => {
    if (userid in userIdToSocketIds) {
      if (typeof server.onPlayerInactive === 'function') {
        tryGameEvent(() => {
          server.onPlayerInactive(userid)
        })
      }
    }
  }

  const handlePlayerSentMessage = (msg) => {
    if (roomid === msg.roomid) {
      const userid = msg.userid
      const message = msg.message
      if (userid in userIdToSocketIds) {
        if (typeof server.onPlayerSentMessage === 'function') {
          tryGameEvent(() => {
            server.onPlayerSentMessage(userid, message)
          })
        }
      }
    }
  }

  const game_pubsub = PubSub()
  const nakama_subs = [
    nakama_pubsub.sub(intra.PLAYER_CONNECTED, handlePlayerConnected),
    nakama_pubsub.sub(intra.PLAYER_JOINED_ROOM, handlePlayerJoinedRoom),
    nakama_pubsub.sub(intra.PLAYER_LEFT_ROOM, handlePlayerLeftRoom),
    nakama_pubsub.sub(intra.PLAYER_WAS_KICKED, handlePlayerWasKicked),
    nakama_pubsub.sub(intra.PLAYER_CHANGED_HANDLE, handlePlayerChangedHandle),
    nakama_pubsub.sub(intra.PLAYER_SENT_IM, handlePlayerSentMessage),
    nakama_pubsub.sub(intra.PLAYER_INACTIVE, handlePlayerInactive)
  ]

  const wrap = (event, body) => {
    return {
      event,
      gseshid,
      body
    }
  }

  return {
    forServer: {
      input: {
        on: (evt, fn) => {
          return game_pubsub.sub(evt, fn)
        }
      },
      output: {
        emit: (evt, msg) => {
          let recipients = io
          //chain up socket.io recipients
          for (let userid of Object.keys(userIdToSocketIds)) {
            for (let socketid of userIdToSocketIds[userid]) {
              recipients = recipients.to(socketid)
            }
          }
          recipients.emit(inter.GAME_EVENT, wrap(evt, msg))
        },
        //to all but id
        broadcast: (id, evt, msg) => {
          var recipients = io
          for (let userid of Object.keys(userIdToSocketIds)) {
            if (userid !== id) {
              for (let socketid of userIdToSocketIds[userid]) {
                recipients = recipients.to(socketid)
              }
            }
          }
          recipients.emit(inter.GAME_EVENT, wrap(evt, msg))
        },
        toMany: (ids) => {
          return {
            emit: (evt, msg) => {
              var recipients = io
              for (let userid of ids) {
                if (userid in userIdToSocketIds) {
                  for (let socketid of userIdToSocketIds[userid]) {
                    recipients = recipients.to(socketid)
                  }
                }
              }
              recipients.emit(inter.GAME_EVENT, wrap(evt, msg))
            }
          }
        },
        to: (userid) => {
          return {
            emit: (evt, msg) => {
              if (userid in userIdToSocketIds) {
                let recipients = io
                for (let socketid of userIdToSocketIds[userid]) {
                  recipients = recipients.to(socketid)
                }
                recipients.emit(inter.GAME_EVENT, wrap(evt, msg))
              }
            }
          }
        }
      },
      external: {
        sendNson: (nson) => {
          nakama_pubsub.pub(intra.APP_SENT_NSON, {
            nson,
            roomid
          })
          io.to(roomid).emit(inter.APP_EMITTED_NSON, {
            nson
          })
        },
        sendMessage: (msg) => {
          const nson = {
            type: 'MESSAGE',
            params: {
              content: msg
            }
          }
          nakama_pubsub.pub(intra.APP_SENT_NSON, {
            nson,
            roomid
          })
          io.to(roomid).emit(inter.APP_EMITTED_NSON, {
            nson
          })
        },
        endGame: () => {
          nakama_pubsub.pub(intra.APP_ENDED, {
            roomid,
            gseshid,
            game_name
          })
        }
      }
    },
    forHandler: {
      processSlashCommand: (userid, command_string) => {
        let result
        tryGameEvent(() => {
          result = server.onSlashCommand(userid, command_string)
        })
        return result
      },
      getSlashCommands: () => {
        if (typeof server.slash_commands === 'object') {
          return server.slash_commands
        } else {
          return {}
        }
      },
      processClientEvent: (event, userid, body) => {
        tryGameEvent(() => {
          game_pubsub.pub(event, userid, body)
        })
      },
      setGseshId: (id) => {
        gseshid = id
      },
      getGseshId: () => {
        return gseshid
      },
      getGameId: () => {
        return gameid
      },
      getCreatorId: () => {
        return creatorid
      },
      getRoomId: () => {
        return roomid
      },
      getGameName: () => {
        return game_name
      },
      restoreGameState: (game_state) => {
        if (typeof server.onGameStateRestoreRequested === 'function') {
          try {
            return server.onGameStateRestoreRequested(game_state)
          } catch (e) {
            logger.error(`Failed to restore game state for ${game_name}`)
          }
        } else {
          logger.warn(`Game ${game_name} doesn't support restoring game state`)
        }
      },
      getGameState: () => {
        if (typeof server.onGameStateRequested === 'function') {
          try {
            return server.onGameStateRequested()
          } catch (e) {
            logger.error(`Failed to request game state for ${game_name}`)
            return {}
          }
        } else {
          logger.warn(`Game ${game_name} doesn't support requesting game state`)
          return {}
        }
      },
      destroy: () => {
        for (let unsub of nakama_subs) {
          unsub()
        }
        tryGameEvent(() => {
          server.destroy()
        })
      },
      registerServer: (_server) => {
        server = _server
      }
    }
  }
}

module.exports = Api
