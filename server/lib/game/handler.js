const Api = require('./api.js')
const inter = require('../../../shared/socket_events.js')
const intra = require('../symbols/intra.js')

const GameEventHandler = (logger, io, players_registry, rooms_registry, games_registry, nakama_pubsub) => {
  const client_bundle_cache = {}

  //convenience
  const games_reader = games_registry.reader
  const players_reader = players_registry.reader
  const rooms_reader = rooms_registry.reader
  const games_writer = games_registry.writer
  const rooms_writer = rooms_registry.writer
  const catalogue = games_reader.getCatalogue()

  const handleAppEnded = (msg) => {
    const {
      gseshid,
      game_name,
      roomid
    } = msg
    rooms_writer.unsetGameOngoing(roomid, gseshid)
    games_writer.removeGameApi(gseshid)
    io.emit(inter.GAME_ENDED, {
      gseshid,
      game_name,
      roomid
    })
    //Send message to everyone
  }

  const handleRoomDestroyed = (roomid) => {
    let gseshid = rooms_reader.getGseshidMaybe(roomid)
    if (gseshid) {
      const gameid = games_reader.getGameApi(gseshid).getGameId()
      const game_name = catalogue[gameid].title
      games_writer.removeGameApi(gseshid)
      io.emit(inter.GAME_ENDED, {
        gseshid,
        game_name,
        roomid
      })
    }
  }

  //from game/api.js
  nakama_pubsub.sub(intra.APP_ENDED, handleAppEnded)
  nakama_pubsub.sub(intra.ROOM_DESTROYED, handleRoomDestroyed)

  return {
    handleIncomingClientEvent: (msg) => {
      const gseshid = msg.gseshid
      const userid = msg.userid
      const body = msg.body
      const event = msg.event
      const api = games_reader.getGameApi(gseshid)
      if (api) {
        api.processClientEvent(event, userid, body)
      }
    },
    fetchClientBundle: (roomid, cb) => {
      const gseshid = rooms_reader.getRoom(roomid).game_ongoing
      if (gseshid) {
        let api = games_reader.getGameApi(gseshid)
        if (api) {
          let gameid = api.getGameId()
          if (gameid in client_bundle_cache) {
            cb(gseshid, client_bundle_cache[gameid])
          } else {
            games_reader.loadClientBundle(gameid, (err, bundle) => {
              if (!err) {
                client_bundle_cache[gameid] = bundle
                cb(gseshid, bundle)
              }
            })
          }
        }
      }
    },
    // → server.forHandler
    initialiseGameFromSnapshot: (userid, roomid, gameid) => {
      const Server = games_reader.loadServerBundle(gameid)
      const player_ids = [...rooms_reader.getRoom(roomid).players]
      const participants = player_ids.map((id) => {
        const p = players_reader.getPlayer(id)
        return {
          id: p.id,
          handle: p.handle,
          active: p.active,
          //empty connections array; these need to be freshly connected
          connections: [],
          creator: userid === p.id
        }
      })
      const game_name = catalogue[gameid].title
      const api = Api(io, userid, participants, nakama_pubsub, roomid, gameid, game_name, players_registry, logger)
      const fs = api.forServer
      const fh = api.forHandler
      try {
        let server = Server(userid, participants.map((p) => {
          return {
            id: p.id,
            handle: p.handle
          }
        }), fs.input, fs.output, fs.external)
        fh.registerServer(server)
      } catch(err) {
        logger.error("Caught game initialisation error. more details: ")
        logger.error(err.stack)
      }
      return fh
    },
    initialiseGame: (userid, roomid, gameid) => {
      const Server = games_reader.loadServerBundle(gameid)
      const player_ids = [...rooms_reader.getRoom(roomid).players]
      const participants = player_ids.map((id) => {
        const p = players_reader.getPlayer(id)
        return {
          id: p.id,
          handle: p.handle,
          active: p.active,
          connections: Object.keys(p.connections),
          creator: userid === p.id
        }
      })
      const game_name = catalogue[gameid].title
      const api = Api(io, userid, participants, nakama_pubsub, roomid, gameid, game_name, players_registry, logger)
      const fs = api.forServer
      const fh = api.forHandler
      const gseshid = games_writer.storeGameApi(fh)
      try {
        let server = Server(userid, participants.map((p) => {
          return {
            id: p.id,
            handle: p.handle
          }
        }), fs.input, fs.output, fs.external)
        fh.registerServer(server)
        fh.setGseshId(gseshid)
        rooms_writer.setGameOngoing(roomid, gseshid)

        nakama_pubsub.pub(intra.APP_STARTED, {
          userid,
          roomid,
          app_name: game_name
        })
        const program_commands = server.slash_commands
        io.emit(inter.GAME_STARTED, {
          gseshid,
          game_name,
          roomid,
          userid,
          program_commands
        })
      } catch(err) {
        nakama_pubsub.pub(intra.APP_CRASHED, {
          game_name
        })
        io.emit(inter.GAME_ERRORED, {
          gseshid,
          roomid
        })
        logger.error("Caught game initialisation error. More details: ")
        logger.error(err.stack)
      }
    }
  }
}

module.exports = GameEventHandler
