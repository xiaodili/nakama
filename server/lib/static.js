const path = require('path')
const url = require('url')
const fs = require('fs')
const h = require('./helpers.js')

const identity = i => i

//path <string>, path <string>, λ(path <string> → path <string>), <Object> →  <bool>
const Handler = (static_root, redirect = identity, allowed_assets = null) => {

  const canTraverse = (req_url) => {
    if (allowed_assets) { //allowed assets are explicitly defined; we should be able to traverse there
      const path_from_root = path.join(static_root, redirect(req_url))
      const nodes = path_from_root.split('/') //is this definitely sound?
      const node_offset = static_root.split('/').length
      var visiting = allowed_assets
      for (let node of nodes.slice(node_offset)) {
        if (node in visiting) {
          visiting = visiting[node]
        } else {
          return false
        }
      }
      return true
    } else { //
      const path_from_root = path.join(static_root, url.resolve('/', req_url))
      return fs.existsSync(path_from_root) && !fs.lstatSync(path_from_root).isDirectory()
    }
  }

  const handle = (req_url, res) => {
    const asset_path = path.join(static_root, redirect(req_url))
    const ext_name = path.extname(asset_path)
    const mime_type = h.mimeLookup[ext_name]
    fs.readFile(asset_path, (err, data) => {
      if (err) {
        res.writeHead(404)
        return res.end('')
      }
      res.setHeader("Content-Type", mime_type)
      res.writeHead(200)
      return res.end(data)
    })
  }

  return {
    canTraverse,
    handle
  }
}
module.exports = Handler
