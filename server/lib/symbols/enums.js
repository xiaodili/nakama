//userid for startup room creator
exports.STARTUP = "STARTUP"

//admin policies from config
exports.ADMIN_POLICIES = {
  MANUAL: "manual",
  CREATOR: "creator",
  OLDEST: "oldest_occupant"
}
