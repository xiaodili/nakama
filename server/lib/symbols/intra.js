//Player disconnect and reconnect events

//Source: system/inactive_players.js
//Sinks: (TODO: game/api)
//userid
exports.PLAYER_INACTIVE = "PLAYER_INACTIVE"

//Source: Nakama Handler
//Sinks: game/api.js, system/inactive_players.js
//{userid, socketid, on_mobile, client_type}
exports.PLAYER_CONNECTED = "PLAYER_CONNECTED"

//Source: Nakama Handler
//Sinks: system/inactive_players.js
//{userid, socketid}
exports.PLAYER_DISCONNECTED = "PLAYER_DISCONNECTED"

//Source: Nakama Handler
//Sinks: game/api.js, services/unoccupied_rooms.js, services/admin_assigner.js, services/nevent_audit.js
//{userid, roomid}
exports.PLAYER_JOINED_ROOM = "PLAYER_JOINED_ROOM"

//Source: Nakama Handler
//Sinks: game/api.js services/nevent_audit.js
//{kickerid, roomid, playerid}
exports.PLAYER_WAS_KICKED = "PLAYER_WAS_KICKED"

//Source: Nakama Handler
//Sinks: game/api.js, services/unoccupied_rooms.js, services/admin_assigner.js, services/nevent_audit.js
//{userid, roomid}, 
exports.PLAYER_LEFT_ROOM = "PLAYER_LEFT_ROOM"

//Source: services/admin_assigner.js
//Sinks: services/nevent_audit
//{userid, roomid}, 
exports.USER_NOMINATED_ADMIN = "USER_NOMINATED_ADMIN"

//Source: game/api.js, Nakama Handler
//Sinks: game/handler.js, services/nevent_audit.js
//{roomid, gseshid, game_name}
exports.APP_ENDED = "APP_ENDED"

//Source: game.api.js
//Sinks: servics/nevent_audit.js
//{nson, roomid}
exports.APP_SENT_NSON = 'APP_SENT_NSON'

//Source: game/api.js, Nakama Handler
//Sinks: services/nevent_audit.js
//{app_name, userid, roomid}
exports.APP_STARTED = "APP_STARTED"

//Source: game.api.js, game/handler.js (when attempting startup)
//Sinks: servics/nevent_audit.js
//app_name
exports.APP_CRASHED = 'APP_CRASHED'


//Source: nakama_handler
//Sinks: game/handler.js
exports.ROOM_DESTROYED = 'ROOM_DESTROYED'

//Source: lib/game/

//Source: Nakama Handler
//Sinks: game/api.js, services/nevent_audit.js
//{userid, handle, old_handle}
exports.PLAYER_CHANGED_HANDLE = "PLAYER_CHANGED_HANDLE"

//Source: Nakama Handler
//Sinks: game/api.js, services/nevent_audit.js
//{userid, message: <string>, roomid}
exports.PLAYER_SENT_IM = "PLAYER_SENT_IM"
