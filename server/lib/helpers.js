const mimeLookup = {
  '.js': 'application/javascript',
  '.map': 'application/octet-stream',
  '.css': 'text/css',
  '.html': 'text/html',
  '.svg': 'image/svg+xml'
}

const adjs = ["Sweet", "Tasteful", "Cool", "Hot", "Gnarly", "Awesome", "Pirate", "Promising", "Bangin'", "Wicked", "Mysterious", "Hidden", "Questionable", "Magical", "Haunted", "Sinister"]
const places = ["Pad", "Hangout", "Dungeon", "Garage", "Cove", "Haunt", "Penthouse", "Basement", "Treehouse", "Alleyway", "Sauna", "Graveyard", "Arena", "Forest", "Cavern"]

//() → <string>
const generateRandomRoomName = () => {
  return 'The ' +  adjs[Math.floor(Math.random() * adjs.length)] + ' ' + places[Math.floor(Math.random() * places.length)]
}

//<string> → <string>
const generateRandomRoomNameFromHandle = (handle) => {
  return handle + "'s " +  adjs[Math.floor(Math.random() * adjs.length)] + ' ' + places[Math.floor(Math.random() * places.length)]
}

//<string>, λ(<string> → <bool>) → <string>
const generateUniqueHandle = (_handle, isHandleTaken) => {
  let handle = _handle
  if (isHandleTaken(handle)) {
    let numberToAppend = 1
    while (isHandleTaken(handle + numberToAppend)) {
      numberToAppend += 1
    }
    handle = handle + numberToAppend
  }
  return handle
}

const crypto = require('crypto')
const NUM_ITERATIONS = 10000
const SALT_BYTES = 16
const HASH_BYTES = 32
const DIGEST = 'sha256'
const SESSION_BYTES = 16

//password <string> → [hash <hexstring>, salt <hexstring>]
const generatePasswordHashSync = (password) => {
  //https://nodejs.org/api/crypto.html#crypto_crypto_pbkdf2_password_salt_iterations_keylen_digest_callback
  const salt = crypto.randomBytes(SALT_BYTES)
  const hash = crypto.pbkdf2Sync(password, salt, NUM_ITERATIONS, HASH_BYTES, DIGEST)
  return [hash.toString('hex'), salt.toString('hex')]
}

//password <string>, salt <hexstring>, hash <hexstring> → <bool>
const verifyPasswordHashSync = (password, salt, hash) => {
  //TODO: random jitter for sidechannel attacks
  //https://gist.github.com/mba7/979e6c3fe715fc618549fae4d09019ef
  return crypto.pbkdf2Sync(password, Buffer.from(salt, 'hex'), NUM_ITERATIONS, HASH_BYTES, DIGEST).equals(Buffer.from(hash, 'hex'))
}

//<string> → [<hexstring>, <hexstring>]
const generatePasswordHashAsync = (password, callback) => {
  //https://nodejs.org/api/crypto.html#crypto_crypto_pbkdf2_password_salt_iterations_keylen_digest_callback
  const salt = crypto.randomBytes(SALT_BYTES)
  crypto.pbkdf2(password, salt, NUM_ITERATIONS, HASH_BYTES, DIGEST, (err, key) => {
    callback([key.toString('hex'), salt.toString('hex')])
  })
}

//password <string>, salt <hexstring>, hash <hexstring>, λ(bool)
const verifyPasswordHashAsync = (password, salt, hash, callback) => {
  crypto.pbkdf2(password, Buffer.from(salt, 'hex'), NUM_ITERATIONS, HASH_BYTES, DIGEST, (err, key) => {
    callback(key.equals(Buffer.from(hash, 'hex')))
  })
}

// () → session_id <hexstring>
const generateSessionIdSync = () => {
  return crypto.randomBytes(SESSION_BYTES).toString('hex')
}

// () → λ(<string> → ())
const generateSessionIdAsync = (callback) => {
  return crypto.randomBytes(SESSION_BYTES, (err, key) => {
    callback(key.toString('hex'))
  })
}

//left pads an int to a string of length 2
//<int> → <string>
const padLeftWithZero = (quantity) => {
  if (quantity < 10) {
    return "0" + quantity
  } else {
    return quantity.toString()
  }
}

//() → <YYYY-MM-DD-HH:MM:SS>
const createFullDateTime = () => {
  const date = new Date()
  const year = padLeftWithZero(date.getFullYear())
  const month = padLeftWithZero(date.getMonth())
  const day = padLeftWithZero(date.getDate())
  const hours = padLeftWithZero(date.getHours())
  const minutes = padLeftWithZero(date.getMinutes())
  const seconds = padLeftWithZero(date.getSeconds())
  return `${year}-${month}-${day}-${hours}:${minutes}:${seconds}`
}

exports.mimeLookup = mimeLookup
exports.generateRandomRoomName = generateRandomRoomName
exports.generateRandomRoomNameFromHandle = generateRandomRoomNameFromHandle
exports.generateUniqueHandle = generateUniqueHandle
exports.generatePasswordHashAsync = generatePasswordHashAsync
exports.generatePasswordHashSync = generatePasswordHashSync
exports.verifyPasswordHashSync = verifyPasswordHashSync
exports.verifyPasswordHashAsync = verifyPasswordHashAsync
exports.generateSessionIdAsync = generateSessionIdAsync
exports.generateSessionIdSync = generateSessionIdSync
exports.createFullDateTime = createFullDateTime
