const fs = require('fs')
const path = require('path')
const http = require('http')
const https = require('https')
const makeId = require('../shared/server_helpers.js').makeId
const StaticHandler = require('./lib/static.js')
const socket = require('socket.io')
const config_default = require('./config/default.js')

const configMerge = (built_in, specified) => {
  //Do a dumb assign for the moment
  return Object.assign({},
    built_in,
    specified)
}

const debugConfig = (logger, config) => {
  if (!config.client.abs_root_path || !fs.existsSync(config.client.abs_root_path)) {
    throw Error(`abs_root_path ${config.client.abs_root_path} not specified for client or doesn't exist; instance incorrectly configured`)
  }
  if (!config.abs_assets_path || !fs.existsSync(config.abs_assets_path)) {
    throw Error(`abs_assets_path ${config.client.abs_assets_path} not specified or doesn't exist; instance incorrectly configured`)
  }
  if (!config.admin) {
    logger.warn(`config.admin isn't specified; skipping creating its server`)
  } else if (!config.admin.abs_root_path || !fs.existsSync(config.admin.abs_root_path)) {
    throw Error(`abs_root_path ${config.admin.abs_root_path} not specified for admin or doesn't exist; instance incorrectly configured`)
  }
  if (!config.abs_snapshot_path) {
    logger.warn(`abs_snapshot_path ${config.abs_snapshot_path} isn't valid; snapshot api won't be loaded`)
  }
  if (!config.abs_apps_path) {
    logger.warn(`abs_apps_path ${config.abs_apps_path} isn't valid; your instance will not have any available apps`)
  }
  if (config.abs_log_filepath) {
    logger.info(`Filepath supplied; logging to ${config.abs_log_filepath}`);
  } else {
    logger.info(`No filepath supplied; logging to console`);
  }
}

//{protocol, port, https?}, <string> → <Server>
//where https = {http_server_redirect_url, options <Object>, port <int>}
const createServer = (client_config, description, logger, handler, abs_certs_path) => {
  const protocol = client_config.protocol
  let app
  if (protocol === 'http') {
    const port = client_config.port
    app = http.createServer(handler)
    app.listen(port)
    logger.log(`${description} server running on port ${port}`)
  } else if (protocol === 'https') {
    const port = client_config.https.port
    const http_port = client_config.port
    const https_options = client_config.https.options
    let options = {}
    for (const key of Object.keys(https_options)) {
      let filename = https_options[key]
      options[key] = fs.readFileSync(path.join(abs_certs_path, filename))
    }
    app = https.createServer(options, handler)
    app.listen(port)
    logger.log(`${description} server running on port ${port}`)
    //set up a redirection server from http to https
    const redirect_url = client_config.https.http_server_redirect_url
    const redirect_app = http.createServer((req, res) => {
      res.writeHead(301, {
        'Location': `${redirect_url}`
      })
      res.end()
    })
    redirect_app.listen(http_port)
    logger.log(`${description} redirection server running on port ${http_port}, redirecting to ${redirect_url}`)
  } else {
    logger.error(`bad protocol given: ${protocol}`)
    logger.destroy(() => {
      process.exit(1)
    })
  }
  return app
}

const restartIoServer = (fn) => {
  //console.log("Object.keys(io): ", Object.keys(io));
  //io.close()
  fn()
  //io.listen(client_app) //This isn't re-listening
  
  //This async version doesn't seem to fire either...
  //io.close(() => {
    //fn()
    //io.listen(client_app)
  //})
}

const STATIC_REDIRECTS = {
  '': 'index.html',
  '/': 'index.html',
  //client
  '/about': 'index.html',
  '/user': 'index.html',
  //admin
  '/login': 'index.html',
  '/setup': 'index.html'
}

const adminRedirect = (url) => {
  if (url in STATIC_REDIRECTS) {
    return STATIC_REDIRECTS[url]
  } else {
    return url
  }
}

const clientRedirect = (url) => {
  if (url in STATIC_REDIRECTS) {
    return STATIC_REDIRECTS[url]
  } else {
    const nodes = url.split('/')
    const first = nodes[1]
    const rest = nodes.slice(2)
    if (first === 'rooms' && rest.length === 1 && /^[A-Za-z0-9]+$/.test(rest[0])) { //matches /rooms/Alphanum123
      return 'index.html'
    } else if (first === 'r' && rest.length === 1 && /^[A-Za-z0-9_]+$/.test(rest[0])) { //matches /r/dogs
      return 'index.html'
    } else if (first === 'play' && rest.length === 1 && /^[A-Za-z0-9_]+$/.test(rest[0])) { //matches /play/who_am_i
      return 'index.html'
    } else {
      return url
    }
  }
}

const STATIC_ALLOWED_ASSETS = {
  'index.html': true,
  'bundle.js': true
}

const matchesPort = (port, client_config) => {
  if (client_config.protocol === 'http') { //http
    return port === client_config.port
  } else {
    return port === client_config.https.port
  }
}

module.exports = (custom_config) => {
  const config = configMerge(config_default, custom_config)
  const logger = require('./lib/system/log5k.js')(config.abs_log_filepath)

  debugConfig(logger, config)

  const players_registry = require('./lib/registries/players.js')(config.cookies.default_preference)
  const rooms_registry = require('./lib/registries/rooms.js')()
  const nevents_registry = require('./lib/registries/nevents.js').NeventsRegistry(config.nevents_limit)

  const games_registry = require('./lib/registries/games.js')(logger, config.abs_apps_path)
  const app_catalogue = games_registry.reader.getCatalogue()

  //relevant server_config for the client
  const server_id = makeId(15)
  //mutating the config object in case it gets snapshotted
  config.server_id = server_id
  const config_for_client = {
    server_id, 
    create_room_enabled: config.create_room_enabled,
    cookies_enabled: config.cookies.enabled,
    account: config.account,
    app_catalogue
  }

  //always do the client server
  const client_app = createServer(config.client, 'client', logger, handler, config.abs_certs_path)
  const clientStaticFileHandler = StaticHandler(config.client.abs_root_path, clientRedirect, STATIC_ALLOWED_ASSETS)
  if (config.admin) {
    const admin_app = createServer(config.admin, 'admin', logger, handler, config.abs_certs_path)
    const adminStaticFileHandler = StaticHandler(config.admin.abs_root_path, adminRedirect, STATIC_ALLOWED_ASSETS)
  }

  //...and the assets static server
  const assetsStaticFileHandler = StaticHandler(config.abs_assets_path)

  const io = socket(client_app, {
    serveClient: false //no need; already bundled into client
  })

  const nakama_handler = require('./lib/nakama_handler.js')(io, players_registry, rooms_registry, nevents_registry, games_registry, config, config_for_client, logger)

  //API stuff
  const auth_api = require('./api/auth.js')(logger, io, players_registry, rooms_registry.reader, config_for_client)
  const ep = require('../shared/endpoints.js')
  const REST_ENDPOINTS = [
    //players
    {
      methods: ['GET'],
      path: '/api/players',
      handler: players_registry.api.players
    },
    {
      methods: ['GET'],
      path: '/api/player/{id}',
      handler: players_registry.api.player
    },
    {
      methods: ['GET'],
      path: ep.GET_ADMIN_STATUS,
      handler: players_registry.api.adminStatus
    },
    {
      methods: ['POST'],
      path: ep.CHECK_SESSION_VALIDITY,
      handler: players_registry.api.sessionValidity
    },
    //rooms
    {
      methods: ['GET'],
      path: '/api/rooms',
      handler: rooms_registry.api.rooms
    },
    //nevents
    {
      methods: ['GET'],
      path: '/api/nevents/{id}/messages',
      handler: nevents_registry.api.nevents
    },
    //games
    {
      methods: ['GET'],
      path: '/api/games',
      handler: games_registry.api.games
    },
    {
      methods: ['GET'],
      path: '/api/game/{id}',
      handler: games_registry.api.game
    },
    //other
    ...auth_api
  ]

  if (config.abs_snapshot_path) {
    const snapshot_api = require('./api/snapshot.js')(logger, players_registry, rooms_registry, nevents_registry, games_registry, config, config.abs_snapshot_path, restartIoServer, nakama_handler.initialiseGameFromSnapshot)
    REST_ENDPOINTS.push(...snapshot_api)
  }

  const apiHandler = require('./lib/rest_api.js')(REST_ENDPOINTS)

  function handler(req, res) {
    //https://nodejs.org/api/all.html#net_socket_localport
    const port = req.socket.localPort
    if (req.method === 'GET' && req.url === '/favicon.ico') { //TODO: is less verbose way to serve the favicon?
      fs.readFile(path.join(config.abs_assets_path, 'favicon.ico'), (err, data) => {
        if (err) {
          res.writeHead(500)
          return res.end('')
        }
        res.writeHead(200)
        return res.end(data)
      })
    } else if (req.method === 'GET' && assetsStaticFileHandler.canTraverse(req.url)) {
      assetsStaticFileHandler.handle(req.url, res)
    } else if (req.method === 'GET' && matchesPort(port, config.client) && clientStaticFileHandler.canTraverse(req.url)) {
      clientStaticFileHandler.handle(req.url, res)
    } else if (req.method === 'GET' && config.admin && matchesPort(port, config.admin) && adminStaticFileHandler.canTraverse(req.url)) {
      adminStaticFileHandler.handle(req.url, res)
    } else if (apiHandler.canRoute(req.url, req.method)) {
      apiHandler.handle(req.url, req.method, res, req)
    } else {
      logger.warn(`url not found: ${req.url}`);
      res.writeHead(404)
      return res.end(`Uh oh! Double check the address`)
    }
  }

  nakama_handler.init(config.startup)

  process.on('uncaughtException', (err) => {
    logger.error(err.stack)
    logger.info(JSON.stringify(nakama_handler.getState()))
    logger.error(`Server terminated due to uncaught exception. Inspect logs and reboot manually.`)
    logger.destroy(() => {
      process.exit(1)
    })
  })

  process.on('SIGINT', () => {
    logger.log(`Server manually stopped.`)
    logger.destroy(() => {
      process.exit()
    })
  })
}
