const ep = require('../../shared/endpoints.js')
const createFullDateTime = require('../lib/helpers.js').createFullDateTime
const path = require('path')
const fs = require('fs')

//appends the date on it
//<string> → <name-YYYY-MM-DD-HH:MM:SS.json>
const nameToFilename = (snapshot_name) => {
  return `${snapshot_name}-${createFullDateTime()}.json`
}

//<name-YYYY-MM-DD-HH:MM:SS.json> → <string>
const filenameToName = (snapshot_filename) => {
  const first_hyphen_index = snapshot_filename.indexOf('-')
  return snapshot_filename.slice(0, first_hyphen_index)
}

//<name-YYYY-MM-DD-HH:MM:SS.json> → <YYYY-MM-DD-HH:MM:SS>
const filenameToDate = (snapshot_filename) => {
  const first_hyphen_index = snapshot_filename.indexOf('-')
  return snapshot_filename.slice(first_hyphen_index + 1).slice(0, -'.json'.length)
}

const restoreConfig = (config, config_snapshot) => {
  //write over config keys one by one
  for (const key of Object.keys(config_snapshot)) {
    config[key] = config_snapshot[key]
  }
}

const SnapshotRegistry = (snapshot_dir, snapshot_filenames) => {
  const nameToFilenameLookup = {}
  for (const snapshot_filename of snapshot_filenames) {
    const name = filenameToName(snapshot_filename)
    nameToFilenameLookup[name] = snapshot_filename
  }
  return {
    getSnapshots: () => {
      return Object.keys(nameToFilenameLookup).map((name) => {
        return {
          name,
          date_created: filenameToDate(nameToFilenameLookup[name])
        }
      })
    },
    getSnapshotContent: (snapshot_name) => {
      const snapshot_filename = nameToFilenameLookup[snapshot_name]
      if (snapshot_filename) {
        return [JSON.parse(fs.readFileSync(
          path.join(snapshot_dir, snapshot_filename))), null]
      } else {
        return [null, 'nonexistent snapshot']
      }
    },
    store: (snapshot_name, snapshot) => {
      const snapshot_filename = nameToFilename(snapshot_name)
      nameToFilenameLookup[snapshot_name] = snapshot_filename
      fs.writeFileSync(
        path.join(snapshot_dir, snapshot_filename),
        snapshot
      )
    },
    delete: (snapshot_name) => {
      const snapshot_filename = nameToFilenameLookup[snapshot_name]
      const snapshot_path = path.join(snapshot_dir, snapshot_filename)
      if (snapshot_filename) {
        fs.unlinkSync(snapshot_path)
        delete nameToFilenameLookup[snapshot_name]
        return true
      } else {
        return false
      }
    },
    validateExists: (snapshot_name) => {
      if (!(snapshot_name in nameToFilenameLookup)) {
        return [false, `snapshot name '${snapshot_name}' doesn't exist`]
      } else {
        return [true, null]
      }
    },
    validatePut: (snapshot_name) => {
      if (!/^[a-z0-9]+$/i.test(snapshot_name)) {
        return [false, "invalid snapshot name (only alphanumeric characters allowed)"]
      } else if (snapshot_name in nameToFilenameLookup) {
        return [false, `snapshot name '${snapshot_name}' already taken`]
      } else {
        return [true, null]
      }
    }
  }
}


module.exports = (logger, players_registry, rooms_registry, nevents_registry, games_registry, config, abs_snapshot_dir, restartIo, initialiseGameFromSnapshot) => {
  const players_reader = players_registry.reader
  const rooms_reader = rooms_registry.reader
  const nevents_reader = nevents_registry.reader
  const games_reader = games_registry.reader
  const players_writer = players_registry.writer
  const rooms_writer = rooms_registry.writer
  const nevents_writer = nevents_registry.writer
  const games_writer = games_registry.writer
  if (!fs.existsSync(abs_snapshot_dir)){
    fs.mkdirSync(abs_snapshot_dir)
  }

  const snapshots_registry = SnapshotRegistry(abs_snapshot_dir, fs.readdirSync(abs_snapshot_dir))

  const handleSnapshot = (snapshot_name, method, data) => {
    if (method === 'PUT') {
      logger.log(`received PUT ${ep.SNAPSHOT} for snapshot name ${snapshot_name}`)
      const [valid, error] = snapshots_registry.validatePut(snapshot_name)
      if (valid) {
        const snapshot = JSON.stringify({
          config,
          players: players_reader.getPlayersForSnapshotSerialisation(),
          rooms: rooms_reader.getRoomsForSnapshotSerialisation(),
          nevents: nevents_reader.getNeventsForSnapshotSerialisation(),
          games: games_reader.getGamesForSnapshotSerialisation()
        })
        snapshots_registry.store(snapshot_name, snapshot)
        logger.log(`finished writing snapshot ${snapshot_name}`)
        return [`snapshot '${snapshot_name}' written`, 200]
      } else {
        return [error, 400]
      }
    } else if (method === 'GET') {
      logger.log(`received GET ${ep.SNAPSHOT} for snapshot name ${snapshot_name}`)
      const [snapshot_contents, error] = snapshots_registry.getSnapshotContent(snapshot_name)
      if (!error) {
        return [snapshot_contents, 200]
      } else {
        return [error, 400]
      }
    } else if (method === 'DELETE') {
      const [valid, error] = snapshots_registry.validateExists(snapshot_name)
      if (valid) {
        if (snapshots_registry.delete(snapshot_name)) {
          return [`snapshot '${snapshot_name}' deleted`, 200]
        } else {
          return [`delete unsuccessful for ${snapshot_name}`, 500]
        }
      } else {
        return [error, 400]
      }
      //TODO
    } else {
      return [null, 405]
    }
  }
  const handleSnapshots = (method, data) => {
    if (method === 'GET') {
      logger.log(`received GET ${ep.SNAPSHOTS}`)
      return [snapshots_registry.getSnapshots(), 200]
    } else {
      return [null, 405]
    }
  }
  const handleSnapshotRestore = (snapshot_name, method, data) => {
    if (method === 'POST') {
      logger.log(`received POST ${ep.SNAPSHOT_RESTORE}`)
      const [snapshot_contents, error] = snapshots_registry.getSnapshotContent(snapshot_name)
      if (!error) {
        const {
          players,
          rooms,
          nevents,
          games
        } = snapshot_contents
        //avoid shadowing...
        const snapshot_config = snapshot_contents.config
        //these need to be exclusive.
        //Actually, is this necessary?
        //http://stackoverflow.com/questions/5481675/node-js-and-mutexes
        restartIo(() => {
          restoreConfig(config, snapshot_config)
          players_writer.restoreSnapshot(players)
          rooms_writer.restoreSnapshot(rooms)
          nevents_writer.restoreSnapshot(nevents)
          //This comes last, since it relies on players and rooms
          games_writer.restoreSnapshot(games, initialiseGameFromSnapshot)
        })
        return [`snapshot '${snapshot_name}' restored`, 200]
      } else {
        return [error, 400]
      }
    } else {
      return [null, 405]
    }
  }
  return [
    {
      methods: ['PUT', 'GET', 'DELETE'],
      path: ep.SNAPSHOT,
      handler: handleSnapshot
    },
    {
      methods: ['GET'],
      path: ep.SNAPSHOTS,
      handler: handleSnapshots
    },
    {
      methods: ['POST'],
      path: ep.SNAPSHOT_RESTORE,
      handler: handleSnapshotRestore
    }
  ]
}
