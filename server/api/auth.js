const h = require('../lib/helpers.js')
const makeId = require('../../shared/server_helpers.js').makeId
const ep = require('../../shared/endpoints.js')
const inter = require('../../shared/socket_events.js')

module.exports = (logger, io, players_registry, rooms_reader, config_for_client) => {
  const players_reader = players_registry.reader
  const players_writer = players_registry.writer

  const handleRoomChallenge = (roomid, method, data) => {
    logger.log(`received ${method} ${ep.ROOM_CHALLENGE} with roomid ${roomid}`)
    if (method === 'POST') {
      const passphrase = data.passphrase
      const hash_salt = rooms_reader.getRoom(roomid).passphrase
      if (!hash_salt) {
        return [null, 400]
      } else {
        return [h.verifyPasswordHashSync(passphrase, hash_salt[1], hash_salt[0]), 200]
      }
    } else {
      return [null, 405]
    }
  }

  const handleLogin = (method, data) => {
    if (method !== 'POST') {
      return [null, 405]
    } else {
      logger.log(`received POST ${ep.LOGIN} or ${ep.ADMIN_LOGIN}`)
      const handle = data.handle
      const passphrase = data.passphrase
      const account = players_reader.getAccountDetails(handle)
      if (!account) { //handle doesn't exist
        return [null, 401]
      } else {
        const hash = account.hash
        const salt = account.salt
        if (h.verifyPasswordHashSync(passphrase, salt, hash)) {
          const userid = players_reader.getUserIdFromHandle(handle)
          const session_id = h.generateSessionIdSync()
          players_writer.loginPlayer(userid, session_id)
          return [{
            userid,
            session_id,
            config_for_client
          }, 200]
        } else { //handle exists; incorrect passphrase
          return [null, 401]
        }
      }
      return ["ok", 200]
    }
  }

  const handleLogout = (method, data) => {
    if (method === 'POST') {
      const {
        userid,
        session_id
      } = data
      logger.log(`received POST ${ep.LOGOUT} for ${userid}`)
      if (players_writer.logoutPlayer(userid, session_id)) {
        return ["successfully logged out", 200]
      } else {
        return ["invalid session or user id", 400]
      }
    } else {
      return [null, 405]
    }
  }

  const handleAdminRegister = (method, data) => {
    if (method !== 'POST') {
      return [null, 405]
    } else {
      logger.log(`received POST ${ep.ADMIN_REGISTER}`)
      const handle = data.handle
      const passphrase = data.passphrase
      if (!players_reader.isHandleTaken(handle)) {
        const userid = makeId(15)
        const session_id = h.generateSessionIdSync()
        const [hash, salt] = h.generatePasswordHashSync(passphrase)
        players_writer.registerAdmin(userid, handle, salt, hash)
        players_writer.loginPlayer(userid, session_id)
        io.emit(inter.NEW_PLAYER_REGISTERED, {
          userid,
          handle
        })
        return [{
          userid,
          session_id,
          config_for_client
        }, 200]
      } else {
        return ["handle_taken", 400]
      }
    }
  }

  return [
    {
      methods: ['POST'],
      path: ep.LOGOUT,
      handler: handleLogout
    },
    {
      methods: ['POST'],
      path: ep.LOGIN,
      handler: handleLogin
    },
    {
      methods: ['POST'],
      path: ep.ADMIN_LOGIN,
      handler: handleLogin
    },
    {
      methods: ['POST'],
      path: ep.ADMIN_REGISTER,
      handler: handleAdminRegister
    },
    {
      methods: ['POST'],
      path: ep.ROOM_CHALLENGE,
      handler: handleRoomChallenge
    }
  ]
}
