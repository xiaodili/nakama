module.exports = {
  client: {
    protocol: "http",
    port: 8080
  },
  nevents_limit: 50, //number of nevents per room to keep on the server
  unoccupied_room_cleanup_policy_seconds: 30, //number of seconds of unoccupancy before a room is deleted
  create_room_enabled: true, //if true, anyone can create a room
  account: {
    anonymous_user_allowed: true, //if true, non-signed in players are auto-assigned userids and handles when visiting the client app. If false, they'll be redirected to a login screen instead
    //"anonymous_user_allowed": false,

    //(only relevant if account.anonymous_user_allowed is true)
    allow_conversion_to_account: true  //if true, anonymous users can convert their handle into an account to sign in with (on the client app)
  },
  cookies: { //cookie behaviour
    enabled: true, //allow cookies to be persisted on the client and admin app

    //(only relevant if cookies.enabled is true)
    default_preference: true //default preference for a new user to be using cookies or not
  },
  disconnection_grace_timeout_seconds: { //mobile clients tend to disconnect a lot. tune accordingly
    while_gaming: 1800, // 30 minutes
    mobile: 15, // 1 minute
    desktop: 15
  },
  default_room_creation_properties: {
    autojoin: false,
    player_list_enabled: true,
    nevent_log_enabled: true,
    games_enabled: true,
    admin_policy: "creator", //person who originally created the room is the admin
    default_tab: "app" //needs to match one of shared/symbols/tab_types.js
  },
  startup: {
    rooms: [{
        name: "The Rec Room",
        description: "Kick back during your free periods here. Also, why not purchase some refreshments from the tuck shop or vending machine?",
        games_enabled: false,
        //"passphrase": "harlequins",
        admin_policy: "oldest_occupant" //person who has been in the room the longest is the admin
      }, {   
        name: "The Octogon",
        description: "When having a chat, please be mindful of others studying. Feel free to play the piano, though.",
        player_list_enabled: false,
        games_enabled: false,
        //"passphrase": "forthewheel",
        admin_policy: "oldest_occupant",
        default_tab: "messages" //when joining this room, this is the default tab that's focused on. Needs to match one of shared/symbols/tab_types.js
      }, {   
        name: "The Fort",
        description: "Bring out the games! But don't lean on the walls. Those aren't load-bearing walls.",
        passphrase: "n1n1an",
        admin_policy: "oldest_occupant"
      }
    ]
  }
}
