const assert = require('assert')
const StaticHandler = require('../../server/lib/static.js')
const path = require('path')

const static_root = path.join(__dirname, 'assets')

describe('Static File Server', () => {
  describe('whitelisting', () => {
    const redirect = (url) => {
      return url
    }
    const allowed_assets = {
      'assets_dir1': {
        'style.css': true
      },
      'index.html': true,
    }
    const static_server = StaticHandler(static_root, redirect, allowed_assets)
    it('should be able to serve the whitelisted file', () => {
      assert.ok(static_server.canTraverse('/index.html'))
      assert.ok(static_server.canTraverse('/assets_dir1/style.css'))
    })
    it('should reject nonwhitelisted files', () => {
      assert.notEqual(static_server.canTraverse('/notarealfile.html'))
    })
  })

  describe('redirection', () => {
    const redirect = (url) => {
      const redirects = {
        '/': 'index.html',
        '': 'index.html'
      }
      if (url in redirects) {
        return redirects[url]
      } else {
        const nodes = url.split('/')
        const first = nodes[1]
        const rest = nodes.slice(2)
        if (first === 'rooms' && rest.length >= 1) {
          const roomid = rest[0] //this is fine
          return 'index.html'
        } else {
          return url
        }
      }
    }
    const allowed_assets = {
      'index.html': true
    }
    const static_server = StaticHandler(static_root, redirect, allowed_assets)
    it('should be able to serve the root', () => {
      assert.ok(static_server.canTraverse(''))
    })
    it('should be able to serve a trailing /', () => {
      assert.ok(static_server.canTraverse('/'))
    })
    it('should be able to handle wildcard redirections', () => {
      assert.ok(static_server.canTraverse('/rooms/123123'))
    })
  })

  describe('no allowed_assets passed in should act like standard file server', () => {
    const static_server = StaticHandler(static_root)
    it('should be able to serve a standard file', () => {
      assert.ok(static_server.canTraverse('/index.html'))
    })
    it('should be able to serve nested files', () => {
      assert.ok(static_server.canTraverse('/asset_dir1/style.css'))
    })
    it('should not "serve" directories', () => {
      assert.equal(false, static_server.canTraverse('/asset_dir1'))
    })
    it('should be able to prevent directory traversal', () => {
      assert.equal(false, static_server.canTraverse('../static.js'))
    })
  })
})
