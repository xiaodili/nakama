const assert = require('assert')
const Nevents = require('../../server/lib/registries/nevents.js').Nevents

describe('Circular array logic', () => {
  it('should behave normally under the message limit', () => {
    const message_limit = 3
    const nevents = Nevents(message_limit)
    nevents.storeMessage('1')
    nevents.storeMessage('2')
    assert.deepEqual(['1', '2'], nevents.getMessages())
    nevents.storeMessage('3')
    assert.deepEqual(['1', '2', '3'], nevents.getMessages())
  })

  it('should behave correctly at message limit', () => {
    const message_limit = 3
    const nevents = Nevents(message_limit)
    nevents.storeMessage('1')
    nevents.storeMessage('2')
    nevents.storeMessage('3')
    assert.deepEqual(['1', '2', '3'], nevents.getMessages())
  })

  it('should overwrite indices from the start', () => {
    const message_limit = 3
    const nevents = Nevents(message_limit)
    nevents.storeMessage('1')
    nevents.storeMessage('2')
    nevents.storeMessage('3')
    nevents.storeMessage('4')
    assert.deepEqual(['2', '3', '4'], nevents.getMessages())
  })

  it('should not keep growing the message array', () => {
    const message_limit = 3
    const nevents = Nevents(message_limit)
    nevents.storeMessage('1')
    nevents.storeMessage('2')
    nevents.storeMessage('3')
    nevents.storeMessage('4')
    nevents.storeMessage('5')
    assert.deepEqual(['3', '4', '5'], nevents.getMessages())
  })
})
