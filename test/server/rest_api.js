const assert = require('assert')
const rest_api = require('../../server/lib/rest_api.js')

describe('Rest API routing', () => {
  const rest_endpoints = [
    {
      methods: ['GET'],
      path: '/api/v1/player/{id}',
      handler: () => {}
    },
    {
      methods: ['GET'],
      path: '/api/v1/players',
      handler: () => {}
    },
    {
      methods: ['POST'],
      path: '/api/login',
      handler: () => {}
    }
  ]
  const api_handler = rest_api(rest_endpoints)
  it('should be able to route a valid GET endpoint', () => {
    assert.ok(api_handler.canRoute('/api/v1/players', 'GET'))
  })
	it('should not be able to route an endpoint with incorrect method', () => {
		assert.equal(false, api_handler.canRoute('/api/v1/players', 'POST'))
	})
	it('should not be able to route an invalid POST endpoint', () => {
		assert.equal(false, api_handler.canRoute('/api/v1/players/cat', 'POST'))
	})
	it('should not be able to route a valid POST endpoint', () => {
		assert.equal(true, api_handler.canRoute('/api/login', 'POST'))
	})
  it('should be able to route an endpoint with parameter', () => {
    assert.ok(api_handler.canRoute('/api/v1/player/123', 'GET'))
  })
  it('should be able to call the handler with that parameter', () => {
    assert.ok(api_handler.canRoute('/api/v1/player/123', 'GET'))
  })

})

describe('Rest API handling', () => {
  //IIFE approach:
  //var res_mock = new (function () {
  //return {
  //writeHead: () => {
  //}
  //}
  //})()
  const ResMock = () => {
    var response_code
    var header
    var payload
    return {
      writeHead: (_code, _header) => {
        response_code = _code
        header = _header
      },
      end: (json_string) => {
        payload = json_string
      },
      gotCalledWith: () => {
        return [response_code, header, payload]
      }
    }
  }
	const PostRequestDataMock = () => {
		let request_data
    let onHandled
		const hook = {
			end: (cb) => {
        cb()
        onHandled()
			},
			data: (cb) => {
        cb(JSON.stringify(request_data))
			}
		}
		return {
			setupRequestData: (_request_data) => {
				request_data = _request_data
			},
      setupOnHandled: (_onHandled) => {
        onHandled = _onHandled
      },
      req: {
        on: (event_name, cb) => {
          hook[event_name](cb)
        }
      }
		}
	}
  it('should pass in the captured url parameter to the handler', () => {
    const res_mock = ResMock()
    var rest_endpoints = [
      {
        methods: ['GET'],
        path: '/api/v1/player/{id}',
        handler: (id) => {
          return [{
            "result": parseInt(id) * 2
          }, 200]
        }
      }
    ]
    const api_handler = rest_api(rest_endpoints)
    api_handler.handle('/api/v1/player/123', 'GET', res_mock)
    const [code, header, payload] = res_mock.gotCalledWith()
    assert.equal(200, code)
    assert.deepEqual({"Content-Type": "application/json"}, header)
    assert.equal('{"result":246}', payload)
  })

  it('should pass in the attached request data to the handler', () => {
    var rest_endpoints = [
      {
        methods: ['POST'],
        path: '/api/login',
        handler: (method, request_data) => {
          const username = request_data.username
          const passphrase = request_data.passphrase
          return [{
            passphrase
          }, 200]
        }
      }
    ]
    const api_handler = rest_api(rest_endpoints)
    const res_mock = ResMock()
    const post_request_data_mock = PostRequestDataMock()
    post_request_data_mock.setupRequestData({
      username: 'eddie',
      passphrase: 'querty'
    })
    post_request_data_mock.setupOnHandled(() => {
      const [code, header, payload] = res_mock.gotCalledWith()
      assert.equal(200, code)
      assert.deepEqual({"Content-Type": "application/json"}, header)
      assert.equal('{"passphrase":"querty"}', payload)
    })
    api_handler.handle('/api/login', 'POST', res_mock, post_request_data_mock.req)
  })

  it('should be able to escape the request', () => {
    const res_mock = ResMock()
    const rest_endpoints = [
      {
        methods: ['GET'],
        path: '/api/v1/player/{id}',
        handler: (id, method) => {
          return [id, 200]
        }
      }
    ]
    const api_handler = rest_api(rest_endpoints)
    const url = "/api/v1/player/%2Flobby%230JylhERyhNiF_GyHAAAA"
    api_handler.handle(url, 'GET', res_mock)
    var [_, _, payload] = res_mock.gotCalledWith()
    assert.equal('"/lobby#0JylhERyhNiF_GyHAAAA"', payload)
  })

})
