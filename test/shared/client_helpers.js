const assert = require('assert')
const h = require('../../shared/client_helpers.js')

describe('Escaping HTML', () => {
  const bad_string = "<script>alert('hi')</script>"
  it('should escape the string properly', () => {
    assert.equal(h.escapeHtml(bad_string),'&lt;script&gt;alert(&#39;hi&#39;)&lt;&#x2F;script&gt;')
  })
})

