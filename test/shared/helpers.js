const assert = require('assert')
const h = require('../../shared/helpers.js')

describe('PubSub', () => {
  it('should apply the arguments properly', () => {
    const TEST_EVENT = "TEST_EVENT"
    const pubsub = h.PubSub()
    var value_mock
    const test_arg = "hi there!"
    pubsub.sub(TEST_EVENT, (args) => {
      value_mock = args
    })
    pubsub.pub(TEST_EVENT, test_arg)
    assert.equal(value_mock, test_arg)
  })
  it('should remove listeners properly', () => {
    const TEST_EVENT = "TEST_EVENT"
    const pubsub = h.PubSub()
    var value_mock
    const test_arg1 = "1"
    const test_arg2 = "2"
    const unsub = pubsub.sub(TEST_EVENT, (args) => {
      value_mock = args
    })
    pubsub.pub(TEST_EVENT, test_arg1)
    unsub()
    pubsub.pub(TEST_EVENT, test_arg2)
    assert.equal(value_mock, test_arg1)
  })
})
