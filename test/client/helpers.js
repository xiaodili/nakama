const assert = require('assert')
const h = require('../../client/lib/helpers.js')

describe('Detecting stock Android user-agent', () => {
  const samsung_note4_chrome_user_agent = "Mozilla/5.0 (Linux; Android 6.0.1; SM-N910C Build/MMB29K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.85 Mobile Safari/537.36"
  const samsung_note4_stock_user_agent = "Mozilla/5.0 (Linux; Android 6.0.1; SAMSUNG SM-N910C Build/MMB29K) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/4.0 Chrome/44.0.2403.133 Mobile Safari/537.36"
  const samsung_s3_stock_user_agent = "Mozilla/5.0 (Linux; U; Android 4.3; en-gb; GT-I9300 Build/JSS15J) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30"
  //http://android.stackexchange.com/questions/75532/galaxy-s5-stock-browser-user-agent
  const samsung_s5_stock_user_agent = "Mozilla/5.0 (Linux; Android 4.4.2; en-us; SAMSUNG-SM-G900A Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/1.6 Chrome/28.0.1500.94 Mobile Safari/537.36"
  it('should detect stock android properly', () => {
    assert.equal(1, 1)
  })
})
